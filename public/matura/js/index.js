  // VARIABLE DECLARATIONS ------

  // pages
  var initPage,
      questionsPage,
      resultsPage,
      // buttons
      startBtn,
      submitBtn,
      continueBtn,
      retakeBtn,
      spanishBtn,
      // question and answers
      question,
      answerList,
      answerSpan,
      answerA,
      answerB,
      answerC,
      answerD,
      // event listeners
      answerDiv,
      answerDivA,
      answerDivB,
      answerDivC,
      answerDivD,
      feedbackDiv,
      selectionDiv,
      toBeHighlighted,
      toBeMarked,
      userScore,
      // quiz
      quiz,
      questionCounter,
      correctAnswer,
      correctAnswersCounter,
      userSelectedAnswer,
      // function names
      newQuiz,
      generateQuestionAndAnswers,
      getCorrectAnswer,
      getUserAnswer,
      selectAnswer,
      deselectAnswer,
      selectCorrectAnswer,
      deselectCorrectAnswer,
      slikica,
      clearHighlightsAndFeedback,
      prekidac, countdownTimer, bodovi = 0,
      vrijeme = 0,
      highlightCorrectAnswerGreen,
      highlightIncorrectAnswerRed,
      ostali,
      jedna;

  function ProgressCountdown(timeleft, bar, text) {
      return new Promise((resolve, reject) => {
          countdownTimer = setInterval(() => {
              timeleft--;
              document.getElementById(bar).value = timeleft;
              document.getElementById(text).textContent = timeleft;
              if (timeleft <= 0) {
                  clearInterval(countdownTimer);
                  resolve(true);
              } else if (timeleft <= 1) {
                  $("#sekunde").html("sekunda")
                  $("#ostalo").html("ostala")
              } else if (timeleft <= 4) {
                  $("#sekunde").html("sekunde")
              }

          }, 1000);
      });
  }

  $(document).ready(function() {
      $(".opis").text("Odaberi ispit. U ispitu se boduje i vrijeme potrebno za rješavanje zadataka.")
      // DOM SELECTION ------
      // App pages
      // Page 1 - Initial
      initPage = $('.init-page');
      // Page 2 - Questions/answers
      questionsPage = $('.questions-page');
      // Page 3 - Results
      resultsPage = $('.results-page');
      slikica = $('.slikica');

      // Buttons
      startBtn = $('.init-page__btn, .results-page__retake-btn');
      submitBtn = $('.mrzim');
      continueBtn = $('.questions-page__continue-btn');
      retakeBtn = $('.results-page__retake-btn');
      spanishBtn = $('.results-page__spanish-btn');

      // Answer block divs
      answerDiv = $('.questions-page__answer-div');
      answerDivA = $('.questions-page__answer-div-a');
      answerDivB = $('.questions-page__answer-div-b');
      answerDivC = $('.questions-page__answer-div-c');
      answerDivD = $('.questions-page__answer-div-d');

      // Selection div (for the pointer, on the left)
      selectionDiv = $('.questions-page__selection-div');

      // Feedback div (for the checkmark or X, on the right)
      feedbackDiv = $('.questions-page__feedback-div');

      // Questions and answers
      question = $('.questions-page__question');
      answerList = $('.questions-page__answer-list');
      answerSpan = $('.questions-page__answer-span');
      answerA = $('.questions-page__answer-A');
      answerB = $('.questions-page__answer-B');
      answerC = $('.questions-page__answer-C');
      answerD = $('.questions-page__answer-D');
      // User final score
      userScore = $('.results-page__score');
      prikazBodova = $('.results-page__bodovi');
      // FUNCTION DECLARATIONS ------
      $.fn.declasse = function(re) {
          return this.each(function() {
              var c = this.classList
              for (var i = c.length - 1; i >= 0; i--) {
                  var classe = "" + c[i]
                  if (classe.match(re)) c.remove(classe)
              }
          })
      }



      // Start the quiz
      newQuiz = function() {
          prekidac = 1;
          bodovi = 0;
          // Set the question counter to 0
          questionCounter = 0;
          // Set the total correct answers counter to 0
          correctAnswersCounter = 0;
          // Hide other pages of the app
          questionsPage.hide();
          stvori_pitanja();
      };

      // Load the next question and set of answers
      generateQuestionAndAnswers = function() {
          $(".questions-page__answer-list").show()
          question.html("<span style='font-size: 1.3rem;'>" + (questionCounter + 1) + "/" + quiz.length + ".</span> <br>");
          shuffle(quiz[questionCounter].answers);
          answerA.html(quiz[questionCounter].answers[0]);
          if (answerA.html() == "" || null) {
              answerDivA.hide()
          } else {
              answerDivA.show()
          };
          answerB.html(quiz[questionCounter].answers[1]);
          if (answerB.html() == "" || null) {
              answerDivB.hide()
          } else {
              answerDivB.show()
          };
          answerC.html(quiz[questionCounter].answers[2]);
          if (answerC.html() == "" || null) {
              answerDivC.hide()
          } else {
              answerDivC.show()
          };
          answerD.html(quiz[questionCounter].answers[3]);
          if (answerD.html() == "" || null) {
              answerDivD.hide()
          } else {
              answerDivD.show()
          };
          $("#opis").html("<em>" + quiz[questionCounter].opis + "</em>")
          $(".vrijeme").html('<progress value="120" max="120" id="pageBeginCountdown"></progress><p>vrijeme preostalo za odgovor: <span id="pageBeginCountdownText">120 </span></p>')
          $("body").css({
              "background-color": quiz[questionCounter].boja_pozadine
          })
          question.html("<span style='font-size:16px'>" + (questionCounter + 1) + "/" + quiz.length + "</span><br>" + quiz[questionCounter].question)
          if (prekidac == 1) {
              ProgressCountdown(120, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => odgovor());
          }
          
          if((questionCounter + 1) == quiz.length){
              $(".ctn").text("provjeri rezultat")
          }
      };

      // Store the correct answer of a given question
      getCorrectAnswer = function() {
          correctAnswer = quiz[questionCounter].correctAnswer;
      };

      // Store the user's selected (clicked) answer
      getUserAnswer = function(target) {
          userSelectedAnswer = $(target).find(answerSpan).html();
      };

      // Add the pointer to the clicked answer
      selectAnswer = function(target) {
          $(target).find(selectionDiv).addClass('ion-chevron-right');
          $(target).addClass("odabir")

      };

      // Remove the pointer from any answer that has it
      deselectAnswer = function() {
          if (selectionDiv.hasClass('ion-chevron-right')) {
              selectionDiv.removeClass('ion-chevron-right');
              selectionDiv.parent().removeClass("odabir")
          }
      };

      // Get the selected answer's div for highlighting purposes
      getSelectedAnswerDivs = function(target) {
          toBeHighlighted = $(target);
          toBeMarked = $(target).find(feedbackDiv);
      };

      // Make the correct answer green and add checkmark
      highlightCorrectAnswerGreen = function(target) {
          if (correctAnswer === answerA.html()) {
              answerDivA.addClass('questions-page--correct');
              answerDivA.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerB.html()) {
              answerDivB.addClass('questions-page--correct');
              answerDivB.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerC.html()) {
              answerDivC.addClass('questions-page--correct');
              answerDivC.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerD.html()) {
              answerDivD.addClass('questions-page--correct');
              answerDivD.find(feedbackDiv).addClass('ion-checkmark-round');
          }
      };

      // Make the incorrect answer red and add X
      highlightIncorrectAnswerRed = function() {
          toBeHighlighted.addClass('questions-page--incorrect');
          toBeMarked.addClass('ion-close-round');
      };

      // Clear all highlighting and feedback
      clearHighlightsAndFeedback = function() {
          answerDiv.removeClass('questions-page--correct');
          answerDiv.removeClass('questions-page--incorrect');
          feedbackDiv.removeClass('ion-checkmark-round');
          feedbackDiv.removeClass('ion-close-round');
      };

      // APP FUNCTIONALITY ------

      /* --- PAGE 1/3 --- */

      // Start the quiz:


      submitBtn.hide();
      continueBtn.hide();


      // Clicking on start button:
      startBtn.on('click', function() {
          newQuiz();
          // Advance to questions page
          initPage.hide();
          questionsPage.show(300);
          // Load question and answers
          generateQuestionAndAnswers();
          // Store the correct answer in a variable
          getCorrectAnswer();
          // Hide the submit and continue buttons
          submitBtn.hide();
          continueBtn.hide();

      });

      /* --- PAGE 2/3 --- */

      // Clicking on an answer:
      answerDiv.on('click', function() {
          submitBtn.show(300);
          continueBtn.show(300)
              // Remove pointer from any answer that already has it
          deselectAnswer();
          // Put pointer on clicked answer
          selectAnswer(this);
          // Store current selection as user answer
          getUserAnswer(this);
          getSelectedAnswerDivs(this);
          // Store current answer div for highlighting purposes
          odgovor();

      });


      function odgovor() {
          vrijeme = parseInt($("#pageBeginCountdownText").text())
          bodovi += vrijeme
          prekidac = 0;
          var ide = 0
              // Disable ability to select an answer
          answerDiv.off('click');
          if (questionCounter != quiz.length - 1) {
              ide = 1
          } else {
              ide = 0
          }
          highlightCorrectAnswerGreen();
          // Make correct answer green and add a checkmark
          clearInterval(countdownTimer);
          if (document.getElementById("pageBeginCountdown").value == "0") {
              submitBtn.show(300);
              continueBtn.show(300);
              highlightCorrectAnswerGreen();
              $("#krivo")[0].play();
              bodovi -= 10;
              $(".mrzim").unbind("click").click(function() {
                  swal({
                      title: "<em>" + quiz[questionCounter].question + "</em>",
                      html: "<ul class='opis-lista'><li>" + quiz[questionCounter].answers[0] + "</li><li>" + quiz[questionCounter].answers[1] + "</li><li>" + quiz[questionCounter].answers[2] + "</li><li>" + quiz[questionCounter].answers[3] + "</li></ul><p style='text-align:justify'>" + quiz[questionCounter].opis + "</p>",
                      showCloseButton: true,
                      confirmButtonText: 'dalje',
                      backdrop: false,
                      allowOutsideClick: false,
                      allowEscapeKey: false
                  });
                  $(".swal2-confirm").unbind("click").click(function() {
                      clearInterval(countdownTimer)
                      if (ide == 1) {
                          ProgressCountdown(120, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => odgovor());
                      }
                      submitBtn.hide();
                      nastavi()
                  })
                  $(".swal2-close").unbind("click").click(function() {
                      clearInterval(countdownTimer)
                      if (ide == 1) {
                          ProgressCountdown(120, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => odgovor());
                      }
                      nastavi()
                  })
              })

          } else {
              // Evaluate if the user got the answer right or wrong
              if (userSelectedAnswer === correctAnswer) {
                  // Increment the total correct answers counter
                  correctAnswersCounter++;
                  bodovi += 10;
                  $("#tocno")[0].play();
                  broj = vrijeme + 10

                  $(".mrzim").unbind("click").click(function() {

                      swal({
                          title: "+ <span style='color:green'>" + broj + "</span>",
                          html: "<p style='text-align:justify'>" + quiz[questionCounter].opis + "</p>",
                          showCloseButton: true,
                          confirmButtonText: 'dalje',
                          backdrop: false,
                          allowOutsideClick: false,
                          allowEscapeKey: false

                      });

                      $(".swal2-confirm").unbind("click").click(function() {
                          clearInterval(countdownTimer)
                          if (ide == 1) {
                              ProgressCountdown(120, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => odgovor());
                          }
                          submitBtn.hide();
                          nastavi()
                      })
                      $(".swal2-close").unbind("click").click(function() {
                          clearInterval(countdownTimer)
                          if (ide == 1) {
                              ProgressCountdown(120, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => odgovor());
                          }
                          nastavi()
                      })

                  })
              } else {
                  highlightIncorrectAnswerRed();
                  bodovi -= 10;
                  $("#krivo")[0].play();

                  $(".mrzim").unbind("click").click(function() {
                      swal({
                          title: "<em><span style='color:#bb422a'>"+quiz[questionCounter].question+"</span></em>",
                          html: "<ul class='opis-lista'><li>" + quiz[questionCounter].answers[0] + "</li><li>" + quiz[questionCounter].answers[1] + "</li><li>" + quiz[questionCounter].answers[2] + "</li><li>" + quiz[questionCounter].answers[3] + "</li></ul><p style='text-align:justify'>" + quiz[questionCounter].opis + "</p>",
                          showCloseButton: true,
                          confirmButtonText: 'dalje',
                          backdrop: false,
                          allowOutsideClick: false,
                          allowEscapeKey: false
                      });

                      $(".swal2-confirm").unbind("click").click(function() {
                          clearInterval(countdownTimer)
                          if (ide == 1) {
                              ProgressCountdown(120, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => odgovor());
                          }
                          nastavi()
                      })
                      $(".swal2-close").unbind("click").click(function() {
                          clearInterval(countdownTimer)
                          if (ide == 1) {
                              ProgressCountdown(120, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => odgovor());
                          }
                          nastavi()
                      })


                  })
              }
          }

          // Substitute the submit button for the continue button:

      }
      // Clicking on the submit button:





      function nastavi() {
          submitBtn.hide();
          continueBtn.hide()
              // Increment question number until there are no more questions, then advance to the next page
          if (questionCounter < quiz.length - 1) {
              questionCounter++;
              // Load the next question and set of answers
              generateQuestionAndAnswers();
              // Store the correct answer in a variable
              getCorrectAnswer();
              // Remove all selections, highlighting, and feedback
              deselectAnswer();
              clearHighlightsAndFeedback();
              // Hide the continue button
              continueBtn.hide(300);

              // Enable ability to select an answer
              answerDiv.on('click', function() {
                  submitBtn.show(300);
                  continueBtn.show(300)

                  // Remove pointer from any answer that already has it
                  deselectAnswer();
                  // Put pointer on clicked answer
                  selectAnswer(this);
                  // Store current selection as user answer
                  getUserAnswer(this);
                  getSelectedAnswerDivs(this);
                  // Store current answer div for highlighting purposes
                  odgovor();

              });
          } else {
              document.getElementsByClassName('questions-page')[0].style.display = "none"
              document.getElementsByClassName('sakri')[0].style.display = "block"
                  // Display user score as a percentage
              userScore.text(Math.floor((correctAnswersCounter / quiz.length) * 100) + " %");
              prikazBodova.text(bodovi);
              $("#60656686").attr("value", bodovi)
          }
      }

      // Clicking on the continue button:
      continueBtn.unbind("click").click(function() {
          clearInterval(countdownTimer)

          ProgressCountdown(120, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => odgovor());

          nastavi();
      });


      /* --- PAGE 3/3 --- */

      // Clicking on the retake button:
      retakeBtn.on('click', function() {
          // Go to the first page
          // Start the quiz over
          newQuiz();
          questionsPage.show(300);
          // Load question and answers
          generateQuestionAndAnswers();
          // Store the correct answer in a variable
          getCorrectAnswer();
          // Hide the submit and continue buttons
          submitBtn.hide();
          continueBtn.hide();
      });

      // Clicking on the spanish button:
      // Link takes user to Duolingo

  });

  function touchHandler(event) {
      var touches = event.changedTouches,
          first = touches[0],
          type = "";
      switch (event.type) {
          case "touchstart":
              type = "mousedown";
              break;
          case "touchmove":
              type = "mousemove";
              break;
          case "touchend":
              type = "mouseup";
              break;
          default:
              return;
      }


      // initMouseEvent(type, canBubble, cancelable, view, clickCount, 
      //                screenX, screenY, clientX, clientY, ctrlKey, 
      //                altKey, shiftKey, metaKey, button, relatedTarget);

      var simulatedEvent = document.createEvent("MouseEvent");
      simulatedEvent.initMouseEvent(type, true, true, window, 1,
          first.screenX, first.screenY,
          first.clientX, first.clientY, false,
          false, false, false, 0 /*left*/ , null);

      first.target.dispatchEvent(simulatedEvent);
      event.preventDefault();
  }


  function shuffle(array) { //izmješaj pitanja
      var i = 0,
          j = 0,
          temp = null

      for (i = array.length - 1; i > 0; i -= 1) {
          j = Math.floor(Math.random() * (i + 1))
          temp = array[i]
          array[i] = array[j]
          array[j] = temp
      }
  }

  function stvori_pitanja() {


      // QUIZ CONTENT ------

      function stvori(tekst, tekst2, tekst3) {
          do {
              predmet = jedno[Math.floor(Math.random() * jedno.length)];
          }
          while (predmet == tekst || predmet == tekst2 || predmet == tekst3);
          return predmet
      }

      function stvori2(tekst, tekst2, tekst3) {
          do {
              predmet = dvo[Math.floor(Math.random() * dvo.length)];
          }
          while (predmet == tekst || predmet == tekst2 || predmet == tekst3);
          return predmet
      }
      if (jedna == 1) {
          $('#bootstrapForm').attr('action', 'https://docs.google.com/forms/d/e/1FAIpQLScA8hBnt78WSyz0WB9SgrkU0Pkei_P2wLzk-Th_GzBNkAaRgA/formResponse');

          quiz = [{
              question: "U kojemu su polju riječi pravopisno točne?",
              answers: ["podcrtaj, Nijemica", "uskrsni, Sveta Ana (svetica)", " Panamski kanal, riječnik", "Nijemac, Kineski zid"],
              correctAnswer: "Nijemac, Kineski zid",
              opis: "Točan je odgovor <strong style='color:green'><em>Nijemac, Kineski zid</em></strong>. Pogrešno je napisano: <em>Nijemica, Sveta Ana i riječnik</em> umjesto: <em>Njemica, sveta Ana i rječnik</em>. <em>Sveti</em> i <em>sveta</em> uz ime svetca ili svetice te <em>blaženi</em> i <em>blažena</em> uz ime blaženika ili blaženice pišu se malim slovom. Riječi <em>rječnik</em> i <em>vjesnik</em> zapisuju se s kratkim odrazom jata iako se on izgovara i kao dug.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu je polju izraz pravopisno netočan?",
              answers: ["Baudelaireova pjesma", "baudelaireovski stil", "šekspirovski stil", "Shakespeareov sonet"],
              correctAnswer: "baudelaireovski stil",
              opis: "Točan je odgovor <strong style='color:green'><em>baudelaireovski stil</em></strong>. Iz toga je razloga točan zapis <em>šekspirovski stil</em>. Posvojni se pridjevi (pridjevi koji se tvore dodavanjem sufikasa -<em>ov</em>, -<em>ev </em>ili -<em>in</em>) zapisuju tako da se čuva izvorni zapis imena (<em>Baudelaireova pjesma</em>, <em>Shakespeareov sonet</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu je polju riječ pravopisno netočna?",
              answers: ["vat (mjerna jedinica)", "tesla (mjerna jedinica)", "yuan (novčana jedinica)", "euro (novčana jedinica)"],
              correctAnswer: "yuan (novčana jedinica)",
              opis: "Točan je odgovor <strong style='color:green'><em>yuan (novčana jedinica)</em></strong>. Naziv je novčane jedinice <em>juan</em>. Nazivi novčanih i mjernih jedinica zapisuju se kako se izgovaraju i uvijek se pišu malim slovom.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu polju nema pravopisne pogreške?",
              answers: ["Andaluzijski pas film je Luisa Buñuela i Salvadora Dalía.", "„Andaluzijski pas” film je Luisa Buñuela i Salvadora Dalíja.", "”Andaluzijski pas” film je Luis Buñuela i Salvador Dalía", "Andaluzijski pas film je Luis Buñuela i Salvador Dalíja."],
              correctAnswer: "„Andaluzijski pas” film je Luisa Buñuela i Salvadora Dalíja.",
              opis: "Točan je odgovor <strong style='color:green'><em>„Andaluzijski pas” film je Luisa Buñuela i Salvadora Dalíja.</em></strong> Naslov filma (kao i naslov književnoga djela, novina itd.) označuje se navodnicima („”) ili ukošenim slovima (<em>Andaluzijski </em><em>pas</em>). U imenima muških osoba (i domaćim i stranim) uvijek se sklanjaju obje sastavnice (<em>Luisa Buñuela </em>i <em>Salvadora Dalíja</em>, a u imenima koja u izgovoru završavaju na -<em>i </em>(<em>Dali, Vigny, Charlie</em>), -<em>io </em>(<em>Mario</em>) i -<em>ia </em>(<em>Mia</em>) između osnove i nastavka u sklonidbi i pri izvođenju posvojnih pridjeva umeće se <em>j </em>(G <em>Dalija</em>, <em>Vignyja</em>, <em>Charlieja</em>, <em>Marija</em>, <em>Mije</em>; posvojni pridjev: <em>Dalijev</em>, <em>Vignyjev</em>, <em>Charliejev</em>, <em>Marijev</em>, <em>Mijin</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu polju nema pravopisne pogreške?",
              answers: ["Francis S.K. Fitzgerald (24.9.1896–21.12.1940)", "Francis S.K. Fitzgerald (24.9.1896. – 21.12.1940.)", "Francis S. K. Fitzgerald (24. 9. 1896–21. 12. 1940)", "Francis S. K. Fitzgerald (24. 9. 1896. – 21. 12. 1940.)"],
              correctAnswer: "Francis S. K. Fitzgerald (24. 9. 1896. – 21. 12. 1940.)",
              opis: "Točan je odgovor <strong style='color:green'><em>Francis S. K. Fitzgerald (24. 9. 1896. – 21. 12. 1940.)</em></strong>. Između inicijala piše se bjelina (S. K.); raspon <em>od do </em>označuje se crticom, a taj se pravopisni znak uvijek piše s bjelinama s obiju strana osim u bibliografskim podatcima. Između oznaka dana, mjeseca i godine u brojčanome se zapisu piše bjelina (24. 9. 1896.).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu polju nema pravopisne pogreške?",
              answers: ["Hemingway pripada tzv. ”Izgubljenoj generaciji”.", "Hemingway pripada tzv. izgubljenoj generaciji.", "Hemingway pripada tzv. „izgubljenoj generaciji”.", "Hemingway pripada tzv. „Izgubljenoj generaciji”."],
              correctAnswer: "Hemingway pripada tzv. izgubljenoj generaciji.",
              opis: "Točan je odgovor <strong style='color:green'><em>Hemingway pripada tzv. izgubljenoj generaciji.</em></strong> Iza kratice <em>tzv. </em>riječ ili izraz ne stavlja se u navodnike. S obzirom na to da <em>izgubljena generacija </em>nije ime generacije, piše se malim slovom.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu je polju riječ pravopisno netočna?",
              answers: ["odhraniti", "pothlađen", "nadcestar", "pretprijava"],
              correctAnswer: "odhraniti",
              opis: "Točan je odgovor <strong style='color:green'><em>odhraniti</em></strong>. U toj se riječi provodi jednačenje po zvučnosti, pa zvučni glas <em>d </em>ispred bezvučnoga glasa <em>h </em>prelazi u svoj bezvučni parnjak <em>t </em>te je točan zapis te riječi <em>othraniti</em>. Jednačenje po zvučnosti zabilježeno je u zapisu <em>pretprijava</em>, a riječ <em>nadcestar </em>dobro je napisana temeljem pravopisnoga pravila da se jednačenje po zvučnosti ne bilježi u riječima kojima prefiks završava s <em>d</em>, a osnova počinje glasovima <em>c</em>, <em>č</em>, <em>ć</em>, <em>s </em>ili <em>š</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu polju nema pravopisne pogreške?",
              answers: ["Dobro došli!", "Dobrodošli!", "Prirodoslovnomatematički fakultet", "sudsko-medicinski priručnik"],
              correctAnswer: "Dobro došli!",
              opis: "Točan je odgovor <strong style='color:green'><em>Dobro došli!</em></strong>. U toj je rečenici glagolski način optativ kojim se izriče želja, kao i u npr. <em>Dobro se naspavali! Sretni bili! </em>U hrvatskome standardnom jeziku postoji i pridjev <em>dobrodošao</em>, npr. <em>dobrodošli gost</em>, <em>dobrodošao porast prihoda</em>. Pridjev <em>prirodoslovno-</em><em>matematički </em>nastao je od dviju riječi koje označuju dva pojma: <em>prirodoslovlje </em>i <em>matematika </em>te ima značenje ‘prirodoslovni i matematički’. Pridjev <em>sudskomedicinski </em>nastao je od dviju riječi koje označuju jedan pojam: <em>sudska medicina</em>, te ima značenje ‘koji se odnosi na sudsku medicinu’.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Kako se zove pismo nazvano po čovjeku čije je prezime Braille (braj)?",
              answers: ["Brailleevo pismo", "Brailleovo pismo", "Braillevo pismo", "Braillovo pismo"],
              correctAnswer: "Brailleevo pismo",
              opis: "Točan je odgovor <strong style='color:green'><em>Brailleevo pismo</em></strong>. Strano ime, da bismo mu zapisali oblik, trebamo prvo izgovoriti (u nominativu), potom se upitati o odnosu izgovorenoga i zapisa (dakle, izgovorenoga imena u nominativu i odnosa prema zapisu imena u nominativu), potom izgovoriti oblik te vidjeti što se od nominativa ponavlja te to prepisati i dopisati ono što se razlikuje. Dakle: ime <em>Braille </em> čitamo [braj]; pridjev je [brajev], pa na zapis imena u nominativu treba samo dopisati ono što smo u izgovoru dodali, a to je -<em>ev</em>: <em>Brailleev</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu polju nema pravopisne pogreške?",
              answers: ["Zbog nepovoljnih uvjeta, nismo podignuli kredit.", "To je žena, koju sam jučer upoznao.", "To je direktor naše tvrtke, s kojim se dobro slažem.", "Neću doći, jer sam umorna."],
              correctAnswer: "To je direktor naše tvrtke, s kojim se dobro slažem.",
              opis: "Točan je odgovor <strong style='color:green'><em>To je direktor naše tvrtke, s kojim se dobro slažem</em></strong>. U rečenici <em>Zbog nepovoljnih uvjeta, nismo podignuli kredit.</em> pogrešno se piše zarez iza priložne oznake, rečenica <em>To je žena, koju sam jučer upoznao.</em> restriktivna je (nerestriktivna je npr. rečenica <em>To je Ana Horvat, koju znam odmalena</em>. i rečenica <em>To je direktor naše tvrtke, s kojim se dobro slažem.</em> u ovome zadatku) i u njoj ne treba pisati zarez, rečenica <em>Neću doći, jer sam umorna.</em> zavisnosložena je rečenica u kojoj je poredak glavna surečenica – zavisna surečenica, pa u njoj ne treba pisati zarez.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu je polju pokrata točno napisana?",
              answers: ["Radi u FBI-ju.", "Radi u FBIu.", "Boluje od PTSP-a.", "Boluje od PTSP-ja."],
              correctAnswer: "Boluje od PTSP-a.",
              opis: "Točan je odgovor <strong style='color:green'><em>Boluje od PTSP-a.</em></strong> Pokrata PTSP čita se [pe te es pe] te između nje i nastavka ne treba umetati <em>j</em>. <em>J </em>se umeće samo kad pokrata u izgovoru završava na <em>-i</em>, <em>-io </em>ili <em>-ia</em>. Zbog toga bi druga rečenica točno bila napisana: <em>Radi u FBI-u </em>jer se FBI čita [ef bi aj].",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je rečenica pravopisno točna?",
              answers: ["Kupi vrećicu vanilin-šećera.", "Jedan od likova u filmu <em>Batman</em> je i žena-mačka.", "Bon koji se poklanja je poklon bon.", "Čistač koji je i dostavljač je čistač-dostavljač."],
              correctAnswer: "Kupi vrećicu vanilin-šećera.",
              opis: "Točan je odgovor <strong style='color:green'><em>Kupi vrećicu vanilin-šećera.</em></strong> U imeničkim polusloženicama spojnica označuje da se prvi dio polusloženice ne sklanja (G <em>vanilin-šećera</em>, D <em>vanilin-šećeru</em>…). Tako je i udrugom primjeru točan zapis <em>poklon-bon</em>. U ostalim primjerima sklanjaju se obje sastavnice (G <em>žene mačke</em>, <em>čistača dostavljača</em>, D <em>ženi mački</em>, <em>čistaču </em> <em>dostavljaču</em>) te između njih ne treba pisati spojnicu.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koje je ime pogrešno napisano?",
              answers: ["Kopački rit", "Boka kotorska", "Slavonski brod", "Plitvička jezera"],
              correctAnswer: "Slavonski brod",
              opis: "Točan je odgovor <strong style='color:green'><em>Slavonski brod</em></strong>. Višerječna imena naseljenih mjesta (kao i imena država) pišu se tako da se svaka od sastavnica, osim veznika i prijedloga (npr. <em>Sveti Petar u Šumi</em>), piše velikim slovom. U ostalim se zemljopisnim imenima velikim slovom piše samo prva riječ.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Što je točno napisano?",
              answers: ["zagrebčanin", "Zagrebčanin", "zagrepčanin", "Zagrepčanin"],
              correctAnswer: "Zagrepčanin",
              opis: "Točan je odgovor <strong style='color:green'><em>Zagrepčanin</em></strong>. Imena stanovnika naseljenih mjesta (etnici) pišu se velikim slovom. U tome se etniku provodi jednačenje po zvučnosti.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu su retku svi primjeri pravopisno točni?",
              answers: ["boca od 2 l, 5 % šećera, joga", "3cm, ilirski narodni preporod, taksi", "5% šećera, Ilirski narodni preporod, yoga", "taxi, 3 cm, 5 % šećera"],
              correctAnswer: "boca od 2 l, 5 % šećera, joga",
              opis: "Točan je odgovor <strong style='color:green'><em>boca od 2 l, 5 % šećera, joga</em></strong>. Između broja i znaka mjerne jedinice piše se bjelina (3 cm), pravopisno je točno <em>joga </em>i <em>taksi</em>, nazivi pokreta pišu se malim slovom te je točno <em>ilirski narodni preporod.</em>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu su retku svi primjeri pravopisno točni?",
              answers: ["Europska Komisija, ekoproizvod", "Europska unija, bio-gnojivo", "Europska Banka, eko-proizvodnja", "Europska zajednica, biouzgoj"],
              correctAnswer: "Europska zajednica, biouzgoj",
              opis: "Točan je odgovor <strong style='color:green'><em>Europska zajednica, biouzgoj</em></strong>. Točan je zapis <em>Europska unija</em>, <em>Europska komisija </em>i <em>Europska banka </em>(samo u višerječnim imenima naseljenih mjesta i država sve se sastavnice pišu velikim početnim slovima; u ostalim se imenima velikim početnim slovom piše samo prva riječ i, naravno, ime ako se nalazi u tome imenu). Točan je zapis <em>ekoproizvod, biognojivo </em>i <em>ekoproizvodnja </em>jer se prefiksoidi uvijek pišu sastavljeno s riječju kojoj se dodaju.</em>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je od navedenih pozdravnih formula pravopisno točna?",
              answers: ["Sa poštovanjem,<br>dr. sc. Pero Martić", "S poštovanjem,<br>Dr. sc. Pero Martić", "Sa štovanjem<br>dr.sc. Pero Martić", "S poštovanjem <br> dr. sc. Pero Martić"],
              correctAnswer: "S poštovanjem <br> dr. sc. Pero Martić",
              opis: "Točan je odgovor <strong style='color:green'><em>S poštovanjem <br> dr. sc. Pero Martić</em></strong>. <em>Sa </em>se piše samo ispred riječi koje počinju sa <em>s</em>, <em>š</em>, <em>z </em>ili <em>ž</em>. Titule se pišu malim slovom, pa se tako piše i kratica <em>dr.</em>, a između <em>dr. </em>i <em>sc. </em>piše se bjelina. Iza pozdravne formule (<em>S poštovanjem</em>) ne piše se zarez.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je od navedenih oznaka mjesta i datuma pravopisno točna?",
              answers: ["U Zagrebu 19. svibnja 2016.", "U Zagrebu 19.5.2016.", "U Zagrebu, 19. svibanj 2016.", "U Zagrebu, 19. 5. 2016."],
              correctAnswer: "U Zagrebu 19. svibnja 2016.",
              opis: "Točan je odgovor <strong style='color:green'><em>U Zagrebu 19. svibnja 2016</em></strong>. Iza oznake mjesta u lokativu ne piše se zarez. Zarez se piše iza oznake mjesta u nominativu (npr. Zagreb, 19. svibnja 2016.). Ako se piše kao riječ, mjesec se pri pisanju datuma navodi u genitivu, a ako se datum ispisuje brojevima, između rednih brojeva kojima se označuje dan, mjesec i godina treba biti bjelina.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu su retku svi primjeri točno napisani?",
              answers: ["TV-pretplata, C-vitamin, pH-vrijednost", "TV  pretplata, C  vitamin, pH vrijednost", "TV-pretplata, C vitamin, pH-vrijednost", "TV pretplata, C-vitamin, pH-vrijednost"],
              correctAnswer: "TV pretplata, C-vitamin, pH-vrijednost",
              opis: "Točan je odgovor <strong style='color:green'><em>TV pretplata, C-vitamin, pH-vrijednost</em></strong>. Višeslovne pokrate ne povezuju se s riječju ispred koje se nalaze spojnicom (<em>TV pretplata</em>), a jednoslovne povezuju <em>(C-vitamin</em>). Kratice <em>pH </em>i <em>Rh </em>povezuju se s riječju ispred koje se nalaze (<em>vrijednost, faktor</em>) spojnicom.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu su retku svi primjeri pravopisno točni?",
              answers: ["lijenčina, cjenik, ocijenjen", "ljenčina, cjenik, ocjenjen", "lijenčina, cijenik, ocijenjen", "ljenčina, cijenik, ocjenjen"],
              correctAnswer: "lijenčina, cjenik, ocijenjen",
              opis: "Točan je odgovor <strong style='color:green'><em>lijenčina, cjenik, ocijenjen</em></strong>. Glagolski pridjev trpni uvijek ima isti odraz jata kao infinitiv (<em>ocijeniti </em>– <em>ocijenjen</em>, <em>primijeniti </em>–<em> primijenjen</em>, <em>ocjenjivati </em>– <em>ocjenjivan</em>, <em>primjenjivati </em>– <em>primjenjivan</em>).<em></em>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu polju nijedan primjer ne treba pisati velikim početnim slovom?",
              answers: ["GRAŠEVINA,	ŠKOTSKI	TERIJER,	ZAGREBAČKA KATEDRALA", "MOLIŠKI HRVATI, BERLINSKI ZID, BOŽIĆNI", "PUSTINJSKA OLUJA (vojna akcija); MUPOVAC, MODERNA", "SPLITSKI, BOLONJSKI PROCES, ZADRANIN"],
              correctAnswer: "GRAŠEVINA,	ŠKOTSKI	TERIJER,	ZAGREBAČKA KATEDRALA",
              opis: "Točan je odgovor <strong style='color:green'><em>GRAŠEVINA,	ŠKOTSKI	TERIJER,	ZAGREBAČKA KATEDRALA</em></strong>. Nazivi proizvoda, jela, pasmina i sorta pišu se malim slovom. <em>Zagrebačka katedrala </em>nije ime crkve nego označuje da je to katedrala u Zagrebu. Malim se početnim slovom pišu i: <em>božićni</em>, <em>splitski </em>(jer se malim slovom pišu odnosni pridjevi),<em> bolonjski proces </em>(jer je to naziv procesa, a ne njegovo ime; ali: <em>Bolonjska deklaracija </em>jer je to dokument), <em>moderna </em>(jer se malim slovom pišu nazivi pokreta), <em>moliški </em><em>Hrvati </em>(jer su to Hrvati koji žive u pokrajini Molise) te <em>mupovac </em>(malim se slovom pišu nazivi pripadnika organizacija i pokreta). Velikim se slovom piše: <em>Berlinski zid </em>(ime spomenika), <em>Pustinjska oluja </em> (ime vojne akcije) i <em>Zadranin </em>(etnik).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu su retku svi posvojni pridjevi dobro napisani?",
              answers: ["CARnetov, Googleov, Mc’Donald’sov, FIFA-in", "CARnet-ov, Googlov, Mc’Donaldsov, FIFIN", "CARnet-ov, Google-ov, MC’Donald’s-ov, Fifin", "CARnet-ov, Googlov, Mc’Donald’sov, Fifa-in"],
              correctAnswer: "CARnetov, Googleov, Mc’Donald’sov, FIFA-in",
              opis: "Točan je odgovor <strong style='color:green'><em>CARnetov, Googleov, Mc’Donald’sov, FIFA-in</em></strong>. Posvojni pridjev od <em>CARnet </em>piše se bez spojnice jer se njegovzavršni dio piše malim slovima. Bez spojnice piše se i pridjev <em>Googleov </em>jer se pridjevi od imena (i domaćih i stranih) pišu bez nje. <em>Mc’Donald’sov </em>je pridjev izveden od engleskoga posvojnoga genitiva imena Mc’Donald, ali u hrvatskome ga upotrebljavamo u imeničkoj funkciji (kao ime lanca restorana) te od njega izvodimo pridjev kao od svakoga drugog imena. <em>FIFA </em>je pokrata ženskoga roda. Pokratama se padežni ili tvorbeni nastavak dodaje s pomoću spojnice. Pokrate ženskoga roda često se leksikaliziraju, te je uz zapis <em>INA</em>, <em>HINA</em>, <em>FINA </em>i <em>NASA </em>dobar i zapis <em>Ina</em>, <em>Hina</em>, <em>Fina </em>i <em>Nasa</em>. Te se leksikalizirane pokrate sklanjaju<em>Ine, Ini</em>…, <em>Hine, Hini</em>…, <em>Fine, Fini</em>…, <em>Nase, Nasi</em>…",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu polju nema pravopisne pogreške?",
              answers: ["jednadžba, đurđica, patliđan", "porođaj, maharađa, narudžba", "džamija, đemper, šeširdžija", "đuveč, đon, svjedodžba"],
              correctAnswer: "đuveč, đon, svjedodžba",
              opis: "Točan je odgovor <strong style='color:green'><em>đuveč, đon, svjedodžba</em></strong>. U zadatku su pogrešno napisane riječi <em>patiđan</em>, <em>maharađa </em>i    <em>đemper</em>. Te se riječi pišu: <em>patlidžan</em>, <em>maharadža </em>i <em>džemper</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je riječ pogrešno napisana?",
              answers: ["stranputica", "isčeznuti", "bombon", "simfonijski"],
              correctAnswer: "isčeznuti",
              opis: "Točan je odgovor <strong style='color:green'><em>isčeznuti</em></strong>. U toj se riječi provodi jednačenje po zvučnosti i po mjestu tvorbe i ono se bilježi. U riječi <em>bombon </em> provodi se i bilježi jednačenje po mjestu tvorbe. Jednačenje po mjestu tvorbe provodi se i u riječi <em>stranputica </em>te se ona izgovara [stramputica], ali se to jednačenje ne bilježi u pismu.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu je polju pravopisna pogreška?",
              answers: ["naddijalektni", "kilogrammetar", "dvadesettrećina", "bezzvučan"],
              correctAnswer: "bezzvučan",
              opis: "Točan je odgovor <strong style='color:green'><em>bezzvučan</em></strong>. Točan je zapis <em>bezvučan</em>. U toj se riječi provodi i bilježi glasovna promjena ispadanje suglasnika. U ostalim se navedenim tvorenicama ta glasovna promjena ne provodi.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu polju nema pravopisne pogreške?",
              answers: ["promijeniti, zamijenjen, cijenjen", "namijeniti, zamjenjen, promjenjen", "zamjeniti, namijenjen, cijeniti", "namjeniti, namjenjen, cijenjen"],
              correctAnswer: "promijeniti, zamijenjen, cijenjen",
              opis: "Točan je odgovor <strong style='color:green'><em>promijeniti, zamijenjen, cijenjen</em></strong>. S pogrešnim su odrazom jata zapisane riječi: <em>zamjenjen</em>, <em>promjenjen</em>, <em>zamjeniti</em>, <em>namjeniti</em>, <em>namjenjen</em>. Točan je zapis: <em>zamijenjen</em>, <em>promijenjen</em>, <em>zamijeniti</em>, <em>namijeniti</em>, <em>namijenjen</em>. U glagolskome pridjevu trpnomu (<em>zamijenjen</em>, <em>promijenjen</em>, <em>namijenjen</em>, <em>cijenjen</em>) odraz jata uvijek je isti kao u infinitivu glagola.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojoj je rečenici oblik pokrate točno napisan?",
              answers: ["Član je HSLSa.", "Član je HSLS-a.", "Član je HSLS a.", "Član je HSLS."],
              correctAnswer: "Član je HSLS-a.",
              opis: "Točan je odgovor <strong style='color:green'><em>Član je HSLS-a</em></strong>. Između pokrate i obličnoga ili tvorbenoga nastavka piše se spojnica.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu polju nema pravopisne pogreške?",
              answers: ["Savezna republika Njemačka, dozreti, koščica", "Savezna Republika Njemačka, dozreti, košćica", "Savezna republika Njemačka, dozrjeti, košćica", "Savezna Republika Njemačka, dozrjeti, koščica"],
              correctAnswer: "Savezna Republika Njemačka, dozreti, košćica",
              opis: "Točan je odgovor <strong style='color:green'><em>Savezna Republika Njemačka, dozreti, košćica</em></strong>. U višerječnim imenima država sve se riječi osim prijedloga i veznika pišu velikim početnim slovom; osnova je svršenih glagola <em>zre</em>, a nije <em>zrje</em>. S <em>ć </em>se pišu riječi <em>košćica </em>i <em>košćurina</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu polju nema pravopisne pogreške?",
              answers: ["majca, buča (‘tikva’), supkultura, adhezija", "majica, buća (‘tikva’), supkultura, athezija", "majica, buča (‘tikva’), supkultura, adhezija", "majca, buća (‘tikva’), supkultura, athezija"],
              correctAnswer: "majica, buča (‘tikva’), supkultura, adhezija",
              opis: "Točan je odgovor <strong style='color:green'><em>majica, buča (‘tikva’), supkultura, adhezija</em></strong>. U riječi <em>supkultura </em>provodi se i bilježi jednačenje po zvučnosti. Ono se ne bilježi u riječima latinskoga podrijetla koje počinju s <em>ad</em>- (<em>adhezija</em>) i riječima latinskoga podrijetla koje počinju sa <em>sub- </em>iza kojega slijedi <em>p </em>(<em>subpolarni</em>). Riječ <em>buča </em>označuje tikvu, bundevu, a riječ <em>buća </em> sportski rekvizit.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je od navedenih uvodnih formula pravopisno točna?",
              answers: ["Poštovani gospodine Petroviću i gospodine Miliću,<br>Želimo Vas obavijestiti...", "Poštovani gospodine Petroviću i gospodine Miliću,<br>želimo vas obavijestiti...", "Poštovani gospodine Petroviću i gospodine Miliću,<br>želimo Vas obavijestiti...", "Poštovani gospodine Petroviću i gospodine Miliću,<br>Želimo Vas obavijestiti..."],
              correctAnswer: "Poštovani gospodine Petroviću i gospodine Miliću,<br>želimo vas obavijestiti...",
              opis: "Točan je odgovor <strong style='color:green'><em>Poštovani gospodine Petroviću i gospodine Miliću,<br>želimo vas obavijestiti..</em></strong><br>U uvodnoj formuli nakon oslovljavanja koje završava zarezom tekst u sljedećemu retku započinje malim slovom. U obraćanju usmjerenome na više osoba zamjenica <em>vi </em>ne piše se velikim slovom. Velikim se slovom piše isključivo u obraćanju s poštovanjem jednoj osobi, npr.: <em> Poštovani gospodine Petroviću,<br>želimo Vas obavijestiti... </em>",
              boja_pozadine: "#FCE4EC"
          }];
      } else if (jedna == 2) {
          $('#bootstrapForm').attr('action', 'https://docs.google.com/forms/d/e/1FAIpQLSc_WNkgpfrTJilX9N_wrFi8OocTCQl3WDjRa2Up_iky64utWQ/formResponse');
          quiz = [{
              question: "Kad je njemačka riječ <em>der Strudl</em> (koja ima značenja: 1. ‘vrtlog’, 2. ‘vrsta kolača’) u hrvatski ušla u obliku <em>štrudla</em>, došlo je do ovih prilagodba:",
              answers: ["pravopisne, fonološke, sintaktičke", "pravopisne, fonološke, morfološke", "pravopisne, fonološke, morfološke, sintaktičke", "pravopisne, fonološke, morfološke, semantičke"],
              correctAnswer: "pravopisne, fonološke, morfološke, semantičke",
              opis: "Točan je odgovor <strong style='color:green'><em>pravopisne, fonološke, morfološke, semantičke</em></strong>. Riječ se pi&scaron;e malim slovom (pravopisna norma), <em>st </em>se mijenja u <em>&scaron; </em>(fonolo&scaron;ka norma), dodaje se nastavak -<em>a </em>te postaje imenica ženskoga roda (morfolo&scaron;ka norma) i sužava joj se značenje (semantička norma).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koji od sljedećih parova riječi čini minimalni fonološki par?",
              answers: ["most – kost", "rak – raj", "hod – pod", "rat – sat"],
              correctAnswer: "most – kost",
              opis: "Točan je odgovor <strong style='color:green'><em>most – kost</em></strong>. Riječi koje se razlikuju samo jednim fonemom (a imaju isti naglasak) tvore minimalni par (<em>m&ocirc;st </em>i <em>k&ocirc;st</em>, <em>r</em>ȁ<em>k </em>i <em>r&acirc;j</em>, <em>h&ocirc;d </em>i <em>p</em>ȍ<em>d </em>te <em>r</em>ȁ<em>t </em>i <em>s&acirc;t </em>razlikuju se i naglasno).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je od sljedećih riječi točno rastavljena na slogove?",
              answers: ["sa-drža-va- ti", "li-jen-či-na", "lije-ha", "čet-vrtak"],
              correctAnswer: "lije-ha",
              opis: "Točan je odgovor <strong style='color:green'><em>lijeha</em></strong>. Odraz jata <em>ije </em>izgovara se jednosložno i ne rastavlja se na slogove. U riječima <em>sadržavati </em>i <em>četvrtak r </em>je slogotvoran, pa te riječi treba rastaviti <em>sa-dr-ža-va-ti </em>i <em>čet-vr-tak</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojoj je riječi <em>r</em> slogotvorni glas?",
              answers: ["mrak", "ruda", "brdo", "trokut"],
              correctAnswer: "brdo",
              opis: "Točan je odgovor <strong style='color:green'><em>brdo</em></strong>. Slogotvorni je glas nosilac sloga i na njemu se može nalaziti naglasak. Riječ je <em>mrak </em>jednosložna i nosilac sloga je <em>a</em>, riječ <em>ruda </em>se na slogove rastavlja <em>ru-da </em>i nosilac prvoga sloga je <em>u</em>, a riječ <em>trokut </em>se na slogove rastavlja <em>tro-kut </em>i nosilac prvoga sloga je <em>o</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je od ovih riječi trosložna?",
              answers: ["slabokrvan", "naprstak", "prijeglas", "cvijetak"],
              correctAnswer: "naprstak",
              opis: "Točan je odgovor <strong style='color:green'><em>naprstak</em></strong>. <em>Naprstak </em>se rastavlja na slogove <em>na-pr-stak</em>. Riječ <em>slabokrvan </em>rastavlja se na slogove <em>sla-bo-kr-van </em>(<em>r </em>je slogotvoran), riječ <em>prijeglas </em>rastavlja se na slogove <em>prije-glas</em>, a riječ <em>cvijetak </em>rastavlja se na slogove <em>cvije-tak</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koji je slog naglašen u riječi <em>telefonirati</em>?",
              answers: ["prvi", "drugi", "treći", "četvrti"],
              correctAnswer: "četvrti",
              opis: "Točan je odgovor <strong style='color:green'><em>četvrti</em></strong>. Na slogu <em>ni</em> nalazi se dugouzlazni naglasak.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je od ovih riječi pogrešno naglašena?",
              answers: ["cȓn", "gláva", "čòvjek", "pročȉtati"],
              correctAnswer: "pročȉtati",
              opis: "Točan je odgovor <strong style='color:green'><em>pročȉtati</em></strong>. U toj se riječi silazni naglasak nalazi u sredini riječi. Time se kr&scaron;i pravilo o nagla&scaron;avanju koje glasi: Silazni se naglasak nikad ne nalazi u sredini riječi. Točan je naglasak te riječi <em>proč&igrave;tati.</em>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi netočnu tvrdnju.",
              answers: ["Naglasak ne može biti na zadnjemu slogu u riječi.", "Jednosložne riječi imaju samo silazne naglaske.", "Višesložne riječi mogu imati samo uzlazni naglasak na prvome slogu.", "Na unutarnjim slogovima višesložne riječi mogu biti samo uzlazni naglasci."],
              correctAnswer: "Višesložne riječi mogu imati samo uzlazni naglasak na prvome slogu.",
              opis: "Točan je odgovor <strong style='color:green'><em>Višesložne riječi mogu imati samo uzlazni naglasak na prvome slogu</em></strong>. Ta je tvrdnja netočna jer višesložne riječi mogu imati bilo koji naglasak na prvome slogu.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je od ovih riječi pogrešno naglašena?",
              answers: ["gȍdina", "dalèko", "grád", "literatúra"],
              correctAnswer: "grád",
              opis: "Točan je odgovor <strong style='color:green'><em>grád</em></strong>. Ta je riječ pogre&scaron;no nagla&scaron;ena jer jednosložne riječi ne mogu imati uzlazni naglasak. Riječ <em>gr</em>ȃ<em>d </em>ima značenje &lsquo;naseljeno mjeso&rsquo;, a riječ <em>gr</em>ȁ<em>d </em>&lsquo;oborina&rsquo;. Te su dvije riječi homografi jer se isto pi&scaron;u, ali različito izgovaraju.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi netočnu tvrdnju.",
              answers: ["Proklitika se naslanja na naglašenu riječ iza sebe.", "Enklitika se naslanja na naglašenu riječ ispred sebe.", "Enklitike nikad ne mogu imati naglasak.", "Proklitike nikad ne mogu imati naglasak."],
              correctAnswer: "Proklitike nikad ne mogu imati naglasak.",
              opis: "Točan je odgovor <strong style='color:green'><em>Proklitike nikad ne mogu imati naglasak</em></strong>. Ako nagla&scaron;ena riječ koja slijedi iza proklitike ima silazan naglasak, taj se naglasak, zbog pravila da silazni naglasci ne mogu biti u sredini riječi, prebacuje na proklitiku.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je glasovna promjena provedena u riječi <em>obrambeni</em>?",
              answers: ["jotacija", "jednačenje po mjestu tvorbe", "jednačenje po zvučnosti", "epenteza"],
              correctAnswer: "jednačenje po mjestu tvorbe",
              opis: "Točan je odgovor <strong style='color:green'><em>jednačenje po mjestu tvorbe</em></strong>. Suglasnici različiti po mjestu tvorbe jednače se u suglasničkim skupovima tako da se prvi suglasnik skupa zamjenjuje suglasnikom koji je po mjestu tvorbe jednak drugomu suglasniku skupa. Zubno-usneni <em>n </em>prelazi u dvousneni <em>m </em>ispred dvousnenih <em>p</em>, <em>b</em>: <em>obran + beni = obrambeni</em>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja glasovna promjena nije fonološki uvjetovana?",
              answers: ["jednačenje po zvučnosti", "jotacija", "jednačenje po mjestu tvorbe", "ispadanje suglasnika."],
              correctAnswer: "jotacija",
              opis: "Točan je odgovor <strong style='color:green'><em>jotacija</em></strong>. Glasovne promjene mogu biti fonolo&scaron;ki (okolnim glasovima) ili morfolo&scaron;ki (morfolo&scaron;kim oblikom) uvjetovane. Fonolo&scaron;ki su uvjetovane glasovne promjene jednačenje po zvučnosti, jednačenje po mjestu tvorbe i ispadanje suglasnika, a morfolo&scaron;ki su uvjetovane sve ostale glasovne promjene (npr. jotacija).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pritisni točan odgovor.",
              answers: ["isčupati", "izčupati", "iščupati", "ižčupati"],
              correctAnswer: "iščupati",
              opis: "Točan je odgovor <strong style='color:green'><em>iščupati</em></strong>. Prefiks <em>iz-</em> dodaje se osnovi <em>čupati</em>. Jednačenjem po zvučnosti dobivamo <em>isčupati </em>jer je <em>z </em>zvučan, a <em>č </em>bezvučan. Jednačenjem po mjestu tvorbe dobivamo <em>i&scaron;čupati </em>jer je <em>s </em>nepalatal, a <em>č </em>palatal.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U riječi <em>raščešljati</em> dolazi do ovih glasovnih promjena:",
              answers: ["jednačenja po zvučnosti i ispadanja suglasnika", "jednačenja po mjestu tvorbe i ispadanja suglasnika", "jednačenja po mjestu tvorbe i jotacije", "jednačenja po zvučnosti i jednačenja po mjestu tvorbe"],
              correctAnswer: "jednačenja po zvučnosti i jednačenja po mjestu tvorbe",
              opis: "Točan je odgovor <strong style='color:green'><em>jednačenja po zvučnosti i jednačenja po mjestu tvorbe</em></strong>. Prefiks <em>raz- </em>dodaje se osnovi <em>čupati</em>. Jednačenjem po zvučnosti dobivamo <em>rasče&scaron;ljati </em>jer je <em>z </em>zvučan, a <em>č </em>bezvučan. Jednačenjem po mjestu tvorbe dobivamo <em>ra&scaron;če&scaron;ljati </em>jer je <em>s </em>nepalatal, a <em>č </em>palatal.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu se retku nalaze samo bezvučni suglasnici?",
              answers: ["b, d, g", "č, ž, š", "m, n, nj", "p, t, k"],
              correctAnswer: "p, t, k",
              opis: "Točan je odgovor <strong style='color:green'><em>p, t, k</em></strong>. Zvučni su suglasnici <em>b, d, g</em>, bezvučni su <em>č </em>i <em>&scaron;</em>, a <em>ž </em>je zvučan, <em>m, n </em>i <em>nj </em>zvučni su suglasnici.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu se retku nalaze samo palatali?",
              answers: ["č, ž, š", "b, d, g", "m, n, nj", "p, t, k"],
              correctAnswer: "č, ž, š",
              opis: "Točan je odgovor <strong style='color:green'><em>č, ž, š</em></strong>. Svi su ostali navedeni glasovi osim <em>nj</em> nepalatali.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U riječima <em>striče</em>, <em>zeče</em> dolazi do:",
              answers: ["palatalizacije", "sibilarizacije", "jotacije", "jednačenja po mjestu tvorbe"],
              correctAnswer: "palatalizacije",
              opis: "Točan je odgovor <strong style='color:green'><em>palatalizacije</em></strong>. Palatalizacija je glasovna promjena u kojoj se nepalatalni suglasnici <em>k, g, h </em>i <em>c </em>zamjenjuju palatalnima <em>č, ž </em>i <em>&scaron; </em>ispred <em>e </em>i <em>i.</em>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U riječi <em>ronioče</em> dolazi do ovih glasovnih promjena:",
              answers: ["zamjene <em>l</em> s <em>o</em> (vokalizacije), nepostojanoga <em>a</em>, jotacije", "zamjene <em>l</em>  s <em>o</em>   (vokalizacije), palatalizacije", "palatalizacije, sibilarizacije, nepostojanoga <em>a</em>", "zamjene <em>l</em>  s <em>o</em> (vokalizacije), nepostojanoga <em>a</em> i palatalizacije"],
              correctAnswer: "zamjene <em>l</em>  s <em>o</em> (vokalizacije), nepostojanoga <em>a</em> i palatalizacije",
              opis: "Točan je odgovor <strong style='color:green'><em>zamjene l  s o (vokalizacije), nepostojanoga a i palatalizacije</em></strong>. Od riječi <em>ronilac </em>zamjenom <em>l </em>s <em>o </em>(vokalizacijom) dobili bismo <em>ronioac</em>. Nepostojanim <em>a </em>dobivamo <em>ronioc</em>, a dodavanjem padežnoga nastavka za vokativ <em>e </em>i palatalizacijom dobivamo <em>ronioče</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Od <em>svjedočiti</em> dobivamo <em>svjedodžba</em> i tu dolazi do ovih glasovnih promjena:",
              answers: ["jednačenja po zvučnosti", "sibilarizacije", "jotacije", "jednačenja po mjestu tvorbe"],
              correctAnswer: "jednačenja po zvučnosti",
              opis: "Točan je odgovor <strong style='color:green'><em>jednačenja po zvučnosti</em></strong>. Glas <em>č </em>je bezvučan, a <em>b </em>je zvučan, pa <em>č </em>ispred <em>b </em>prelazi u svoj zvučni parnjak <em>dž</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "S obzirom na to titraju li glasnice pri proizvodnji glasa, glasovi se dijele na:",
              answers: ["palatale i nepalatale", "samoglasnike i suglasnike", "otvornike i zatvornike", "zvučne i bezvučne"],
              correctAnswer: "zvučne i bezvučne",
              opis: "Točan je odgovor <strong style='color:green'><em>zvučne i bezvučne</em></strong>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U rečenici <em>Djevojka je čitala lijepe knjige.</em> ima:",
              answers: ["pet naglasnih cjelina", "četiri naglasne cjeline", "tri naglasne cjeline", "dvije naglasne cjeline"],
              correctAnswer: "četiri naglasne cjeline",
              opis: "Točan je odgovor <strong style='color:green'><em>četiri naglasne cjeline</em></strong>. Rečenica se dijeli &nbsp;na &nbsp;naglasne cjeline tako da se odijele skupine riječi koje imaju jedan naglasak, npr. <em>Djevojka je / čitala / lijepe / knjige</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu je od ovih glagola provedena palatalizacija?",
              answers: ["plačem", "skačem", "pečem", "vičem"],
              correctAnswer: "pečem",
              opis: "Točan je odgovor <strong style='color:green'><em>pečem</em></strong>. To se može vidjeti prema trećemu licu množine, koje glasi <em>peku</em>, pa je jasno da je prezent tvoren s pomoću nastavaka -<em>em, -e&scaron;, -e, -emo, -ete, -u</em>. U ostalim je primjerima provedena jotacija. Oni u trećemu licu množine glase <em>plaču, skaču, viču</em>, pa je jasno da je prezent tvoren s pomoću nastavaka -<em>jem, -je&scaron;, -je, -jemo, -jete, -ju</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je od sljedećih riječi pravopisno točna?",
              answers: ["izvambrodski", "pretprijamni", "potcrtati", "pretsjedati"],
              correctAnswer: "pretprijamni",
              opis: "Točan je odgovor <strong style='color:green'><em>pretprijamni</em></strong>. U primjerima <em>podcrtati </em>i <em>predsjedati </em>dolazi do jednačenja po zvučnosti, ali se ono ne ostvaruje u pismu prema pravopisnome pravilu da se <em>d </em>ispred <em>c, č, ć, s </em>i <em>&scaron; </em>ne pi&scaron;e kao <em>t</em>. U prefiksu <em>izvan </em>ne bilježi se jednačenje po mjestu tvorbe ispred dvousnenih suglasnika.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojoj se od ovih riječi ne nalazi epentetski <em>l</em>?",
              answers: ["kapljem", "ljiljan", "Rabljanin", "grmlje"],
              correctAnswer: "ljiljan",
              opis: "Točan je odgovor <strong style='color:green'><em>ljiljan</em></strong>. U primjerima <em>kapljem</em>, <em>Rabljanin </em>i <em>grmlje </em>do&scaron;lo je do jotacije pri kojoj se nepalatalni suglasnici zamjenjuju palatalnim suglasnicima, a iza <em>p, b, m, v, f </em>umeće se epentetski <em>l.</em>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U riječi <em>žući</em> dolazi do ove glasovne promjene:",
              answers: ["palatalizacije", "sibilarizacije", "jotacije", "jednačenja po mjestu tvorbe"],
              correctAnswer: "jotacije",
              opis: "Točan je odgovor <strong style='color:green'><em>jotacije</em></strong>. Kad se nepalatalni suglasnici <em>s, z, h, l, n </em>nađu ispred <em>j</em>, zamjenjuju se palatalnim suglasnicima <em>&scaron;, ž, lj, nj.</em>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu su nizu navedene samo proklitike?",
              answers: ["bez, ne, iz, i", "ja, ti, kod, kroz", "je, ga, na, ni", "on, ona, pa, te"],
              correctAnswer: "bez, ne, iz, i",
              opis: "Točan je odgovor <strong style='color:green'><em>bez, ne, iz, i</em></strong>. U hrvatskome jeziku proklitike mogu biti prijedlozi, i to svi jednosložni (<em>bez, do, iz, kod, kroz, na</em>) te neki dvosložni (<em>ispod, iznad, među, mimo</em>) i trosložni (<em>okolo</em>), veznici (<em>i, pa, te</em>) i čestice (<em>ne, ni</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U riječi <em>bezakonje</em> dolazi do:",
              answers: ["jotacije i jednačenja po zvučnosti", "palatalizacije i jednačenja po zvučnosti", "sibilarizacije i jednačenja po mjestu tvorbe", "ispadanja suglasnika i jotacije"],
              correctAnswer: "ispadanja suglasnika i jotacije",
              opis: "Točan je odgovor <strong style='color:green'><em>ispadanja suglasnika i jotacije</em></strong>. Riječ <em>bezakonje</em> tvorena je prefiksalno-sufiksalnom tvorbom od elemenata <em>bez + zakon</em> <em>+ je</em>; <em>zz </em>daje <em>z </em>(ispadanje suglasnika), a <em>n + j nj </em>(jotacija).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Do kojih glasovnih promjena dolazi u primjeru <em>zadatak – zadatci</em>?",
              answers: ["nepostojani <em>a</em>, sibilarizacija", "ispadanje suglasnika, jednačenje po zvučnosti, sibilarizacija", "zamjena <em>l</em> s <em>o</em> (vokalizacija), ispadanje suglasnika, sibilarizacija", "zamjena <em>l</em> s <em>o</em> (vokalizacija), nepostojani <em>a</em>, palatalizacija"],
              correctAnswer: "nepostojani <em>a</em>, sibilarizacija",
              opis: "Točan je odgovor <strong style='color:green'><em>nepostojani a, sibilarizacija</em></strong>. Suglasnici <em>k, g, h </em>zamjenjuju se suglasnicima <em>c, z, s </em>ispred <em>i</em>. Nepostojani <em>a </em>samoglasnik je <em>a </em>koji se pojavljuje u pojedinim oblicima imenica (<em>mom<strong>a</strong>k &ndash; momka, djevojka &ndash; </em>G mn. <em>djevoj<strong>a</strong>ka</em>), pridjeva (<em>s<strong>a</strong>v &ndash; sva, rijed<strong>a</strong>k &ndash; rijetka</em>) i zamjenica (<em>tak<strong>a</strong>v </em>&ndash; <em>takva</em>), dok u drugima izostaje. Kad govorimo o glasovnoj promjeni nepostojani <em>a, </em>zapravo treba razlikovati dva slučaja: dolazi do alternacije <em>a/</em><em>&Oslash; </em>u primjerima kao <em>mom<strong>a</strong>k &ndash; momka, pijet<strong>a</strong>o &ndash; pijetla, </em>tj. do gubljenja nepostojanoga <em>a </em>koji se nalazi u nominativu imenica ili mu&scaron;kome rodu pridjeva ili dolazi do alternacije <em>&Oslash;</em><em>/a </em>u primjerima kao <em>bačva &ndash; bač<strong>a</strong>va, djevojka &ndash; djevoj<strong>a</strong>ka</em>, tj. do dodavanja nepostojanoga <em>a </em>koji se ne nalazi u nominativu.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Do kojih glasovnih promjena dolazi u riječi <em>kabao – kabla</em>?",
              answers: ["nepostojani <em>a</em>, sibilarizacija", "ispadanje suglasnika, jednačenje po zvučnosti, sibilarizacija", "zamjena <em>l</em> s o (vokalizacija), ispadanje suglasnika, sibilarizacija", "zamjena <em>l</em> s <em>o</em> (vokalizacija), nepostojani <em>a</em>"],
              correctAnswer: "zamjena <em>l</em> s <em>o</em> (vokalizacija), nepostojani <em>a</em>",
              opis: "Točan je odgovor <strong style='color:green'><em>zamjena l s o (vokalizacija), nepostojani a</em></strong>. Suglasnik <em>l </em>na kraju sloga ili riječi zamjenjuje se s <em>o</em>, npr. <em>ličilac &ndash; ličioci &ndash; ličiočev, orao &ndash; orlov &ndash; orla, selo &ndash; seoce, kabao &ndash; kabla, kablovi. </em>Nepostojani <em>a </em>u gornjemu primjeru alternira s <em>&Oslash;</em>, tj. gubi se.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Kako se zovu govorne varijante podcrtanoga grafema u primjeru <em>A<u>n</u><a</em> i <em>A<u>n</u>ka</em>?",
              answers: ["fonemi", "morfemi", "alomorfi", "alofoni"],
              correctAnswer: "alofoni",
              opis: "Točan je odgovor <strong style='color:green'><em>alofoni</em></strong>. Alofoni su različiti glasovni ostvaraji istoga fonema. Oni ovise o okolnim glasovima, npr. fonem <em>n </em>u <em>Ana </em>i <em>Anka </em>ostvaraju se različitim alofonima jer u primjeru <em>Ana </em>iza njega slijedi samoglasnik (otvornik) <em>a</em>, a u primjeru <em>Anka </em>velar (jedrenik, mekonepčanik) <em>k</em>.",
              boja_pozadine: "#FCE4EC"
          }, ];
      } else if (jedna == 3) {
          $('#bootstrapForm').attr('action', 'https://docs.google.com/forms/d/e/1FAIpQLSfqu-Tdm2Eq9UF59uXCG_AGXWI9xP5bwvcrMuToj3t9jx10Bw/formResponse');
          quiz = [{
              question: "Pronađi redak bez pogreške.",
              answers: ["udata, donesen, prenijet", "udana, donešen, prenesen", "udana, donesen, prenesen", "udata, donijet, prenešen"],
              correctAnswer: "udana, donesen, prenesen",
              opis: "Točan je odgovor <strong style='color:green'><em>udana, donesen, prenesen</em></strong>.  Kad od jednoga glagola postoji više glagolskih pridjeva trpnih, prednost se daje glagolskomu pridjevu bez glasovnih promjena pred onim s glasovnim promjenama (npr. glagolskomu pridjevu trpnom <em>donesen</em> pred glagolskim pridjevom trpnim <em>donešen</em>, <em>izvezen</em> pred <em>izvežen</em>, <em>prenesen</em> pred <em>prenešen</em>, <em> iznesen </em>pred<em> iznešen</em>) te onomu koji završava na<em> -n </em>pred<em> </em>onim koji završava na <em>-t</em> (npr. <em>udana</em> pred <em>udata</em>, <em>dan</em> pred <em>dat</em>, <em>iznesen</em> pred <em>iznijet</em>).<div class='table-responsive'><table width='100%' cellspacing='0' cellpadding='4' style='border:1px solid black'> <colgroup> <col/> <col/> <col/> <col '/> </colgroup> <tbody> <tr valign='top'> <td > <p align='justify'> NE </p></td><td > <p align='justify'> udata, dat, poslat </p></td><td > <p align='justify'> izvežen, prevežen </p></td><td > <p align='justify'> prenijet, prenešen, </p><p align='justify'> donijet, donešen </p></td></tr><tr valign='top'> <td > <p align='justify'> DA </p></td><td > <p align='justify'> udana, dan, poslan </p></td><td > <p align='justify'> izvezen, prevezen </p></td><td > <p align='justify'> prenesen, donesen </p></td></tr></tbody></table></div>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu je točan nominativ i genitiv množine riječi <em>mislilac</em>.",
              answers: ["N mn. mislioci, G mn. mislilaca", "N mn. mislioci, G mn. mislioca", "N mn. mislilaci, G mn. mislilaca", "N mn. mislilaci, G mn. mislioca"],
              correctAnswer: "N mn. mislioci, G mn. mislilaca",
              opis: "Točan je odgovor <strong style='color:green'><em>N mn. mislioci, G mn. mislilaca</em></strong>. U sklonidbi imenica na -<em>lac</em> provodi se zamjena <em>l</em> s <em>o</em> (vokalizacija) u svim padežima osim u nominativu jednine i genitivu množine te cijela paradigama izgleda ovako:<div class='table-responsive'><table width='100%' cellspacing='0' cellpadding='0' style='border: 1px solid black'> <colgroup> <col/> <col/> <col/> <col/> <col/> <col/> <col/> <col/> </colgroup> <tbody> <tr valign='top'> <td > </td><td > <p align='center'> N </p></td><td > <p align='center'> G </p></td><td > <p align='center'> D </p></td><td > <p align='center'> A </p></td><td > <p align='center'> V </p></td><td > <p align='center'> L </p></td><td > <p align='center'> I </p></td></tr><tr valign='top'> <td > <p align='center'> jd. </p></td><td > <p align='center'> mislilac </p></td><td > <p align='center'> mislioca </p></td><td > <p align='center'> misliocu </p></td><td > <p align='center'> mislioca </p></td><td > <p align='center'> mislioče </p></td><td > <p align='center'> misliocu </p></td><td > <p align='center'> misliocem </p></td></tr><tr valign='top'> <td > <p align='center'> mn. </p></td><td > <p align='center'> mislioci </p></td><td > <p align='center'> mislilaca </p></td><td > <p align='center'> misliocima </p></td><td > <p align='center'> mislioce </p></td><td > <p align='center'> mislioci </p></td><td > <p align='center'> misliocima </p></td><td > <p align='center'> misliocima </p></td></tr></tbody> </table></div>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu je točan posvojni pridjev i umanjenica riječi <em>pijetao</em>.",
              answers: ["pijetlov, pijetlić", "pjetlov, pjetlić", "pijetlov, pjetlić", "pjetlov, pijetlić"],
              correctAnswer: "pijetlov, pjetlić",
              opis: "Točan je odgovor <strong style='color:green'><em>pijetlov, pjetlić</em></strong>. U posvojnome pridjevu odraz jata uvijek je isti kao u riječi od koje seizvodi. U umanjenicima izvedenim od imenica muškoga roda sufiksom -<em>ić</em> odraz jata se krati (<em>pijetao –</em> <em>pjetlić</em>,<em> crijep </em>–<em> crepovi</em>/<em>crjepovi</em>,<em> brijeg </em>– <em> bregovi</em>/<em>brjegovi </em>itd.).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koji je genitiv imena <em>Harry Potter</em>, <em>Oprah</em> i <em>Charlie</em> točan?",
              answers: ["Harry Pottera, Oprahe, Charlija", "Harryja Pottera, Opre, Charlieja", "Harrya Pottera, Oprah, Charlia", "Harry Pottera, Oprah, Charlieja"],
              correctAnswer: "Harryja Pottera, Opre, Charlieja",
              opis: "Točan je odgovor <strong style='color:green'><em>Harryja Pottera, Opre, Charlieja</em></strong>. Uvijek se sklanja i ime i prezime muške osobe (G <em>Raya Charlesa</em>, <em>Baracka Obame</em>, <em>Brada</em> <em>Pitta</em>; D <em> Rayu Charlesu</em>,<em> Baracku Obami</em>,<em> Bradu Pittu </em> itd.). U oblicima<em> </em>imena <em>Harry</em> i <em>Charlie</em> između osnove i nastavka umeće se <em>j</em> koje se izgovara i bilježi. Tako je kod svih imena koja u izgovoru završavaju na <em>-i</em>, <em>-io </em>i -<em>ia</em>. Pritom nije važno kako se ime koje završava glasom <em> -i </em>zapisuje. <div class='table-responsive'> <table width='100%' cellspacing='0' cellpadding='0'> <colgroup> <col /> <col /> <col /> <col /> <col /> <col /> <col /> </colgroup> <tbody> <tr valign='top'> <td  > <p align='center'> N </p></td><td > <p align='center'> G </p></td><td > <p align='center'> D </p></td><td > <p align='center'> A </p></td><td > <p align='center'> V </p></td><td > <p align='center'> L </p></td><td > <p align='center'> I </p></td></tr><tr valign='top'> <td  > <p align='center'> Toni </p></td><td > <p align='center'> Tonija </p></td><td > <p align='center'> Toniju </p></td><td > <p align='center'> Tonija </p></td><td > <p align='center'> Toni </p></td><td > <p align='center'> Toniju </p></td><td > <p align='center'> Tonijem </p></td></tr><tr valign='top'> <td  > <p align='center'> Johnny </p></td><td > <p align='center'> Johnnyja </p></td><td > <p align='center'> Johnnyju </p></td><td > <p align='center'> Johnnyja </p></td><td > <p align='center'> Johnny </p></td><td > <p align='center'> Johnnyju </p></td><td > <p align='center'> Johnnyjem </p></td></tr><tr valign='top'> <td  > <p align='center'> Eddie </p></td><td > <p align='center'> Eddieja </p></td><td > <p align='center'> Eddieju </p></td><td > <p align='center'> Eddieja </p></td><td > <p align='center'> Eddie </p></td><td > <p align='center'> Eddieju </p></td><td > <p align='center'> Eddiejem </p></td></tr><tr valign='top'> <td  > <p align='center'> Chelsea </p></td><td > <p align='center'> Chelseaja </p></td><td > <p align='center'> Chelseaju </p></td><td > <p align='center'> Chelseaja </p></td><td > <p align='center'> Chelsea </p></td><td > <p align='center'> Chelseaju </p></td><td > <p align='center'> Chelseajem </p></td></tr><tr valign='top'> <td  > <p align='center'> Atlee </p></td><td > <p align='center'> Atleeja </p></td><td > <p align='center'> Atleeju </p></td><td > <p align='center'> Attleeja </p></td><td > <p align='center'> Atlee </p></td><td > <p align='center'> Atleeju </p></td><td > <p align='center'> Atleejem </p></td></tr></tbody> </table> </div><br><p style='text-align:justify'> Ime <em>Oprah</em> sklanja se ovako jer se -<em>ah</em> u nominativu izgovara kao <em>a</em> te se u ostalim padežima zamjenjuje odgovarajućim padežnim nastavkom.</p><div class='table-responsive'> <table width='100%' cellspacing='0' cellpadding='0'> <colgroup> <col /> <col /> <col /> <col /> <col /> <col /> <col /> </colgroup> <tbody> <tr valign='top'> <td  > <p align='center'> N </p></td><td > <p align='center'> G </p></td><td > <p align='center'> D </p></td><td > <p align='center'> A </p></td><td > <p align='center'> V </p></td><td > <p align='center'> L </p></td><td > <p align='center'> I </p></td></tr><tr valign='top'> <td  >Oprah</td><td > <p align='center'> Opre </p></td><td > <p align='center'> Opri </p></td><td > <p align='center'> Opru </p></td><td > <p align='center'> Oprah </p></td><td > <p align='center'> Opri </p></td><td > <p align='center'> Oprom </p></td></tr></tbody> </table> </div>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Genitiv sintagme <em>neupotrebljiv otpadak</em> glasi:",
              answers: ["neupotrebljiva otpadka", "neupotrebljiva otpatka", "neupotrebljivog otpadka", "neupotrebljivoga otpatka"],
              correctAnswer: "neupotrebljiva otpatka",
              opis: "Točan je odgovor <strong style='color:green'><em>neupotrebljiva otpatka</em></strong>. <em>Neupotrebljiv</em> je neodređeni pridjev, pa se sklanja po imenskoj sklonidbi (G<em> neupotrebljiva</em>, DL <em>neupotrebljivu</em>, A<em> neupotrebljiv</em>, I <em> neupotrebljivim</em>). U genitivu se <em> </em>provodi i bilježi jednačenje po zvučnosti (zvučni <em>d</em> ispred bezvučnoga <em>k </em>prelazi u svoj bezvučni parnjak<em> t</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Djevojka kojoj se otac zove <em>Mario</em> je:",
              answers: ["Marijeva kćer", "Marijova kći", "Marijeva kći", "Marijeva kćerka"],
              correctAnswer: "Marijeva kći",
              opis: "Točan je odgovor <strong style='color:green'><em>Marijeva kći</em></strong>. U sklonidbi se umeće palatal <em>j</em>, a iza palatala dodaje se sufiks -    <em>ev</em>. Imenica <em>kći</em> i <em>mati</em> pripadaju posebnomu sklonidbenom tipu i sklanjaju se ovako:<div class='table-responsive'> <table width='100%' cellspacing='0' cellpadding='0'> <colgroup> <col/> <col/> <col/> <col > <col/> <col/> <col > <col/> </colgroup> <tbody> <tr valign='top'> <td > </td><td > <p align='center'> N </p></td><td > <p align='center'> G </p></td><td > <p align='center'> D </p></td><td > <p align='center'> A </p></td><td > <p align='center'> V </p></td><td > <p align='center'> L </p></td><td > <p align='center'> I </p></td></tr><tr valign='top'> <td > <p align='center'> jd. </p></td><td > <p align='center'> kći </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćer </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćeri </p></td></tr><tr valign='top'> <td > <p align='center'> mn. </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćerima </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćerima </p></td><td > <p> kćerima </p></td></tr><tr valign='top'> <td height='26'> </td><td > <p align='center'> N </p></td><td > <p align='center'> G </p></td><td > <p align='center'> D </p></td><td > <p align='center'> A </p></td><td > <p align='center'> V </p></td><td > <p align='center'> L </p></td><td > <p> I </p></td></tr><tr valign='top'> <td > <p align='center'> jd. </p></td><td > <p align='center'> mati </p></td><td > <p align='center'> matere </p></td><td > <p align='center'> materi </p></td><td > <p align='center'> mater </p></td><td > <p align='center'> mati </p></td><td > <p align='center'> materi </p></td><td > <p> materom </p></td></tr><tr valign='top'> <td > <p align='center'> mn. </p></td><td > <p align='center'> matere </p></td><td > <p align='center'> matera </p></td><td > <p align='center'> materama </p></td><td > <p align='center'> matere </p></td><td > <p align='center'> matere </p></td><td > <p align='center'> materama </p></td><td > <p> materama </p></td></tr></tbody> </table> </div><br><p style='text-align:justify'>U sklonidbi se umeće palatal <em>j</em>, a iza palatala dodaje se sufiks -    <em>ev</em>. Imenica <em>kći</em> i <em>mati</em> pripadaju posebnomu sklonidbenom tipu i sklanjaju se ovako:</p><div class='table-responsive'> <table width='100%' cellspacing='0' cellpadding='0'> <colgroup> <col/> <col/> <col/> <col > <col/> <col/> <col > <col/> </colgroup> <tbody> <tr valign='top'> <td > </td><td > <p align='center'> N </p></td><td > <p align='center'> G </p></td><td > <p align='center'> D </p></td><td > <p align='center'> A </p></td><td > <p align='center'> V </p></td><td > <p align='center'> L </p></td><td > <p align='center'> I </p></td></tr><tr valign='top'> <td > <p align='center'> jd. </p></td><td > <p align='center'> kći </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćer </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćeri </p></td></tr><tr valign='top'> <td > <p align='center'> mn. </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćerima </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćeri </p></td><td > <p align='center'> kćerima </p></td><td > <p> kćerima </p></td></tr><tr valign='top'> <td height='26'> </td><td > <p align='center'> N </p></td><td > <p align='center'> G </p></td><td > <p align='center'> D </p></td><td > <p align='center'> A </p></td><td > <p align='center'> V </p></td><td > <p align='center'> L </p></td><td > <p> I </p></td></tr><tr valign='top'> <td > <p align='center'> jd. </p></td><td > <p align='center'> mati </p></td><td > <p align='center'> matere </p></td><td > <p align='center'> materi </p></td><td > <p align='center'> mater </p></td><td > <p align='center'> mati </p></td><td > <p align='center'> materi </p></td><td > <p> materom </p></td></tr><tr valign='top'> <td > <p align='center'> mn. </p></td><td > <p align='center'> matere </p></td><td > <p align='center'> matera </p></td><td > <p align='center'> materama </p></td><td > <p align='center'> matere </p></td><td > <p align='center'> matere </p></td><td > <p align='center'> materama </p></td><td > <p> materama </p></td></tr></tbody> </table> </div>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu je točan genitiv od <em>natrijev klorid</em>, <em>Borovo Selo</em> i <em>njihov uspjeh</em>.",
              answers: ["natrijevoga klorida, Borovoga Sela, njihovoga uspjeha", "natrijevog klorida, Borovog Sela, njihova uspjeh", "natrijeva klorida, Borova Sela, njihova uspjeha", "natrijeva klorida, Borova Sela, njihovoga uspjeha"],
              correctAnswer: "natrijeva klorida, Borova Sela, njihova uspjeha",
              opis: "Točan je odgovor <strong style='color:green'><em>natrijeva klorida, Borova Sela, njihova uspjeha</em></strong>. Svi se pridjevi na -<em>ov</em>, -<em>ev</em> i -<em>in</em> sklanjaju po imenskoj sklonidbi bez obzira na to jesu li tvoreni od imena ili apelativa. Obrnuto od toga, svi se odnosni pridjevi, tj. pridjevi koji završavaju na - <em>ski</em>, -<em>ni</em>, -<em>ji</em> itd. sklanjaju po pridjevnoj sklonidbi. Ovako izgleda sklonidba posvojnoga i odnosnoga pridjeva u jednini:<div class='table-responsive'> <table width='100%' cellspacing='0' cellpadding='0'> <colgroup> <col > <col/> <col/> <col/> <col/> <col/> <col/> </colgroup> <tbody> <tr valign='top'> <td > <p align='center'> N </p></td><td > <p align='center'> G </p></td><td> <p align='center'> D </p></td><td > <p align='center'> A </p></td><td > <p align='center'> V </p></td><td > <p align='center'> L </p></td><td > <p align='center'> I </p></td></tr><tr valign='top'> <td > <p align='center'> bratov </p></td><td > <p align='center'> bratova </p></td><td> <p align='center'> bratovu </p></td><td > <p align='center'> bratova </p></td><td > </td><td > <p align='center'> bratovu </p></td><td > <p align='center'> bratovim </p></td></tr><tr valign='top'> <td > <p align='center'> božićni </p></td><td > <p align='center'> božićnoga </p></td><td> <p align='center'> božićnomu </p></td><td > <p align='center'> božićni </p></td><td > <p align='center'> božićni </p></td><td > <p align='center'> božićnomu </p></td><td > <p align='center'> božićnim </p></td></tr></tbody> </table></div>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu nema pogreške.",
              answers: ["Nemoj brinuti, sve će biti u redu.", "Dugo je kolebao što da učini.", "Cijeloga je dana odmarao.", "Skija od pete godine."],
              correctAnswer: "Skija od pete godine.",
              opis: "Točan je odgovor <strong style='color:green'><em>Skija od pete godine</em></strong>. Glagoli <em>brinuti se</em>, <em>kolebati</em> <em>se </em>i <em> odmarati se </em>uvijek su povratni, tj. uz njih uvijek stoji <em> se</em>. Točne bi <em> </em>rečenice bile: <em>Nemoj se brinuti, sve će biti u redu</em>.,<em> Dugo se kolebao što da</em> <em>učini</em>., <em> Cijeloga se dana odmarao</em>. Glagoli koji znače ‘znati što, baviti se<em> </em>čime’, u ovome zadatku ‘znati se kretati skijama po snijegu, baviti se skijanjem’ (isto i <em>klizati</em>, <em>kartati</em> itd.) u tome su značenju nepovratni glagoli, pa odgovor na pitanje <em>Kojim se sportovima baviš?</em> može biti <em>Skijam i kližem</em>. Povratni su u značenju ‘kretati se skijama po snijegu’,<em> </em>‘kretati se klizaljkama po ledu’, ‘igrati se kartama’, pa odgovor na pitanje <em>Što radiš?</em> glasi: <em>Skijam se</em>., <em>Kližem se.</em>, <em>Kartam se.</em>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu je točna množina sintagme <em>zadarski put</em> i <em>Marijin predak</em>.",
              answers: ["zadarski putovi, Marijini pretci", "zadarski putevi, Marijini preci", "Zadarski  putovi, Marijini predci", "zadarski  putovi, Marijini predci"],
              correctAnswer: "zadarski  putovi, Marijini predci",
              opis: "Točan je odgovor <strong style='color:green'><em>zadarski  putovi, Marijini predci</em></strong>. Ktetici (pridjevi izvedeni od zemljopisnih imena) pišu se malim slovom. Imenica <em>put</em> ne završava na palatal, te joj se u instrumentalu jednine dodaje nastavak -<em>om</em> (<em>putom</em>), a u množini -<em>ovi</em> u nominativu (<em>putovi</em>), -<em>ova</em> u genitivu (<em>putova</em>), -<em>ovima</em> u dativu, lokativu i instrumental (<em>putovima</em>), -<em>ove</em> u akuzativu (<em>putove</em>). U imenici <em>predak</em> provodi se gubljenje suglasnika <em>d </em>te se npr. nominativ množine izgovara [preci], ali se to gubljenje ne<em> </em> bilježi. U svim se imenicama (osim imenice <em>otac</em>) na -<em>tak</em>, -<em>tac</em>, -<em>dak</em> i -<em>dac</em> ispred <em>c</em> i <em>č</em> bilježi glas <em>t</em> ili <em>d</em> koji se u izgovoru gubi, a one se zapisuju ovako:<div class='table-responsive'> <table width='100%' cellspacing='0' cellpadding='0'> <colgroup> <col/> <col/> <col/> <col/> <col/> <col/> <col/> </colgroup> <tbody> <tr valign='top'> <td > </td><td > <p align='center'> N </p></td><td > <p align='center'> G </p></td><td > <p align='center'> D </p></td><td > <p align='center'> A </p></td><td > <p align='center'> L </p></td><td > <p align='center'> I </p></td></tr><tr valign='top'> <td > <p align='center'> jd. </p></td><td > <p align='center'> zadatak </p></td><td > <p align='center'> zadatka </p></td><td > <p align='center'> zadatku </p></td><td > <p align='center'> zadatak </p></td><td > <p align='center'> zadatku </p></td><td > <p align='center'> zadatkom </p></td></tr><tr valign='top'> <td > <p align='center'> mn. </p></td><td > <p align='center'> zadatci </p></td><td > <p align='center'> zadataka </p></td><td > <p align='center'> zadatcima </p></td><td > <p align='center'> zadatke </p></td><td > <p align='center'> zadatcima </p></td><td > <p align='center'> zadatcima </p></td></tr><tr valign='top'> <td > </td><td > <p align='center'> N </p></td><td > <p align='center'> G </p></td><td > <p align='center'> D </p></td><td > <p align='center'> A </p></td><td > <p align='center'> L </p></td><td > <p align='center'> I </p></td></tr><tr valign='top'> <td > <p align='center'> jd. </p></td><td > <p align='center'> napredak </p></td><td > <p align='center'> napretka </p></td><td > <p align='center'> napretku </p></td><td > <p align='center'> napredak </p></td><td > <p align='center'> napretku </p></td><td > <p align='center'> napretkom </p></td></tr><tr valign='top'> <td > <p align='center'> mn. </p></td><td > <p align='center'> napredci </p></td><td > <p align='center'> napredaka </p></td><td > <p align='center'> napredcima </p></td><td > <p align='center'> napretke </p></td><td > <p align='center'> napredcima </p></td><td > <p align='center'> napredcima </p></td></tr></tbody> </table> </div>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu su točno napisani 2. lice jednine prezenta glagola <em>sjeći</em> i promjene koje se u tome obliku provode.",
              answers: ["siječeš – palatalizacija, duljenje jata", "sječeš – palatalizacija", "sijećeš – jotacija, duljenje jata", "sjećeš – jotacija"],
              correctAnswer: "siječeš – palatalizacija, duljenje jata",
              opis: "Točan je odgovor <strong style='color:green'><em>siječeš – palatalizacija, duljenje jata</em></strong>. Iz 3. lica jednine prezenta koje glasi <em>sijeku</em> vidi se da se naosnovu <em>sijek-</em> dodaju nastavci -<em>em</em>, -<em>eš</em>, - <em>e</em>, -<em>emo</em>, -<em>ete</em>, -<em>u</em> te da nije riječ o jotaciji, nego o palatalizaciji. I domaće i strane imenske formule muških osoba sklanjaju<em> </em>se tako da se sklanja i ime i prezime. Prezime <em>Carravaggio</em> (kao i prezime <em>Boccaccio</em>) završava u pismu na -<em>io</em>, ali se <em>i</em> ne izgovara jer se prezime čita [karavađo]. Stoga se u sklonidbi toga imena ne umeće <em>j </em>(tako je i s imenima poput<em> Perugia</em>). Prezime<em> Hemingway </em>čita se<em> </em>[hemingvej], dakle završava glasom <em>j</em>, te se u sklonidbi između osnove i nastavka ne umeće <em>j</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak bez pogreške.",
              answers: ["Razgovaramo o Hemingwayu, Carravaggiu i Jo Nesbøu.", "Razgovaramo o Hemingwayju, Caravaggiju i Jou Nesbøu.", "Razgovaramo o Hemingwayu, Caravaggiou i Jou Nesbøu.", "Razgovaramo o Hemingwayu, Caravaggiu i Jou Nesbøu."],
              correctAnswer: "Razgovaramo o Hemingwayu, Caravaggiu i Jou Nesbøu.",
              opis: "Točan je odgovor <strong style='color:green'><em>Razgovaramo o Hemingwayu, Caravaggiu i Jou Nesbøu</em></strong>. I domaće i strane imenske formule muških osoba sklanjaju<em> </em>se tako da se sklanja i ime i prezime. Prezime <em>Carravaggio</em> (kao i prezime <em>Boccaccio</em>) završava u pismu na -<em>io</em>, ali se <em>i</em> ne izgovara jer se prezime čita [karavađo]. Stoga se u sklonidbi toga imena ne umeće <em>j </em>(tako je i s imenima poput<em> Perugia</em>). Prezime<em> Hemingway </em>čita se<em> </em>[hemingvej], dakle završava glasom <em>j</em>, te se u sklonidbi između osnove i nastavka ne umeće <em>j</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu je pogreška.",
              answers: ["Oslabili su njihov otpor.", "Njihov je otpor oslabio.", "Oživjeli su ideju zajedništva.", "Ideja zajedništva je oživjela."],
              correctAnswer: "Oživjeli su ideju zajedništva.",
              opis: "Točan je odgovor <strong style='color:green'><em>Oživjeli su ideju zajedništva.</em></strong> Ta bi rečenica trebala glasiti <em>Oživili su ideju zajedništva.</em> Kad se značenje glagola na -<em>iti</em> i glagola na -<em>jeti</em> nerazlikuje (npr. <em>visiti</em> i <em>visjeti</em>, <em>voliti</em> i <em>voljeti</em>), prednost se u hrvatskome standardnom jeziku daje glagoluna -<em>jeti</em>. U primjerima u zadatku razlikuju se glagol <em>oslabiti</em> ‘učiniti slabim’ i <em>oslabjeti</em> ‘postati slabim’ te <em>oživiti</em> ‘učiniti živim’ i <em>oživjeti</em> ‘postati živim’. U muškome rodu jednine ne razlikuju se glagolski pridjevi radni glagola na - <em>iti</em> i -<em>jeti</em> tvorenih od iste osnove (od <em>oslabiti</em> i <em>oslabjeti </em>oni glase: <em> oslabio</em>, od<em> oživiti </em>i <em> oživjeti </em>oni glase: <em> oživio</em>). U<em> </em>ostalim se licima u oba broja razlikuju. <div class='table-responsive'><table width='100%' cellspacing='0' cellpadding='0'> <colgroup> <col/> <col/> <col/> <col/> <col/> <col/> <col/> </colgroup> <tbody> <tr valign='top'><td colspan='7'> <p> glagolski pridjev radni </p></td></tr><tr valign='top'> <td > </td><td colspan='3'> <p align='center'> jednina </p></td><td colspan='3'> <p align='center'> množina </p></td></tr><tr valign='top'> <td > </td><td > <p align='center'> m. r. </p></td><td > <p align='center'> ž. r. </p></td><td > <p align='center'> s. r. </p></td><td > <p align='center'> m.r. </p></td><td > <p align='center'> ž. r. </p></td><td > <p align='center'> s. r. </p></td></tr><tr valign='top'> <td > <p align='center'> oslabiti </p></td><td > <p align='center'> oslabio </p></td><td > <p align='center'> oslabila </p></td><td > <p align='center'> oslabilo </p></td><td > <p align='center'> oslabili </p></td><td > <p align='center'> oslabile </p></td><td > <p align='center'> oslabila </p></td></tr><tr valign='top'> <td > <p align='center'> oslabjeti </p></td><td > <p align='center'> oslabio </p></td><td > <p align='center'> oslabjela </p></td><td > <p align='center'> oslabjelo </p></td><td > <p align='center'> oslabjeli </p></td><td > <p align='center'> oslabjele </p></td><td > <p align='center'> oslabjela </p></td></tr><tr valign='top'> <td > <p align='center'> oživiti </p></td><td > <p align='center'> oživio </p></td><td > <p align='center'> oživila </p></td><td > <p align='center'> oživilo </p></td><td > <p align='center'> oživili </p></td><td > <p align='center'> oživile </p></td><td > <p align='center'> oživila </p></td></tr><tr valign='top'> <td > <p align='center'> oživjeti </p></td><td > <p align='center'> oživio </p></td><td > <p align='center'> oživjela </p></td><td > <p align='center'> oživjelo </p></td><td > <p align='center'> oživjeli </p></td><td > <p align='center'> oživjele </p></td><td > <p align='center'> oživjela </p></td></tr></tbody> </table> </div>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu je pogreška.",
              answers: ["Ako ćeš ići u grad, kupi nam sladoled.", "Ako ideš u grad, kupi nam sladoled.", "Budeš li išao u grad, kupi nam sladoled.", "Ako budeš išao u grad, kupi nam sladoled."],
              correctAnswer: "Ako ćeš ići u grad, kupi nam sladoled.",
              opis: "Točan je odgovor <strong style='color:green'><em>Ako ćeš ići u grad, kupi nam sladoled</em></strong>. U toj se rečenici nalaze dvije buduće radnje od kojih je jedna preduvjet ostvarivanju druge. Tu radnju treba izraziti predikatom u futuru II. (<em>Ako budeš išao</em> <em>u grad, kupi nam sladoled.</em>). Ona se može izraziti i prezentom, kao u primjeru <em>Ako ideš u grad, kupi nam sladoled.</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Imenica <em>bijenale</em> je:",
              answers: ["samo muškoga roda u jednini, a muškoga i srednjega u množini", "samo srednjega roda u jednini, a muškoga u množini", "samo muškoga roda i u jednini i u množini", "muškoga i srednjega roda i u jednini i u množini"],
              correctAnswer: "muškoga i srednjega roda i u jednini i u množini",
              opis: "Točan je odgovor <strong style='color:green'><em>muškoga i srednjega roda i u jednini i u množini</em></strong>. Točno je i <em>Bijenale je počeo</em>. i <em>Bijenale je počelo.</em> te <em>Bijenali su počeli.</em> i <em>Bijenala su počela.</em> Takva je i imenica<em> finale.</em>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu su svi oblici točni oblici svršenih glagola.",
              answers: ["izvežen, donesen, izvođen", "izvezen, donesen, izveden", "izvožen, donošen, izvođen", "izvezen, donešen, izveden"],
              correctAnswer: "izvezen, donesen, izveden",
              opis: "Točan je odgovor <strong style='color:green'><em>izvezen, donesen, izveden</em></strong>. Oblici <em>izvežen</em> i <em>donešen</em> ne pripadaju standardnomu jeziku, a oblici <em>izvođen</em>, <em>izvožen</em> i <em>donošen</em> glagolski su pridjevi trpni nevršenih glagola <em>izvoditi</em>, <em>izvoziti</em> i <em>donositi</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu je točan genitiv jednine imenice <em>kabel</em> i <em>filtar</em>.",
              answers: ["kabela, filtera", "kabla, filtra", "kabela, filtra", "kabla, filtera"],
              correctAnswer: "kabela, filtra",
              opis: "Točan je odgovor <strong style='color:green'><em>kabela, filtra</em></strong>. Glasovna promjena nepostojani <em>e</em> provodi se samo u nekim kajkavskim prezimenima i toponimima, pa genitiv jednine imenica <em>kabel</em> glasi <em>kabela</em> (množina <em>kabeli</em>, umanjenica <em>kabelić</em>). U riječi <em> filtar </em>provodi se glasovna promjena nepostojani <em> a</em>, pa<em> </em>genitiv glasi <em>filtra</em>. U ostalim se primjerima nalaze oblici glagola<em> zasuti </em>(<em>zaspem, zaspeš, zaspe, zaspemo, zaspete, zaspu</em>) umjesto oblika glagola<em> zaspati </em>(<em>zaspim</em>, <em>zaspiš</em>, <em>zaspi</em>, <em>zaspimo</em>, <em>zaspite</em>, <em>zaspe</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak s točnim komparativima pridjeva <em>gorak</em>, <em>zao</em>.",
              answers: ["gorkiji, zliji", "gorči, gori", "gorči, zliji", "gorkiji, gori"],
              correctAnswer: "gorči, gori",
              opis: "Točan je odgovor <strong style='color:green'><em>gorči, gori</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Točna je rečenica:",
              answers: ["Kad večeras zaspem, sanjat ću nešto lijepo.", "Kad djeca zaspu, gledat ćemo film.", "Kad zaspemo rupu pijeskom, možeš se opet voziti biciklom.", "Kad zaspeš, i ja ću leći."],
              correctAnswer: "Kad zaspemo rupu pijeskom, možeš se opet voziti biciklom.",
              opis: "Točan je odgovor <strong style='color:green'><em>Kad zaspemo rupu pijeskom, možeš se opet voziti biciklom</em></strong>. U ostalim se primjerima nalaze oblici glagola<em> zasuti </em>(<em>zaspem, zaspeš, zaspe, zaspemo, zaspete, zaspu</em>) umjesto oblika glagola<em> zaspati </em>(<em>zaspim</em>, <em>zaspiš</em>, <em>zaspi</em>, <em>zaspimo</em>, <em>zaspite</em>, <em>zaspe</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Bez prijedloga mogu ili moraju biti ovi oblici zamjenica:",
              answers: ["ga, nj, nas", "njega, nj, ga", "njega, ga, nas", "nj, njega, vas"],
              correctAnswer: "njega, ga, nas",
              opis: "Točan je odgovor <strong style='color:green'><em>njega, ga, nas</em></strong>. Akuzativ <em>nj</em> zamjenice <em>on</em> uvijek dolazi uz prijedlog.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koliko je morfema u riječi gledateljičin?",
              answers: ["tri", "četiri", "pet", "šest"],
              correctAnswer: "šest",
              opis: "Točan je odgovor <strong style='color:green'><em>šest</em></strong>. Ta se riječ ovako rastavlja na morfeme: <em>gled-a-telj-ič-in-Ø</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Supletivni su alomorfi:",
              answers: ["<em>plov</em>, <em>pliv</em> i <em>plav</em> u riječima <em>ploviti</em>, <em>plivač</em> i <em>splav</em>", "<em>čovjek</em> i <em>ljud</em> u riječima <em>čovjek</em> i <em>ljudi</em>", "Ø i <em>i</em> u riječima <em>laž</em> i <em>laži</em>", "<em>ač</em> i <em>ačica</em> u riječima <em>boksač</em> i <em>boksačica</em>"],
              correctAnswer: "<em>čovjek</em> i <em>ljud</em> u riječima <em>čovjek</em> i <em>ljudi</em>",
              opis: "Točan je odgovor <strong style='color:green'><em>čovjek i ljud u riječima čovjek i ljudi</em></strong>. Supletivni su alomorfi potpuno različiti izrazi istoga morfema. <em>Plov, pliv</em> i <em>plav</em> djelomično su različiti alomorfi. U riječima <em>laž</em> i <em>laži</em> riječ je o različitim afiksalnim morfemima – nastavcima",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koje dvije kategorije mogu imati i imenske i glagolske riječi?",
              answers: ["padež i broj", "lice i broj", "vid i vrijeme", "način i rod"],
              correctAnswer: "lice i broj",
              opis: "Točan je odgovor <strong style='color:green'><em>lice i broj</em></strong>. Morfološke kategorije koje mogu imati imenske riječi (imenice, zamjenice, pridjevi i brojevi) kategorija su roda, kategorija broja, kategorija padeža i kategorija lica (imaju je zamjenice). Morfološke kategorije glagolskih riječi kategorija su lica, kategorija broja, kategorija vremena, kategorija vida i kategorija načina.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu je retku točno označen rod zbirnim imenicama?",
              answers: ["šiblje – srednji rod, momčad – ženski rod", "šiblje – ženski rod, momčad – ženski rod", "šiblje – srednji rod, momčad – muški rod", "šiblje – ženski rod, momčad – muški rod"],
              correctAnswer: "šiblje – srednji rod, momčad – ženski rod",
              opis: "Točan je odgovor <strong style='color:green'><em>šiblje – srednji rod, momčad – ženski rod</em></strong>. Zbirne imenice nisu imenice u množini, nego imenice u jednini.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "<em>Pluralia tantum</em> su:",
              answers: ["brašno, mlijeko, sol, kava", "lišće, grmlje, perje", "grablje, hlače, škare, ljestve", "zvjerad, gamad"],
              correctAnswer: "grablje, hlače, škare, ljestve",
              opis: "Točan je odgovor <strong style='color:green'><em>grablje, hlače, škare, ljestve</em></strong>. <em>Pluralia tantum</em> imenice su koje imaju samo množinski oblik te njime označuju i jedan predmet i više njih.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Sibilarizacija se provodi:",
              answers: ["u etnicima koji označuju žene", "u imenima", "u prezimenima", "u množini imenica muškoga roda na -<em>k</em>, -<em>g</em>, -<em>h</em>"],
              correctAnswer: "u množini imenica muškoga roda na -<em>k</em>, -<em>g</em>, -<em>h</em>",
              opis: "Točan je odgovor <strong style='color:green'><em>u množini imenica muškoga roda na -k, -g, -h</em></strong>. U etnicima koji označuju žene (<em>Splićanka</em>, <em>Zagrepčanka</em>, <em>Podravka</em>), imenima (<em>Branka</em>, <em>Anka</em>) i prezimenima (<em>Truhelka</em>) sibilarizacija se ne provodi.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi pogrešnu rečenicu.",
              answers: ["S objema prijateljicama lijepo sam se slagao.", "Dao sam knjigu dvama prijateljima.", "Našao se između dvojih vrata.", "Razgovarali su predstavnici dvaju država."],
              correctAnswer: "Razgovarali su predstavnici dvaju država.",
              opis: "Točan je odgovor <strong style='color:green'><em>Razgovarali su predstavnici dvaju država</em></strong>. <em>Dva</em> i <em>dvije </em>dekliniraju se ovako: <div class='table-responsive'><table width='100%' cellspacing='0' cellpadding='0'> <colgroup> <col/> <col/> <col/> <col/> <col/> <col/> <col/> <col/> </colgroup> <tbody> <tr valign='top'> <td > </td><td > <p align='center'> N </p></td><td > <p align='center'> G </p></td><td > <p align='center'> D </p></td><td > <p align='center'> A </p></td><td > <p align='center'> V </p></td><td > <p align='center'> L </p></td><td > <p align='center'> I </p></td></tr><tr valign='top'> <td > <p> m. i s. rod </p></td><td > <p align='center'> dva </p></td><td > <p align='center'> dvaju </p></td><td > <p align='center'> dvama </p></td><td > <p align='center'> dva </p></td><td > <p align='center'> dva </p></td><td > <p align='center'> dvama </p></td><td > <p align='center'> dvama </p></td></tr><tr valign='top'> <td > <p> ž. rod </p></td><td > <p align='center'> dvije </p></td><td > <p align='center'> dviju </p></td><td > <p align='center'> dvjema </p></td><td > <p align='center'> dvije </p></td><td > <p align='center'> dvije </p></td><td > <p align='center'> dvjema </p></td><td > <p align='center'> dvjema </p></td></tr></tbody></table></div>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi rečenicu u kojoj se nalazi jezična pogreška:",
              answers: ["Crvenila je usne crvenim ružem.", "Rumenila se od stida.", "Problijedjela je od straha.", "Trava se zelenjela."],
              correctAnswer: "Rumenila se od stida.",
              opis: "Točan je odgovor <strong style='color:green'><em>Rumenila se od stida</em></strong>. <em>Rumeniti</em> znači ‘činiti rumenim’, <em>crveniti </em>‘činiti crvenim’,<em> problijedjeti </em>‘postati blijedim’,<em> zelenjeti se </em>‘postati <em> </em>zelenim ili isticati se zelenom bojom’. Razlika između glagola na -<em>iti</em> (npr. <em>zeleniti</em>, <em>plaviti</em>, <em>slabiti</em>) i glagola na -<em>jeti</em> (npr. <em>zelenjeti</em>, <em>plavjeti</em>, <em>slabjeti</em>) tvorenih od iste osnove jest u tome da prvi znače ‘činiti kakvim (zelenim, plavim, slabim)’, a drugi ‘postajati kakvim (zelenim, plavim, slabi)’ ili (kad su povratni) ‘isticati se kakvom bojom (zelenom, plavom)’.<div class='table-responsive'> <table width='100%' cellspacing='0' cellpadding='0'><colgroup> <col/> <col/> </colgroup><tbody><tr valign='top'><td><p align='center'>glagoli na -<em>iti</em> (<em>zeleniti</em>, <em>plaviti</em>, <em>slabiti</em>&hellip;)</p></td><td><p align='center'>&lsquo;činiti &scaron;to kakvim (zelenim, plavim, slabim&hellip;)&rsquo;</p></td></tr><tr valign='top'><td><p align='center'>glagoli na -<em>jeti</em> (<em>zelenjeti</em>, <em>plavjeti</em>, <em>slabjeti</em> &hellip;)</p></td><td><p align='center'>&lsquo;postajati kakvim (zelenim, plavim, slabim&hellip;) ili isticati se kakvom bojom (zelenom, plavom&hellip;)&rsquo;</p></td></tr></tbody></table> </div><br><p align='justify'>Glagolski pridjev radni od glagola na -<em>iti</em> i glagola na - <em>jeti</em> tvorenih od iste osnove u muškome rodu jednine glasi jednako, npr. <em>zelenio</em>, <em>plavio</em>, <em>slabio</em>…, a razlikuju se glagolski pridjevi u ostalim rodovima u oba broja<em> </em>(npr. <em>zelenila, zelenilo, zelenili, zelenile</em> i <em>zelenila</em> od glagola <em>zeleniti</em> te <em>zelenjela</em>,<em> zelenjelo</em>,<em> zelenjeli</em>, <em> zelenjele</em>, <em>zelenjela </em>od glagola <em> zelenjeti</em>). </p>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Oblik <em>uzet</em> je:",
              answers: ["glagolski pridjev radni", "glagolski pridjev trpni", "glagolski prilog sadašnji", "glagolski prilog prošli"],
              correctAnswer: "glagolski pridjev trpni",
              opis: "Točan je odgovor <strong style='color:green'><em>glagolski pridjev trpni</em></strong>. Glagolski pridjev radni od glagola <em>uzeti</em> u muškome rodu jednine glasi <em>uzeo</em>, a glagolski prilog prošli <em>uzevši</em>. Glagolski prilog sadašnji ne može se tvoriti od svršenoga glagola (može se tvoriti od nesvršenoga, npr. od glagola <em>uzimati</em> glagolski prilog sadašnji glasi <em>uzimajući</em>)<em>.</em>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi rečenicu u kojoj nema nijednoga pridjeva.",
              answers: ["Djevojčice lijepo i veselo pjevaju dok nose svoju tešku torbu.", "Moj auto brzo juri po širokoj cesti.", "Tvoj je sin veoma inteligentan i lako rješava složene zadatke.", "Dječak Marijan voli lijepo pjevati i često svira violinu."],
              correctAnswer: "Dječak Marijan voli lijepo pjevati i često svira violinu.",
              opis: "Točan je odgovor <strong style='color:green'><em>Dječak Marijan voli lijepo pjevati i često svira violinu</em></strong>. U svim se ostalim rečenicama uz priloge (<em>lijepo</em>,<em> veselo</em>,<em> brzo</em>,<em> lako</em>) nalaze i pridjevi (<em>tešku</em>,    <em> širokoj</em>,<em> složene</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi rečenicu u kojoj je pogreška.",
              answers: ["Imam prijatelja kakvoga sam oduvijek želio.", "To je onaj zakon kojega su jučer donijeli.", "Upoznat ću te s momkom kojega sam jučer upoznala.", "Mi smo razred kakvoga nema."],
              correctAnswer: "To je onaj zakon kojega su jučer donijeli.",
              opis: "Točan je odgovor <strong style='color:green'><em>To je onaj zakon kojega su jučer donijeli</em></strong>.  Akuzativ pridjeva i zamjenice koje se sklanjaju po pridjevnoj sklonidbi za neživo jednak je nominativu, a za živo genitivu. S obzirom na to da imenica <em>zakon </em>označuje neživo, rečenica bi trebala glasiti: <em> To je onaj zakon koji su jučer donijeli.</em>",
              boja_pozadine: "#FCE4EC"
          }]

      } else if (jedna == 4) {
          $('#bootstrapForm').attr('action', 'https://docs.google.com/forms/d/e/1FAIpQLSd7DMzeT15-RgggVQpkfHtO3mguuknYBy-ApfUx-vVwTdNbcA/formResponse')

          quiz = [{
              question: "Donja je granica rečenice:",
              answers: ["riječ", "slovo", "glas", "morfem"],
              correctAnswer: "riječ",
              opis: "Točan je odgovor <strong style='color:green'><em>riječ</em></strong>. Rečenica je sintaktička jedinica kojoj je donja granica riječ, a gornja diskurs. Diskurs je veća jezična cjelina koju čine rečenice.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja vrsta rečenica ne pripada podjeli rečenica po priopćajnoj svrsi?",
              answers: ["izjavne", "upitne", "usklične", "eliptične"],
              correctAnswer: "eliptične",
              opis: "Točan je odgovor <strong style='color:green'><em>eliptične</em></strong>. Po priopćajnoj svrsi rečenice se dijele na izjavne (izrične), upitne i usklične. Eliptične su rečenice kojima nedostaje koji dio, npr. <em>Uskočio u automobil i pobjegao.</em> (nedostaje <em>je</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U sintagmi moja <em>Ines</em> riječi se nalaze u odnosu:",
              answers: ["sročnosti", "jakoga upravljanja", "slaboga upravljanja", "pridruživanja"],
              correctAnswer: "sročnosti",
              opis: "Točan je odgovor <strong style='color:green'><em>sročnosti</em></strong>. Riječi u sintagmi nalaze se u odnosu sročnosti kad se podudaraju u rodu, broju i padežu (<em>lijepa djevojka</em>) ili samo u nekim od tih kategorija, npr. u padežu (<em>u gradu Vinkovcima</em>, <em>na rijeci Dunavu</em>). U sintagmi <em>moja Ines </em> glavna je sastavnica sintagme<em> </em>(odredbenica) nesklonjiva riječ <em>Ines</em>, a zavisna sastavnica sintagme zamjenica <em>moja</em>. Pri upravljanju glavna sastavnica određuje oblik zavisne sastavnice. Pri jakomu upravljanju zavisna sastavnica mora biti u točno određenome obliku (npr. u sintagmi <em>čitati knjigu</em> zavisna sastavnica mora biti u akuzativu jer takvu dopunu zahtijeva glagol <em>čitati</em>). Pri slabome upravljanju zavisna sastavnica može biti u različitim oblicima (npr. <em>lutati šumom</em>, <em>lutati po šumi</em>, <em>lutati kroz šumu</em>). Pri pridruživanju zavisni je dio nepromjenjiva riječ (prilog) ili prijedložni izraz, npr.<em>sjediti mirno</em>,<em> sjediti slušajući</em>, <em> sjediti na stolu</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojoj je rečenici imenski predikat?",
              answers: ["Prije odlaska na more posjetio je mlađega sina.", "Prije dolaska na more bila je loše volje.", "On je dosad pokazao samo najbolje namjere.", "Uvijek je namjeravao činiti samo dobro."],
              correctAnswer: "Prije dolaska na more bila je loše volje.",
              opis: "Točan je odgovor <strong style='color:green'><em>Prije dolaska na more bila je loše volje.</em></strong> U ostalim rečenicama predikat je glagolski (<em>posjetio je</em>, <em>je pokazao</em>, <em>namjeravao činiti</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je tvrdnja točna?",
              answers: ["Glagolski su predikati uvijek složeni.", "Imenski su predikati uvijek složeni.", "Apozicija se s imenicom uz koju stoji uvijek slaže u rodu, broju i padežu.", "Apozicija se s imenicom uz koju stoji uvijek slaže u broju i padežu."],
              correctAnswer: "Imenski su predikati uvijek složeni.",
              opis: "Točan je odgovor <strong style='color:green'><em>Imenski su predikati uvijek složeni.</em></strong> Imenski se predikat sastoji od kopule (najčešće glagola <em>biti</em>) i imenske riječi, te je zbog toga nužno složen.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U rečenici <em>Nasmijao me je do suza.</em> priložna je oznaka:",
              answers: ["količine", "uzroka", "posljedice", "načina"],
              correctAnswer: "posljedice",
              opis: "Točan je odgovor <strong style='color:green'><em>posljedice</em></strong>. Ona označuje posljedicu radnje označene predikatom i odgovara na pitanje <em>S kojom posljedicom</em>?",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U rečenici <em><u>Na rubu snaga</u></em> <em>držao sam</em> <em><u>predavanje</u></em><em>.</em> podcrtani su dijelovi:",
              answers: ["priložna oznaka uzroka i bliži objekt", "priložna oznaka načina i dalji objekt", "priložna oznaka uzroka i dalji objekt", "priložna oznaka načina i bliži objekt"],
              correctAnswer: "priložna oznaka načina i bliži objekt",
              opis: "Točan je odgovor <strong style='color:green'><em>priložna oznaka načina i bliži objekt</em></strong>. Priložna oznaka načina označuje način na koji se odvija radnja označena predikatom i odgovara na pitanje <em>Kako?</em> Uz prijelazne glagole stoje objekti u neprijedložnome akuzativu ili u neprijedložnome genitivu koji se može zamijeniti neprijedložnim akuzativom i takvi se objekti nazivaju bližim ili izravnim objektima.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Točno je:",
              answers: ["čizme iz kože", "profesorica iz matematike", "između svih država", "između sestre i brata"],
              correctAnswer: "između sestre i brata",
              opis: "Točan je odgovor <strong style='color:green'><em>između sestre i brata</em></strong>. Prijedlog <em>između</em> upotrebljava se u standardnome jeziku kao veza između dvaju predmeta. Ostali bi primjeri trebali glasiti: <em>čizme od kože</em> jer prijedlog <em>iz</em> ne treba upotrebljavati kako bi se označila građa od koje je što napravljeno, <em>profesorica matematike </em>te<em> među svim državama </em>jer se prijedlog<em> među </em>upotrebljava kao veza u odnosu više predmeta.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pogrešno je:",
              answers: ["Telad je ušla u nastambu.", "Kolege su došle na ispit.", "Dvojica su došli, a četvorica nisu.", "Te su me varalice prevarili."],
              correctAnswer: "Dvojica su došli, a četvorica nisu.",
              opis: "Točan je odgovor <strong style='color:green'><em>Dvojica su došli, a četvorica nisu.</em></strong> Ta bi rečenica trebala glasiti <em>Dvojica su došla, a četvorica nisu.</em> Brojevne imenice <em>obojica, dvojica,</em> <em>trojica </em>itd. slažu se s predikatom kao imenice srednjega roda u množini.<em> </em>Zbirne imenice poput <em>telad</em> slažu se s imenicama i kao imenice ženskoga roda u jednini (<em>Telad je ušla u nastambu.</em>) i kao imenice srednjega roda u množini (<em>Telad su ušla u nastambu</em>.). U prvome je slučaju riječ o gramatičkoj, a u drugome o semantičkoj sročnosti. Imenice<em> kolega, sluga,</em> <em>vojvoda </em>u jednini su muškoga (<em>Kolega je došao na ispit.</em>), a u množini i <em> </em>muškoga i ženskoga roda (<em>Kolege su došli na ispit.</em> i <em>Kolege su došle na ispit.</em>). Imenice <em>varalica</em>, <em>kukavica</em> u oba su broja i muškoga i ženskoga roda kad označuju mušku osobu (<em>Varalica me je prevario.</em>, <em>Varalica me je prevarila</em>. i <em>Varalice su me prevarili</em>., <em>Varalice su me prevarile.</em>), a ženskoga roda kad označuju žensku osobu (<em>Varalica me je prevarila</em>. i <em>Varalice su me prevarile.</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Rečenica <em>Mnogo uče ne bi li postigli što bolje rezultate.</em> je:",
              answers: ["posljedična", "uzročna", "dopusna", "namjerna"],
              correctAnswer: "namjerna",
              opis: "Točan je odgovor <strong style='color:green'><em>namjerna</em></strong>. Zavisna je surečenica <em>ne bi li postigli što</em><em>bolje rezultate </em>uvrštena na mjesto priložne oznake namjere: <em> Mnogo uče radi postizanja što boljih rezultata</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Točno je:",
              answers: ["smetati majci", "kontaktirati Marka", "lagati Anu", "pisati s olovkom"],
              correctAnswer: "smetati majci",
              opis: "Točan je odgovor <strong style='color:green'><em>smetati majci</em></strong>. Glagoli <em>smetati</em> i <em>lagati</em> u hrvatskome standardnom jeziku imaju dopunu u dativu, a ne u genitivu (pa je točno <em>lagati Ani</em>). Glagol <em>kontaktirati</em> u hrvatskome standardnom jeziku ima dopunu u instrumentalu i značenje ‘biti s kim u kontaktu’, a ne značenje ‘javiti se komu’ kao u navedenome primjeru, pa sintagma <em>kontaktirati Marka </em>ne pripada standardnomu jeziku. Instrumental je sredstva neprijedložni te standardnomu jeziku pripada sintagma <em>pisati olovkom</em>, a ne pripada mu sintagma<em> pisati s olovkom</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Rečenica <em>Ako pita za mene, reci mu da sam dobro.</em> primjer je:",
              answers: ["stvarne pogodbene rečenice", "moguće pogodbene rečenice", "stvarne dopusne rečenice", "moguće dopusne rečenice"],
              correctAnswer: "stvarne pogodbene rečenice",
              opis: "Točan je odgovor <strong style='color:green'><em>stvarne pogodbene rečenice</em></strong>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja rečenica odgovara na pitanje <em>Gdje</em>?",
              answers: ["Krenuo sam na trg.", "Stanujem u neboderu.", "Hodam uz rijeku.", "Doskakutao je do majke."],
              correctAnswer: "Stanujem u neboderu.",
              opis: "Točan je odgovor <strong style='color:green'><em>Stanujem u neboderu.</em></strong> <em>Gdje</em> uvodi pitanje o mjestu, <em>kamo </em>o cilju kretanja (<em>Krenuo sam na trg., Doskakutao je do majke.</em>), a<em> kuda </em>o smjeru kretanja, tj. putu po kojemu se kreće (<em>Hodam uz rijeku.</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Prijedlog <em>po</em> dolazi uz:",
              answers: ["genitiv i lokativ", "akuzativ i lokativ", "akuzativ i instrumental", "genitiv, akuzativ i lokativ"],
              correctAnswer: "akuzativ i lokativ",
              opis: "Točan je odgovor <strong style='color:green'><em>akuzativ i lokativ,  npr. otići po kćer, hodati po šumi</em></strong>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Što je riječ <em>medija</em> u rečenici <em>Pismenost medija sve je bolja.</em>?",
              answers: ["objekt", "subjekt", "atribut", "apozicija"],
              correctAnswer: "atribut",
              opis: "Točan je odgovor <strong style='color:green'><em>atribut</em></strong>. U navedenome je primjeru riječ o imenskome atributu u genitivu te o sintagmi koja se ne može preoblikovati u <em>medijska pismenost</em> jer se značenja sintagmi <em>pismenost</em> <em>medija </em>‘pismenost u medijima’ i <em> medijska pismenost </em>‘pismenost za<em> </em>medije’ razlikuju.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Rečenica <em>Kako bih je bolje upoznao, pozvao sam je na izlet i pitao želi li da idemo autobusom ili vlakom.</em> ovoga je ustroja:",
              answers: ["namjerna surečenica – glavna surečenica – sastavna surečenica – objektna surečenica – objektna surečenica", "glavna surečenica – namjerna surečenica – sastavna surečenica – objektna surečenica", "dopusna surečenica – glavna surečenica – sastavna surečenica – objektna surečenica – atributna surečenica", "glavna surečenica – dopusna rečenica – sastavna rečenica – atributna surečenica – atributna surečenica"],
              correctAnswer: "namjerna surečenica – glavna surečenica – sastavna surečenica – objektna surečenica – objektna surečenica",
              opis: "Točan je odgovor <strong style='color:green'><em>namjerna surečenica – glavna surečenica – sastavna surečenica – objektna surečenica – objektna surečenica</em></strong>. Navedena se rečenica<em> </em>raščlanjuje ovako: <em>Kako bih je bolje upoznao</em> (namjerna surečenica) <em>pozvao sam je na izlet </em>(glavna surečenica)<em> i pitao </em> (sastavna surečenica)<em> želi li </em>(objektna surečenica) <em> da idemo autobusom ili vlakom </em>(objektna<em> </em>surečenica).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U rečenici <em>Ženom se ne rađa, ženom se postaje.</em> <em>ženom</em> je:",
              answers: ["atribut", "predikatni proširak", "imenski predikat", "priložna oznaka sredstva"],
              correctAnswer: "predikatni proširak",
              opis: "Točan je odgovor <strong style='color:green'><em>predikatni proširak</em></strong>. <em>Ženom</em> je u navedenoj rečenici predikatni proširak, tj. dio glagolskoga predikata. U ovome je slučaju riječ o imenskome predikatnom proširku, koji može biti i pridjev sročan sa subjektom (<em>Slušala me je smirena. Pronašli su je preplašenu.</em>). Predikatni proširak može biti i glagolski te tada ima oblik glagolskoga priloga sadašnjeg (<em>Hodao je zastajkujući.</em>) ili prošlog (<em>Priznavši, osjetio</em> <em>je olakšanje.</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koji je ustroj rečenice <em>Sadržaj članka sve nas je uznemirio.</em>?",
              answers: ["atribut – subjekt – objekt – predikat", "subjekt – atribut – objekt – predikat", "subjekt – apozicija – predikatni proširak – predikat", "subjekt – apozicija – objekt – predikat"],
              correctAnswer: "subjekt – atribut – objekt – predikat",
              opis: "Točan je odgovor <strong style='color:green'><em>subjekt – atribut – objekt – predikat</em></strong>. Rečenični su dijelovi: <em>Sadržaj</em> (S) <em>članka</em> (At) <em>sve nas</em> (O) <em>je uznemirio</em> (P). Imenski (nesročni) atribut, <em>članka</em> je kao i <em>kralj</em> <em><u>mora</u></em>, <em>fotografija</em> <em><u>u boji</u></em>, <em>losion</em> <em><u>za</u></em> <em><u>tijelo</u></em>,<em> majica </em>    <em><u>s dugim rukavima</u></em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Rečenice po sastavu dijele se na:",
              answers: ["jednostavne i složene", "nezavisnosložene i zavisnosložene", "neproširene i proširene", "vezničke i bezvezničke"],
              correctAnswer: "jednostavne i složene",
              opis: "Točan je odgovor <strong style='color:green'><em>jednostavne i složene</em></strong>. Jednostavne rečenice dijele se dalje na neproširene i proširene, a složene na nezavisnosložene i zavisnosložene. Prema tomu ostvaruje li se veza između surečenica u složenoj rečenici s pomoću veznika ili bez njega, rečenice se dijele na vezničke i bezvezničke (asindetske) rečenice.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pogrešna je rečenica:",
              answers: ["S obzirom na to da ste jeli, niste gladni.", "Niste gladni jer ste jeli.", "Niste gladni budući da ste jeli.", "Niste gladni zato što ste jeli."],
              correctAnswer: "Niste gladni budući da ste jeli.",
              opis: "Točan je odgovor <strong style='color:green'><em>Niste gladni budući da ste jeli.</em></strong> Uzročni veznik <em>budući</em> <em>da </em>upotrebljava se samo u inverziji, tj. kad je zavisna surečenica ispred<em> </em>glavne. Da ta rečenica glasi: <em>Budući da ste jeli, niste gladni</em>., bila bi točna. Osim veznika <em>budući da</em> i veznik <em>jer</em> mjesno je u rečenici ograničen te ne može stajati na početku rečenice. Tako bi pogrešna bila rečenica <em>Jer ste</em> <em>jeli, niste gladni. </em>Ostali se veznici uzročnih rečenica mogu upotrebljavati <em> </em>i u inverziji i u poretku <em>glavna surečenica – zavisna surečenica</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Rečenica <em>Kiši.</em> je:",
              answers: ["rečenica s logičkim subjektom", "besubjektna rečenica", "neoglagoljena rečenica", "rečenica s neizrečenim subjektom"],
              correctAnswer: "besubjektna rečenica",
              opis: "Točan je odgovor <strong style='color:green'><em>besubjektna rečenica</em></strong>. U ustrojstvo takvih rečenica ne može se uvrstiti subjekt. Takve su rečenice npr. i <em>Sniježi</em>., <em>Dani se</em>. Subjekta nema ni u rečenicama u kojima se izriče radnja povezana s kim protiv njegove volje: <em>Spava mi se.</em>, <em>Zijeva mi se., Sram me je.</em> Takve se rečenice zovu rečenicama s logičkim subjektom. U neoglagoljenim rečenicama predikat nije izrečen, ali se podrazumijeva, npr. u rečenicama <em>Požar!</em>,<em> Lavina!</em>. U rečenicama s neizrečenim subjektom subjekt nije<em> </em>izrečen, ali je sadržan u glagolskome obliku, npr. <em>Spavam., Ušao je.</em> ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Akuzativni su prijedlozi:",
              answers: ["k(a), nadomak, nadohvat, nasuprot", "do, nasred, uoči, unutar", "na, o, po, pri", "kroz(a), na, po, u"],
              correctAnswer: "kroz(a), na, po, u",
              opis: "Točan je odgovor <strong style='color:green'><em>kroz(a), na, po, u (glavom kroza zid, ljutit na Marka, otići po kruh, zaljubljen u Anu)</em></strong>. Prijedlozi <em> k(a), nadomak, nadohvat, nasuprot </em>dativni su, <em> do, nasred, uoči, unutar </em>genitivni,<em> na, o, po, pri </em> lokativni.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pogrešna je rečenica:",
              answers: ["Pošto se najela, otišla je spavati.", "Ne želim jesti pošto se ne osjećam dobro.", "Otići ćemo u kino pošto kupimo cipele.", "Pošto sve napravite, javite nam."],
              correctAnswer: "Ne želim jesti pošto se ne osjećam dobro.",
              opis: "Točan je odgovor <strong style='color:green'><em>Ne želim jesti pošto se ne osjećam dobro.</em></strong> U hrvatskome standardnom jeziku <em>pošto</em> je vremenski veznik, a nije uzročni. U svim se ostalim rečenicama <em>pošto</em> može zamijeniti s <em>nakon što</em>, tj. upotrijebljen je dobro – kao vremenski veznik.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je tvrdnja netočna?",
              answers: ["Ispred veznika <em>jer</em> piše se zarez.", "Veznik <em>zahvaljujući</em> uvodi samo pozitivnu posljedicu.", "S <em>obzirom na to</em> da uzročni je složeni veznik.", "Veznik <em>zato jer</em> ne pripada hrvatskomu standardnom jeziku."],
              correctAnswer: "Ispred veznika <em>jer</em> piše se zarez.",
              opis: "Točan je odgovor <strong style='color:green'><em>Ispred veznika jer piše se zarez.</em></strong> Veznik <em>jer</em> upotrebljava se samo u poretku <em>glavna</em> <em>surečenica – uzročna surečenica </em>te se ispred njega ne piše zarez (na<em> </em>temelju pravopisnoga pravila da se u zavisnosloženoj rečenici s tim poretkom surečenica ne piše zarez). Ostale su tvrdnje točne: uzročni veznik <em>zahvaljujući</em> u hrvatskome standardnom jeziku uvodi pozitivnu posljedicu (<em>Zahvaljujući našemu nastojanju, sve je dobro završilo</em>.), <em>s</em> <em>obzirom na to da </em>uzročni je složeni veznik (pogrešno je upotrebljavati <em> </em>veznike <em>obzirom da</em>, <em>s obzirom da</em> itd.), a veznik <em>zato jer</em> pleonastičan je jer udvaja uzročno značenje (izriče isto što i sam veznik <em>jer</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Netočno je:",
              answers: ["Ići ću u kino s ili bez prijatelja.", "Hoćemo li igrati košarku ispred ili iza hotela?", "Letjeli smo iznad i ispod oblaka.", "Učinit ću to usprkos ocu i unatoč tomu."],
              correctAnswer: "Ići ću u kino s ili bez prijatelja.",
              opis: "Točan je odgovor <strong style='color:green'><em>Ići ću u kino s ili bez prijatelja.</em></strong> Kad su dva prijedloga povezana veznikom, a svaki od njih zahtijeva dopunu u drugome padežu, te dopune moraju biti izrečene, npr.: <em>Ići ću u kino s</em> <em>prijateljima ili bez prijatelja/njih</em>. U ostalim primjerima veznicima su<em> </em>povezani prijedlozi koji zahtijevaju istu dopunu.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Konsekutivne rečenice su:",
              answers: ["uzročne rečenice", "načinske rečenice", "vremenske rečenice", "posljedične rečenice"],
              correctAnswer: "posljedične rečenice",
              opis: "Točan je odgovor <strong style='color:green'><em>posljedične rečenice</em></strong>. Uzročne se rečenice nazivaju i <em>kauzalne rečenice</em>, načinske rečenice i <em>finalne rečenice</em>, a vremenske rečenice i <em>temporalne rečenice</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Rečenica <em>Vidjeli smo gdje ulazi u školu.</em> je:",
              answers: ["namjerna", "mjesna", "subjektna", "objektna"],
              correctAnswer: "objektna",
              opis: "Točan je odgovor <strong style='color:green'><em>objektna</em></strong>. U toj je rečenici <em>gdje</em> veznik objektnih rečenica.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je tvrdnja točna?",
              answers: ["Svaki je objekt u akuzativu izravni.", "Izravni objekt može biti u akuzativu i u genitivu.", "Glagoli koji uvode neizravni objekt zovu se prijelazni glagoli.", "Prijedložni objekt uvijek je izravni."],
              correctAnswer: "Izravni objekt može biti u akuzativu i u genitivu.",
              opis: "Točan je odgovor <strong style='color:green'><em>Izravni objekt može biti u akuzativu i u genitivu</em></strong>. Tvrdnja <em> Svaki je objekt u akuzativu izravni.</em> nije točna jer objekt u prijedložnome akuzativu nije izravni, tvrdnja<em> Glagoli koji uvode</em> <em>neizravni objekt zovu se prijelazni glagoli. </em>nije točna jer su glagoli koji <em> </em>uvode neizravni objekt neprijelazni glagoli, a tvrdnja <em> Prijedložni</em> <em>objekt uvijek je izravni. </em>netočna je jer je prijedložni objekt uvijek<em> </em>neizravni.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja rečenica ne pripada skupini?",
              answers: ["mjesne rečenice", "namjerne rečenice", "priložne rečenice", "pogodbene rečenice"],
              correctAnswer: "priložne rečenice",
              opis: "Točan je odgovor <strong style='color:green'><em>priložne rečenice</em></strong>. Mjesne, namjerne i pogodbene rečenica vrste su priložnih rečenica.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojoj je rečenici pogreška?",
              answers: ["Ušli su bez da su pozdravili.", "Odlazim zato što imam posla.", "Pošto sam cijelu noć bio budan, zaspao sam.", "Ivan je rekao kako mu je svega dosta."],
              correctAnswer: "Ušli su bez da su pozdravili.",
              opis: "Točan je odgovor <strong style='color:green'><em>Ušli su bez da su pozdravili.</em></strong> Ustrojstva s veznikom <em>bez da </em>ne pripadaju hrvatskomu standardnom jeziku. Riječ je o<em> </em>prevedenicama s njemačkoga (prema njemačkome <em>ohne dass</em>). Točna bi rečenica glasila <em>Ušli su, a nisu pozdravili. / Ušli su a da nisu pozdravili.</em> <em>/ Ušli su i nisu pozdravili.</em>",
              boja_pozadine: "#FCE4EC"
          }, ]
      } else if (jedna == 5) {
          $('#bootstrapForm').attr('action', 'https://docs.google.com/forms/d/e/1FAIpQLSduhTSptB3isOthiB-5SzVo2y-3K7EinkI9sWv76qO0m1KhkQ/formResponse')

          quiz = [{
              question: "Pronađi redak bez pogreške.",
              answers: ["Sviđa mi se nova postava ove galerije., Zajedno ćemo u posjet mojoj sestri.", "Sviđa mi se nova postava ove galerije., Zajedno ćemo u posjetu mojoj sestri.", "Sviđa mi se novi postav ove galerije., Zajedno ćemo u posjetu mojoj sestri.", "Sviđa mi se novi postav ove galerije., Zajedno ćemo u posjet mojoj sestri."],
              correctAnswer: "Sviđa mi se novi postav ove galerije., Zajedno ćemo u posjet mojoj sestri.",
              opis: "Točan je odgovor <strong style='color:green'><em>Sviđa mi se novi postav ove galerije., Zajedno ćemo u posjet mojoj sestri.</em></strong> Hrvatskomu standardnom jeziku pripada samo riječ<em> posjet </em>(imenica muškoga roda), a ne i riječ<em> posjeta </em>(imenica ženskoga<em> </em> roda). Riječ <em>postava</em> označuje osobe u kakvoj skupini: glazbenoj, sportskoj itd., a riječ <em>postav</em> označuje izloške u muzeju, galeriji itd.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi <em>divan – krasan</em>, <em>gadan – ružan</em>, <em>kompjutor – računalo</em> nalaze su u odnosu:",
              answers: ["sinonimije", "homonimije", "antonimije", "polisemije"],
              correctAnswer: "sinonimije",
              opis: "Točan je odgovor <strong style='color:green'><em>sinonimije</em></strong>. U odnosu sinonimije nalaze se riječi različita izraza (označioca), a istoga sadržaja (označenika).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Označi par koji se nalazi u odnosu hiperonimije.",
              answers: ["mnogo – malo", "tuga – žalost", "ljepota – ružnoća", "životinja – mačka"],
              correctAnswer: "životinja – mačka",
              opis: "Točan je odgovor <strong style='color:green'><em>životinja – mačka</em></strong>. Odnos hiperonimije odnos je između nadređenoga i podređenoga pojma. Nadređeni pojam zove se hiperonim (<em>životinja</em>), a podređeni se pojmovi zovu hiponimi (<em>mačka</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi <em>outsourcing</em>, <em>mobing</em>, <em>smartphone</em> u hrvatskome su jeziku:",
              answers: ["germanizmi", "luzitanizmi", "galicizmi", "anglizmi"],
              correctAnswer: "anglizmi",
              opis: "Točan je odgovor <strong style='color:green'><em>anglizmi</em></strong>. Anglizam je riječ ili koji drugi element engleskoga podrijetla posuđen u koji drugi jezik i prilagođen njegovu jezičnomu sustavu. Germanizmi su riječi iz njemačkoga jezika, luzitanizmi iz portugalskoga, a galicizmi iz francuskoga.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi <em>destinacija</em>, <em>indikator</em> i <em>suficit</em> u hrvatskome su jeziku:",
              answers: ["žargonizmi", "historizmi", "internacionalizmi", "kolokvijalizmi"],
              correctAnswer: "internacionalizmi",
              opis: "Točan je odgovor <strong style='color:green'><em>internacionalizmi</em></strong>. Internacionalizam je riječ ili koji drugi element latinskoga ili grčkoga podrijetla posuđen u koji drugi jezik i prilagođen njegovu jezičnomu sustavu.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Odaberi redak u kojemu su normativno prihvatljivije riječi.",
              answers: ["reiki majstor, hidromasaža", "reiki-majstor, hidro-masaža", "majstor reikija, hidromasaža", "reiki majstor, hidro masaža"],
              correctAnswer: "majstor reikija, hidromasaža",
              opis: "Točan je odgovor <strong style='color:green'><em>majstor reikija, hidromasaža</em></strong>. Polusloženice nisu svojstvene hrvatskomu standardnom jeziku i treba ih, kad god je to moguće, zamijeniti svezom pridjeva i imenice ili imenice i imenice u genitivu. <em>Hidro</em>- je prefiksoid i piše se sastavljeno s osnovom.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi <em>govórnica</em> ‘mjesto s kojega se drži govor’ i <em>gòvōrnica</em> ‘žena koja govori ili drži govor’ primjer su:",
              answers: ["potpunih homonima", "homografa", "homofona", "hiponima"],
              correctAnswer: "homografa",
              opis: "Točan je odgovor <strong style='color:green'><em>homografa</em></strong>. Homografi su riječi koje se isto pišu, a različito izgovaraju.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "<em>Sklopiti umorne oči</em> (u značenju ‘umrijeti’) primjer je:",
              answers: ["metafore", "metonimije", "oksimorona", "eufemizma"],
              correctAnswer: "eufemizma",
              opis: "Točan je odgovor <strong style='color:green'><em>eufemizma</em></strong>. Eufemizam je blaža riječ ili izraz upotrijebljen umjesto riječi ili izraza koji označuju koji ružan ili neugodan pojam, npr. <em>Napustio nas je.</em> umjesto <em>Umro je</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi <em>Njujorčanin</em>, <em>Zagrepčanin</em> i <em>Bečanin</em> su:",
              answers: ["ktetici", "etnici", "etnonimi", "ekonimi"],
              correctAnswer: "etnici",
              opis: "Točan je odgovor <strong style='color:green'><em>etnici</em></strong>. Etnik je naziv za stanovnika izveden prema imenu naseljenih mjesta (ekonim) ili kakva drugoga zemljopisnog područja. Etnonim je naziv naroda i etničkih skupina.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi su <em>zagrebački</em>, <em>splitski</em>, <em>njujorški</em>:",
              answers: ["ktetici", "etnici", "etnonimi", "ekonim"],
              correctAnswer: "ktetici",
              opis: "Točan je odgovor <strong style='color:green'><em>ktetici</em></strong>. Ktetik je posvojni pridjev izveden od imena naseljenoga mjesta (ekonima) ili države.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječ <em>potpalublje</em> tvorena je:",
              answers: ["prefiksalnom tvorbom", "sufiksalnom tvorbom", "prefiksalno-sufiksalnom tvorbom", "slaganjem"],
              correctAnswer: "prefiksalno-sufiksalnom tvorbom",
              opis: "Točan je odgovor <strong style='color:green'><em>prefiksalno-sufiksalnom tvorbom</em></strong>. Prefiksalno-sufiksalna tvorba tvorbeni je način u kojemu se istodobno na tvorbenu osnovu (<em>palub</em>-) dodaju prefiks (<em>pod</em>-) i sufiks (-<em>je</em>). U toj riječi dolazi do jednačenja po zvučnosti (<em>pod</em> + <em>p</em> > <em>potp</em>) i jotacije (<em>b</em> + <em>j</em> > <em>blj</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi <em>prometnica</em> (cesta) i <em>prometnica</em> (žena prometnik) su:",
              answers: ["homonimi", "ekonimi", "sinonimi", "antonimi"],
              correctAnswer: "homonimi",
              opis: "Točan je odgovor <strong style='color:green'><em>homonimi</em></strong>. Homonimi su različite riječi koje imaju isti glasovni sastav i isti naglasak te pripadaju istoj vrsti riječi.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi <em>kivi</em>, <em>iglu</em>, <em>makaroni</em>, <em>paelja</em> primjeri su:",
              answers: ["metonimije", "oksimorona", "eufemizama", "egzotizama"],
              correctAnswer: "egzotizama",
              opis: "Točan je odgovor <strong style='color:green'><em>egzotizama</em></strong>. Egzotizmi su posuđenice koje označuju specifičnosti pojedinoga naroda, pa se ne mogu zamijeniti hrvatskom riječju.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "<em>Mladi starac</em>, <em>siromašni bogataš</em>, <em>ružna ljepotica</em>, <em>slatka bol</em>, <em>novi fosil</em>, <em>rječita tišina</em> primjeri su:",
              answers: ["metafore", "metonimije", "oksimorona", "eufemizma"],
              correctAnswer: "oksimorona",
              opis: "Točan je odgovor <strong style='color:green'><em>oksimorona</em></strong>. Oksimoron je stilska figura utemeljena na odnosu antonimije. U oksimoronu se spajanjem proturječnih pojmova stvara nov pojam. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječ <em>mišolovka</em> nastala je:",
              answers: ["prefiksalnom tvorbom", "sufiksalnom tvorbom", "složeno-sufiksalnom tvorbom", "prefiksalno-sufiksalnom tvorbom"],
              correctAnswer: "složeno-sufiksalnom tvorbom",
              opis: "Točan je odgovor <strong style='color:green'><em>složeno-sufiksalnom tvorbom</em></strong>. Složeno-sufiksalna tvorba tvorbeni je način u kojemu se istodobno spajaju dvije tvorbene osnove (<em>miš</em>, <em>lov</em>) s pomoću spojnika <em>o</em> i dodaje sufiks (-<em>ka</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Znanost koja proučava imena naziva se:",
              answers: ["ortoepija", "onomastika", "frazeologija", "terminologija"],
              correctAnswer: "onomastika",
              opis: "Točan je odgovor <strong style='color:green'><em>onomastika</em></strong>. Onomastika je znanost koja proučava zakonitosti postanka, razvoja i funkcioniranja imena.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koliko je nepunoznačnih riječi u sljedećoj rečenici: <em>On je išao na fakultet u 8 sati.</em>?",
              answers: ["jedna", "dvije", "tri", "četiri"],
              correctAnswer: "dvije",
              opis: "Točan je odgovor <strong style='color:green'><em>dvije</em></strong>.Nepunoznačne su riječi prijedlozi (<em>na</em>, <em>u</em>), veznici, usklici i čestice.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koliko je punoznačnih riječi u sljedećoj rečenici: <em>Ona voli čitati i gledati zanimljive filmove.</em>?",
              answers: ["četiri", "pet", "šest", "sedam"],
              correctAnswer: "šest",
              opis: "Točan je odgovor <strong style='color:green'><em>šest</em></strong>. Punoznačne su riječi: imenice (<em>filmove</em>), glagoli (<em>voli</em>, <em>gledati</em>) pridjevi (<em>zanimljive</em>), prilozi, brojevi i zamjenice (<em>ona</em>).",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu se retku nalaze samo oblici leksema ČITATI?",
              answers: ["pročitati, pročitan", "čitam, čitao je", "čitanje, čitaonica", "načitan, čitateljica."],
              correctAnswer: "čitam, čitao je",
              opis: "Točan je odgovor <strong style='color:green'><em>čitam, čitao je</em></strong>. Leskem je riječ uzeta u ukupnosti svojih oblika i značenja. Leskemu ČITATI pripadaju oblici <em>čitam</em>, <em>čitaš</em>, <em>čita…</em>, <em>čitat ću</em>, <em>čitat ćeš…</em>, <em>čitao sam</em>, <em>čitao si…</em> itd.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Odaberite redak u kojemu se nalaze dijelovi rječničkoga članka.",
              answers: ["natuknica, odrednica, uputnica", "eponim, egzotizam, etnik", "ekonim, hidronim, toponim", "arhaizam, dijalektizam, historizam."],
              correctAnswer: "natuknica, odrednica, uputnica",
              opis: "Točan je odgovor <strong style='color:green'><em>natuknica, odrednica, uputnica</em></strong>. Rječnički je članak skup obavijesti koje se u rječniku nalaze uz jednu leksičku jedinicu. Dijelovi su rječničkoga članka: natuknica, istovrijednice (ako je rječnik višejezični), odrednice (gramatičke, stilske, terminološke), uputnice, leksikografska definicija (ako je rječnik objasnidbeni) itd.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Odaberite redak u kojemu dolazi do metonimijskoga prijenosa značenja.",
              answers: ["To je odlučio Markov trg iako se Pantovčak tomu protivi.", "Kukavica je pobjegao čim su počeli problemi.", "Ona je lijepa kao vila, a on je jak kao bik.", "Ne može se sve rješavati pet do dvanaest."],
              correctAnswer: "To je odlučio Markov trg iako se Pantovčak tomu protivi.",
              opis: "Točan je odgovor <strong style='color:green'><em>To je odlučio Markov trg iako se Pantovčak tomu protivi.</em></strong> Metonimija je stilska figura u kojoj riječ ili sveza dobiva novo značenje različito od onoga koje je prvotno imala, a veza je među tim značenjima prostorna ili vremenska bliskost. <em>Markov trg</em> označuje Vladu, a <em>Pantovčak</em> hrvatskoga predsjednika. U rečenici <em>Kukavica je pobjegao čim</em> <em>su počeli problemi. </em>riječ<em> kukavica </em>je leksikalizirana metafora. U rečenici<em> Ona je lijepa kao vila, a on je jak kao bik. jak kao bik </em>je frazem. U rečenici<em> Ne može se sve rje&scaron;avati pet do dvanaest. pet do dvanaest </em>je frazem.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojoj se rečenici nalazi leksikalizirana metafora?",
              answers: ["Plakala je kao ljuta godina kad se on nije pojavio.", "Ronila je krokodilske suze dok je čitala njegovo pismo.", "Čitava je škola došla na njezin nastup.", "Ti si moje srce i ja te jako volim."],
              correctAnswer: "Ti si moje srce i ja te jako volim.",
              opis: "Točan je odgovor <strong style='color:green'><em>Ti si moje srce i ja te jako volim.</em></strong> Ako se koja metafora često upotrebljava, govornici njezinu metaforičnost počinju sve slabije zamjećivati sve dok se potpuno ne izgubi. Tako je riječ <em>srce</em> u značenju &lsquo;draga osoba&rsquo; leksikalizirana i to se značenje nalazi u rječnicima. <em>Plakati kao ljuta godina</em> i <em>roniti krokodilske suze</em> frazemi su, a u rečenici<em> Čitava je &scaron;kola do&scaron;la na njezin nastup. &scaron;kola </em>znači &lsquo;svi učenici &scaron;kole&rsquo; i riječ je o metonimiji.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "<em>Rim</em>, <em>Budimpešta</em>, <em>Beč</em>, <em>Mletci</em> primjeri su:",
              answers: ["epiteta", "eponima", "egzotizma", "egzonima"],
              correctAnswer: "egzonima",
              opis: "Točan je odgovor <strong style='color:green'><em>egzonima</em></strong>. Egzonim je ime strane pokrajine ili grada prilagođeno hrvatskomu jeziku.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi rečenicu u kojoj se nalazi frazem.",
              answers: ["Manirizam je naziv za kićeni pjesnički stil.", "Sve je to još na vrbi svirala, tko zna što će od toga biti.", "Čitava je škola išla na izlet u Samobor.", "Imena pastira također jasno govore o njihovim osobinama i zanimanju: Radmio, Ljubmir, Zagorko, Ljubdrag, Brštanko, Tratorko."],
              correctAnswer: "Sve je to još na vrbi svirala, tko zna što će od toga biti.",
              opis: "Točan je odgovor <strong style='color:green'><em>Sve je to još na vrbi svirala, tko zna što će od toga biti.</em></strong> Frazem je ustaljena veza riječi koja ima stalan sastav i raspored sastavnica, a značenje joj se obično ne izvodi iz značenja sastavnica, npr. <em>na vrbi svirala</em> znači &lsquo;te&scaron;ko ostvarivo&rsquo;.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koji se odnos više puta ostvaruje u ovome tekstu Stjepka Težaka?<br><br><p style='text-align:justify; font-weight:300'><em>Riječi su čovječje tvorevine, pa im čovjek pridaje i svoje osobine. Mogu biti lijepe i ružne, mile i nemile, bliske i daleke, zanimljive i dosadne, tople i hladne, svježe i otrcane, ugodne i neugodne, prijateljske i neprijateljske, spasonosne i ubitačne.</em></p>",
              answers: ["sinonimije", "antonimije", "hiperonimije", "hiponimije"],
              correctAnswer: "antonimije",
              opis: "Točan je odgovor <strong style='color:green'><em>antonimije</em></strong>. U tekstu se ponavljaju antonimi: <em>lijepe i ružne, mile i nemile, bliske i daleke, zanimljive i dosadne, tople i hladne, svježe i otrcane, ugodne i neugodne, prijateljske i neprijateljske</em>. U posljednjemu je primjeru<em> spasonosne i ubitačne </em>riječ o individualnim antonimima, a u ostalim primjerima o općejezičnim antonimima.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi <em>Jagoda</em> i <em>jagoda</em> su",
              answers: ["sinonimi", "antonimi", "homofoni", "homografi"],
              correctAnswer: "homofoni",
              opis: "Točan je odgovor <strong style='color:green'><em>homofoni</em></strong>. Homofoni su riječi koje se isto izgovaraju, a različito pišu.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "<em>Limuzina</em>, <em>makadam</em>, <em>džul</em>, <em>bovarizam</em> primjeri su:",
              answers: ["ekonima", "eponima", "egzotizma", "egzonima"],
              correctAnswer: "eponima",
              opis: "Točan je odgovor <strong style='color:green'><em>eponima</em></strong>. Eponim je naziv koji sadržava osobno ime, najče&scaron;će izumitelja, posvojni pridjev od imena ili koju drugu izvedenicu od imena. Naziv <em>bovarizam</em> nastao prema prezimenu Emme Bovary označuje čiju sklonost da smatra kako je rođen za ne&scaron;to bolje, naziv <em>makadam</em>, nastao prema prezimenu &scaron;kotskoga graditelja J. L. McAdama, označuje vrstu ceste, naziv <em>limuzina</em>, nastao prema imenu pokrajine Limousin, označuje putnički automobil, a naziv <em>džul</em>, nastao prema prezimenu engleskoga fizičara J. P. Joulea, označuje mjernu jedinicu.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi <em>škuda</em>, <em>politbiro</em>, <em>samoupravljanje</em>, <em>dinar</em>, <em>skojevac</em> primjer su:",
              answers: ["arhaizama", "historizama", "lokalizama", "galicizma"],
              correctAnswer: "historizama",
              opis: "Točan je odgovor <strong style='color:green'><em>historizama</em></strong>. Historizmi su riječi koje su obilježile koji smijenjeni ideološki, državni ili politički sustav i koje su u pasivni leksik prešle promjenom izvanjezične zbilje.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi <em>oloslovlje</em>, <em>seberad</em>, <em>mjerstvo</em>, <em>zloglasje</em> primjer su:",
              answers: ["arhaizama", "historizama", "oživljenica", "pomodnica"],
              correctAnswer: "arhaizama",
              opis: "Točan je odgovor <strong style='color:green'><em>arhaizama</em></strong>. Arhaizmi su riječi koje pripadaju pasivnomu leksiku. Pasivne su riječi one zastarjele riječi koje se ne nalaze u stilski neutralnome hrvatskom jeziku.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Riječi <em>bevanda</em>, <em>gvirc</em>, <em>brudet</em>, <em>pašticada</em>, <em>štrukli</em> su:",
              answers: ["ekonimi", "etnografski dijalektizmi", "egzotizmi", "egzonimi"],
              correctAnswer: "etnografski dijalektizmi",
              opis: "Točan je odgovor <strong style='color:green'><em>etnografski dijalektizmi</em></strong>. Etnografski dijalektizmi dijalektizmi su koji označuju pojavu ili predmet karakterističan za područja na kojima se govori određenim narječjem.",
              boja_pozadine: "#FCE4EC"
          }]
      } else if (jedna == 6) {
          $('#bootstrapForm').attr('action', 'https://docs.google.com/forms/d/e/1FAIpQLSc-wVca0HAHlQvqGrfvCiXqywBXSFhyxBecmaavbqODz9oK4g/formResponse')

          quiz = [{
              question: "Koja od navedenih sveza ne označuje filološku školu koja djeluje u drugoj polovici 19. stoljeća?",
              answers: ["riječka škola", "zagrebačka škola", "zadarska škola", "splitska škola"],
              correctAnswer: "splitska škola",
              opis: "Točan je odgovor <strong style='color:green'><em>splitska škola</em></strong>. U hrvatskome jezikoslovlju u drugoj polovici 19. stoljeća djeluju četiri struje s različitim stavovima o pitanjima normiranja i pravopisa: zagrebačka, zadarska i riječka filološka škola te hrvatski vukovci. Zagrebačka škola teži okupljanju Hrvata oko zajedničke jezične i pravopisne tradicije i nasljeđuje jezičnu koncepciju iliraca. Zadarska škola zalaže se za ikavicu i očuvanje dalmatinsko-slavonske pravopisne tradicije. Riječka škola zastupa nazore izgrađene na idejama zalaganja za starinu i sveslavenstvo, a vukovci se u načelu priklanjaju jezičnim i pravopisnim načelima kojih se pridržavao i Vuk Stefanović Karadžić.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Tko je autor <em>Hrvatskoga pravopisa</em> iz 1892.?",
              answers: ["Ljudevit Gaj", "Vatroslav Rožić", "Tomo Maretić", "Ivan Broz"],
              correctAnswer: "Ivan Broz",
              opis: "Točan je odgovor <strong style='color:green'><em>Ivan Broz</em></strong>. To je prvi službeni pravopis, čija je koncepcija bila takva da se može reći kako je to prvi suvremeni hrvatski pravopisni priručnik.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koje je godine potpisana <em>Deklaracija o nazivu i položaju hrvatskog književnog jezika?</em>",
              answers: ["1989.", "1991.", "1967.", "1941."],
              correctAnswer: "1967.",
              opis: "Točan je odgovor <strong style='color:green'><em>1967.</em></strong> Godine 1967., u političkome ozračju koje je dovelo do hrvatskoga proljeća 1971. godine, hrvatske znanstvene, kulturne i sveučili&scaron;ne ustanove potpisuju <em>Deklaraciju o nazivu i položaju</em> <em>hrvatskog književnog jezika</em>. Objavljena je 17. ožujka u listu<em> Telegram</em>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Tko je autor prvoga hrvatskog rječnika?",
              answers: ["Ljudevit Gaj", "Bartol Kašić", "Faust Vrančić", "Jakov Mikalja"],
              correctAnswer: "Faust Vrančić",
              opis: "Točan je odgovor <strong style='color:green'><em>Faust Vrančić</em></strong>. Godine 1595. objavljen je rječnik koji se zbog svoje potpunosti i leksikografske dorađenosti naziva prvim hrvatskim rječnikom, <em>Dictionarium quinque nobilissimarum Europae</em> <em>linguarum </em>(<em>Rječnik pet najuglednijih europskih jezika</em>) &Scaron;ibenčanina Fausta Vrančića. Vrančićev je rječnik hrvatsku leksikografiju uveo u svjetsku i otvorio put mnogim kasnijim leksikografskim djelima. Rječnik uključuje oko 5000 riječi, od kojih oko 3500 ima hrvatsku istovrijednicu. Riječi se donose na latinskome (latinski je polazi&scaron;ni jezik i u latinskome se stupcu natuknice donose abecedno), talijanskome, njemačkome, mađarskome i hrvatskome (<em>dalmatice</em>) jeziku.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Tko je autor prve hrvatske gramatike?",
              answers: ["Ljudevit Gaj", "Bartol Kašić", "Faust Vrančić", "Jakov Mikalja"],
              correctAnswer: "Bartol Kašić",
              opis: "Točan je odgovor <strong style='color:green'><em>Bartol Kašić</em></strong>. Godine 1604. u Rimu je objavljena prva hrvatska gramatika pod nazivom <em>Institutionum linguae Illyricae</em> <em>libri duo </em>Pažanina Bartola Ka&scaron;ića.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Tko je pripadao ozaljskomu krugu?",
              answers: ["Ljudevit Gaj", "Bartol Kašić", "Faust Vrančić", "Ivan Belostenec"],
              correctAnswer: "Ivan Belostenec",
              opis: "Točan je odgovor <strong style='color:green'><em>Ivan Belostenec</em></strong>. Ivan Belostenec (17. st.) bio je svećenik pavlinskoga reda i pripadao je ozaljskomu krugu, krugu književnika i filologa okupljenih oko obitelji Zrinskih i Frankopana. Pripadnici kruga zalagali su se za mije&scaron;anje svih triju hrvatskih narječja, ali u praksi je &scaron;tokavski bio najmanje zastupljen. Uglavnom su u kajkavsku leksičku osnovicu uno&scaron;ene čakavske riječi. Napisao je <em>Gazophylacium, </em>latinsko-hrvatski i hrvatsko-latinski rječnik.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Tko je napisao <em>Njemačko-hrvatski rječnik</em> i <em>Hrvatsko-njemačko-talijanski rječnik znanstvenoga nazivlja</em>?",
              answers: ["Jakov Mikalja", "Bartol Kašić", "Faust Vrančić", "Bogoslav Šulek"],
              correctAnswer: "Bogoslav Šulek",
              opis: "Točan je odgovor <strong style='color:green'><em>Bogoslav Šulek</em></strong>. Bogoslav &Scaron;ulek objavio je dvosvezačni <em>Njemačko-hrvatski rječnik</em> 1860. godine i <em>Hrvatsko-njemačko-talijanski rječnik znanstvenoga nazivlja </em>1874. &ndash; 1875. godine. &Scaron;ulek je hrvatsko znanstveno nazivlje stvarao preuzimajući riječi iz narječja, iz slavenskih jezika ili tvoreći nove nazive. Smatra se ocem hrvatskoga znanstvenog nazivlja.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koji od navedenih spomenika nije pisan latinicom?",
              answers: ["<em>Red i zakon sestara dominikanki</em>", "<em>Šibenska molitva</em>", "<em>Cantilena pro sabatho</em>", "<em>Plominski natpis</em>"],
              correctAnswer: "<em>Plominski natpis</em>",
              opis: "Točan je odgovor <strong style='color:green'><em>Plominski natpis</em></strong>. <em>Plominski natpis</em> pisan je oblom glagoljicom. <em>Red i zakon sestara dominikanki</em>, <em>&Scaron;ibenska molitva</em>, <em>Cantilena pro sabatho </em>pisani su latinicom.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koji od navedenih spomenika nije pisan glagoljicom?",
              answers: ["<em>Povaljski prag</em>", "<em>Bašćanska ploča</em>", "<em>Krčki natpis</em>", "<em>Valunska ploča</em>"],
              correctAnswer: "<em>Povaljski prag</em>",
              opis: "Točan je odgovor <strong style='color:green'><em>Povaljski prag</em></strong>. Taj je spomenik pisan ćirilicom. <em>Valunska ploča </em>pisana je glagoljicom i latinicom, a<em> Ba&scaron;ćanska ploča </em>i <em>Krčki natpis </em>samo glagoljicom.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Tko od navedenih nije autor hrvatskoga pravopisa?",
              answers: ["Tomo Maretić", "Ivan Broz", "Dragutin Boranić", "Bratoljub Klaić"],
              correctAnswer: "Tomo Maretić",
              opis: "Točan je odgovor <strong style='color:green'><em> Tomo Maretić</em></strong>.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu su retku samo pripadnici zagrebačke filološke škole?",
              answers: ["Bogoslav Šulek, Adolfo Veber Tkalčević", "Antun Mažuranić, Tomo Maretić", "Ivan Broz, Franjo Iveković", "Tomo Maretić, Ante Kuzmanić"],
              correctAnswer: "Bogoslav Šulek, Adolfo Veber Tkalčević",
              opis: "Točan je odgovor <strong style='color:green'><em>Bogoslav Šulek, Adolfo Veber Tkalčević</em></strong>. Ante Kuzmanić bio je pripadnik zadarske škole, a Tomo Maretić, Ivan Broz i Franjo Iveković bili su hrvatski vukovci.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Najstarije hrvatske tiskare bile su u:",
              answers: ["Zadru i Kninu", "Senju i Rijeci", "Rijeci i Zadru", "Zadru i Senju"],
              correctAnswer: "Senju i Rijeci",
              opis: "Točan je odgovor <strong style='color:green'><em>Senju i Rijeci</em></strong>. Senjska je tiskara djelovala u razdoblju od 1494. do 1508. godine, a riječka nepunih šest mjeseci 1530. i 1531. godine.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Prva kajkavska knjiga, <em>Decretum</em> Ivanuša Pergošića, objavljena je u:",
              answers: ["15. stoljeću", "16. stoljeću", "17. stoljeću", "18. stoljeću"],
              correctAnswer: "16. stoljeću",
              opis: "Točan je odgovor <strong style='color:green'><em>16. stoljeću</em></strong>. <em>Decretum</em> Ivanuša Pergošića objavljen je 1574. godine.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu su retku svi navedeni tekstovi pisani glagoljicom?",
              answers: ["<em>Povaljski prag</em> i <em>Povaljska listina</em>", "<em>Šibenska molitva</em> i <em>Hrvojev misal</em>", "<em>Šibenska molitva</em> i <em>Povaljska listina</em>", "<em>Hrvojev misal</em> i <em>Vinodolski zakon</em>"],
              correctAnswer: "<em>Hrvojev misal</em> i <em>Vinodolski zakon</em>",
              opis: "Točan je odgovor <strong style='color:green'><em>Hrvojev misal i Vinodolski zakon</em></strong>. <em>Povaljska listina</em> i <em>Povaljski prag</em> pisani su ćirilicom, a <em>&Scaron;ibenska molitva</em> latinicom.",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Autor <em>Kratke osnove horvatsko-slavenskoga pravopisanja</em> je:",
              answers: ["Antun Mažuranić", "Vjekoslav Babukić", "Antun Mihanović", "Ljudevit Gaj"],
              correctAnswer: "Ljudevit Gaj",
              opis: "Točan je odgovor <strong style='color:green'><em>Ljudevit Gaj</em></strong>.",
              boja_pozadine: "#FCE4EC"
          }]
      } else if (jedna == 7) {
          $('.mrzim').remove()
          $('#bootstrapForm').attr('action', 'https://docs.google.com/forms/d/e/1FAIpQLSefsfWRXCYJpWjmpV-9DqnPxSHiUPqYczEsWE-gzcgJ0c9EaQ/formResponse')
          quiz = [{
              question: "Pronađi redak u kojemu su sve riječi točno napisane.",
              answers: ["prianjati, auto-škola, promjena", "prijanjati, autoškola, promijena", "prianjati, autoškola, promjena", "prijanjati, auto škola, promjena"],
              correctAnswer: "prianjati, autoškola, promjena",
              opis: "Točan je odgovor <strong style='color:green'><em>prianjati, autoškola, promjena</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak bez pogreške.",
              answers: ["Posjete 17 – 20 sati.", "Posjete od 17 – 20 sati.", "Posjeti 17 – 20 sati.", "Posjete 17–20 sati."],
              correctAnswer: "Posjeti 17 – 20 sati.",
              opis: "Točan je odgovor <strong style='color:green'><em>Posjeti 17 – 20 sati.</em></strong> ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak bez pogreške.",
              answers: ["J.K. Rowling autorica je romana o Harryu Potteru.", "J. F. Kennedy bio je američki predsjednik.", "Beyoncé je rođena u Houstonu, 4. rujna 1981.", "Freddie Mercury (1946.–1991.) bio je britanski glazbenik."],
              correctAnswer: "J. F. Kennedy bio je američki predsjednik.",
              opis: "Točan je odgovor <strong style='color:green'><em>J. F. Kennedy bio je američki predsjednik.</em></strong> ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu su sve riječi točno napisane.",
              answers: ["stramputica, bombon, karamfil, simfonija", "odskočiti, otpjevati, podcrtati, potpredsjednik", "samnom, ekoproizvod, neću, odoka", "crvić, dječačić, ribić, sendvić"],
              correctAnswer: "odskočiti, otpjevati, podcrtati, potpredsjednik",
              opis: "Točan je odgovor <strong style='color:green'><em>odskočiti, otpjevati, podcrtati, potpredsjednik</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi rečenicu u kojoj je pogreška.",
              answers: ["Sretna nova godina!", "Sretna Nova godina!", "Dobro došli, dragi gosti!", "Dobrodošli, dragi gosti!"],
              correctAnswer: "Dobrodošli, dragi gosti!",
              opis: "Točan je odgovor <strong style='color:green'><em>Dobrodošli, dragi gosti!</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koliko je naglasnih cjelina u rečenici <em>Ivana je išla u školu.</em>?",
              answers: ["pet naglasnih cjelina", "četiri naglasne cjeline", "tri naglasne cjeline", "dvije naglasne cjeline"],
              correctAnswer: "tri naglasne cjeline",
              opis: "Točan je odgovor <strong style='color:green'><em>tri naglasne cjeline</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Do kojih glasovnih promjena dolazi u riječi <em>iščitati</em>?",
              answers: ["jednačenja po zvučnosti i ispadanja suglasnika", "jednačenja po mjestu tvorbe i ispadanja suglasnika", "jednačenja po mjestu tvorbe i jotacije", "jednačenja po zvučnosti i jednačenja po mjestu tvorbe"],
              correctAnswer: "jednačenja po zvučnosti i jednačenja po mjestu tvorbe",
              opis: "Točan je odgovor <strong style='color:green'><em>jednačenja po zvučnosti i jednačenja po mjestu tvorbe</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koji je slog naglašen u riječi telefonirati?",
              answers: ["prvi", "drugi", "treći", "četvrti"],
              correctAnswer: "četvrti",
              opis: "Točan je odgovor <strong style='color:green'><em>četvrti</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu nema pogreške.",
              answers: ["Toliko je željeo uspjeti da je na kraju i uspio.", "Taj je zakon donijet prošle godine.", "Namještaj je već iznešen iz stana.", "Ovim putom djeca prolaze svakoga dana."],
              correctAnswer: "Ovim putom djeca prolaze svakoga dana.",
              opis: "Točan je odgovor <strong style='color:green'><em>Ovim putom djeca prolaze svakoga dana.</em></strong> ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu je pogreška.",
              answers: ["Volio me je.", "Češljao se je.", "Smijala si se.", "Odmarao sam se."],
              correctAnswer: "Češljao se je.",
              opis: "Točan je odgovor <strong style='color:green'><em>Češljao se je.</em></strong> ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi rečenicu u kojoj se <em>dva/dvije</em> točno sklanja.",
              answers: ["Jučer sam se u gradu našla s dvama prijateljicama.", "Bez svojih dvaju prijateljica ne idem u kino.", "Razgovarao sam s dvjema mladićima.", "Razgovarat ćemo danas o dvama problemima."],
              correctAnswer: "Razgovarat ćemo danas o dvama problemima.",
              opis: "Točan je odgovor <strong style='color:green'><em>Razgovarat ćemo danas o dvama problemima.</em></strong> ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi rečenicu bez pogreške.",
              answers: ["Uživao sam čitavši Shakespeareov sonet.", "Uživao sam u čitanju njegovog soneta.", "Zaista sam uživao u Shakespeareovome sonetu.", "Zbog ljepote njegova soneta čitav sam dan proveo čitajući."],
              correctAnswer: "Zbog ljepote njegova soneta čitav sam dan proveo čitajući.",
              opis: "Točan je odgovor <strong style='color:green'><em>Zbog ljepote njegova soneta čitav sam dan proveo čitajući.</em></strong> ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak bez pogreške.",
              answers: ["Prisjećam se najljepša rođendanskoga dara.", "Nema ničega ljepšeg od božićna ugođaja.", "Nikad nisam sudjelovao u neugodniju razgovoru.", "Danas smo učili o hrvatskome proljeću."],
              correctAnswer: "Danas smo učili o hrvatskome proljeću.",
              opis: "Točan je odgovor <strong style='color:green'><em>Danas smo učili o hrvatskome proljeću.</em></strong> ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koje je ustrojstvo rečenice <em>Ne mogu se sjetiti ni kad sam ga upoznala ni gdje smo prvi put razgovarali ni jesam li ga ikad vidjela gdje ulazi u našu školu.</em>?",
              answers: ["glavna surečenica – objektna surečenica – objektna surečenica – objektna surečenica – objektna surečenica", "glavna surečenica – objektna surečenica – objektna surečenica – objektna surečenica – mjesna surečenica", "glavna surečenica – vremenska surečenica – mjesna surečenica – objektna surečenica – objektna surečenica", "glavna surečenica – vremenska surečenica – mjesna surečenica – objektna surečenica – mjesna surečenica"],
              correctAnswer: "glavna surečenica – objektna surečenica – objektna surečenica – objektna surečenica – objektna surečenica",
              opis: "Točan je odgovor <strong style='color:green'><em>glavna surečenica – objektna surečenica – objektna surečenica – objektna surečenica – objektna surečenica</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojoj su rečenici upotrijebljeni točni prijedložni izrazi?",
              answers: ["Sudjelovao je u radu školskih tijela ispred svih učenika trećega i četvrtoga razreda.", "Moraš znati da je pušenje opasno za zdravlje.", "Po meni, to se nikada nije smjelo dopustiti.", "Nasuprot trgovine nalazi se park."],
              correctAnswer: "Moraš znati da je pušenje opasno za zdravlje.",
              opis: "Točan je odgovor <strong style='color:green'><em>Moraš znati da je pušenje opasno za zdravlje.</em></strong> ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Pronađi redak u kojemu se nalazi priložna oznaka namjere.",
              answers: ["Otputovao je u inozemstvo radi studija.", "Nepouzdan si, zato ti ne namjeravam vjerovati.", "Zbog svojega plana bio je spreman pristati na sve.", "Kupit ću si bicikl sljedećega proljeća."],
              correctAnswer: "Otputovao je u inozemstvo radi studija.",
              opis: "Točan je odgovor <strong style='color:green'><em>Otputovao je u inozemstvo radi studija.</em></strong> ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koju svezu <em>imenica</em> + <em>imenica</em> u (neprijedložnome ili prijedložnome) genitivu nije bolje zamijeniti svezom <em>pridjev</em> + <em>imenica</em>?",
              answers: ["broj telefona", "pismenost medija", "troškovi života", "suknja od sestre"],
              correctAnswer: "pismenost medija",
              opis: "Točan je odgovor <strong style='color:green'><em>pismenost medija</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojoj je rečenici točno upotrijebljen prijedlog?",
              answers: ["Pošto je stigla kući, zaspala je.", "Upoznali smo se pred dvije godine.", "Otišla je u kino sa majkom.", "Svakoga je dana putovao na Rijeku."],
              correctAnswer: "Pošto je stigla kući, zaspala je.",
              opis: "Točan je odgovor <strong style='color:green'><em>Pošto je stigla kući, zaspala je.</em></strong>",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Čega su primjer riječi <em>tekila</em>, <em>votka</em>, <em>boršč</em> i <em>suši</em>?",
              answers: ["ekonima", "eponima", "egzonima", "egzotizama"],
              correctAnswer: "egzotizama",
              opis: "Točan je odgovor <strong style='color:green'><em>egzotizama</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu su od sljedećih primjera frazemi u antonimnome odnosu?",
              answers: ["bijela vrana – crna ovca", "mačji kašalj – majmunska posla", "Ahilova peta – trojanski konj", "hitac u prazno – pun pogodak"],
              correctAnswer: "hitac u prazno – pun pogodak",
              opis: "Točan je odgovor <strong style='color:green'><em>hitac u prazno – pun pogodak</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je tvorbena osnova riječi <em>učiteljica</em>?",
              answers: ["uč", "čit", "učitelj", "učiteljic"],
              correctAnswer: "učitelj",
              opis: "Točan je odgovor <strong style='color:green'><em>učitelj</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je od sljedećih riječ nastala prefiksalnom tvorbom?",
              answers: ["natkoljenica", "predsjednica", "potpredsjednica", "učiteljica"],
              correctAnswer: "potpredsjednica",
              opis: "Točan je odgovor <strong style='color:green'><em>potpredsjednica</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "U kojemu su odnosu riječi <em>pozórnica</em> i <em>pòzōrnica</em>?",
              answers: ["sinonimije", "antonimije", "homografije", "homofonije"],
              correctAnswer: "homografije",
              opis: "Točan je odgovor <strong style='color:green'><em>homografije</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koja je od sljedećih riječi ekonim?",
              answers: ["Kopački rit", "Velika Mlaka", "Vranjsko jezero", "Mohačko polje"],
              correctAnswer: "Velika Mlaka",
              opis: "Točan je odgovor <strong style='color:green'><em>Velika Mlaka</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, {
              question: "Koje ime dobiva <em>Danica ilirska</em> 1843. godine?",
              answers: ["<em>Danica horvatska</em>", "<em>Danica horvatska, slavonska i dalmatinska</em>", "<em>Narodne novine</div>", "<em>Ilirske narodne novine</em>"],
              correctAnswer: "<em>Danica horvatska, slavonska i dalmatinska</em>",
              opis: "Točan je odgovor <strong style='color:green'><em>Danica horvatska, slavonska i dalmatinska</em></strong>. ",
              boja_pozadine: "#FCE4EC"
          }, ]
      }

      shuffle(quiz)
  }
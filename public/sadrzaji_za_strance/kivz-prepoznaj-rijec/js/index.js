  // VARIABLE DECLARATIONS ------

  // pages
  var initPage,
      questionsPage,
      resultsPage,
      // buttons
      startBtn,
      submitBtn,
      continueBtn,
      retakeBtn,
      spanishBtn,
      // question and answers
      question,
      answerList,
      answerSpan,
      answerA,
      answerB,
      answerC,
      answerD,
      // event listeners
      answerDiv,
      answerDivA,
      answerDivB,
      answerDivC,
      answerDivD,
      feedbackDiv,
      selectionDiv,
      toBeHighlighted,
      toBeMarked,
      userScore,
      // quiz
      quiz,
      questionCounter,
      correctAnswer,
      correctAnswersCounter,
      userSelectedAnswer,
      // function names
      newQuiz,
      generateQuestionAndAnswers,
      getCorrectAnswer,
      getUserAnswer,
      selectAnswer,
      deselectAnswer,
      selectCorrectAnswer,
      deselectCorrectAnswer,
      getSelectedAnswerDivs,
      highlightCorrectAnswerGreen,
      highlightIncorrectAnswerRed,
      slikica,
      clearHighlightsAndFeedback, moze = 0,
      prekidac, countdownTimer, bodovi = 0,
      vrijeme = 0,
      jedna, dvije;

  function ProgressCountdown(timeleft, bar, text) {
      return new Promise((resolve, reject) => {
          countdownTimer = setInterval(() => {
              timeleft--;
              document.getElementById(bar).value = timeleft;
              document.getElementById(text).textContent = timeleft;
              if (timeleft <= 0) {
                  clearInterval(countdownTimer);
                  resolve(true);
              } else if (timeleft <= 1) {
                  $("#sekunde").html("sekunda")
                  $("#ostalo").html("ostala")
              } else if (timeleft <= 4) {
                  $("#sekunde").html("sekunde")
              }

          }, 1000);
      });
  }

  $(document).ready(function() {

      // DOM SELECTION ------

      // App pages
      // Page 1 - Initial
      initPage = $('.init-page');
      // Page 2 - Questions/answers
      questionsPage = $('.questions-page');
      // Page 3 - Results
      resultsPage = $('.results-page');
      slikica = $('.slikica');

      // Buttons
      startBtn = $('.init-page__btn, .results-page__retake-btn');
      submitBtn = $('.mrzim');
      continueBtn = $('.questions-page__continue-btn');
      retakeBtn = $('.results-page__retake-btn');
      spanishBtn = $('.results-page__spanish-btn');

      // Answer block divs
      answerDiv = $('.questions-page__answer-div');
      answerDivA = $('.questions-page__answer-div-a');
      answerDivB = $('.questions-page__answer-div-b');
      answerDivC = $('.questions-page__answer-div-c');
      answerDivD = $('.questions-page__answer-div-d');

      // Selection div (for the pointer, on the left)
      selectionDiv = $('.questions-page__selection-div');

      // Feedback div (for the checkmark or X, on the right)
      feedbackDiv = $('.questions-page__feedback-div');

      // Questions and answers
      question = $('.questions-page__question');
      answerList = $('.questions-page__answer-list');
      answerSpan = $('.questions-page__answer-span');
      answerA = $('.questions-page__answer-A');
      answerB = $('.questions-page__answer-B');
      answerC = $('.questions-page__answer-C');
      answerD = $('.questions-page__answer-D');


      // User final score
      userScore = $('.results-page__score');
      prikazBodova = $('.results-page__bodovi');
      // QUIZ CONTENT ------


      // FUNCTION DECLARATIONS ------
      $.fn.declasse = function(re) {
          return this.each(function() {
              var c = this.classList
              for (var i = c.length - 1; i >= 0; i--) {
                  var classe = "" + c[i]
                  if (classe.match(re)) c.remove(classe)
              }
          })
      }


      function shuffle(array) { //izmješaj pitanja
          var i = 0,
              j = 0,
              temp = null

          for (i = array.length - 1; i > 0; i -= 1) {
              j = Math.floor(Math.random() * (i + 1))
              temp = array[i]
              array[i] = array[j]
              array[j] = temp
          }
      }
      // Start the quiz
      newQuiz = function() {
          prekidac = 1;
          bodovi = 0;
          // Set the question counter to 0
          questionCounter = 0;
          // Set the total correct answers counter to 0
          correctAnswersCounter = 0;
          // Hide other pages of the app
          questionsPage.hide();
          resultsPage.hide();

          quiz = [{
                  question: "",
                  answers: ["baka", "baga", "baha", "bata"],
                  correctAnswer: "baka",
                  slika: "../slike/baka.png",
                  zvuk: "../audio/rijeci/baka.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["kava", "kafa", "kavo", "keva"],
                  correctAnswer: "kava",
                  slika: "../slike/kava.png",
                  zvuk: "../audio/rijeci/kava.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["krumpir", "krompir", "krumper", "krimpir"],
                  correctAnswer: "krumpir",
                  slika: "../slike/krumpir.png",
                  zvuk: "../audio/rijeci/krumpir.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["ananas", "amamas", "anamas", "amanas"],
                  correctAnswer: "ananas",
                  slika: "../slike/ananas.png",
                  zvuk: "../audio/rijeci/ananas.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["badem", "badel", "baden", "bedem"],
                  correctAnswer: "badem",
                  slika: "../slike/badem.png",
                  zvuk: "../audio/rijeci/badem.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["ljuljačka", "ljuljaška", "ljuljaćka", "ljuljacka"],
                  correctAnswer: "ljuljačka",
                  slika: "../slike/ljuljacka.png",
                  zvuk: "../audio/rijeci/ljuljacka.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["Engleska", "Ingleska", "Ingliska", "Engliska"],
                  correctAnswer: "Engleska",
                  slika: "../slike/engleska.png",
                  zvuk: "../audio/rijeci/engleska.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["krema", "krima", "kruma", "kroma"],
                  correctAnswer: "krema",
                  slika: "../slike/krema.png",
                  zvuk: "../audio/rijeci/krema.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["mačka", "mička", "močka", "maška"],
                  correctAnswer: "mačka",
                  slika: "../slike/macka.png",
                  zvuk: "../audio/rijeci/macka.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["glava", "klava", "hlava", "glova"],
                  correctAnswer: "glava",
                  slika: "../slike/glava.png",
                  zvuk: "../audio/rijeci/glava.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["kokoš", "kukuš", "kikoš", "kikuš"],
                  correctAnswer: "kokoš",
                  slika: "../slike/kokos.png",
                  zvuk: "../audio/rijeci/kokos.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["Hrvatska", "Hrvacka", "Hrvtska", "Hrvotska"],
                  correctAnswer: "Hrvatska",
                  slika: "../slike/hrvatska.gif",
                  zvuk: "../audio/rijeci/hrvatska.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["mrkva", "merkva", "mirkva", "merikva"],
                  correctAnswer: "mrkva",
                  slika: "../slike/mrkva.png",
                  zvuk: "../audio/rijeci/mrkva.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["brat", "prat", "borat", "bret"],
                  correctAnswer: "brat",
                  slika: "../slike/brat.png",
                  zvuk: "../audio/rijeci/brat.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["duga", "doga", "tuga", "duka"],
                  correctAnswer: "duga",
                  slika: "../slike/duga.png",
                  zvuk: "../audio/rijeci/duga.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["tata", "dada", "titi", "toto"],
                  correctAnswer: "tata",
                  slika: "../slike/tata.png",
                  zvuk: "../audio/rijeci/tata.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["trgovina", "tergovina", "tirgovina", "targvina"],
                  correctAnswer: "trgovina",
                  slika: "../slike/trgovina.png",
                  zvuk: "../audio/rijeci/trgovina.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["stolica", "ctolisa", "stolitsa", "stolisa"],
                  correctAnswer: "stolica",
                  slika: "../slike/stolica.png",
                  zvuk: "../audio/rijeci/stolica.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["kamion", "gamion", "hamijon", "kumion"],
                  correctAnswer: "kamion",
                  slika: "../slike/kamion.png",
                  zvuk: "../audio/rijeci/kamion.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["brzo", "przo", "berzo", "perzo"],
                  correctAnswer: "brzo",
                  slika: "../slike/brzo.png",
                  zvuk: "../audio/rijeci/brzo.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["sapun", "sabun", "sabon", "sapon"],
                  correctAnswer: "sapun",
                  slika: "../slike/sapun.png",
                  zvuk: "../audio/rijeci/sapun.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["dobro", "topro", "tobero", "tobro"],
                  correctAnswer: "dobro",
                  slika: "../slike/dobro.png",
                  zvuk: "../audio/rijeci/dobro.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["lak", "luk", "lik", "lok"],
                  correctAnswer: "lak",
                  slika: "../slike/lak.png",
                  zvuk: "../audio/rijeci/lak.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["jabuka", "jobuka", "jubuka", "joboka"],
                  correctAnswer: "jabuka",
                  slika: "../slike/jabuka.png",
                  zvuk: "../audio/rijeci/jabuka.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["prijateljica", "prijatiljica", "brijateljica", "prijateljisa"],
                  correctAnswer: "prijateljica",
                  slika: "../slike/prijateljica.png",
                  zvuk: "../audio/rijeci/prijateljica.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["frizura", "vrizura", "frizora", "frizerka"],
                  correctAnswer: "frizura",
                  slika: "../slike/frizura.png",
                  zvuk: "../audio/rijeci/frizura.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["zarez", "zariz", "sares", "zares"],
                  correctAnswer: "zarez",
                  slika: "../slike/zarez.png",
                  zvuk: "../audio/rijeci/zarez.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              },
              {
                  question: "",
                  answers: ["džamija", "džomija", "džimija", "džomija"],
                  correctAnswer: "džamija",
                  slika: "../slike/dzamija.png",
                  zvuk: "../audio/rijeci/dzamija.mp3",
                  opis: "",
                  boja_pozadine: "#FCE4EC"
              }
          ];
          shuffle(quiz)


      };

      // Load the next question and set of answers
      generateQuestionAndAnswers = function() {
          $(".zvuk").show()

          question.html("<span style='font-size: 1.3rem;'>" + (questionCounter + 1) + "/" + quiz.length + ".</span> <br>");
          shuffle(quiz[questionCounter].answers);


          slikica.attr("src", quiz[questionCounter].slika)
          $("body").css({
              "background-color": quiz[questionCounter].boja_pozadine
          })

          $(".zvuk").on("click", function() {
              if (!playing) {
                  var element = $(this);
                  var elementID = event.target.id;
                  var oggVar = (quiz[questionCounter].zvuk);
                  var audioElement = $('#izgovor')[0];
                  audioElement.setAttribute('src', oggVar);
                  audioElement.play();
                  $("#opis").html("<em>" + quiz[questionCounter].opis + "</em>")
                  $(".questions-page__answer-list").show()
                  $(".vrijeme").show()
                  answerA.text(quiz[questionCounter].answers[0]);
                  if (answerA.html() == "" || null) {
                      answerDivA.hide()
                  } else {
                      answerDivA.show()
                  };
                  answerB.text(quiz[questionCounter].answers[1]);
                  if (answerB.html() == "" || null) {
                      answerDivB.hide()
                  } else {
                      answerDivB.show()
                  };
                  answerC.text(quiz[questionCounter].answers[2]);
                  if (answerC.html() == "" || null) {
                      answerDivC.hide()
                  } else {
                      answerDivC.show()
                  };
                  answerD.text(quiz[questionCounter].answers[3]);
                  if (answerD.html() == "" || null) {
                      answerDivD.hide()
                  } else {
                      answerDivD.show()
                  };
                  if (prekidac == 1) {
                      $(".vrijeme").html('<progress value="40" max="40" id="pageBeginCountdown"></progress><p> <span id="pageBeginCountdownText">40 </span></p>')
                      ProgressCountdown(40, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => odgovor());
                  }
                  prekidac = 0;
              }
          })

      };

      // Store the correct answer of a given question
      getCorrectAnswer = function() {
          correctAnswer = quiz[questionCounter].correctAnswer;
      };

      // Store the user's selected (clicked) answer
      getUserAnswer = function(target) {
          userSelectedAnswer = $(target).find(answerSpan).text();
      };

      // Add the pointer to the clicked answer
      selectAnswer = function(target) {
          $(target).find(selectionDiv).addClass('ion-chevron-right');
          $(target).addClass("odabir")

      };

      // Remove the pointer from any answer that has it
      deselectAnswer = function() {
          if (selectionDiv.hasClass('ion-chevron-right')) {
              selectionDiv.removeClass('ion-chevron-right');
              selectionDiv.parent().removeClass("odabir")
          }
      };

      // Get the selected answer's div for highlighting purposes
      getSelectedAnswerDivs = function(target) {
          toBeHighlighted = $(target);
          toBeMarked = $(target).find(feedbackDiv);
      };

      // Make the correct answer green and add checkmark
      highlightCorrectAnswerGreen = function(target) {
          if (correctAnswer === answerA.text()) {
              answerDivA.addClass('questions-page--correct');
              answerDivA.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerB.text()) {
              answerDivB.addClass('questions-page--correct');
              answerDivB.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerC.text()) {
              answerDivC.addClass('questions-page--correct');
              answerDivC.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerD.text()) {
              answerDivD.addClass('questions-page--correct');
              answerDivD.find(feedbackDiv).addClass('ion-checkmark-round');
          }
      };

      // Make the incorrect answer red and add X
      highlightIncorrectAnswerRed = function() {
          toBeHighlighted.addClass('questions-page--incorrect');
          toBeMarked.addClass('ion-close-round');
      };

      // Clear all highlighting and feedback
      clearHighlightsAndFeedback = function() {
          answerDiv.removeClass('questions-page--correct');
          answerDiv.removeClass('questions-page--incorrect');
          feedbackDiv.removeClass('ion-checkmark-round');
          feedbackDiv.removeClass('ion-close-round');
      };

      // APP FUNCTIONALITY ------

      /* --- PAGE 1/3 --- */

      // Start the quiz:

      resultsPage.hide();

      submitBtn.hide();
      continueBtn.hide();


      // Clicking on start button:
      startBtn.on('click', function() {

          newQuiz();

          // Advance to questions page
          initPage.hide();
          questionsPage.show(300);

          // Load question and answers
          generateQuestionAndAnswers();

          // Store the correct answer in a variable
          getCorrectAnswer();

          // Hide the submit and continue buttons
          submitBtn.hide();
          continueBtn.hide();

      });

      /* --- PAGE 2/3 --- */

      // Clicking on an answer:
      answerDiv.on('click', function() {

          // Make the submit button visible

          // Remove pointer from any answer that already has it
          deselectAnswer();

          // Put pointer on clicked answer
          selectAnswer(this);

          // Store current selection as user answer
          getUserAnswer(this);

          // Store current answer div for highlighting purposes
          getSelectedAnswerDivs(this);
          odgovor();

      });

      var playing = false;

      $('#izgovor').on('playing', function() {
          playing = true;
          $('.zvuk').attr("src", "slike/n_zvuk.png")
      });
      $('#izgovor').on('ended', function() {
          playing = false;
          $('.zvuk').attr("src", "slike/zvuk.png")
      });


      function odgovor() {
          moze = 0
          vrijeme = parseInt($("#pageBeginCountdownText").text())
          bodovi += vrijeme

          prekidac = 0;
          var ide = 0
              // Disable ability to select an answer
          answerDiv.off('click');
          if (questionCounter != quiz.length - 1) {
              ide = 1
          } else {
              ide = 0
          }

          // Make correct answer green and add a checkmark
          highlightCorrectAnswerGreen();
          clearInterval(countdownTimer);


          if (document.getElementById("pageBeginCountdown").value == "0") {
              $("#krivo")[0].play();

              bodovi -= 10;

              swal({
                  title: "Vrijeme je isteklo.",
                  html: "<p style='text-align:center'><strong>Točan odgovor je <span style='color:#bb422a; font-size:34px' class='comic'>" + quiz[questionCounter].correctAnswer + "</span></strong>.</p><br><em>" + quiz[questionCounter].opis + "</em><img src='" + quiz[questionCounter].slika + " 'class='slikica2'/>",
                  showCloseButton: true,
                  confirmButtonText: ' dalje',
                  backdrop: false,
                  allowOutsideClick: false,
                  allowEscapeKey: false
              });
              $(".swal2-confirm").click(function() {
                  clearInterval(countdownTimer)
                  prekidac = 1
                  $(".questions-page__answer-list").hide()
                  $(".vrijeme").hide()


              })
              $(".swal2-close").click(function() {
                  clearInterval(countdownTimer)
                  prekidac = 1
                  $(".questions-page__answer-list").hide()
                  $(".vrijeme").hide()

              })

          } else {
              // Evaluate if the user got the answer right or wrong
              if (userSelectedAnswer === correctAnswer) {

                  // Increment the total correct answers counter
                  correctAnswersCounter++;
                  bodovi += 10;
                  $("#tocno")[0].play();
                  broj = vrijeme + 10
                  swal({
                      title: "<h2><span style='color:green'>Točno!</span></h2><h2 dir='rtl'>صحيح</h2>",
                      html: "+" + broj + "<br><em>" + quiz[questionCounter].opis + "</em><img src='" + quiz[questionCounter].slika + "'class='slikica2'/>",
                      showCloseButton: true,
                      confirmButtonText: ' dalje',
                      backdrop: false,
                      allowOutsideClick: false,
                      allowEscapeKey: false

                  });

                  $(".swal2-confirm").click(function() {
                      clearInterval(countdownTimer)
                      prekidac = 1
                      $(".questions-page__answer-list").hide()
                      $(".vrijeme").hide()


                  })
                  $(".swal2-close").click(function() {
                      clearInterval(countdownTimer)
                      prekidac = 1
                      $(".questions-page__answer-list").hide()
                      $(".vrijeme").hide()

                  })


              } else {
                  highlightIncorrectAnswerRed();
                  bodovi -= 10;
                  $("#krivo")[0].play();

                  swal({
                      title: " <h2><span style='color:#bb422a' >Netočno!</span></h2><h2 dir='rtl'>خطأ</h2>",
                      html: "<p style='text-align:center'><strong>Točan odgovor je <span style='color:#bb422a; font-size:34px' class='comic'  >" + quiz[questionCounter].correctAnswer + "</span></strong>.</p><br><em>" + quiz[questionCounter].opis + "</em><img src='" + quiz[questionCounter].slika + " 'class='slikica2'/>",
                      showCloseButton: true,
                      confirmButtonText: ' dalje',
                      backdrop: false,
                      allowOutsideClick: false,
                      allowEscapeKey: false
                  });

                  $(".swal2-confirm").click(function() {
                      clearInterval(countdownTimer)
                      prekidac = 1
                      $(".questions-page__answer-list").hide()
                      $(".vrijeme").hide()

                  })
                  $(".swal2-close").click(function() {
                      clearInterval(countdownTimer)
                      prekidac = 1
                      $(".questions-page__answer-list").hide()
                      $(".vrijeme").hide()

                  })
              }
          }

          // Substitute the submit button for the continue button:
          submitBtn.hide(300);
          nastavi()
      }
      // Clicking on the submit button:





      function nastavi() {
          // Increment question number until there are no more questions, then advance to the next page
          if (questionCounter < quiz.length - 1) {
              questionCounter++;
          } else {
              document.getElementsByClassName('questions-page')[0].style.display = "none"

              document.getElementsByClassName('sakri')[0].style.display = "block"
                  // Display user score as a percentage
                  // Display user score as a percentage
              postotak = Math.floor((correctAnswersCounter / quiz.length) * 100)
              userScore.text(postotak + " %");
              prikazBodova.text(bodovi);
              $("#60656686").attr("value", bodovi)

              if (postotak >= 90) {
                  ocjena = 5
              } else if (postotak >= 80) {
                  ocjena = 4
              } else if (postotak >= 70) {
                  ocjena = 3
              } else if (postotak >= 60) {
                  ocjena = 2
              } else {
                  ocjena = 1
              }
              $("#1487903547").attr("value", ocjena)

          }

          // Load the next question and set of answers
          generateQuestionAndAnswers();

          // Store the correct answer in a variable
          getCorrectAnswer();

          // Remove all selections, highlighting, and feedback
          deselectAnswer();
          clearHighlightsAndFeedback();


          // Hide the continue button
          continueBtn.hide(300);

          // Enable ability to select an answer
          answerDiv.on('click', function() {
              // Make the submit button visible
              // Remove pointer from any answer that already has it
              deselectAnswer();
              // Put pointer on clicked answer
              selectAnswer(this);
              // Store current answer div for highlighting purposes
              getSelectedAnswerDivs(this);
              // Store current selection as user answer
              getUserAnswer(this);
              odgovor()
          });

      }

      // Clicking on the continue button:
      continueBtn.on('click', function() {

      });

      $(".questions-page__answer-div").dblclick(function() {
              odgovor()
          })
          /* --- PAGE 3/3 --- */

      // Clicking on the retake button:
      retakeBtn.on('click', function() {
          // Go to the first page
          // Start the quiz over
          newQuiz();
          resultsPage.hide();
          questionsPage.show(300);
          // Load question and answers
          generateQuestionAndAnswers();
          // Store the correct answer in a variable
          getCorrectAnswer();
          // Hide the submit and continue buttons
          submitBtn.hide();
          continueBtn.hide();
      });

      // Clicking on the spanish button:
      // Link takes user to Duolingo

  });

  function touchHandler(event) {
      var touches = event.changedTouches,
          first = touches[0],
          type = "";
      switch (event.type) {
          case "touchstart":
              type = "mousedown";
              break;
          case "touchmove":
              type = "mousemove";
              break;
          case "touchend":
              type = "mouseup";
              break;
          default:
              return;
      }


      // initMouseEvent(type, canBubble, cancelable, view, clickCount, 
      //                screenX, screenY, clientX, clientY, ctrlKey, 
      //                altKey, shiftKey, metaKey, button, relatedTarget);

      var simulatedEvent = document.createEvent("MouseEvent");
      simulatedEvent.initMouseEvent(type, true, true, window, 1,
          first.screenX, first.screenY,
          first.clientX, first.clientY, false,
          false, false, false, 0 /*left*/ , null);

      first.target.dispatchEvent(simulatedEvent);
      event.preventDefault();
  }
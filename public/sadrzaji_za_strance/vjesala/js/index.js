//inicijalizacija vrijednosti i funkcija
(function($, window, undefined) {
    var pogreske = 0;
    var A = 0;
    var B = 0;
    var C = 0;
    var CC = 0;
    var CCC = 0;
    var D = 0;
    var DJ = 0;
    var E = 0;
    var F = 0;
    var G = 0;
    var H = 0;
    var I = 0;
    var J = 0;
    var K = 0;
    var L = 0;
    var M = 0;
    var N = 0;
    var O = 0;
    var P = 0;
    var R = 0;
    var S = 0;
    var SS = 0;
    var T = 0;
    var U = 0;
    var V = 0;
    var Z = 0;
    var ZZ = 0;
    var space = 0;
    var rand_broj = 0;
    var win = 0;
    var lose = 0;
    var tocno = 0;
    var resetiraj = 0;
    var postotak = 0;
    var zvuk = 1;
    var pokusaj = 0;
    var vrime = 0;

    Hangman = {
        init: function(words) {
            this.words = words,
                this.hm = $(".hangman"),
                this.msg = $(".message"),
                this.msgTitle = $(".title"),
                this.msgText = $(".text"),
                this.restart = $(".restart"),
                this.wrd = this.randomWord(),
                this.correct = 0,
                this.guess = $(".guess"),
                this.wrong = $(".wrong"),
                this.wrongGuesses = [],
                this.rightGuesses = [],
                this.guessForm = $(".guessForm"),
                this.guessLetterInput = $(".guessLetter"),
                this.goodSound = new Audio("zvuk/goodbell.ogg"),
                this.badSound = new Audio("zvuk/krivo.mp3"),
                this.winSound = new Audio("zvuk/tocno.mp3"),
                this.loseSound = new Audio("zvuk/krivo.ogg"),
                this.ProgressCountdown(40, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => this.isteklo());
            this.setup();
        },


        setup: function() {
            this.binding();
            this.sounds();
            this.showGuess(this.wrongGuesses);
            this.showWrong();

        },

        //glasnoće zvuka
        sounds: function() {
            this.badSound.volume = .4;
            this.goodSound.volume = .4;
            this.winSound.volume = .8;
            this.loseSound.volume = .4;

        },

        //funkcija za gumbe
        binding: function() {
            this.guessForm.on("submit", $.proxy(this.theGuess, this));
            this.restart.on("click", $.proxy(this.theRestart, this));

        },

        //funkcija za puštanje gumba
        playSound: function(sound) {
            this.stopSound(sound);
            this[sound].play();
        },

        //funkcija za zaustavljanje zvuka
        stopSound: function(sound) {
            this[sound].pause();
            this[sound].currentTime = 0;

        },

        //funkcija resetiranje igre
        theRestart: function(e) {
            if (resetiraj == 0) {
                e.preventDefault();
                this.stopSound("winSound");
                this.stopSound("loseSound");
                pogreske = 0;
                document.hm.src = "hm0.png";
                win = 0;
                lose = 0;
                this.reset();
            } else {
                location.reload();
            }
        },


        //funkcija pogađanja
        theGuess: function(e) {
            e.preventDefault();
            if (A == 1) {
                var guess = "A";
                A = 0;
            } else if (B == 1) {
                var guess = "B";
                B = 0;
            } else if (C == 1) {
                var guess = "C";
                C = 0;
            } else if (CC == 1) {
                var guess = "Č";
                CC = 0;
            } else if (CCC == 1) {
                var guess = "Ć";
                CCC = 0;
            } else if (D == 1) {
                var guess = "D";
                D = 0;
            } else if (DJ == 1) {
                var guess = "Đ";
                DJ = 0;
            } else if (E == 1) {
                var guess = "E";
                E = 0;
            } else if (F == 1) {
                var guess = "F";
                F = 0;
            } else if (G == 1) {
                var guess = "G";
                G = 0;
            } else if (H == 1) {
                var guess = "H";
                H = 0;
            } else if (I == 1) {
                var guess = "I";
                I = 0;
            } else if (J == 1) {
                var guess = "J";
                J = 0;
            } else if (K == 1) {
                var guess = "K";
                K = 0;
            } else if (L == 1) {
                var guess = "L";
                L = 0;
            } else if (M == 1) {
                var guess = "M";
                M = 0;
            } else if (N == 1) {
                var guess = "N";
                N = 0;
            } else if (O == 1) {
                var guess = "O";
                O = 0;
            } else if (P == 1) {
                var guess = "P";
                P = 0;
            } else if (R == 1) {
                var guess = "R";
                R = 0;
            } else if (S == 1) {
                var guess = "S";
                S = 0;
            } else if (SS == 1) {
                var guess = "Š";
                SS = 0;
            } else if (T == 1) {
                var guess = "T";
                T = 0;
            } else if (U == 1) {
                var guess = "U";
                U = 0;
            } else if (V == 1) {
                var guess = "V";
                V = 0;
            } else if (Z == 1) {
                var guess = "Z";
                Z = 0;
            } else if (ZZ == 1) {
                var guess = "Ž";
                ZZ = 0;
            } else if (space == 1) {
                var guess = " ";
                space = 0;
            } else {
                var guess = this.guessLetterInput.val().toUpperCase();
            }

            //dozvoljena slova
            if (guess.match(/^[a-žA-Ž\s]*$/) && guess.length == 1) {
                pokusaj += 1
                    //ako se ponovi isto slovo
                if ($.inArray(guess, this.rightGuesses) > -1 || $.inArray(guess, this.wrongGuesses) > -1) {
                    this.playSound("badSound");
                    this.guessLetterInput.val("").focus();

                } else if (guess) {
                    //ako se upiše več krivo uneseno slovo
                    var foundLetters = this.checkGuess(guess);
                    if (foundLetters.length > 0) {
                        this.setLetters(foundLetters);
                        if (zvuk == 1) {
                            this.playSound("goodSound");
                            this.rightGuesses.push(guess);

                        } else {
                            zvuk = 1
                        }
                        this.guessLetterInput.val("").focus();
                    } else {
                        //ako se upiše pogrešno slovo
                        this.wrongGuesses.push(guess);
                        if (this.wrongGuesses.length == 5) {
                            this.lose();
                        } else {
                            this.showWrong(this.wrongGuesses);
                            this.playSound("badSound");
                            pogreske++;
                            document.hm.src = "hm" + pogreske + ".png";
                        }
                        this.guessLetterInput.val("").focus();
                    }
                }
            } else {
                this.guessLetterInput.val("").focus();
            }
        },

        ProgressCountdown: function(timeleft, bar, text) {
            if (window.countdownTimer) {
                clearInterval(countdownTimer);
            }
            return new Promise((resolve, reject) => {
                countdownTimer = setInterval(() => {
                    timeleft--;
                    document.getElementById(bar).value = timeleft;
                    document.getElementById(text).textContent = timeleft;
                    if (timeleft <= 0) {
                        clearInterval(countdownTimer);
                        resolve(true);
                    } else if (timeleft <= 1) {
                        $("#sekunde").html("sekunda")
                        $("#ostalo").html("ostala")
                    } else if (timeleft <= 4) {
                        $("#sekunde").html("sekunde")
                    }

                }, 1000);
            });
        },
        //random riječ izaberi
        randomWord: function() {
            rand_broj = Math.floor(Math.random() * this.words.length);
            $("#slicica").attr("src", "slike/" + slicice[rand_broj] + ".png");
            return this._wordData(this.words[rand_broj]);

        },
        showGuess: function() {
            var frag = "<ul class='word'>";
            $.each(this.wrd.letters, function(key, val) {

                frag += "<li data-pos='" + key + "' class='letter'>*</li>";
            });
            frag += "</ul>";
            this.guess.html(frag);

            rijec = Hangman.wrd["word"].toString()
            for (var i = 0; i < rijec.length; i++) {
                if (rijec.charAt(i) == " ") {
                    space = 1
                    zvuk = 0
                    $(".guessForm").submit();
                }
            }
        },

        //krivo napisana slova
        showWrong: function(wrongGuesses) {
            if (wrongGuesses) {
                var frag = "<ul class='wrongLetters'>";
                frag += "<p>Pogrešna slova: </p>";
                $.each(wrongGuesses, function(key, val) {
                    frag += "<li>" + val + "</li>";
                });
                frag += "</ul>";
            } else {
                frag = "";
            }
            this.wrong.html(frag);
        },

        //pogođena slova
        checkGuess: function(guessedLetter) {
            var _ = this;
            var found = [];
            $.each(this.wrd.letters, function(key, val) {
                if (guessedLetter == val.letter.toUpperCase()) {
                    found.push(val);
                    _.rightGuesses.push(val.letter);
                }
            });
            return found;
        },

        setLetters: function(letters) {
            var _ = this;
            _.correct = _.correct += letters.length;
            $.each(letters, function(key, val) {
                var letter = $("li[data-pos=" + val.pos + "]");

                letter.html(val.letter);
                letter.addClass("correct");
                if (val.letter == " ") {
                    letter.addClass("sakrij");
                }
                if (_.correct == _.wrd.letters.length) {
                    _.win();
                }
            });
        },

        _wordData: function(word) {

            return {
                letters: this._letters(word),
                word: word.toUpperCase(),
                totalLetters: word.length
            };
        },


        hideMsg: function() {
            this.msg.hide();
            this.msgTitle.hide();
            this.restart.hide();
            this.msgText.hide();
        },

        //pokaži porku
        showMsg: function() {
            var _ = this;
            _.msg.show("blind", function() {
                _.msgTitle.show("bounce", "slow", function() {
                    _.msgText.show("slide", function() {
                        _.restart.show("fade");
                    });

                });

            }, 4000);
        },


        reset: function() {
            this.hideMsg(); $(".guessForm").show();
            $(window).scrollTop(0);
            this.init(this.words);
            this.hm.find(".guessLetter").focus();
        },


        _letters: function(word) {
            var letters = [];
            for (var i = 0; i < word.length; i++) {
                letters.push({
                    letter: word[i],
                    pos: i
                });
            }
            return letters;
        },


        rating: function() {
            var right = this.rightGuesses.length,
                wrong = this.wrongGuesses.length || 0,
                rating = {
                    rating: Math.floor((right / (wrong + right)) * 100),
                    guesses: (right + wrong)

                };
            return rating;
        },

        win: function() {
            win = 1;
            var rating = this.rating();
            tocno++;
            vrijeme = parseInt($("#pageBeginCountdownText").text())
            vrime += parseInt($("#pageBeginCountdownText").text())
            clearInterval(countdownTimer);
            if (wordList.length == 1) {
                resetiraj = 1;
                postotak += rating.rating;
                this.msgTitle.html("Završili ste igru!");
                // this is messy
                rezultat = (postotak / broj_pitanja) + vrime
                $("#60656686").attr("value", rezultat)

                if (postotak >= 90) {
                    ocjena = 5
                } else if (postotak >= 80) {
                    ocjena = 4
                } else if (postotak >= 70) {
                    ocjena = 3
                } else if (postotak >= 60) {
                    ocjena = 2
                } else {
                    ocjena = 1
                }
                $(".guess").remove()
                $("#1487903547").attr("value", ocjena)
                this.msgText.html("Posljednje odgovor <span class='highlight'>" + this.wrd.word + "</span><br>pogođen je u <span class='highlight'>" + pokusaj + "</span> pokušaja. <p>Dodatni bodovi ostvareni s brzim odgovorima na vrijeme:<p><p dir='rtl'>نقاط اضافية مع أوقات استجابة سريعة:</p> <span class='highlight'>" + vrime + "</span><p>Rezultat:<p><p dir='rtl'>النتيجة:</p> <span class='highlight'>" + rating.rating + "%</span><p>Ukupni broj bodova u igri:</p><p dir='rtl'>مجموعة النقاط في اللعبة </p> <span class='highlight'>" + rezultat + "</span>");
                this.showMsg(); $(".guessForm").hide(); $(".wrong").hide(); 
                $(".jedan").show()
                this.playSound("winSound");
                wordList.splice(rand_broj, 1);
                slicice.splice(rand_broj, 1);
                $("#gumb").html("<span>nova igra</span><br><span dir='rtl'>لعبة جديدة</span>");
                $("#predaj").show()
                lose = 1;
                tocno = 0;
                Hangman.init(wordList);
            } else {
                wordList.splice(rand_broj, 1);
                slicice.splice(rand_broj, 1);
                this.msgTitle.html("<p>Čestitamo!</p><p dir='rtl'>تهانينا</p>");
                // this is messy
                this.msgText.html("<p>Pogodili ste odgovor <span class='highlight'>" + this.wrd.word + "</span><br> u <span class='highlight'>" + pokusaj + "</span> pokušaja.</p><p dir='rtl'>محاولات <span class='highlight'>" + this.wrd.word + "</span><br> إجابة في <span class='highlight'>" + pokusaj + "</span> لقد حققت.</p><p>Vrijeme potrebno za odgovor:</p><p dir='rtl'>زمن الإجابة:</p> <p><span class='highlight'>" + (40 - vrijeme) + "</span> s<br></p><p>Rezultat:</p><p>النتيجة</p><p> <span class='highlight'>" + rating.rating + "%</span></p> <p>Broj točno napisanih riječi:</p> <p dir='rtl'>عدد الحلول (الإجابات) الصحيحة:</p> <span class='highlight'>" + tocno + "/" + broj_pitanja + "</span>");
                this.showMsg(); $(".guessForm").hide(); $(".wrong").hide(); 
                this.playSound("winSound");
                postotak += rating.rating;
                $("#gumb").html("Nova riječ");
                pokusaj = 0;
            }

        },


        lose: function() {
            lose = 1;
            this.msgTitle.html("<p>Izvisio si!</p><img src='hm6.png' class='velicina'><p>Odgovor je:<\p> <p dir='rtl'>الإجابة هي:<\p> <p><span class='highlight'>" + this.wrd.word + "</span></p>");
            this.msgText.html("<p>Broj točno riješenih:</p><p dir='rtl'>عدد الحلول (الإجابات) الصحيحة:</p> <p><span class='highlight'>" + tocno + "/" + broj_pitanja + "</span></p>" + "<p>Ne uzrujavaj se i pokušaj ponovno.</p><p dir='rtl'>لا تيأس وحاول مرة أخرى.</dir>");
            this.showMsg(); $(".guessForm").hide();  $(".wrong").hide(); 
            this.playSound("loseSound");
            clearInterval(countdownTimer);
            $("#gumb").html("<span>nova igra</span><br><span dir='rtl'>لعبة جديدة</span>");
            tocno = 0;
        },

        isteklo: function() {
            clearInterval(countdownTimer);

            lose = 1;
            resetiraj = 1;
            this.msgTitle.html("Isteklo je vrijeme!<br><img src='sat.png' class='velicina'><br>Odgovor je : <span class='highlight'>" + this.wrd.word + "</span>");
            this.msgText.html("Broj točno riješenih: <span class='highlight'>" + tocno + "/" + broj_pitanja + "</span>" + "<br>Ne uzrujavaj se i pokušaj ponovno.");
            this.showMsg(); $(".guessForm").hide();  $(".wrong").hide(); 
            this.playSound("loseSound");

            Hangman.init(wordList);
            $("#gumb").html("<span>nova igra</span><br><span dir='rtl'>لعبة جديدة</span>");
            tocno = 0;
        }

    };


    var wordList = ["baka", "Europa", "oko", "vilica", "zub", "jaje"]
    var broj_pitanja = wordList.length
    hintovi = ["", "", "", "", "", ""];
    var slicice = ["baka", "europa", "oko", "vilica", "zub", "jaje"]
    var duljina = wordList.length;


    Hangman.init(wordList);




    $("body").keyup(function(event) {
        if (win == 1 || lose == 1) {
            return
        } else {
            if (event.which == 65) {
                if (A == 0) {
                    A = 1
                    $(".guessForm").submit();
                } else {
                    A = 0
                };
            } else if (event.which == 69) {
                if (E == 0) {
                    E = 1
                    $(".guessForm").submit();
                } else {
                    E = 0
                };
            } else if (event.which == 66) {
                if (B == 0) {
                    B = 1
                    $(".guessForm").submit();
                } else {
                    B = 0
                };
            } else if (event.which == 67) {
                if (C == 0) {
                    C = 1
                    $(".guessForm").submit();
                } else {
                    C = 0
                };
            } else if (event.which == 59) {
                if (CC == 0) {
                    CC = 1
                    $(".guessForm").submit();
                } else {
                    CC = 0
                };

            } else if (event.which == 222) {
                if (CCC == 0) {
                    CCC = 1
                    $(".guessForm").submit();
                } else {
                    CCC = 0
                };
            } else if (event.which == 68) {
                if (D == 0) {
                    D = 1
                    $(".guessForm").submit();
                } else {
                    D = 0
                };
            } else if (event.which == 221) {
                if (DJ == 0) {
                    DJ = 1
                    $(".guessForm").submit();
                } else {
                    DJ = 0
                };
            } else if (event.which == 70) {
                if (F == 0) {
                    F = 1
                    $(".guessForm").submit();
                } else {
                    F = 0
                };
            } else if (event.which == 71) {
                if (G == 0) {
                    G = 1
                    $(".guessForm").submit();
                } else {
                    G = 0
                };

            } else if (event.which == 72) {
                if (H == 0) {
                    H = 1
                    $(".guessForm").submit();
                } else {
                    H = 0
                };
            } else if (event.which == 73) {
                if (I == 0) {
                    I = 1
                    $(".guessForm").submit();
                } else {
                    I = 0
                };
            } else if (event.which == 74) {
                if (J == 0) {
                    J = 1
                    $(".guessForm").submit();
                } else {
                    J = 0
                };
            } else if (event.which == 75) {
                if (K == 0) {
                    K = 1
                    $(".guessForm").submit();
                } else {
                    K = 0
                };
            } else if (event.which == 76) {
                if (L == 0) {
                    L = 1
                    $(".guessForm").submit();
                } else {
                    L = 0
                };
            } else if (event.which == 77) {
                if (M == 0) {
                    M = 1
                    $(".guessForm").submit();
                } else {
                    M = 0
                };
            } else if (event.which == 78) {
                if (N == 0) {
                    N = 1
                    $(".guessForm").submit();
                } else {
                    N = 0
                };
            } else if (event.which == 79) {
                if (O == 0) {
                    O = 1
                    $(".guessForm").submit();
                } else {
                    O = 0
                };
            } else if (event.which == 80) {
                if (P == 0) {
                    P = 1
                    $(".guessForm").submit();
                } else {
                    P = 0
                };

            } else if (event.which == 82) {
                if (R == 0) {
                    R = 1
                    $(".guessForm").submit();
                } else {
                    R = 0
                };
            } else if (event.which == 83) {
                if (S == 0) {
                    S = 1
                    $(".guessForm").submit();
                } else {
                    S = 0
                };

            } else if (event.which == 219) {

                if (SS == 0) {
                    SS = 1
                    $(".guessForm").submit();
                } else {
                    SS = 0
                };
            } else if (event.which == 84) {

                if (T == 0) {
                    T = 1
                    $(".guessForm").submit();
                } else {
                    T = 0
                };
            } else if (event.which == 85) {

                if (U == 0) {
                    U = 1
                    $(".guessForm").submit();
                } else {
                    U = 0
                };
            } else if (event.which == 86) {

                if (V == 0) {
                    V = 1
                    $(".guessForm").submit();
                } else {
                    V = 0
                };
            } else if (event.which == 90) {

                if (Z == 0) {
                    Z = 1
                    $(".guessForm").submit();
                } else {
                    Z = 0
                };
            } else if (event.which == 220) {

                if (ZZ == 0) {
                    ZZ = 1
                    $(".guessForm").submit();
                } else {
                    ZZ = 0
                };
            }
        }

    })

    $(document).ready(function() {
        $("#A").click(function() { //č
            if (A == 0) {
                A = 1
                $(".guessForm").submit();
            } else {
                A = 0
            };

        });


        $("#B").click(function() { //ć
            if (B == 0) {
                B = 1
                $(".guessForm").submit();
            } else {
                B = 0
            };

        });


        $("#C").click(function() { //ć

            if (C == 0) {
                C = 1
                $(".guessForm").submit();
            } else {
                C = 0
            };

        });

        $("#CC").click(function() { //ć

            if (CC == 0) {
                CC = 1
                $(".guessForm").submit();
            } else {
                CC = 0
            };

        });
        $("#CCC").click(function() { //ć

            if (CCC == 0) {
                CCC = 1
                $(".guessForm").submit();
            } else {
                CCC = 0
            };

        });
        $("#D").click(function() { //ć

            if (D == 0) {
                D = 1
                $(".guessForm").submit();
            } else {
                D = 0
            };

        });
        $("#DJ").click(function() { //ć

            if (DJ == 0) {
                DJ = 1
                $(".guessForm").submit();
            } else {
                DJ = 0
            };

        });
        $("#E").click(function() { //ć

            if (E == 0) {
                E = 1
                $(".guessForm").submit();
            } else {
                E = 0
            };

        });
        $("#F").click(function() { //ć

            if (F == 0) {
                F = 1
                $(".guessForm").submit();
            } else {
                F = 0
            };

        });
        $("#G").click(function() { //ć

            if (G == 0) {
                G = 1
                $(".guessForm").submit();
            } else {
                G = 0
            };

        });
        $("#H").click(function() { //ć

            if (H == 0) {
                H = 1
                $(".guessForm").submit();
            } else {
                H = 0
            };

        });


        $("#I").click(function() { //ć

            if (I == 0) {
                I = 1
                $(".guessForm").submit();
            } else {
                I = 0
            };

        });
        $("#J").click(function() { //ć

            if (J == 0) {
                J = 1
                $(".guessForm").submit();
            } else {
                J = 0
            };

        });
        $("#K").click(function() { //ć

            if (K == 0) {
                K = 1
                $(".guessForm").submit();
            } else {
                K = 0
            };

        });

        $("#L").click(function() { //ć

            if (L == 0) {
                L = 1
                $(".guessForm").submit();
            } else {
                L = 0
            };

        });
        $("#M").click(function() { //ć

            if (M == 0) {
                M = 1
                $(".guessForm").submit();
            } else {
                M = 0
            };

        });
        $("#N").click(function() { //ć

            if (N == 0) {
                N = 1
                $(".guessForm").submit();
            } else {
                N = 0
            };

        });
        $("#O").click(function() { //ć

            if (O == 0) {
                O = 1
                $(".guessForm").submit();
            } else {
                O = 0
            };

        });
        $("#P").click(function() { //ć

            if (P == 0) {
                P = 1
                $(".guessForm").submit();
            } else {
                P = 0
            };

        });
        $("#R").click(function() { //ć

            if (R == 0) {
                R = 1
                $(".guessForm").submit();
            } else {
                R = 0
            };

        });
        $("#S").click(function() { //ć

            if (S == 0) {
                S = 1
                $(".guessForm").submit();
            } else {
                S = 0
            };

        });
        $("#SS").click(function() { //ć

            if (SS == 0) {
                SS = 1
                $(".guessForm").submit();
            } else {
                SS = 0
            };

        });
        $("#T").click(function() { //ć

            if (T == 0) {
                T = 1
                $(".guessForm").submit();
            } else {
                T = 0
            };

        });
        $("#U").click(function() { //ć

            if (U == 0) {
                U = 1
                $(".guessForm").submit();
            } else {
                U = 0
            };

        });
        $("#V").click(function() { //ć

            if (V == 0) {
                V = 1
                $(".guessForm").submit();
            } else {
                V = 0
            };

        });
        $("#Z").click(function() { //ć

            if (Z == 0) {
                Z = 1
                $(".guessForm").submit();
            } else {
                Z = 0
            };

        });

        $("#ZZ").click(function() { //ć

            if (ZZ == 0) {
                ZZ = 1
                $(".guessForm").submit();
            } else {
                ZZ = 0
            };

        });

        $("#space").click(function() { //ć

            if (space == 0) {
                space = 1
                $(".guessForm").submit();
            } else {
                space = 0
            };

        });

    });

    $(document).keypress(function(e) {
        if (e.which == 13 && (win == 1 || lose == 1)) {
            $(".restart").trigger("click");
        }
    });


    var el = document.getElementById('slovo');

    el.focus();

    el.onblur = function() {
        setTimeout(function() {
            el.focus();
        });
    };

})(jQuery, window);
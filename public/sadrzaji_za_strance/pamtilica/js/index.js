

// Memory Game
// © 2014 Nate Wiley
// License -- MIT
// best in full screen, works on phones/tablets (min height for game is 500px..) enjoy ;)
// Follow me on Codepen
$("footer").hide();
var razina = 1;
var broj_karata = 3;
$(".zivotinje").click(function () {
	$(".modal").html("<h2 class='winner'>Odaberi broj parova:</h2><button id='prva'>4</button> <button id='druga'>8</button><button id='treca'>12</button>");
	$("#prva").click(function () {
		razina = "1";
		igra()
	})
	$("#druga").click(function () {
		razina = "2";
		igra();
$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"});
	})
	$("#treca").click(function () {
	razina = "3";
	igra();
	$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
	})

	function igra() {

		if (razina == 1) {
			broj_karata = 4;

		} else if (razina == 2) {
			broj_karata = 8;
		} else {
			broj_karata = 12
		}
		$("footer").fadeIn(1000);
		$(".modal").fadeOut(1000);
		$(".modal-overlay").delay(1000).slideUp(1000);
		$(".game").show("slow");
		$("#okretanje")[0].play();
		//localStorage.clear();
		var br = 1;
		var sec = 0;
		var pokusaj = 0;
		var vrijeme = 1;
		var bodovi = 0;
		var najbolje_vrijeme;
		var najmanji_broj_pokusaja;
		var karte;
		function pad(val) {
			return val > 9 ? val : "0" + val;
		}
		setInterval(function () {
			if (vrijeme == 1) {
				$("#seconds").html(pad(++sec % 60));
				$("#minutes").html(pad(parseInt(sec / 60, 10)));
			}
		}, 1000);

		var Memory = {
			init: function (cards) {
				this.$game = $(".game");
				this.$modal = $(".modal");
				this.$overlay = $(".modal-overlay");
				this.$zivotinje = $(".zivotinje");
				this.$ljudi = $(".ljudi");
				this.cardsArray = $.merge(cards, cards);
				this.shuffleCards(this.cardsArray);
				this.setup();
			},

			shuffleCards: function (cardsArray) {
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			setup: function () {
				this.html = this.buildHTML();
				this.$game.html(this.html);
				this.$memoryCards = $(".card");
				this.binding();
				this.paused = false;
				this.guess = null;
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			binding: function () {
				this.$memoryCards.on("click", this.cardClicked);
				this.$zivotinje.on("click", $.proxy(this.reset, this));
			},
			// kinda messy but hey
			cardClicked: function () {
				$("#okret")[0].play();

				var _ = Memory;
				var $card = $(this);
				if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

					$card.find(".inside").addClass("picked");
					if (!_.guess) {
						_.guess = $(this).attr("data-id");
						$(this).find('p').toggle();
					} else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
						$(".picked").addClass("matched");
						switch ($(this).find('img').attr('alt')) {

							case "mačak":
								$("#maca")[0].play();
								break;
							case "mačka":
								$("#maca")[0].play();
								break;
							case "guska":
								$("#guska")[0].play();
								break;
							case "gusak":
								$("#guska")[0].play();
								break;
							case "jelen":
								$("#jelen")[0].play();
								break;
							case "košuta":
								$("#jelen")[0].play();
								break;
							case "kokoš":
								$("#kokos")[0].play();
								break;
							case "pijetao":
								$("#kokos")[0].play();
								break;
							case "koza":
								$("#koza")[0].play();
								break;
							case "jarac":
								$("#koza")[0].play();
								break;
							case "lav":
								$("#lav")[0].play();
								break;
							case "lavica":
								$("#lav")[0].play();
								break;
							case "lisica":
								$("#lisica")[0].play();
								break;
							case "lisac":
								$("#lisica")[0].play();
								break;
							case "ovca":
								$("#ovca")[0].play();
								break;
							case "ovan":
								$("#ovca")[0].play();
								break;
							case "pas":
								$("#peso")[0].play();
								break;
							case "kuja":
								$("#peso")[0].play();
								break;
							case "patka":
								$("#patka")[0].play();
								break;
							case "patak":
								$("#patka")[0].play();
								break;
							case "pčela":
								$("#pcela")[0].play();
								break;
							case "trut":
								$("#pcela")[0].play();
								break;
							case "slon":
								$("#slon")[0].play();
								break;
							case "slonica":
								$("#slon")[0].play();
								break;
							case "srna":
								$("#jelen")[0].play();
								break;
							case "srndać":
								$("#jelen")[0].play();
								break;
							case "žaba":
								$("#zaba")[0].play();
								break;
							case "žabac":
								$("#zaba")[0].play();
								break;
							case "majmun":
								$("#majmun")[0].play();
								break;
							case "majmunica":
								$("#majmun")[0].play();
								break;
						}

						bodovi = bodovi + 15;
						_.guess = null;
						$(".matched").find('p').remove();
						pokusaj++;
					} else {
						pokusaj++;
						$(this).find('p').toggle();
						_.guess = null;
						_.paused = true;
						setTimeout(function () {
							$(".picked").removeClass("picked");
							Memory.paused = false;
							$(".brojevi").show();
							bodovi = bodovi - 5
						}, 1200);
					}
					if ($(".matched").length == $(".card").length) {
						_.win();
					}
				}
			},

			win: function () {
				this.paused = true;
				setTimeout(function () {
					Memory.showModal();
					Memory.$game.fadeOut();
				}, 1000);
			},

			showModal: function () {
				var minute = Math.floor(sec / 60);
				var sekunde = sec - minute * 60;
				this.$overlay.show();
				this.$modal.fadeIn("slow");
				var najvrijeme = localStorage.getItem('najvrijeme');

				if (najvrijeme === undefined || najvrijeme === null) {
					najvrijeme = sec;
					localStorage.setItem('najvrijeme', sec);
				}

				// If the user has more points than the currently stored high score then
				if (sec < najvrijeme) {
					// Set the high score to the users' current points
					najvrijeme = sec;
					// Store the high score
					localStorage.setItem('najvrijeme', sec);
				}
				// Return the high score

				var najpokusaji = localStorage.getItem('najpokusaji');

				if (najpokusaji === undefined || najpokusaji === null) {
					najpokusaji = pokusaj;
					localStorage.setItem('najpokusaji', pokusaj);
				}
				// If the user has more points than the currently stored high score then
				if (pokusaj < najpokusaji) {
					// Set the high score to the users' current points
					najpokusaji = pokusaj;
					// Store the high score
					localStorage.setItem('najpokusaji', pokusaj);
				}
				var naj_minute = Math.floor(najvrijeme / 60);
				var naj_sekunde = najvrijeme - naj_minute * 60;
				$(".modal").show();
				$(".modal-overlay").show();
				$(".winner").hide();
				bodovi = bodovi - sec
				$(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja: " + pokusaj + "</br>vrijeme igre: " + minute + ":" + sekunde + "</br><p><form id='input-form' action='' method='POST' target='no-target'><br><select id='ikona' style='height:30px'></select> <label for='ime'> Ime: </label><input style='height:30px' id='input-q1' required name='q1'><br> <label for='bodovi'>Bodovi: </label><input id='input-q2' placeholder='q2' name='q2' value='" + bodovi + "' disabled style='display:none'> <label for='bodovi'>" + bodovi + "</label><br><button id='form-submit' type='submit'>predaj rezultat</button> </form>    <iframe src='#' id='no-target' name='no-target' style='visibility:hidden;display:none'></iframe><br><a href='index.html' style='color:black;'>nova igra</a></p></div>");

				var target = document.getElementById("ikona");
				var emojiCount = emoji.length;

				for (var index = 0; index < emojiCount; index++) {
					addEmoji(emoji[index]);
				}
				function addEmoji(code) {
					var option = document.createElement('option');
					option.innerHTML = code;
					option.value = code;
					target.appendChild(option);
				}
				if (localStorage.getItem("ime") != null) {
					$('#input-q1').val(localStorage.getItem("ime"))
					$('#ikona').val(localStorage.getItem("ikona"))
				}
				if (razina == 1) {
					$('#input-form').one('submit', function () {
                    $(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLScvLUeV4g2gsbsS9bnwauoCLPMkXJ8FLxv_vtbuI8zL8djtYw/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez.html';
							}, 1000);
					});
				} else if (razina == 2) {
					$('#input-form').one('submit', function () {
                    $(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSc0vV1EX7fzTpVkO6L61b1OprPPUAAqD7DYYa9PQNbr5QhFnQ/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez2.html';
							}, 1000);
					});
				} else {
					$('#input-form').one('submit', function () {
                    $(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";
						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSc8MB7meCvrSeLMz-MX7wGD4VQXWLE4QivxGbMa6PwokZpNiQ/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez3.html';
							}, 1000);
					});
				}


			},

			hideModal: function () {
				this.$overlay.hide();
				this.$modal.hide();
			},

			reset: function () {
				this.hideModal();
				this.shuffleCards(this.cardsArray);
				this.setup();
				this.$game.show("slow");
				pokusaj = 0;
				sec = 0;
				br = 1;
				$(".back").addClass("pozadina-zivotinje");
			},

			// Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
			shuffle: function (array) {
				var counter = array.length,
					temp, index;
				// While there are elements in the array
				while (counter > 0) {
					// Pick a random index
					index = Math.floor(Math.random() * counter);
					// Decrease counter by 1
					counter--;
					// And swap the last element with it
					temp = array[counter];
					array[counter] = array[index];
					array[index] = temp;
				}
				return array;
			},

			buildHTML: function () {


				var frag = '';
				br = 1;
				var lista_slika = [];
				var lista_imena = [];
				this.$cards.each(function (k, v) {
					if (Math.floor((Math.random() * 2) + 1) == 1) {
						if ($.inArray(v.name, lista_imena) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);

						}
					} else {
						if ($.inArray(v.img, lista_slika) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);

						}
					}
				});
				return frag;
			}
		};


		var cards = [{
			name: "guska",
			img: "slike/zivotinje/guska.jpg",
			id: 1,
		}, {
			name: "jelen",
			img: "slike/zivotinje/jelen.jpg",
			id: 2
		}, {
			name: "kokoš",
			img: "slike/zivotinje/kokos.jpg",
			id: 3
		}, {
			name: "koza",
			img: "slike/zivotinje/koza.jpg",
			id: 4
		}, {
			name: "lav",
			img: "slike/zivotinje/lav.jpg",
			id: 5
		}, {
			name: "lisica",
			img: "slike/zivotinje/lisica.jpg",
			id: 6
		}, {
			name: "mačka",
			img: "slike/zivotinje/macka.jpg",
			id: 7
		}, {
			name: "ovca",
			img: "slike/zivotinje/ovca.jpg",
			id: 8
		}, {
			name: "pas",
			img: "slike/zivotinje/pas.jpg",
			id: 9
		}, {
			name: "patka",
			img: "slike/zivotinje/patka.jpg",
			id: 10
		}, {
			name: "pčela",
			img: "slike/zivotinje/pcela.jpg",
			id: 11
		}, {
			name: "slon",
			img: "slike/zivotinje/slon.jpg",
			id: 12
		}, {
			name: "srna",
			img: "slike/zivotinje/srna.jpg",
			id: 13
		}, {
			name: "žaba",
			img: "slike/zivotinje/zaba.jpg",
			id: 14
		}, {
			name: "majmun",
			img: "slike/zivotinje/majmun.jpg",
			id: 15
		}
		/*, {
					name: "svinja",
					img: "slike/zivotinje/svinja.jpg",
					id: 16
				}, {
					name: "krava",
					img: "slike/zivotinje/krava.jpg",
					id: 17
				}, {
					name: "riba",
					img: "slike/zivotinje/riba.jpg",
					id: 18
				}, {
					name: "patka",
					img: "slike/zivotinje/patka.jpg",
					id: 19
				}, {
					name: "vjeverica",
					img: "slike/zivotinje/vjeverica.jpg",
					id: 20
				}, {
					name: "miš",
					img: "slike/zivotinje/mis.jpg",
					id: 21
				}, {
					name: "jazavac",
					img: "slike/zivotinje/jazavac.jpg",
					id: 23
				}*/
	]

		function shuffle(array) {
			var currentIndex = array.length,
				temporaryValue, randomIndex;

			// While there remain elements to shuffle...
			while (0 !== currentIndex) {

				// Pick a remaining element...
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;

				// And swap it with the current element.
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
			}

			return array;
		}

		cards = shuffle(cards);

		cards = cards.slice(0, broj_karata);

		Memory.init(cards);


		if (razina == 1) {
			$(".card").css({
				"width": "25%",
				"height": "50%"
			})
		} else if (razina == 2) {
			$(".card").css({
				"width": "25%",
				"height": "25%"
			})
		} else if (razina == 3) {
			$(".card").css({
				"width": "16.66666%",
				"height": "25%"
			})
		}
		$(".back").addClass("pozadina-zivotinje");
	}
});
$(".voce").click(function () {
	$(".modal").html("<h2 class='winner'>Odaberi broj parova:</h2><button id='prva'>4</button> <button id='druga'>8</button><button id='treca'>12</button>");
	$("#prva").click(function () {
		razina = "1";
		igra()
	})
	$("#druga").click(function () {
		razina = "2";
		igra();
$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"});
	})
	$("#treca").click(function () {
razina = "3";
igra();
$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
	})

	function igra() {

		if (razina == 1) {
			broj_karata = 4;

		} else if (razina == 2) {
			broj_karata = 8;
		} else {
			broj_karata = 12
		}
		$("footer").fadeIn(1000);
		$(".modal").fadeOut(1000);
		$(".modal-overlay").delay(1000).slideUp(1000);
		$(".game").show("slow");
		$("#okretanje")[0].play();
		//localStorage.clear();
		var br = 1;
		var sec = 0;
		var pokusaj = 0;
		var vrijeme = 1;
		var bodovi = 0;

		var najbolje_vrijeme;
		var najmanji_broj_pokusaja;
		var karte;


		function pad(val) {
			return val > 9 ? val : "0" + val;
		}
		setInterval(function () {
			if (vrijeme == 1) {
				$("#seconds").html(pad(++sec % 60));
				$("#minutes").html(pad(parseInt(sec / 60, 10)));
			}
		}, 1000);

		var Memory = {
			init: function (cards) {
				this.$game = $(".game");
				this.$modal = $(".modal");
				this.$overlay = $(".modal-overlay");
				this.$zivotinje = $(".zivotinje");
				this.$ljudi = $(".ljudi");
				this.cardsArray = $.merge(cards, cards);
				this.shuffleCards(this.cardsArray);
				this.setup();
			},

			shuffleCards: function (cardsArray) {
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			setup: function () {
				this.html = this.buildHTML();
				this.$game.html(this.html);
				this.$memoryCards = $(".card");
				this.binding();
				this.paused = false;
				this.guess = null;
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			binding: function () {
				this.$memoryCards.on("click", this.cardClicked);
				this.$zivotinje.on("click", $.proxy(this.reset, this));
			},
			// kinda messy but hey
			cardClicked: function () {
				$("#okret")[0].play();

				var _ = Memory;
				var $card = $(this);
				if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

					$card.find(".inside").addClass("picked");
					if (!_.guess) {
						_.guess = $(this).attr("data-id");
						$(this).find('p').toggle();
					} else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
						$(".picked").addClass("matched");
						$("#tocno")[0].play();

						bodovi = bodovi + 15;
						_.guess = null;
						$(".matched").find('p').remove();
						pokusaj++;
					} else {
						pokusaj++;
						$(this).find('p').toggle();
						_.guess = null;
						_.paused = true;
						setTimeout(function () {
							$(".picked").removeClass("picked");
							Memory.paused = false;
							$(".brojevi").show();
							bodovi = bodovi - 5
						}, 1200);
					}
					if ($(".matched").length == $(".card").length) {
						_.win();
					}
				}
			},

			win: function () {
				this.paused = true;
				setTimeout(function () {
					Memory.showModal();
					Memory.$game.fadeOut();
				}, 1000);
			},

			showModal: function () {
				var minute = Math.floor(sec / 60);
				var sekunde = sec - minute * 60;
				this.$overlay.show();
				this.$modal.fadeIn("slow");
				var najvrijeme = localStorage.getItem('najvrijeme');

				if (najvrijeme === undefined || najvrijeme === null) {
					najvrijeme = sec;
					localStorage.setItem('najvrijeme', sec);
				}

				// If the user has more points than the currently stored high score then
				if (sec < najvrijeme) {
					// Set the high score to the users' current points
					najvrijeme = sec;
					// Store the high score
					localStorage.setItem('najvrijeme', sec);
				}
				// Return the high score

				var najpokusaji = localStorage.getItem('najpokusaji');

				if (najpokusaji === undefined || najpokusaji === null) {
					najpokusaji = pokusaj;
					localStorage.setItem('najpokusaji', pokusaj);
				}
				// If the user has more points than the currently stored high score then
				if (pokusaj < najpokusaji) {
					// Set the high score to the users' current points
					najpokusaji = pokusaj;
					// Store the high score
					localStorage.setItem('najpokusaji', pokusaj);
				}
				var naj_minute = Math.floor(najvrijeme / 60);
				var naj_sekunde = najvrijeme - naj_minute * 60;
				$(".modal").show();
				$(".modal-overlay").show();
				$(".winner").hide();
				bodovi = bodovi - sec
				$(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja: " + pokusaj + "</br>vrijeme igre: " + minute + ":" + sekunde + "</br><p><form id='input-form' action='' method='POST' target='no-target'><br><select id='ikona' style='height:30px'></select><label for='ime'> Ime: </label><input style='height:30px' id='input-q1' name='q1'><br> <label for='bodovi'>Bodovi: </label><input id='input-q2' placeholder='q2' name='q2' value='" + bodovi + "' disabled style='display:none'> <label for='bodovi'>" + bodovi + "</label><br><button id='form-submit' type='submit'>predaj rezultat</button> </form>    <iframe src='#' id='no-target' name='no-target' style='visibility:hidden;display:none'></iframe><br><a href='index.html' style='color:black;'>nova igra</a></p></div>");

				var target = document.getElementById("ikona");
				var emojiCount = emoji.length;

				for (var index = 0; index < emojiCount; index++) {
					addEmoji(emoji[index]);
				}

				function addEmoji(code) {
					var option = document.createElement('option');
					option.innerHTML = code;
					option.value = code;
					target.appendChild(option);
				}
				if (localStorage.getItem("ime") != null) {
					$('#input-q1').val(localStorage.getItem("ime"))
					$('#ikona').val(localStorage.getItem("ikona"))
				}
				if (razina == 1) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSdQBjazxBs0PC-dJBvoW3E-6OEvAUbVpmIzUdH3DdiWVPDkIQ/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez4.html';
							}, 1000);
					});
				} else if (razina == 2) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSeJqhTrbh9Leqgcd-g6FSPalJC-HTEZa8SovZVC6XC_jf_q5g/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez5.html';
							}, 1000);
					});
				} else {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSeeSq2ftK8gi0WrfXq1mz4l6ORuJZ6NZ8t-i3oDdjZ6IEczlg/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez6.html';
							}, 1000);
					});
				}


			},

			hideModal: function () {
				this.$overlay.hide();
				this.$modal.hide();
			},

			reset: function () {
				this.hideModal();
				this.shuffleCards(this.cardsArray);
				this.setup();
				this.$game.show("slow");
				pokusaj = 0;
				sec = 0;
				br = 1;
				$(".back").addClass("pozadina-voce");
			},

			// Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
			shuffle: function (array) {
				var counter = array.length,
					temp, index;
				// While there are elements in the array
				while (counter > 0) {
					// Pick a random index
					index = Math.floor(Math.random() * counter);
					// Decrease counter by 1
					counter--;
					// And swap the last element with it
					temp = array[counter];
					array[counter] = array[index];
					array[index] = temp;
				}
				return array;
			},

			buildHTML: function () {


				var frag = '';
				br = 1;
				var lista_slika = [];
				var lista_imena = [];
				this.$cards.each(function (k, v) {
					if (Math.floor((Math.random() * 2) + 1) == 1) {
						if ($.inArray(v.name, lista_imena) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);

						}
					} else {
						if ($.inArray(v.img, lista_slika) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);

						}
					}
				});
				return frag;
			}
		};
		var cards = [{
			name: "ananas",
			img: "slike/voce/ananas.jpg",
			id: 1,
		}, {
			name: "banana",
			img: "slike/voce/banana.jpg",
			id: 2
		}, {
			name: "borovnica",
			img: "slike/voce/borovnica.jpg",
			id: 3
		}, {
			name: "breskva",
			img: "slike/voce/breskva.jpg",
			id: 4
		}, {
			name: "dinja",
			img: "slike/voce/dinja.jpg",
			id: 5
		}, {
			name: "grejp",
			img: "slike/voce/grejp.jpg",
			id: 6
		}, {
			name: "grožđe",
			img: "slike/voce/grozde.jpg",
			id: 7
		}, {
			name: "jabuka",
			img: "slike/voce/jabuka.jpg",
			id: 8
		}, {
			name: "jagoda",
			img: "slike/voce/jagoda.jpg",
			id: 9
		}, {
			name: "kivi",
			img: "slike/voce/kivi.jpg",
			id: 10
		}, {
			name: "kruška",
			img: "slike/voce/kruska.jpg",
			id: 11
		}, {
			name: "kupina",
			img: "slike/voce/kupina.jpg",
			id: 12
		}, {
			name: "limun",
			img: "slike/voce/limun.jpg",
			id: 13
		}, {
			name: "lubenica",
			img: "slike/voce/lubenica.jpg",
			id: 14
		}, {
			name: "malina",
			img: "slike/voce/malina.jpg",
			id: 15
		}, {
			name: "mandarina",
			img: "slike/voce/mandarina.jpg",
			id: 16
		}, {
			name: "naranča",
			img: "slike/voce/naranca.jpg",
			id: 17
		}, {
			name: "smokva",
			img: "slike/voce/smokva.jpg",
			id: 18
		}, {
			name: "trešnja",
			img: "slike/voce/tresnja.jpg",
			id: 19
		}]

		function shuffle(array) {
			var currentIndex = array.length,
				temporaryValue, randomIndex;

			// While there remain elements to shuffle...
			while (0 !== currentIndex) {

				// Pick a remaining element...
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;

				// And swap it with the current element.
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
			}

			return array;
		}

		cards = shuffle(cards);

		cards = cards.slice(0, broj_karata);

		Memory.init(cards);


		if (razina == 1) {
			$(".card").css({
				"width": "25%",
				"height": "50%"
			})
		} else if (razina == 2) {
			$(".card").css({
				"width": "25%",
				"height": "25%"
			})
		} else if (razina == 3) {
			$(".card").css({
				"width": "16.66666%",
				"height": "25%"
			})
		}

		$(".back").addClass("pozadina-voce");
	}
});
$(".povrce").click(function () {
	$(".modal").html("<h2 class='winner'>Odaberi broj parova:</h2><button id='prva'>4</button> <button id='druga'>8</button><button id='treca'>12</button>");
	$("#prva").click(function () {
		razina = "1";
		igra()
	})
	$("#druga").click(function () {
		razina = "2";
		igra();
$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
	})
	$("#treca").click(function () {
razina = "3";
igra();
$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
	})

	function igra() {


		if (razina == 1) {
			broj_karata = 4;

		} else if (razina == 2) {
			broj_karata = 8;
		} else {
			broj_karata = 12
		}
		$("footer").fadeIn(1000);
		$(".modal").fadeOut(1000);
		$(".modal-overlay").delay(1000).slideUp(1000);
		$(".game").show("slow");
		$("#okretanje")[0].play();
		//localStorage.clear();
		var br = 1;
		var sec = 0;
		var pokusaj = 0;
		var vrijeme = 1;
		var bodovi = 0;

		var najbolje_vrijeme;
		var najmanji_broj_pokusaja;
		var karte;

		function pad(val) {
			return val > 9 ? val : "0" + val;
		}
		setInterval(function () {
			if (vrijeme == 1) {
				$("#seconds").html(pad(++sec % 60));
				$("#minutes").html(pad(parseInt(sec / 60, 10)));
			}
		}, 1000);

		var Memory = {
			init: function (cards) {
				this.$game = $(".game");
				this.$modal = $(".modal");
				this.$overlay = $(".modal-overlay");
				this.$zivotinje = $(".zivotinje");
				this.$ljudi = $(".ljudi");
				this.cardsArray = $.merge(cards, cards);
				this.shuffleCards(this.cardsArray);
				this.setup();
			},

			shuffleCards: function (cardsArray) {
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			setup: function () {
				this.html = this.buildHTML();
				this.$game.html(this.html);
				this.$memoryCards = $(".card");
				this.binding();
				this.paused = false;
				this.guess = null;
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			binding: function () {
				this.$memoryCards.on("click", this.cardClicked);
				this.$zivotinje.on("click", $.proxy(this.reset, this));
			},
			// kinda messy but hey
			cardClicked: function () {
				$("#okret")[0].play();

				var _ = Memory;
				var $card = $(this);
				if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

					$card.find(".inside").addClass("picked");
					if (!_.guess) {
						_.guess = $(this).attr("data-id");
						$(this).find('p').toggle();
					} else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
						$(".picked").addClass("matched");
						$("#tocno")[0].play();

						bodovi = bodovi + 15;
						_.guess = null;
						$(".matched").find('p').remove();
						pokusaj++;
					} else {
						pokusaj++;
						$(this).find('p').toggle();
						_.guess = null;
						_.paused = true;
						setTimeout(function () {
							$(".picked").removeClass("picked");
							Memory.paused = false;
							$(".brojevi").show();
							bodovi = bodovi - 5
						}, 1200);
					}
					if ($(".matched").length == $(".card").length) {
						_.win();
					}
				}
			},

			win: function () {
				this.paused = true;
				setTimeout(function () {
					Memory.showModal();
					Memory.$game.fadeOut();
				}, 1000);
			},

			showModal: function () {
				var minute = Math.floor(sec / 60);
				var sekunde = sec - minute * 60;
				this.$overlay.show();
				this.$modal.fadeIn("slow");
				var najvrijeme = localStorage.getItem('najvrijeme');

				if (najvrijeme === undefined || najvrijeme === null) {
					najvrijeme = sec;
					localStorage.setItem('najvrijeme', sec);
				}

				// If the user has more points than the currently stored high score then
				if (sec < najvrijeme) {
					// Set the high score to the users' current points
					najvrijeme = sec;
					// Store the high score
					localStorage.setItem('najvrijeme', sec);
				}
				// Return the high score

				var najpokusaji = localStorage.getItem('najpokusaji');

				if (najpokusaji === undefined || najpokusaji === null) {
					najpokusaji = pokusaj;
					localStorage.setItem('najpokusaji', pokusaj);
				}
				// If the user has more points than the currently stored high score then
				if (pokusaj < najpokusaji) {
					// Set the high score to the users' current points
					najpokusaji = pokusaj;
					// Store the high score
					localStorage.setItem('najpokusaji', pokusaj);
				}
				var naj_minute = Math.floor(najvrijeme / 60);
				var naj_sekunde = najvrijeme - naj_minute * 60;
				$(".modal").show();
				$(".modal-overlay").show();
				$(".winner").hide();
				bodovi = bodovi - sec
				$(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja: " + pokusaj + "</br>vrijeme igre: " + minute + ":" + sekunde + "</br><p><form id='input-form' action='' method='POST' target='no-target'><br><select id='ikona' style='height:30px'></select><label for='ime'> Ime: </label><input style='height:30px' id='input-q1' name='q1'><br> <label for='bodovi'>Bodovi: </label><input id='input-q2' placeholder='q2' name='q2' value='" + bodovi + "' disabled style='display:none'> <label for='bodovi'>" + bodovi + "</label><br><button id='form-submit' type='submit'>predaj rezultat</button> </form>    <iframe src='#' id='no-target' name='no-target' style='visibility:hidden;display:none'></iframe><br><a href='index.html' style='color:black;'>nova igra</a></p></div>");

				var target = document.getElementById("ikona");
				var emojiCount = emoji.length;

				for (var index = 0; index < emojiCount; index++) {
					addEmoji(emoji[index]);
				}

				function addEmoji(code) {
					var option = document.createElement('option');
					option.innerHTML = code;
					option.value = code;
					target.appendChild(option);
				}
				if (localStorage.getItem("ime") != null) {
					$('#input-q1').val(localStorage.getItem("ime"))
					$('#ikona').val(localStorage.getItem("ikona"))
				}
				if (razina == 1) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSdZeSM2ZKPUrdwMH-AGEInw_X5jzw2200zgDrw8i2kGm76q0Q/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez7.html';
							}, 1000);
					});
				} else if (razina == 2) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSegsFx0aWGp5bfU1OsEZZ4vdBw1lGUvcv7WTfF8ImNy6KldFA/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez8.html';
							}, 1000);
					});
				} else {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSe4uz4O2hTXAPDTeEQtxIeFV4wN_Ub52I_Vooy_BsIwJr3dWQ/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez9.html';
							}, 1000);
					});
				}


			},

			hideModal: function () {
				this.$overlay.hide();
				this.$modal.hide();
			},

			reset: function () {
				this.hideModal();
				this.shuffleCards(this.cardsArray);
				this.setup();
				this.$game.show("slow");
				pokusaj = 0;
				sec = 0;
				br = 1;
				$(".back").addClass("pozadina-povrce");
			},

			// Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
			shuffle: function (array) {
				var counter = array.length,
					temp, index;
				// While there are elements in the array
				while (counter > 0) {
					// Pick a random index
					index = Math.floor(Math.random() * counter);
					// Decrease counter by 1
					counter--;
					// And swap the last element with it
					temp = array[counter];
					array[counter] = array[index];
					array[index] = temp;
				}
				return array;
			},

			buildHTML: function () {


				var frag = '';
				br = 1;
				var lista_slika = [];
				var lista_imena = [];
				this.$cards.each(function (k, v) {
					if (Math.floor((Math.random() * 2) + 1) == 1) {
						if ($.inArray(v.name, lista_imena) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);

						}
					} else {
						if ($.inArray(v.img, lista_slika) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);

						}
					}
				});
				return frag;
			}
		};
		var cards = [{
			name: "češnjak",
			img: "slike/povrce/cesnjak.jpg",
			id: 1,
		}, {
			name: "cikla",
			img: "slike/povrce/cikla.jpg",
			id: 2
		}, {
			name: "grah",
			img: "slike/povrce/grah.jpg",
			id: 3
		}, {
			name: "grašak",
			img: "slike/povrce/grasak.jpg",
			id: 4
		}, {
			name: "krastavac",
			img: "slike/povrce/krastavac.jpg",
			id: 5
		}, {
			name: "krumpir",
			img: "slike/povrce/krumpir.jpg",
			id: 6
		}, {
			name: "kupus",
			img: "slike/povrce/kupus.jpg",
			id: 7
		}, {
			name: "luk",
			img: "slike/povrce/luk.jpg",
			id: 8
		}, {
			name: "mrkva",
			img: "slike/povrce/mrkva.jpg",
			id: 9
		}, {
			name: "patlidžan",
			img: "slike/povrce/patlidzan.jpg",
			id: 10
		}, {
			name: "rajčica",
			img: "slike/povrce/rajcica.jpg",
			id: 11
		}, {
			name: "repa",
			img: "slike/povrce/repa.jpg",
			id: 12
		}, {
			name: "rotkvica",
			img: "slike/povrce/rotkvica.jpg",
			id: 13
		}, {
			name: "salata",
			img: "slike/povrce/salata.jpg",
			id: 14
		}, {
			name: "špinat",
			img: "slike/povrce/spinat.jpg",
			id: 15
		}, {
			name: "tikvica",
			img: "slike/povrce/tikvica.jpg",
			id: 16
		}]

		function shuffle(array) {
			var currentIndex = array.length,
				temporaryValue, randomIndex;

			// While there remain elements to shuffle...
			while (0 !== currentIndex) {

				// Pick a remaining element...
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;

				// And swap it with the current element.
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
			}

			return array;
		}

		cards = shuffle(cards);

		cards = cards.slice(0, broj_karata);

		Memory.init(cards);


		if (razina == 1) {
			$(".card").css({
				"width": "25%",
				"height": "50%"
			})
		} else if (razina == 2) {
			$(".card").css({
				"width": "25%",
				"height": "25%"
			})
		} else if (razina == 3) {
			$(".card").css({
				"width": "16.66666%",
				"height": "25%"
			})
		}

		$(".back").addClass("pozadina-povrce");
	}
});

$(".hrana").click(function () {
	$(".modal").html("<h2 class='winner'>Odaberi broj parova:</h2><button id='prva'>4</button> <button id='druga'>8</button><button id='treca'>12</button>");
	$("#prva").click(function () {
		razina = "1";
		igra()
	})
	$("#druga").click(function () {
		razina = "2";
		igra();
$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
	})
	$("#treca").click(function () {
razina = "3";
igra();
$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
	})

	function igra() {


		if (razina == 1) {
			broj_karata = 4;

		} else if (razina == 2) {
			broj_karata = 8;
		} else {
			broj_karata = 12
		}
		$("footer").fadeIn(1000);
		$(".modal").fadeOut(1000);
		$(".modal-overlay").delay(1000).slideUp(1000);
		$(".game").show("slow");
		$("#okretanje")[0].play();
		//localStorage.clear();
		var br = 1;
		var sec = 0;
		var pokusaj = 0;
		var vrijeme = 1;
		var bodovi = 0;

		var najbolje_vrijeme;
		var najmanji_broj_pokusaja;
		var karte;

		function pad(val) {
			return val > 9 ? val : "0" + val;
		}
		setInterval(function () {
			if (vrijeme == 1) {
				$("#seconds").html(pad(++sec % 60));
				$("#minutes").html(pad(parseInt(sec / 60, 10)));
			}
		}, 1000);

		var Memory = {
			init: function (cards) {
				this.$game = $(".game");
				this.$modal = $(".modal");
				this.$overlay = $(".modal-overlay");
				this.$zivotinje = $(".zivotinje");
				this.$ljudi = $(".ljudi");
				this.cardsArray = $.merge(cards, cards);
				this.shuffleCards(this.cardsArray);
				this.setup();
			},

			shuffleCards: function (cardsArray) {
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			setup: function () {
				this.html = this.buildHTML();
				this.$game.html(this.html);
				this.$memoryCards = $(".card");
				this.binding();
				this.paused = false;
				this.guess = null;
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			binding: function () {
				this.$memoryCards.on("click", this.cardClicked);
				this.$zivotinje.on("click", $.proxy(this.reset, this));
			},
			// kinda messy but hey
			cardClicked: function () {
				$("#okret")[0].play();

				var _ = Memory;
				var $card = $(this);
				if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

					$card.find(".inside").addClass("picked");
					if (!_.guess) {
						_.guess = $(this).attr("data-id");
						$(this).find('p').toggle();
					} else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
						$(".picked").addClass("matched");
						$("#tocno")[0].play();

						bodovi = bodovi + 15;
						_.guess = null;
						$(".matched").find('p').remove();
						pokusaj++;
					} else {
						pokusaj++;
						$(this).find('p').toggle();
						_.guess = null;
						_.paused = true;
						setTimeout(function () {
							$(".picked").removeClass("picked");
							Memory.paused = false;
							$(".brojevi").show();
							bodovi = bodovi - 5
						}, 1200);
					}
					if ($(".matched").length == $(".card").length) {
						_.win();
					}
				}
			},

			win: function () {
				this.paused = true;
				setTimeout(function () {
					Memory.showModal();
					Memory.$game.fadeOut();
				}, 1000);
			},

			showModal: function () {
				var minute = Math.floor(sec / 60);
				var sekunde = sec - minute * 60;
				this.$overlay.show();
				this.$modal.fadeIn("slow");
				var najvrijeme = localStorage.getItem('najvrijeme');

				if (najvrijeme === undefined || najvrijeme === null) {
					najvrijeme = sec;
					localStorage.setItem('najvrijeme', sec);
				}

				// If the user has more points than the currently stored high score then
				if (sec < najvrijeme) {
					// Set the high score to the users' current points
					najvrijeme = sec;
					// Store the high score
					localStorage.setItem('najvrijeme', sec);
				}
				// Return the high score

				var najpokusaji = localStorage.getItem('najpokusaji');

				if (najpokusaji === undefined || najpokusaji === null) {
					najpokusaji = pokusaj;
					localStorage.setItem('najpokusaji', pokusaj);
				}
				// If the user has more points than the currently stored high score then
				if (pokusaj < najpokusaji) {
					// Set the high score to the users' current points
					najpokusaji = pokusaj;
					// Store the high score
					localStorage.setItem('najpokusaji', pokusaj);
				}
				var naj_minute = Math.floor(najvrijeme / 60);
				var naj_sekunde = najvrijeme - naj_minute * 60;
				$(".modal").show();
				$(".modal-overlay").show();
				$(".winner").hide();
				bodovi = bodovi - sec
				$(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja: " + pokusaj + "</br>vrijeme igre: " + minute + ":" + sekunde + "</br><p><form id='input-form' action='' method='POST' target='no-target'><br><select id='ikona' style='height:30px'></select><label for='ime'> Ime: </label><input style='height:30px' id='input-q1' name='q1'><br> <label for='bodovi'>Bodovi: </label><input id='input-q2' placeholder='q2' name='q2' value='" + bodovi + "' disabled style='display:none'> <label for='bodovi'>" + bodovi + "</label><br><button id='form-submit' type='submit'>predaj rezultat</button> </form>    <iframe src='#' id='no-target' name='no-target' style='visibility:hidden;display:none'></iframe><br><a href='index.html' style='color:black;'>nova igra</a></p></div>");

				var target = document.getElementById("ikona");
				var emojiCount = emoji.length;

				for (var index = 0; index < emojiCount; index++) {
					addEmoji(emoji[index]);
				}

				function addEmoji(code) {
					var option = document.createElement('option');
					option.innerHTML = code;
					option.value = code;
					target.appendChild(option);
				}
				if (localStorage.getItem("ime") != null) {
					$('#input-q1').val(localStorage.getItem("ime"))
					$('#ikona').val(localStorage.getItem("ikona"))
				}
				if (razina == 1) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSfRitklgH--GC9Q3qa2XdHMkDS6R3xwj_TZEEgaWrxDExlkdw/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez10.html';
							}, 1000);
					});
				} else if (razina == 2) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSfiT-JrQQEQ-JsCK4IHjuETsUeBWpzr5SCjmA74T4mlRfOKqA/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez11.html';
							}, 1000);
					});
				} else {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSeTJEAstcId2QKGmUuC64P5Sp3Z1LWj9LeYDmnhcma8F0tVyA/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez12.html';
							}, 1000);
					});
				}


			},

			hideModal: function () {
				this.$overlay.hide();
				this.$modal.hide();
			},

			reset: function () {
				this.hideModal();
				this.shuffleCards(this.cardsArray);
				this.setup();
				this.$game.show("slow");
				pokusaj = 0;
				sec = 0;
				br = 1;
				$(".back").addClass("pozadina-hrana");
			},

			// Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
			shuffle: function (array) {
				var counter = array.length,
					temp, index;
				// While there are elements in the array
				while (counter > 0) {
					// Pick a random index
					index = Math.floor(Math.random() * counter);
					// Decrease counter by 1
					counter--;
					// And swap the last element with it
					temp = array[counter];
					array[counter] = array[index];
					array[index] = temp;
				}
				return array;
			},

			buildHTML: function () {


				var frag = '';
				br = 1;
				var lista_slika = [];
				var lista_imena = [];
				this.$cards.each(function (k, v) {
					if (Math.floor((Math.random() * 2) + 1) == 1) {
						if ($.inArray(v.name, lista_imena) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);

						}
					} else {
						if ($.inArray(v.img, lista_slika) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);

						}
					}
				});
				return frag;
			}
		};
		var cards = [{
			name: "čaj",
			img: "slike/hrana/caj.jpg",
			id: 1,
		}, {
			name: "čokolada",
			img: "slike/hrana/cokolada.jpg",
			id: 2
		}, {
			name: "kava",
			img: "slike/hrana/kava.jpg",
			id: 3
		}, {
			name: "kolač",
			img: "slike/hrana/kolac.jpg",
			id: 4
		}, {
			name: "kruh",
			img: "slike/hrana/kruh.jpg",
			id: 5
		}, {
			name: "meso",
			img: "slike/hrana/meso.jpg",
			id: 6
		}, {
			name: "mlijeko",
			img: "slike/hrana/mlijeko.jpg",
			id: 7
		}, {
			name: "pivo",
			img: "slike/hrana/piva.jpg",
			id: 8
		}, {
			name: "povrće",
			img: "slike/hrana/povrce.jpg",
			id: 9
		}, {
			name: "riba",
			img: "slike/hrana/riba.jpg",
			id: 10
		}, {
			name: "riža",
			img: "slike/hrana/riza.jpg",
			id: 11
		}, {
			name: "salama",
			img: "slike/hrana/salama.jpg",
			id: 12
		}, {
			name: "šečer",
			img: "slike/hrana/secer.jpg",
			id: 13
		}, {
			name: "sir",
			img: "slike/hrana/sir.jpg",
			id: 14
		}, {
			name: "sladoled",
			img: "slike/hrana/sladoled.jpg",
			id: 15
		}, {
			name: "slatkiši",
			img: "slike/hrana/slatkisi.jpg",
			id: 16
		}, {
			name: "sok",
			img: "slike/hrana/sok.jpg",
			id: 17
		}, {
			name: "sol",
			img: "slike/hrana/sol.jpg",
			id: 18
		}, {
			name: "šunka",
			img: "slike/hrana/sunka.jpg",
			id: 19
		}, {
			name: "tjestenina",
			img: "slike/hrana/tjestenina.jpg",
			id: 20
		}, {
			name: "voće",
			img: "slike/hrana/voce.jpg",
			id: 21
		}]

		function shuffle(array) {
			var currentIndex = array.length,
				temporaryValue, randomIndex;

			// While there remain elements to shuffle...
			while (0 !== currentIndex) {

				// Pick a remaining element...
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;

				// And swap it with the current element.
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
			}

			return array;
		}

		cards = shuffle(cards);

		cards = cards.slice(0, broj_karata);

		Memory.init(cards);


		if (razina == 1) {
			$(".card").css({
				"width": "25%",
				"height": "50%"
			})
		} else if (razina == 2) {
			$(".card").css({
				"width": "25%",
				"height": "25%"
			})
		} else if (razina == 3) {
			$(".card").css({
				"width": "16.66666%",
				"height": "25%"
			})
		}

		$(".back").addClass("pozadina-hrana");
	}
});

$(".odjeca").click(function () {
	$(".modal").html("<h2 class='winner'>Odaberi broj parova:</h2><button id='prva'>4</button> <button id='druga'>8</button><button id='treca'>12</button>");
	$("#prva").click(function () {
		razina = "1";
		igra()
	})
	$("#druga").click(function () {
		razina = "2";
		igra();
	$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
		})
		$("#treca").click(function () {
	razina = "3";
	igra();
	$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
		})

	function igra() {


		if (razina == 1) {
			broj_karata = 4;

		} else if (razina == 2) {
			broj_karata = 8;
		} else {
			broj_karata = 12
		}
		$("footer").fadeIn(1000);
		$(".modal").fadeOut(1000);
		$(".modal-overlay").delay(1000).slideUp(1000);
		$(".game").show("slow");
		$("#okretanje")[0].play();
		//localStorage.clear();
		var br = 1;
		var sec = 0;
		var pokusaj = 0;
		var vrijeme = 1;
		var bodovi = 0;

		var najbolje_vrijeme;
		var najmanji_broj_pokusaja;
		var karte;

		function pad(val) {
			return val > 9 ? val : "0" + val;
		}
		setInterval(function () {
			if (vrijeme == 1) {
				$("#seconds").html(pad(++sec % 60));
				$("#minutes").html(pad(parseInt(sec / 60, 10)));
			}
		}, 1000);

		var Memory = {
			init: function (cards) {
				this.$game = $(".game");
				this.$modal = $(".modal");
				this.$overlay = $(".modal-overlay");
				this.$zivotinje = $(".zivotinje");
				this.$ljudi = $(".ljudi");
				this.cardsArray = $.merge(cards, cards);
				this.shuffleCards(this.cardsArray);
				this.setup();
			},

			shuffleCards: function (cardsArray) {
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			setup: function () {
				this.html = this.buildHTML();
				this.$game.html(this.html);
				this.$memoryCards = $(".card");
				this.binding();
				this.paused = false;
				this.guess = null;
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			binding: function () {
				this.$memoryCards.on("click", this.cardClicked);
				this.$zivotinje.on("click", $.proxy(this.reset, this));
			},
			// kinda messy but hey
			cardClicked: function () {
				$("#okret")[0].play();

				var _ = Memory;
				var $card = $(this);
				if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

					$card.find(".inside").addClass("picked");
					if (!_.guess) {
						_.guess = $(this).attr("data-id");
						$(this).find('p').toggle();
					} else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
						$(".picked").addClass("matched");
						$("#tocno")[0].play();

						bodovi = bodovi + 15;
						_.guess = null;
						$(".matched").find('p').remove();
						pokusaj++;
					} else {
						pokusaj++;
						$(this).find('p').toggle();
						_.guess = null;
						_.paused = true;
						setTimeout(function () {
							$(".picked").removeClass("picked");
							Memory.paused = false;
							$(".brojevi").show();
							bodovi = bodovi - 5
						}, 1200);
					}
					if ($(".matched").length == $(".card").length) {
						_.win();
					}
				}
			},

			win: function () {
				this.paused = true;
				setTimeout(function () {
					Memory.showModal();
					Memory.$game.fadeOut();
				}, 1000);
			},

			showModal: function () {
				var minute = Math.floor(sec / 60);
				var sekunde = sec - minute * 60;
				this.$overlay.show();
				this.$modal.fadeIn("slow");
				var najvrijeme = localStorage.getItem('najvrijeme');

				if (najvrijeme === undefined || najvrijeme === null) {
					najvrijeme = sec;
					localStorage.setItem('najvrijeme', sec);
				}

				// If the user has more points than the currently stored high score then
				if (sec < najvrijeme) {
					// Set the high score to the users' current points
					najvrijeme = sec;
					// Store the high score
					localStorage.setItem('najvrijeme', sec);
				}
				// Return the high score

				var najpokusaji = localStorage.getItem('najpokusaji');

				if (najpokusaji === undefined || najpokusaji === null) {
					najpokusaji = pokusaj;
					localStorage.setItem('najpokusaji', pokusaj);
				}
				// If the user has more points than the currently stored high score then
				if (pokusaj < najpokusaji) {
					// Set the high score to the users' current points
					najpokusaji = pokusaj;
					// Store the high score
					localStorage.setItem('najpokusaji', pokusaj);
				}
				var naj_minute = Math.floor(najvrijeme / 60);
				var naj_sekunde = najvrijeme - naj_minute * 60;
				$(".modal").show();
				$(".modal-overlay").show();
				$(".winner").hide();
				bodovi = bodovi - sec
				$(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja: " + pokusaj + "</br>vrijeme igre: " + minute + ":" + sekunde + "</br><p><form id='input-form' action='' method='POST' target='no-target'><br><select id='ikona' style='height:30px'></select><label for='ime'> Ime: </label><input style='height:30px' id='input-q1' name='q1'><br> <label for='bodovi'>Bodovi: </label><input id='input-q2' placeholder='q2' name='q2' value='" + bodovi + "' disabled style='display:none'> <label for='bodovi'>" + bodovi + "</label><br><button id='form-submit' type='submit'>predaj rezultat</button> </form>    <iframe src='#' id='no-target' name='no-target' style='visibility:hidden;display:none'></iframe><br><a href='index.html' style='color:black;'>nova igra</a></p></div>");

				var target = document.getElementById("ikona");
				var emojiCount = emoji.length;

				for (var index = 0; index < emojiCount; index++) {
					addEmoji(emoji[index]);
				}

				function addEmoji(code) {
					var option = document.createElement('option');
					option.innerHTML = code;
					option.value = code;
					target.appendChild(option);
				}
				if (localStorage.getItem("ime") != null) {
					$('#input-q1').val(localStorage.getItem("ime"))
					$('#ikona').val(localStorage.getItem("ikona"))
				}
				if (razina == 1) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSeSGSSOTlXaimqkEW5zRpQZvGD5tWL8CAs4qc4V-jDpwCA25Q/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez13.html';
							}, 1000);
					});
				} else if (razina == 2) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSdO3hpeQ7f6ZYKgmlhf6RsCy2bW8rV9aacjk6zg_Lk__KH9AA/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez14.html';
							}, 1000);
					});
				} else {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSe7TVlu9JCxDY1VTZRh8MdI9ShaUnDi_hVDy0pDLN9JNGCDQg/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez15.html';
							}, 1000);
					});
				}


			},

			hideModal: function () {
				this.$overlay.hide();
				this.$modal.hide();
			},

			reset: function () {
				this.hideModal();
				this.shuffleCards(this.cardsArray);
				this.setup();
				this.$game.show("slow");
				pokusaj = 0;
				sec = 0;
				br = 1;
				$(".back").addClass("pozadina-odjeca");
			},

			// Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
			shuffle: function (array) {
				var counter = array.length,
					temp, index;
				// While there are elements in the array
				while (counter > 0) {
					// Pick a random index
					index = Math.floor(Math.random() * counter);
					// Decrease counter by 1
					counter--;
					// And swap the last element with it
					temp = array[counter];
					array[counter] = array[index];
					array[index] = temp;
				}
				return array;
			},

			buildHTML: function () {


				var frag = '';
				br = 1;
				var lista_slika = [];
				var lista_imena = [];
				this.$cards.each(function (k, v) {
					if (Math.floor((Math.random() * 2) + 1) == 1) {
						if ($.inArray(v.name, lista_imena) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);

						}
					} else {
						if ($.inArray(v.img, lista_slika) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);

						}
					}
				});
				return frag;
			}
		};
		var cards = [{
			name: "čarape",
			img: "slike/odjeca/carape.jpg",
			id: 1,
		}, {
			name: "cipele",
			img: "slike/odjeca/cipele.jpg",
			id: 2
		}, {
			name: "čizme",
			img: "slike/odjeca/cizme.jpg",
			id: 3
		}, {
			name: "gače",
			img: "slike/odjeca/gace.jpg",
			id: 4
		}, {
			name: "grudnjak",
			img: "slike/odjeca/grudnjak.jpg",
			id: 5
		}, {
			name: "haljina",
			img: "slike/odjeca/haljina.jpg",
			id: 6
		}, {
			name: "hlače",
			img: "slike/odjeca/hlace.jpg",
			id: 7
		}, {
			name: "jakna",
			img: "slike/odjeca/jakna.jpg",
			id: 8
		}, {
			name: "kaput",
			img: "slike/odjeca/kaput.jpg",
			id: 9
		}, {
			name: "košulja",
			img: "slike/odjeca/kosulja.jpg",
			id: 10
		}, {
			name: "kravata",
			img: "slike/odjeca/kravata.jpg",
			id: 11
		}, {
			name: "majca",
			img: "slike/odjeca/majca.jpg",
			id: 12
		}, {
			name: "pulover",
			img: "slike/odjeca/pulover.jpg",
			id: 13
		}, {
			name: "suknja",
			img: "slike/odjeca/suknja.jpg",
			id: 14
		}, {
			name: "tenisice",
			img: "slike/odjeca/tenisice.jpg",
			id: 15
		}]

		function shuffle(array) {
			var currentIndex = array.length,
				temporaryValue, randomIndex;

			// While there remain elements to shuffle...
			while (0 !== currentIndex) {

				// Pick a remaining element...
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;

				// And swap it with the current element.
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
			}

			return array;
		}

		cards = shuffle(cards);

		cards = cards.slice(0, broj_karata);

		Memory.init(cards);


		if (razina == 1) {
			$(".card").css({
				"width": "25%",
				"height": "50%"
			})
		} else if (razina == 2) {
			$(".card").css({
				"width": "25%",
				"height": "25%"
			})
		} else if (razina == 3) {
			$(".card").css({
				"width": "16.66666%",
				"height": "25%"
			})
		}

		$(".back").addClass("pozadina-odjeca");
	}
});

$(".poslovi").click(function () {
	$(".modal").html("<h2 class='winner'>Odaberi broj parova:</h2><button id='prva'>4</button> <button id='druga'>8</button><button id='treca'>12</button>");
	$("#prva").click(function () {
		razina = "1";
		igra()
	})
	$("#druga").click(function () {
		razina = "2";
		igra();
$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
	})
	$("#treca").click(function () {
razina = "3";
igra();
$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
	})

	function igra() {

		if (razina == 1) {
			broj_karata = 4;

		} else if (razina == 2) {
			broj_karata = 8;
		} else {
			broj_karata = 12
		}
		$("footer").fadeIn(1000);
		$(".modal").fadeOut(1000);
		$(".modal-overlay").delay(1000).slideUp(1000);
		$(".game").show("slow");
		$("#okretanje")[0].play();
		//localStorage.clear();
		var br = 1;
		var sec = 0;
		var pokusaj = 0;
		var vrijeme = 1;
		var bodovi = 0;

		var najbolje_vrijeme;
		var najmanji_broj_pokusaja;
		var karte;


		function pad(val) {
			return val > 9 ? val : "0" + val;
		}
		setInterval(function () {
			if (vrijeme == 1) {
				$("#seconds").html(pad(++sec % 60));
				$("#minutes").html(pad(parseInt(sec / 60, 10)));
			}
		}, 1000);

		var Memory = {
			init: function (cards) {
				this.$game = $(".game");
				this.$modal = $(".modal");
				this.$overlay = $(".modal-overlay");
				this.$zivotinje = $(".zivotinje");
				this.$ljudi = $(".ljudi");
				this.cardsArray = $.merge(cards, cards);
				this.shuffleCards(this.cardsArray);
				this.setup();
			},

			shuffleCards: function (cardsArray) {
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			setup: function () {
				this.html = this.buildHTML();
				this.$game.html(this.html);
				this.$memoryCards = $(".card");
				this.binding();
				this.paused = false;
				this.guess = null;
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			binding: function () {
				this.$memoryCards.on("click", this.cardClicked);
				this.$zivotinje.on("click", $.proxy(this.reset, this));
			},
			// kinda messy but hey
			cardClicked: function () {
				$("#okret")[0].play();

				var _ = Memory;
				var $card = $(this);
				if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

					$card.find(".inside").addClass("picked");
					if (!_.guess) {
						_.guess = $(this).attr("data-id");
						$(this).find('p').toggle();
					} else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
						$(".picked").addClass("matched");
						$("#tocno")[0].play();

						bodovi = bodovi + 15;
						_.guess = null;
						$(".matched").find('p').remove();
						pokusaj++;
					} else {
						pokusaj++;
						$(this).find('p').toggle();
						_.guess = null;
						_.paused = true;
						setTimeout(function () {
							$(".picked").removeClass("picked");
							Memory.paused = false;
							$(".brojevi").show();
							bodovi = bodovi - 5
						}, 1200);
					}
					if ($(".matched").length == $(".card").length) {
						_.win();
					}
				}
			},

			win: function () {
				this.paused = true;
				setTimeout(function () {
					Memory.showModal();
					Memory.$game.fadeOut();
				}, 1000);
			},

			showModal: function () {
				var minute = Math.floor(sec / 60);
				var sekunde = sec - minute * 60;
				this.$overlay.show();
				this.$modal.fadeIn("slow");
				var najvrijeme = localStorage.getItem('najvrijeme');

				if (najvrijeme === undefined || najvrijeme === null) {
					najvrijeme = sec;
					localStorage.setItem('najvrijeme', sec);
				}

				// If the user has more points than the currently stored high score then
				if (sec < najvrijeme) {
					// Set the high score to the users' current points
					najvrijeme = sec;
					// Store the high score
					localStorage.setItem('najvrijeme', sec);
				}
				// Return the high score

				var najpokusaji = localStorage.getItem('najpokusaji');

				if (najpokusaji === undefined || najpokusaji === null) {
					najpokusaji = pokusaj;
					localStorage.setItem('najpokusaji', pokusaj);
				}
				// If the user has more points than the currently stored high score then
				if (pokusaj < najpokusaji) {
					// Set the high score to the users' current points
					najpokusaji = pokusaj;
					// Store the high score
					localStorage.setItem('najpokusaji', pokusaj);
				}
				var naj_minute = Math.floor(najvrijeme / 60);
				var naj_sekunde = najvrijeme - naj_minute * 60;
				$(".modal").show();
				$(".modal-overlay").show();
				$(".winner").hide();
				bodovi = bodovi - sec
				$(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja: " + pokusaj + "</br>vrijeme igre: " + minute + ":" + sekunde + "</br><p><form id='input-form' action='' method='POST' target='no-target'><br><select id='ikona' style='height:30px'></select><label for='ime'> Ime: </label><input style='height:30px' id='input-q1' name='q1'><br> <label for='bodovi'>Bodovi: </label><input id='input-q2' placeholder='q2' name='q2' value='" + bodovi + "' disabled style='display:none'> <label for='bodovi'>" + bodovi + "</label><br><button id='form-submit' type='submit'>predaj rezultat</button> </form>    <iframe src='#' id='no-target' name='no-target' style='visibility:hidden;display:none'></iframe><br><a href='index.html' style='color:black;'>nova igra</a></p></div>");

				var target = document.getElementById("ikona");
				var emojiCount = emoji.length;

				for (var index = 0; index < emojiCount; index++) {
					addEmoji(emoji[index]);
				}

				function addEmoji(code) {
					var option = document.createElement('option');
					option.innerHTML = code;
					option.value = code;
					target.appendChild(option);
				}

				if (localStorage.getItem("ime") != null) {
					$('#input-q1').val(localStorage.getItem("ime"))
					$('#ikona').val(localStorage.getItem("ikona"))
				}

				if (razina == 1) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSex9EYKY8E5f4ixJZAkc8Tr7bcGEOR0MSpFigpP3TURcjKz7w/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez16.html';
							}, 1000);
					});
				} else if (razina == 2) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSeZkYUq8u2IA9AwUscjYeVgct7-evXz-AVuAok2O3A2e4ex4g/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez17.html';
							}, 1000);
					});
				} else {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSfmlykn0ydr84zQJCEqaPGQft3N5JgXEPRhaF3BjSCFEYldDg/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez18.html';
							}, 1000);
					});
				}
			},

			hideModal: function () {
				this.$overlay.hide();
				this.$modal.hide();
			},

			reset: function () {
				this.hideModal();
				this.shuffleCards(this.cardsArray);
				this.setup();
				this.$game.show("slow");
				pokusaj = 0;
				sec = 0;
				br = 1;
				$(".back").addClass("pozadina-poso");
			},

			// Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
			shuffle: function (array) {
				var counter = array.length,
					temp, index;
				// While there are elements in the array
				while (counter > 0) {
					// Pick a random index
					index = Math.floor(Math.random() * counter);
					// Decrease counter by 1
					counter--;
					// And swap the last element with it
					temp = array[counter];
					array[counter] = array[index];
					array[index] = temp;
				}
				return array;
			},

			buildHTML: function () {


				var frag = '';
				br = 1;
				var lista_slika = [];
				var lista_imena = [];
				this.$cards.each(function (k, v) {
					if (Math.floor((Math.random() * 2) + 1) == 1) {
						if ($.inArray(v.name, lista_imena) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);

						}
					} else {
						if ($.inArray(v.img, lista_slika) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);

						}
					}
				});
				return frag;
			}
		};

		var cards = [{
			name: "astronaut",
			img: "slike/poslovi/astronaut.jpg",
			id: 1,
		}, {
			name: "bankar",
			img: "slike/poslovi/bankar.jpg",
			id: 2
		}, {
			name: "bravar",
			img: "slike/poslovi/bravar.jpg",
			id: 3
		}, {
			name: "čistač",
			img: "slike/poslovi/cistac.jpg",
			id: 4
		}, {
			name: "doktor",
			img: "slike/poslovi/doktor.jpg",
			id: 5
		}, {
			name: "frizer",
			img: "slike/poslovi/frizer.jpg",
			id: 6
		}, {
			name: "glazbenik",
			img: "slike/poslovi/glazbenik.jpg",
			id: 7
		}, {
			name: "graditelj",
			img: "slike/poslovi/graditelj.jpg",
			id: 8
		}, {
			name: "informatičar",
			img: "slike/poslovi/informaticar.jpg",
			id: 9
		}, {
			name: "konobar",
			img: "slike/poslovi/konobar.jpg",
			id: 10
		}, {
			name: "kuhar",
			img: "slike/poslovi/kuhar.jpg",
			id: 11
		}, {
			name: "ljekarnik",
			img: "slike/poslovi/ljekarnik.jpg",
			id: 12
		}, {
			name: "lovac",
			img: "slike/poslovi/lovac.jpg",
			id: 13
		}, {
			name: "medicinski tehničar",
			img: "slike/poslovi/medicinski-tehnicar.jpg",
			id: 14
		}, {
			name: "menadžer",
			img: "slike/poslovi/menadzer.jpg",
			id: 15
		}, {
			name: "nogometaš",
			img: "slike/poslovi/nogometas.jpg",
			id: 16
		}, {
			name: "odvjetnik",
			img: "slike/poslovi/odvjetnik.jpg",
			id: 17
		}, {
			name: "pekar",
			img: "slike/poslovi/pekar.jpg",
			id: 18
		}, {
			name: "pilot",
			img: "slike/poslovi/pilot.jpg",
			id: 19
		}, {
			name: "policajac",
			img: "slike/poslovi/policajac.jpg",
			id: 20
		}, {
			name: "političar",
			img: "slike/poslovi/politicar.jpg",
			id: 21
		}, {
			name: "poštar",
			img: "slike/poslovi/postar.jpg",
			id: 22
		}, {
			name: "prodavač",
			img: "slike/poslovi/prodavac.jpg",
			id: 23
		}, {
			name: "slikar",
			img: "slike/poslovi/slikar.jpg",
			id: 24
		}, {
			name: "smetlar",
			img: "slike/poslovi/smetlar.jpg",
			id: 25
		}, {
			name: "sportaš",
			img: "slike/poslovi/sportas.jpg",
			id: 26
		}, {
			name: "stolar",
			img: "slike/poslovi/stolar.jpg",
			id: 27
		}, {
			name: "sudac",
			img: "slike/poslovi/sudac.jpg",
			id: 28
		}, {
			name: "svečenik",
			img: "slike/poslovi/svecenik.jpg",
			id: 29
		}, {
			name: "učitelj",
			img: "slike/poslovi/ucitelj.jpg",
			id: 30
		}, {
			name: "vatrogasac",
			img: "slike/poslovi/vatrogasac.jpg",
			id: 31
		}, {
			name: "vozač",
			img: "slike/poslovi/vozac.jpg",
			id: 32
		}, {
			name: "znanstvenik",
			img: "slike/poslovi/znanstvenik.jpg",
			id: 33
		}]

		function shuffle(array) {
			var currentIndex = array.length,
				temporaryValue, randomIndex;

			// While there remain elements to shuffle...
			while (0 !== currentIndex) {

				// Pick a remaining element...
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;

				// And swap it with the current element.
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
			}

			return array;
		}

		cards = shuffle(cards);

		cards = cards.slice(0, broj_karata);

		Memory.init(cards);


		if (razina == 1) {
			$(".card").css({
				"width": "25%",
				"height": "50%"
			})
		} else if (razina == 2) {
			$(".card").css({
				"width": "25%",
				"height": "25%"
			})
		} else if (razina == 3) {
			$(".card").css({
				"width": "16.66666%",
				"height": "25%"
			})
		}

		$(".back").addClass("pozadina-poso");
	}
});

$(".prometna").click(function () {
	$(".modal").html("<h2 class='winner'>Odaberi broj parova:</h2><button id='prva'>4</button> <button id='druga'>8</button><button id='treca'>12</button>");
	$("#prva").click(function () {
		razina = "1";
		igra()
	})
	$("#druga").click(function () {
		razina = "2";
		igra();
$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
	})
	$("#treca").click(function () {
razina = "3";
igra();
$(".front span").css({"font-size":"40px"});$(".card .front").css({"padding":"10px"})
	})

	function igra() {


		if (razina == 1) {
			broj_karata = 4;

		} else if (razina == 2) {
			broj_karata = 8;
		} else {
			broj_karata = 12
		}
		$("footer").fadeIn(1000);
		$(".modal").fadeOut(1000);
		$(".modal-overlay").delay(1000).slideUp(1000);
		$(".game").show("slow");
		$("#okretanje")[0].play();
		//localStorage.clear();
		var br = 1;
		var sec = 0;
		var pokusaj = 0;
		var vrijeme = 1;
		var bodovi = 0;

		var najbolje_vrijeme;
		var najmanji_broj_pokusaja;
		var karte;


		function pad(val) {
			return val > 9 ? val : "0" + val;
		}
		setInterval(function () {
			if (vrijeme == 1) {
				$("#seconds").html(pad(++sec % 60));
				$("#minutes").html(pad(parseInt(sec / 60, 10)));
			}
		}, 1000);

		var Memory = {
			init: function (cards) {
				this.$game = $(".game");
				this.$modal = $(".modal");
				this.$overlay = $(".modal-overlay");
				this.$zivotinje = $(".zivotinje");
				this.$ljudi = $(".ljudi");
				this.cardsArray = $.merge(cards, cards);
				this.shuffleCards(this.cardsArray);
				this.setup();
			},

			shuffleCards: function (cardsArray) {
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			setup: function () {
				this.html = this.buildHTML();
				this.$game.html(this.html);
				this.$memoryCards = $(".card");
				this.binding();
				this.paused = false;
				this.guess = null;
				this.$cards = $(this.shuffle(this.cardsArray));
			},

			binding: function () {
				this.$memoryCards.on("click", this.cardClicked);
				this.$zivotinje.on("click", $.proxy(this.reset, this));
			},
			// kinda messy but hey
			cardClicked: function () {
				$("#okret")[0].play();

				var _ = Memory;
				var $card = $(this);
				if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

					$card.find(".inside").addClass("picked");
					if (!_.guess) {
						_.guess = $(this).attr("data-id");
						$(this).find('p').toggle();
					} else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
						$(".picked").addClass("matched");
						$("#tocno")[0].play();

						bodovi = bodovi + 15;
						_.guess = null;
						$(".matched").find('p').remove();
						pokusaj++;
					} else {
						pokusaj++;
						$(this).find('p').toggle();
						_.guess = null;
						_.paused = true;
						setTimeout(function () {
							$(".picked").removeClass("picked");
							Memory.paused = false;
							$(".brojevi").show();
							bodovi = bodovi - 5
						}, 1200);
					}
					if ($(".matched").length == $(".card").length) {
						_.win();
					}
				}
			},

			win: function () {
				this.paused = true;
				setTimeout(function () {
					Memory.showModal();
					Memory.$game.fadeOut();
				}, 1000);
			},

			showModal: function () {
				var minute = Math.floor(sec / 60);
				var sekunde = sec - minute * 60;
				this.$overlay.show();
				this.$modal.fadeIn("slow");
				var najvrijeme = localStorage.getItem('najvrijeme');

				if (najvrijeme === undefined || najvrijeme === null) {
					najvrijeme = sec;
					localStorage.setItem('najvrijeme', sec);
				}

				// If the user has more points than the currently stored high score then
				if (sec < najvrijeme) {
					// Set the high score to the users' current points
					najvrijeme = sec;
					// Store the high score
					localStorage.setItem('najvrijeme', sec);
				}
				// Return the high score

				var najpokusaji = localStorage.getItem('najpokusaji');

				if (najpokusaji === undefined || najpokusaji === null) {
					najpokusaji = pokusaj;
					localStorage.setItem('najpokusaji', pokusaj);
				}
				// If the user has more points than the currently stored high score then
				if (pokusaj < najpokusaji) {
					// Set the high score to the users' current points
					najpokusaji = pokusaj;
					// Store the high score
					localStorage.setItem('najpokusaji', pokusaj);
				}
				var naj_minute = Math.floor(najvrijeme / 60);
				var naj_sekunde = najvrijeme - naj_minute * 60;
				$(".modal").show();
				$(".modal-overlay").show();
				$(".winner").hide();
				bodovi = bodovi - sec
				$(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja: " + pokusaj + "</br>vrijeme igre: " + minute + ":" + sekunde + "</br><p><form id='input-form' required action='' method='POST' target='no-target'><br><select id='ikona' style='height:30px'></select><label for='ime'> Ime: </label><input style='height:30px' id='input-q1' name='q1'><br> <label for='bodovi'>Bodovi: </label><input id='input-q2' placeholder='q2' name='q2' value='" + bodovi + "' disabled style='display:none'> <label for='bodovi'>" + bodovi + "</label><br><button id='form-submit' type='submit'>predaj rezultat</button> </form>    <iframe src='#' id='no-target' name='no-target' style='visibility:hidden;display:none'></iframe><br><a href='index.html' style='color:black;'>nova igra</a></p></div>");

				var target = document.getElementById("ikona");
				var emojiCount = emoji.length;

				for (var index = 0; index < emojiCount; index++) {
					addEmoji(emoji[index]);
				}

				function addEmoji(code) {
					var option = document.createElement('option');
					option.innerHTML = code;
					option.value = code;
					target.appendChild(option);
				}
				if (localStorage.getItem("ime") != null) {
					$('#input-q1').val(localStorage.getItem("ime"))
					$('#ikona').val(localStorage.getItem("ikona"))
				}
				if (razina == 1) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSc6ML5kBM2bjCr3Zi7it5IlbX21g0H3IGs2_kZxHVXl3K63uQ/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez19.html';
							}, 1000);
					});
				} else if (razina == 2) {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSe79c23wm64A7kM_dk2fm5EoW-Yj0O0fpHKLlrYvmC8tiBpGg/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez20.html';
							}, 1000);
					});
				} else {
					$('#input-form').one('submit', function () {
$(this).fadeOut(300)
						localStorage.setItem("ikona", $('#ikona').val())
						localStorage.setItem('pokrenuto', "da")
						localStorage.setItem("ime", $('#input-q1').val())
						var inputq1 = encodeURIComponent($("#ikona").val() + $('#input-q1').val());
						var inputq2 = encodeURIComponent($('#input-q2').val());
						var q1ID = "entry.412821582";
						var q2ID = "entry.902512960";

						var baseURL =
							'https://docs.google.com/forms/d/e/1FAIpQLSd7-2Clhq3-_AO5a-JIQ0hPQeC8sIBgoPdtUrRKNGdV8r-s3A/formResponse?';
						var submitRef = '&submit=970054585833720596';
						var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
						console.log(submitURL);
						$(this)[0].action = submitURL;
						setTimeout(
							function () {
								window.location.href = 'rez21.html';
							}, 1000);
					});
				}
			},

			hideModal: function () {
				this.$overlay.hide();
				this.$modal.hide();
			},

			reset: function () {
				this.hideModal();
				this.shuffleCards(this.cardsArray);
				this.setup();
				this.$game.show("slow");
				pokusaj = 0;
				sec = 0;
				br = 1;
				$(".back").addClass("pozadina-vozila");
			},

			// Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
			shuffle: function (array) {
				var counter = array.length,
					temp, index;
				// While there are elements in the array
				while (counter > 0) {
					// Pick a random index
					index = Math.floor(Math.random() * counter);
					// Decrease counter by 1
					counter--;
					// And swap the last element with it
					temp = array[counter];
					array[counter] = array[index];
					array[index] = temp;
				}
				return array;
			},

			buildHTML: function () {


				var frag = '';
				br = 1;
				var lista_slika = [];
				var lista_imena = [];
				this.$cards.each(function (k, v) {
					if (Math.floor((Math.random() * 2) + 1) == 1) {
						if ($.inArray(v.name, lista_imena) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);

						}
					} else {
						if ($.inArray(v.img, lista_slika) == -1) {

							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_slika.push(v.img);


						} else {
							frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
							if (br < cards.length) {
								br++;
							};

							lista_imena.push(v.name);

						}
					}
				});
				return frag;
			}
		};

		var cards = [{
			name: "autobus",
			img: "slike/prometna_sredstva/autobus.jpg",
			id: 1,
		}, {
			name: "automobil",
			img: "slike/prometna_sredstva/automobil.jpg",
			id: 2
		}, {
			name: "balon",
			img: "slike/prometna_sredstva/balon.jpg",
			id: 3
		}, {
			name: "bicikl",
			img: "slike/prometna_sredstva/bicikl.jpg",
			id: 4
		}, {
			name: "brod",
			img: "slike/prometna_sredstva/brod.jpg",
			id: 5
		}, {
			name: "čamac",
			img: "slike/prometna_sredstva/camac.jpg",
			id: 6
		}, {
			name: "helikopter",
			img: "slike/prometna_sredstva/helikopter.jpg",
			id: 7
		}, {
			name: "kamion",
			img: "slike/prometna_sredstva/kamion.jpg",
			id: 8
		}, {
			name: "motor",
			img: "slike/prometna_sredstva/motor.jpg",
			id: 9
		}, {
			name: "trajekt",
			img: "slike/prometna_sredstva/trajekt.jpg",
			id: 10
		}, {
			name: "traktor",
			img: "slike/prometna_sredstva/traktor.jpg",
			id: 11
		}, {
			name: "tramvaj",
			img: "slike/prometna_sredstva/tramvaj.jpg",
			id: 12
		}, {
			name: "vlak",
			img: "slike/prometna_sredstva/vlak.jpg",
			id: 13
		}, {
			name: "željeznica",
			img: "slike/prometna_sredstva/zeljeznica.jpg",
			id: 14
		}, {
			name: "zrakoplov",
			img: "slike/prometna_sredstva/zrakoplov.jpg",
			id: 15
		}]

		function shuffle(array) {
			var currentIndex = array.length,
				temporaryValue, randomIndex;

			// While there remain elements to shuffle...
			while (0 !== currentIndex) {

				// Pick a remaining element...
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;

				// And swap it with the current element.
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
			}

			return array;
		}

		cards = shuffle(cards);

		cards = cards.slice(0, broj_karata);

		Memory.init(cards);


		if (razina == 1) {
			$(".card").css({
				"width": "25%",
				"height": "50%"
			})
		} else if (razina == 2) {
			$(".card").css({
				"width": "25%",
				"height": "25%"
			})
		} else if (razina == 3) {
			$(".card").css({
				"width": "16.66666%",
				"height": "25%"
			})
		}

		$(".back").addClass("pozadina-vozila");
	}
});


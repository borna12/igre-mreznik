// Memory Game
// © 2014 Nate Wiley
// License -- MIT
// best in full screen, works on phones/tablets (min height for game is 500px..) enjoy ;)
// Follow me on Codepen

$("footer").hide();
var razina = 1;
var broj_karata = 3;
$(".zivotinje").click(function () {
    $(".modal").html("<h2 class='winner'>Odaberi broj parova:</h2><button id='prva'>4</button> <button id='druga'>8</button><button id='treca'>12</button>");
    $("#prva").click(function () {
        razina = "1";
        igra()
    })
    $("#druga").click(function () {
        razina = "2";
        igra()
    })
    $("#treca").click(function () {
        razina = "3";
        igra()
    })

    function igra() {

        if (razina == 1) {
            broj_karata = 4;

        } else if (razina == 2) {
            broj_karata = 8;
        } else {
            broj_karata = 12
        }
        $("footer").fadeIn(1000);
        $(".modal").fadeOut(1000);
        $(".modal-overlay").delay(1000).slideUp(1000);
        $(".game").show("slow");
        $("#okretanje")[0].play();
        //localStorage.clear();
        var br = 1;
        var sec = 0;
        var pokusaj = 0;
        var vrijeme = 1;
        var bodovi = 0;

        var najbolje_vrijeme;
        var najmanji_broj_pokusaja;
        var karte;



        function pad(val) {
            return val > 9 ? val : "0" + val;
        }
        setInterval(function () {
            if (vrijeme == 1) {
                $("#seconds").html(pad(++sec % 60));
                $("#minutes").html(pad(parseInt(sec / 60, 10)));
            }
        }, 1000);

        var Memory = {
            init: function (cards) {
                this.$game = $(".game");
                this.$modal = $(".modal");
                this.$overlay = $(".modal-overlay");
                this.$zivotinje = $(".zivotinje");
                this.$ljudi = $(".ljudi");
                this.cardsArray = $.merge(cards, cards);
                this.shuffleCards(this.cardsArray);
                this.setup();
            },

            shuffleCards: function (cardsArray) {
                this.$cards = $(this.shuffle(this.cardsArray));
            },

            setup: function () {
                this.html = this.buildHTML();
                this.$game.html(this.html);
                this.$memoryCards = $(".card");
                this.binding();
                this.paused = false;
                this.guess = null;
                this.$cards = $(this.shuffle(this.cardsArray));
            },

            binding: function () {
                this.$memoryCards.on("click", this.cardClicked);
                this.$zivotinje.on("click", $.proxy(this.reset, this));
            },
            // kinda messy but hey
            cardClicked: function () {
                $("#okret")[0].play();

                var _ = Memory;
                var $card = $(this);
                if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

                    $card.find(".inside").addClass("picked");
                    if (!_.guess) {
                        _.guess = $(this).attr("data-id");
                        $(this).find('p').toggle();
                    } else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
                        $(".picked").addClass("matched");

                        switch ($(this).find('img').attr('alt')) {

                            case "mačak":
                                $("#maca")[0].play();
                                break;
                            case "mačka":
                                $("#maca")[0].play();
                                break;
                            case "guska":
                                $("#guska")[0].play();
                                break;
                            case "gusak":
                                $("#guska")[0].play();
                                break;
                            case "jelen":
                                $("#jelen")[0].play();
                                break;
                            case "košuta":
                                $("#jelen")[0].play();
                                break;
                            case "kokoš":
                                $("#kokos")[0].play();
                                break;
                            case "pijetao":
                                $("#kokos")[0].play();
                                break;
                            case "koza":
                                $("#koza")[0].play();
                                break;
                            case "jarac":
                                $("#koza")[0].play();
                                break;
                            case "lav":
                                $("#lav")[0].play();
                                break;
                            case "lavica":
                                $("#lav")[0].play();
                                break;
                            case "lisica":
                                $("#lisica")[0].play();
                                break;
                            case "lisac":
                                $("#lisica")[0].play();
                                break;
                            case "ovca":
                                $("#ovca")[0].play();
                                break;
                            case "ovan":
                                $("#ovca")[0].play();
                                break;
                            case "pas":
                                $("#peso")[0].play();
                                break;
                            case "kuja":
                                $("#peso")[0].play();
                                break;
                            case "patka":
                                $("#patka")[0].play();
                                break;
                            case "patak":
                                $("#patka")[0].play();
                                break;
                            case "pčela":
                                $("#pcela")[0].play();
                                break;
                            case "trut":
                                $("#pcela")[0].play();
                                break;
                            case "slon":
                                $("#slon")[0].play();
                                break;
                            case "slonica":
                                $("#slon")[0].play();
                                break;
                            case "srna":
                                $("#jelen")[0].play();
                                break;
                            case "srndać":
                                $("#jelen")[0].play();
                                break;
                            case "žaba":
                                $("#zaba")[0].play();
                                break;
                            case "žabac":
                                $("#zaba")[0].play();
                                break;
                            case "majmun":
                                $("#majmun")[0].play();
                                break;
                            case "majmunica":
                                $("#majmun")[0].play();
                                break;
                        }

                        bodovi = bodovi + 15;
                        _.guess = null;
                        $(".matched").find('p').remove();
                        pokusaj++;
                    } else {
                        pokusaj++;
                        $(this).find('p').toggle();
                        _.guess = null;
                        _.paused = true;
                        setTimeout(function () {
                            $(".picked").removeClass("picked");
                            Memory.paused = false;
                            $(".brojevi").show();
                            bodovi = bodovi - 5
                        }, 1200);
                    }
                    if ($(".matched").length == $(".card").length) {
                        _.win();
                    }
                }
            },

            win: function () {
                this.paused = true;
                setTimeout(function () {
                    Memory.showModal();
                    Memory.$game.fadeOut();
                }, 1000);
            },

            showModal: function () {
                var minute = Math.floor(sec / 60);
                var sekunde = sec - minute * 60;
                this.$overlay.show();
                this.$modal.fadeIn("slow");
                var najvrijeme = localStorage.getItem('najvrijeme');

                if (najvrijeme === undefined || najvrijeme === null) {
                    najvrijeme = sec;
                    localStorage.setItem('najvrijeme', sec);
                }

                // If the user has more points than the currently stored high score then
                if (sec < najvrijeme) {
                    // Set the high score to the users' current points
                    najvrijeme = sec;
                    // Store the high score
                    localStorage.setItem('najvrijeme', sec);
                }



                // Return the high score

                var najpokusaji = localStorage.getItem('najpokusaji');

                if (najpokusaji === undefined || najpokusaji === null) {
                    najpokusaji = pokusaj;
                    localStorage.setItem('najpokusaji', pokusaj);
                }

                // If the user has more points than the currently stored high score then
                if (pokusaj < najpokusaji) {
                    // Set the high score to the users' current points
                    najpokusaji = pokusaj;
                    // Store the high score
                    localStorage.setItem('najpokusaji', pokusaj);
                }
                var naj_minute = Math.floor(najvrijeme / 60);
                var naj_sekunde = najvrijeme - naj_minute * 60;
                $(".modal").show();
                $(".modal-overlay").show();
                $(".winner").hide();
                bodovi = bodovi - sec
                $(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja : " + pokusaj + "</br>vrijeme igre : " + minute + " : " + sekunde + "</br><p><form id='input-form' action='' method='POST' target='no-target'><br><label for='ime'>Ime : </label><input id='input-q1' name='q1'><br> <label for='bodovi'>Bodovi : </label><input id='input-q2' placeholder='q2' name='q2' value='" + bodovi + "' disabled style='display:none'> <label for='bodovi'>" + bodovi + "</label><br><button id='form-submit' type='submit' disabled='true' >predaj rezultat</button> </form>    <iframe src='#' id='no-target' name='no-target' style='visibility:hidden;display:none'></iframe><br><a href='index.html' style='color:black;'>odaberite drugu igru</a></p></div>");
                
                $(' #input-q1').keyup(function () {
                    $('#form-submit').prop('disabled', this.value == "" ? true : false);
                })
                if (razina == 1) {
                    $('#input-form').one('submit', function () {
                        localStorage.setItem('pokrenuto', "da")
                        var inputq1 = encodeURIComponent($('#input-q1').val());
                        var inputq2 = encodeURIComponent($('#input-q2').val());
                        var q1ID = "entry.412821582";
                        var q2ID = "entry.902512960";

                        var baseURL =
                            'https://docs.google.com/forms/d/e/1FAIpQLSfS4253kDLO5PQ800WvlG6eFfk3Y2U48QetSuHkNeIuWzG26A/formResponse?';
                        var submitRef = '&submit=970054585833720596';
                        var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
                        console.log(submitURL);
                        $(this)[0].action = submitURL;
                        setTimeout(
                            function () {
                                window.location.href = 'rez.html';
                            }, 1000);
                    });
                } else if (razina == 2) {
                    $('#input-form').one('submit', function () {
                        localStorage.setItem('pokrenuto', "da")
                        var inputq1 = encodeURIComponent($('#input-q1').val());
                        var inputq2 = encodeURIComponent($('#input-q2').val());
                        var q1ID = "entry.412821582";
                        var q2ID = "entry.902512960";

                        var baseURL =
                            'https://docs.google.com/forms/d/e/1FAIpQLScffOhQxa0BlBTL8Kj9BPfVJj-5WyxupqmoKhnAyDCwxz2VBQ/formResponse?';
                        var submitRef = '&submit=970054585833720596';
                        var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
                        console.log(submitURL);
                        $(this)[0].action = submitURL;
                        setTimeout(
                            function () {
                                window.location.href = 'rez2.html';
                            }, 1000);
                    });
                } else {
                    $('#input-form').one('submit', function () {
                        localStorage.setItem('pokrenuto', "da")
                        var inputq1 = encodeURIComponent($('#input-q1').val());
                        var inputq2 = encodeURIComponent($('#input-q2').val());
                        var q1ID = "entry.412821582";
                        var q2ID = "entry.902512960";

                        var baseURL =
                            'https://docs.google.com/forms/d/e/1FAIpQLSeYAHremNM7kqYwUPWeitFIvs4ahVjnzGQf7a5YMwerxg6_1w/formResponse?';
                        var submitRef = '&submit=970054585833720596';
                        var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
                        console.log(submitURL);
                        $(this)[0].action = submitURL;
                        setTimeout(
                            function () {
                                window.location.href = 'rez3.html';
                            }, 1000);
                    });
                }


            },

            hideModal: function () {
                this.$overlay.hide();
                this.$modal.hide();
            },

            reset: function () {
                this.hideModal();
                this.shuffleCards(this.cardsArray);
                this.setup();
                this.$game.show("slow");
                pokusaj = 0;
                sec = 0;
                br = 1;
                $(".back").addClass("pozadina-zivotinje");
            },

            // Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
            shuffle: function (array) {
                var counter = array.length,
                    temp, index;
                // While there are elements in the array
                while (counter > 0) {
                    // Pick a random index
                    index = Math.floor(Math.random() * counter);
                    // Decrease counter by 1
                    counter--;
                    // And swap the last element with it
                    temp = array[counter];
                    array[counter] = array[index];
                    array[index] = temp;
                }
                return array;
            },

            buildHTML: function () {


                var frag = '';
                br = 1;
                var lista_slika = [];
                var lista_imena = [];
                this.$cards.each(function (k, v) {
                    if (Math.floor((Math.random() * 2) + 1) == 1) {
                        if ($.inArray(v.name, lista_imena) == -1) {

                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_imena.push(v.name);


                        } else {
                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name2 + '" /><span>' + v.name2 + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_slika.push(v.img);

                        }
                    } else {
                        if ($.inArray(v.img, lista_slika) == -1) {

                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name2 + '" /><span>' + v.name2 + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_slika.push(v.img);


                        } else {
                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_imena.push(v.name);

                        }
                    }
                });
                return frag;
            }
        };



        var cards = [{
            name: "guska",
            name2: "gusak",
            img: "slike/guska.png",
            id: 1,
        }, {
            name: "jelen",
            name2: "košuta",
            img: "slike/jelen.png",
            id: 2
        }, {
            name: "kokoš",
            name2: "pijetao",
            img: "slike/kokos.png",
            id: 3
        }, {
            name: "koza",
            name2: "jarac",
            img: "slike/koza.png",
            id: 4
        }, {
            name: "lav",
            name2: "lavica",
            img: "slike/lav.png",
            id: 5
        }, {
            name: "lisica",
            name2: "lisac",
            img: "slike/lisica.png",
            id: 6
        }, {
            name: "mačka",
            name2: "mačak",
            img: "slike/macka.png",
            id: 7
        }, {
            name: "ovca",
            name2: "ovan",
            img: "slike/ovca.png",
            id: 8
        }, {
            name: "pas",
            name2: "kuja",
            img: "slike/pas.png",
            id: 9
        }, {
            name: "patka",
            name2: "patak",
            img: "slike/patka.png",
            id: 10
        }, {
            name: "pčela",
            name2: "trut",
            img: "slike/pcela.png",
            id: 11
        }, {
            name: "slon",
            name2: "slonica",
            img: "slike/slon.png",
            id: 12
        }, {
            name: "srna",
            name2: "srndać",
            img: "slike/srna.png",
            id: 13
        }, {
            name: "žaba",
            name2: "žabac",
            img: "slike/zaba.png",
            id: 14
        }, {
            name: "majmun",
            name2: "majmunica",
            img: "slike/majmun.png",
            id: 15
        }]



        function shuffle(array) {
            var currentIndex = array.length,
                temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        }

        cards = shuffle(cards);

        cards = cards.slice(0, broj_karata);

        Memory.init(cards);


        if (razina == 1) {
            $(".card").css({
                "width": "25%",
                "height": "50%"
            })
        } else if (razina == 2) {
            $(".card").css({
                "width": "25%",
                "height": "25%"
            })
        } else if (razina == 3) {
            $(".card").css({
                "width": "16.66666%",
                "height": "25%"
            })
        }

        $(".back").addClass("pozadina-zivotinje");
    }
});
$(".zanimanja").click(function () {

    $(".modal").html("<h2 class='winner'>Odaberi broj parova:</h2><button id='prva'>4</button> <button id='druga'>8</button><button id='treca'>12</button>");
    $("#prva").click(function () {
        razina = "1";
        igra()
    })
    $("#druga").click(function () {
        razina = "2";
        igra()
    })
    $("#treca").click(function () {
        razina = "3";
        igra()
    })


    function igra() {

        if (razina == 1) {
            broj_karata = 4;

        } else if (razina == 2) {
            broj_karata = 8;
        } else {
            broj_karata = 12
        }

        $("footer").fadeIn(1000);
        $(".modal").fadeOut(1000);
        $(".modal-overlay").delay(1000).slideUp(1000);
        $(".game").show("slow");
        $("#okretanje")[0].play();

        //localStorage.clear();
        var br = 1;
        var sec = 0;
        var pokusaj = 0;
        var vrijeme = 1;
        var bodovi = 0;

        var najbolje_vrijeme;
        var najmanji_broj_pokusaja;
        var karte;



        function pad(val) {
            return val > 9 ? val : "0" + val;
        }
        setInterval(function () {
            if (vrijeme == 1) {
                $("#seconds").html(pad(++sec % 60));
                $("#minutes").html(pad(parseInt(sec / 60, 10)));
            }
        }, 1000);

        var Memory = {
            init: function (cards) {
                this.$game = $(".game");
                this.$modal = $(".modal");
                this.$overlay = $(".modal-overlay");
                this.$zanimanja = $(".zanimanja");
                this.$ljudi = $(".ljudi");
                this.cardsArray = $.merge(cards, cards);
                this.shuffleCards(this.cardsArray);
                this.setup();
            },

            shuffleCards: function (cardsArray) {
                this.$cards = $(this.shuffle(this.cardsArray));
            },

            setup: function () {
                this.html = this.buildHTML();
                this.$game.html(this.html);
                this.$memoryCards = $(".card");
                this.binding();
                this.paused = false;
                this.guess = null;
                this.$cards = $(this.shuffle(this.cardsArray));
            },

            binding: function () {
                this.$memoryCards.on("click", this.cardClicked);
                this.$zanimanja.on("click", $.proxy(this.reset, this));
            },
            // kinda messy but hey
            cardClicked: function () {
                $("#okret")[0].play();

                var _ = Memory;
                var $card = $(this);
                if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

                    $card.find(".inside").addClass("picked");
                    if (!_.guess) {
                        _.guess = $(this).attr("data-id");
                        $(this).find('p').toggle();
                    } else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
                        $(".picked").addClass("matched");
                        $("#win")[0].play();
                        bodovi = bodovi + 15;

                        _.guess = null;
                        $(".matched").find('p').remove();
                        pokusaj++;
                    } else {
                        pokusaj++;
                        $(this).find('p').toggle();
                        _.guess = null;
                        _.paused = true;
                        setTimeout(function () {
                            $(".picked").removeClass("picked");
                            Memory.paused = false;
                            $(".brojevi").show();
                            bodovi = bodovi - 5
                        }, 1200);
                    }
                    if ($(".matched").length == $(".card").length) {
                        _.win();
                    }
                }
            },

            win: function () {
                this.paused = true;
                setTimeout(function () {
                    Memory.showModal();
                    Memory.$game.fadeOut();
                }, 1000);
            },

            showModal: function () {
                var minute = Math.floor(sec / 60);
                var sekunde = sec - minute * 60;
                this.$overlay.show();
                this.$modal.fadeIn("slow");
                var najvrijeme = localStorage.getItem('najvrijeme');

                if (najvrijeme === undefined || najvrijeme === null) {
                    najvrijeme = sec;
                    localStorage.setItem('najvrijeme', sec);
                }

                // If the user has more points than the currently stored high score then
                if (sec < najvrijeme) {
                    // Set the high score to the users' current points
                    najvrijeme = sec;
                    // Store the high score
                    localStorage.setItem('najvrijeme', sec);
                }



                // Return the high score

                var najpokusaji = localStorage.getItem('najpokusaji');

                if (najpokusaji === undefined || najpokusaji === null) {
                    najpokusaji = pokusaj;
                    localStorage.setItem('najpokusaji', pokusaj);
                }

                // If the user has more points than the currently stored high score then
                if (pokusaj < najpokusaji) {
                    // Set the high score to the users' current points
                    najpokusaji = pokusaj;
                    // Store the high score
                    localStorage.setItem('najpokusaji', pokusaj);
                }
                var naj_minute = Math.floor(najvrijeme / 60);
                var naj_sekunde = najvrijeme - naj_minute * 60;
                $(".modal").show();
                $(".modal-overlay").show();
                bodovi = bodovi - sec
                $(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja : " + pokusaj + "</br>vrijeme igre : " + minute + " : " + sekunde + "</br><p><form id='input-form' action='' method='POST' target='no-target'><br><label for='ime'>Ime : </label><input id='input-q1' name='q1'><br> <label for='bodovi'>Bodovi : </label><input id='input-q2' placeholder='q2' name='q2' value='" + bodovi + "' disabled style='display:none'> <label for='bodovi'>" + bodovi + "</label><br><button id='form-submit' type='submit' disabled='true'>predaj rezultat</button> </form>    <iframe src='#' id='no-target' name='no-target' style='visibility:hidden;display:none'></iframe><br><a href='index.html' style='color:black;'>odaberite drugu igru</a></p></div>");
                
                $(' #input-q1').keyup(function () {
                    $('#form-submit').prop('disabled', this.value == "" ? true : false);
                })
                if (razina == 1) {
                    $('#input-form').one('submit', function () {
                        localStorage.setItem('pokrenuto', "da")
                        var inputq1 = encodeURIComponent($('#input-q1').val());
                        var inputq2 = encodeURIComponent($('#input-q2').val());
                        var q1ID = "entry.412821582";
                        var q2ID = "entry.902512960";

                        var baseURL =
                            'https://docs.google.com/forms/d/e/1FAIpQLSfRb98-qN51s2qZlkqeZE1fIu4SPquaeEWbyI178o91GbU_ug/formResponse?';
                        var submitRef = '&submit=970054585833720596';
                        var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
                        console.log(submitURL);
                        $(this)[0].action = submitURL;
                        setTimeout(
                            function () {
                                window.location.href = 'rez4.html';
                            }, 1000);
                    });
                } else if (razina == 2) {
                    $('#input-form').one('submit', function () {
                        localStorage.setItem('pokrenuto', "da")
                        var inputq1 = encodeURIComponent($('#input-q1').val());
                        var inputq2 = encodeURIComponent($('#input-q2').val());
                        var q1ID = "entry.412821582";
                        var q2ID = "entry.902512960";

                        var baseURL =
                            'https://docs.google.com/forms/d/e/1FAIpQLSfpaRZnYKUtZfyNuU24--fcO36pq2qfcZskUudy5UWw2Kby4A/formResponse?';
                        var submitRef = '&submit=970054585833720596';
                        var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
                        console.log(submitURL);
                        $(this)[0].action = submitURL;
                        setTimeout(
                            function () {
                                window.location.href = 'rez5.html';
                            }, 1000);
                    });
                } else {
                    $('#input-form').one('submit', function () {
                        localStorage.setItem('pokrenuto', "da")
                        var inputq1 = encodeURIComponent($('#input-q1').val());
                        var inputq2 = encodeURIComponent($('#input-q2').val());
                        var q1ID = "entry.412821582";
                        var q2ID = "entry.902512960";

                        var baseURL =
                            'https://docs.google.com/forms/d/e/1FAIpQLSc-0Vk2O5aJj_y4w-Ehl-ZFFz6qU1XNB8ms4P5H22vQWsDayg/formResponse?';
                        var submitRef = '&submit=970054585833720596';
                        var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
                        console.log(submitURL);
                        $(this)[0].action = submitURL;
                        setTimeout(
                            function () {
                                window.location.href = 'rez6.html';
                            }, 1000);
                    });
                }


            },

            hideModal: function () {
                this.$overlay.hide();
                this.$modal.hide();
            },

            reset: function () {
                this.hideModal();
                this.shuffleCards(this.cardsArray);
                this.setup();
                this.$game.show("slow");
                pokusaj = 0;
                sec = 0;
                br = 1;
                $(".back").addClass("pozadina-zanimaje");
            },

            // Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
            shuffle: function (array) {
                var counter = array.length,
                    temp, index;
                // While there are elements in the array
                while (counter > 0) {
                    // Pick a random index
                    index = Math.floor(Math.random() * counter);
                    // Decrease counter by 1
                    counter--;
                    // And swap the last element with it
                    temp = array[counter];
                    array[counter] = array[index];
                    array[index] = temp;
                }
                return array;
            },

            buildHTML: function () {


                var frag = '';
                br = 1;
                var lista_slika = [];
                var lista_imena = [];
                this.$cards.each(function (k, v) {
                    if (Math.floor((Math.random() * 2) + 1) == 1) {
                        if ($.inArray(v.name, lista_imena) == -1) {

                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_imena.push(v.name);


                        } else {
                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img2 + '"\
				alt="' + v.name2 + '" /><span>' + v.name2 + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_slika.push(v.img);

                        }
                    } else {
                        if ($.inArray(v.img, lista_slika) == -1) {

                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img2 + '"\
				alt="' + v.name2 + '" /><span>' + v.name2 + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_slika.push(v.img);


                        } else {
                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_imena.push(v.name);

                        }
                    }
                });
                return frag;
            }
        };



        var cards = [{
            name: "književnik",
            name2: "književnica",
            img: "slike/knjizevnik.jpg",
            img2: "slike/knizevnica.png",
            id: 1,
        }, {
            name: "liječnik",
            name2: "liječnica",
            img: "slike/doktor.jpg",
            img2: "slike/doktorica.jpg",
            id: 2
        }, {
            name: "inženjer",
            name2: "inženjerka",
            img2: "slike/inzenjer.png",
            img: "slike/inzenjer-2.png",
            id: 3
        }, {
            name: "istraživač",
            name2: "istraživačica",
            img2: "slike/istrazivac.png",
            img: "slike/istrazivac-2.png",
            id: 4
        }, {
            name: "konobar",
            name2: "konobarica",
            img: "slike/konobar.png",
            img2: "slike/konobarica.png",
            id: 5
        }, {
            name: "kuhar",
            name2: "kuharica",
            img2: "slike/kuhar.png",
            img: "slike/kuhar-2.png",
            id: 6
        }, {
            name: "medicinska sestra",
            name2: "medicinski tehničar",
            img: "slike/medicinska_sestra.png",
            img2: "slike/medicinski_tehnicar.png",
            id: 7
        }, {
            name: "učitelj",
            name2: "učiteljica",
            img2: "slike/ucitelj.png",
            img: "slike/ucitelj-2.png",
            id: 8
        }, {
            name: "vozač",
            name2: "vozačica",
            img2: "slike/vozac.png",
            img: "slike/vozac-2.png",
            id: 9
        }, {
            name: "blagajnik",
            name2: "blagajnica",
            img2: "slike/blagajnik.jpg",
            img: "slike/blagajnik.gif",
            id: 10
        }, {
            name: "zubar",
            name2: "zubarica",
            img: "slike/zubar.jpg",
            img2: "slike/zubarica.jpeg",
            id: 11
        }, {
            name: "odgojitelj",
            name2: "odgojiteljica",
            img2: "slike/pedagog.png",
            img: "slike/pedagogo-2.jpg",
            id: 12
        }]



        function shuffle(array) {
            var currentIndex = array.length,
                temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        }

        cards = shuffle(cards);

        cards = cards.slice(0, broj_karata);

        Memory.init(cards);


        $(".back").addClass("pozadina-zanimaje");
        if (razina == 1) {
            $(".card").css({
                "width": "25%",
                "height": "50%"
            })
        } else if (razina == 2) {
            $(".card").css({
                "width": "25%",
                "height": "25%"
            })
        } else if (razina == 3) {
            $(".card").css({
                "width": "16.66666%",
                "height": "25%"
            })
        }
    }
});
$(".ljudi").click(function () {

    $(".modal").html("<h2 class='winner'>Odaberi broj parova:</h2><button id='prva'>4</button> <button id='druga'>8</button><button id='treca'>12</button>");
    $("#prva").click(function () {
        razina = "1";
        igra()
    })
    $("#druga").click(function () {
        razina = "2";
        igra()
    })
    $("#treca").click(function () {
        razina = "3";
        igra()
    })


    function igra() {

        if (razina == 1) {
            broj_karata = 4;

        } else if (razina == 2) {
            broj_karata = 8;
        } else {
            broj_karata = 12
        }

        $("footer").fadeIn(1000);
        $(".modal").fadeOut(1000);
        $(".modal-overlay").delay(1000).slideUp(1000);
        $(".game").show("slow");
        $("#okretanje")[0].play();

        //localStorage.clear();
        var br = 1;
        var sec = 0;
        var pokusaj = 0;
        var vrijeme = 1;
        var bodovi = 0;

        var najbolje_vrijeme;
        var najmanji_broj_pokusaja;
        var karte;



        function pad(val) {
            return val > 9 ? val : "0" + val;
        }
        setInterval(function () {
            if (vrijeme == 1) {
                $("#seconds").html(pad(++sec % 60));
                $("#minutes").html(pad(parseInt(sec / 60, 10)));
            }
        }, 1000);

        var Memory = {
            init: function (cards) {
                this.$game = $(".game");
                this.$modal = $(".modal");
                this.$overlay = $(".modal-overlay");
                this.$zanimanja = $(".zanimanja");
                this.$ljudi = $(".ljudi");
                this.cardsArray = $.merge(cards, cards);
                this.shuffleCards(this.cardsArray);
                this.setup();
            },

            shuffleCards: function (cardsArray) {
                this.$cards = $(this.shuffle(this.cardsArray));
            },

            setup: function () {
                this.html = this.buildHTML();
                this.$game.html(this.html);
                this.$memoryCards = $(".card");
                this.binding();
                this.paused = false;
                this.guess = null;
                this.$cards = $(this.shuffle(this.cardsArray));
            },

            binding: function () {
                this.$memoryCards.on("click", this.cardClicked);
                this.$zanimanja.on("click", $.proxy(this.reset, this));
            },
            // kinda messy but hey
            cardClicked: function () {
                $("#okret")[0].play();

                var _ = Memory;
                var $card = $(this);
                if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

                    $card.find(".inside").addClass("picked");
                    if (!_.guess) {
                        _.guess = $(this).attr("data-id");
                        $(this).find('p').toggle();
                    } else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
                        $(".picked").addClass("matched");
                        $("#win")[0].play();
                        bodovi = bodovi + 15;

                        _.guess = null;
                        $(".matched").find('p').remove();
                        pokusaj++;
                    } else {
                        pokusaj++;
                        $(this).find('p').toggle();
                        _.guess = null;
                        _.paused = true;
                        setTimeout(function () {
                            $(".picked").removeClass("picked");
                            Memory.paused = false;
                            $(".brojevi").show();
                            bodovi = bodovi - 5
                        }, 1200);
                    }
                    if ($(".matched").length == $(".card").length) {
                        _.win();
                    }
                }
            },

            win: function () {
                this.paused = true;
                setTimeout(function () {
                    Memory.showModal();
                    Memory.$game.fadeOut();
                }, 1000);
            },

            showModal: function () {
                var minute = Math.floor(sec / 60);
                var sekunde = sec - minute * 60;
                this.$overlay.show();
                this.$modal.fadeIn("slow");
                var najvrijeme = localStorage.getItem('najvrijeme');

                if (najvrijeme === undefined || najvrijeme === null) {
                    najvrijeme = sec;
                    localStorage.setItem('najvrijeme', sec);
                }

                // If the user has more points than the currently stored high score then
                if (sec < najvrijeme) {
                    // Set the high score to the users' current points
                    najvrijeme = sec;
                    // Store the high score
                    localStorage.setItem('najvrijeme', sec);
                }



                // Return the high score

                var najpokusaji = localStorage.getItem('najpokusaji');

                if (najpokusaji === undefined || najpokusaji === null) {
                    najpokusaji = pokusaj;
                    localStorage.setItem('najpokusaji', pokusaj);
                }

                // If the user has more points than the currently stored high score then
                if (pokusaj < najpokusaji) {
                    // Set the high score to the users' current points
                    najpokusaji = pokusaj;
                    // Store the high score
                    localStorage.setItem('najpokusaji', pokusaj);
                }
                var naj_minute = Math.floor(najvrijeme / 60);
                var naj_sekunde = najvrijeme - naj_minute * 60;
                $(".modal").show();
                $(".modal-overlay").show();
                bodovi = bodovi - sec
                $(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja : " + pokusaj + "</br>vrijeme igre : " + minute + " : " + sekunde + "</br><p><form id='input-form' action='' method='POST' target='no-target'><br><label for='ime'>Ime : </label><input id='input-q1' name='q1'><br> <label for='bodovi'>Bodovi : </label><input id='input-q2' placeholder='q2' name='q2' value='" + bodovi + "' disabled style='display:none'> <label for='bodovi'>" + bodovi + "</label><br><button id='form-submit' type='submit' disabled='true'>predaj rezultat</button> </form>    <iframe src='#' id='no-target' name='no-target' style='visibility:hidden;display:none'></iframe><br><a href='index.html' style='color:black;'>odaberite drugu igru</a></p></div>");
                
                $(' #input-q1').keyup(function () {
                    $('#form-submit').prop('disabled', this.value == "" ? true : false);
                })
                if (razina == 1) {
                    $('#input-form').one('submit', function () {
                        localStorage.setItem('pokrenuto', "da")
                        var inputq1 = encodeURIComponent($('#input-q1').val());
                        var inputq2 = encodeURIComponent($('#input-q2').val());
                        var q1ID = "entry.412821582";
                        var q2ID = "entry.902512960";

                        var baseURL =
                            'https://docs.google.com/forms/d/1Tw77II1CdPH76Pexi1ft6MRoyshOSoGfICN-09sL0-4/formResponse?';
                        var submitRef = '&submit=970054585833720596';
                        var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
                        console.log(submitURL);
                        $(this)[0].action = submitURL;
                        setTimeout(
                            function () {
                                window.location.href = 'rez7.html';
                            }, 1000);
                    });
                } else if (razina == 2) {
                    $('#input-form').one('submit', function () {
                        localStorage.setItem('pokrenuto', "da")
                        var inputq1 = encodeURIComponent($('#input-q1').val());
                        var inputq2 = encodeURIComponent($('#input-q2').val());
                        var q1ID = "entry.412821582";
                        var q2ID = "entry.902512960";

                        var baseURL =
                            'https://docs.google.com/forms/d/1P5ek42a1-xuHdXaKgX4M2RWlQdZuogr9oM3i-aeFT7g/formResponse?';
                        var submitRef = '&submit=970054585833720596';
                        var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
                        console.log(submitURL);
                        $(this)[0].action = submitURL;
                        setTimeout(
                            function () {
                                window.location.href = 'rez8.html';
                            }, 1000);
                    });
                } else {
                    $('#input-form').one('submit', function () {
                        localStorage.setItem('pokrenuto', "da")
                        var inputq1 = encodeURIComponent($('#input-q1').val());
                        var inputq2 = encodeURIComponent($('#input-q2').val());
                        var q1ID = "entry.412821582";
                        var q2ID = "entry.902512960";

                        var baseURL =
                            'https://docs.google.com/forms/d/1M9L9nzL8Zm_lGrs-_PR2JLmTBrlUZSRRpBluc_QuLOA/formResponse?';
                        var submitRef = '&submit=970054585833720596';
                        var submitURL = (baseURL + q1ID + "=" + inputq1 + "&" + q2ID + "=" + inputq2 + submitRef);
                        console.log(submitURL);
                        $(this)[0].action = submitURL;
                        setTimeout(
                            function () {
                                window.location.href = 'rez9.html';
                            }, 1000);
                    });
                }


            },

            hideModal: function () {
                this.$overlay.hide();
                this.$modal.hide();
            },

            reset: function () {
                this.hideModal();
                this.shuffleCards(this.cardsArray);
                this.setup();
                this.$game.show("slow");
                pokusaj = 0;
                sec = 0;
                br = 1;
                $(".back").addClass("pozadina-ljudi");
            },

            // Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
            shuffle: function (array) {
                var counter = array.length,
                    temp, index;
                // While there are elements in the array
                while (counter > 0) {
                    // Pick a random index
                    index = Math.floor(Math.random() * counter);
                    // Decrease counter by 1
                    counter--;
                    // And swap the last element with it
                    temp = array[counter];
                    array[counter] = array[index];
                    array[index] = temp;
                }
                return array;
            },

            buildHTML: function () {


                var frag = '';
                br = 1;
                var lista_slika = [];
                var lista_imena = [];
                this.$cards.each(function (k, v) {
                    if (Math.floor((Math.random() * 2) + 1) == 1) {
                        if ($.inArray(v.name, lista_imena) == -1) {

                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_imena.push(v.name);


                        } else {
                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img2 + '"\
				alt="' + v.name2 + '" /><span>' + v.name2 + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_slika.push(v.img);

                        }
                    } else {
                        if ($.inArray(v.img, lista_slika) == -1) {

                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img2 + '"\
				alt="' + v.name2 + '" /><span>' + v.name2 + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_slika.push(v.img);


                        } else {
                            frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="' + v.img + '"\
				alt="' + v.name + '" /><span>' + v.name + '</span></div>\
				<div class="back"><p class="brojevi">' + br + '</p></div></div>\
				</div>';
                            if (br < cards.length) {
                                br++;
                            };

                            lista_imena.push(v.name);

                        }
                    }
                });
                return frag;
            }
        };

        var cards = [{
            name: "biciklist",
            name2: "biciklistica",
            img: "slike/biciklist.png",
            img2: "slike/biciklistica.png",
            id: 1,
        }, {
            name: "brat",
            name2: "sestra",
            img: "slike/brat.png",
            img2: "slike/sestra.png",
            id: 2
        }, {
            name: "čitatelj",
            name2: "čitateljica",
            img: "slike/citatelj.png",
            img2: "slike/citateljica.png",
            id: 3
        }, {
            name: "književnik",
            name2: "književnica",
            img: "slike/knjizevnik.jpg",
            img2: "slike/knizevnica.png",
            id: 4
        }, {
            name: "muškarac",
            name2: "žena",
            img: "slike/muskarac.png",
            img2: "slike/zena.png",
            id: 5
        }, {
            name: "starac",
            name2: "starica",
            img: "slike/starac.png",
            img2: "slike/starica.png",
            id: 6
        }, {
            name: "učenik",
            name2: "učenica",
            img: "slike/ucenik.png",
            img2: "slike/ucenica.png",
            id: 7
        }, {
            name: "građanin",
            name2: "građanka",
            img: "slike/gradanin.png",
            img2: "slike/gradanka.png",
            id: 8
        }, {
            name: "kralj",
            name2: "kraljica",
            img: "slike/kralj.png",
            img2: "slike/kraljica.png",
            id: 9
        }, {
            name: "seljak",
            name2: "seljakinja",
            img: "slike/seljak.png",
            img2: "slike/seljanka.jpg",
            id: 10
        }, {
            name: "političar",
            name2: "političarka",
            img: "slike/politicar.png",
            img2: "slike/politicarka.png",
            id: 11
        }, {
            name: "sportaš",
            name2: "sportašica",
            img: "slike/sportas.png",
            img2: "slike/sportasica.png",
            id: 12
        }]

        function shuffle(array) {
            var currentIndex = array.length,
                temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        }

        cards = shuffle(cards);

        cards = cards.slice(0, broj_karata);

        Memory.init(cards);


        $(".back").addClass("pozadina-ljudi");
        if (razina == 1) {
            $(".card").css({
                "width": "25%",
                "height": "50%"
            })
        } else if (razina == 2) {
            $(".card").css({
                "width": "25%",
                "height": "25%"
            })
        } else if (razina == 3) {
            $(".card").css({
                "width": "16.66666%",
                "height": "25%"
            })
        }
    }
});
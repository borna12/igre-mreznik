
// VARIABLE DECLARATIONS ------
// pages
var initPage,
    questionsPage,
    resultsPage,
    // buttons
    startBtn,
    submitBtn,
    continueBtn,
    spanishBtn,
    // question and answers
    question,
    answerList,
    answerSpan,
    answerA,
    answerB,
    answerC,
    answerD,
    // event listeners
    answerDiv,
    answerDivA,
    answerDivB,
    answerDivC,
    answerDivD,
    feedbackDiv,
    selectionDiv,
    toBeHighlighted,
    toBeMarked, iskljuci_v = 0,
    userScore,
    // quiz
    prezent,
    questionCounter,
    correctAnswer,
    correctAnswersCounter,
    userSelectedAnswer,
    // function names
    newQuiz,
    generateQuestionAndAnswers,
    getCorrectAnswer,
    getUserAnswer, tajming,
    selectAnswer,
    deselectAnswer,
    selectCorrectAnswer,
    deselectCorrectAnswer,
    getSelectedAnswerDivs,
    highlightCorrectAnswerGreen,
    highlightIncorrectAnswerRed,
    slikica, brb = 0,
    clearHighlightsAndFeedback, r1,
    prekidac, countdownTimer, bodovi = 0,
    vrijeme = 0;

function ProgressCountdown(timeleft, bar, text) {
    return new Promise((resolve, reject) => {
        countdownTimer = setInterval(() => {
            timeleft--;
            document.getElementById(bar).value = timeleft;
            document.getElementById(text).textContent = timeleft;
            if (timeleft <= 0) {
                clearInterval(countdownTimer);
                resolve(true);
            }
        }, 1000);
    });
}

var public_spreadsheet_url =
    'https://docs.google.com/spreadsheets/d/1NgWaa0eh-WD0Lqtht_yXvHu7HYFsJs5q4WH7AtQBXJg/edit#gid=327934038';

function init() {
    Tabletop.init({
        key: public_spreadsheet_url,
        callback: showInfo,
        simpleSheet: true
    });
}

lista_pijan = []
lista_kocka = []
lista_kosa = []
function showInfo(data) {
    for (var i = 0; i < data.length; i++) {
        p = data[i].pijankao
        p2 = data[i].VrijednostPijan
        k = data[i].kockaje
        k2 = data[i].VrijednostKocka
        c = data[i].cupati
        c2 = data[i].VrijednostCupati
        if (p == undefined) {
            init()
            return
        }
        else {
            frazemi[0]["odgovori"].push(p)
            frazemi[0]["bodovi"].push(p2)
            frazemi[1]["odgovori"].push(k)
            frazemi[1]["bodovi"].push(k2)
            frazemi[2]["odgovori"].push(c)
            frazemi[2]["bodovi"].push(c2)
        }
    }
    lista_pijan = frazemi[0]["odgovori"].filter(function (el) {
        return el != null && el != "";
    });
    lista_kocka = frazemi[1]["odgovori"].filter(function (el) {
        return el != null && el != "";
    });
    lista_kosa = frazemi[2]["odgovori"].filter(function (el) {
        return el != null && el != "";
    });
    $('.preloader').fadeOut('slow');
    shuffle(frazemi);
    $("#40").click()

}

init()

$(document).ready(function () {
    $('body').on('keydown', function (event) {
        var x = event.which;
        if (x === 13) {
            event.preventDefault();
        }
    });
    // DOM SELECTION ------
    // App pages
    // Page 1 - Initial
    initPage = $('.init-page');
    // Page 2 - Questions/answers
    questionsPage = $('.questions-page');
    // Page 3 - Results
    resultsPage = $('.results-page');
    slikica = $('.slikica');
    // Buttons
    startBtn = $('.init-page__btn, .results-page__retake-btn');
    submitBtn = $('.mrzim');
    continueBtn = $('.questions-page__continue-btn');
    spanishBtn = $('.results-page__spanish-btn');
    // Answer block divs
    answerDiv = $('.questions-page__answer-div');
    answerDivA = $('.questions-page__answer-div-a');
    answerDivB = $('.questions-page__answer-div-b');
    answerDivC = $('.questions-page__answer-div-c');
    answerDivD = $('.questions-page__answer-div-d');
    // Selection div (for the pointer, on the left)
    selectionDiv = $('.questions-page__selection-div');
    // Feedback div (for the checkmark or X, on the right)
    feedbackDiv = $('.questions-page__feedback-div');
    // Questions and answers
    question = $('.questions-page__question');
    answerList = $('.questions-page__answer-list');
    answerSpan = $('.questions-page__answer-span');
    answerA = $('.questions-page__answer-A');
    answerB = $('.questions-page__answer-B');
    answerC = $('.questions-page__answer-C');
    answerD = $('.questions-page__answer-D');
    // User final score
    userScore = $('.results-page__score');
    prikazBodova = $('.results-page__bodovi');
    // QUIZ CONTENT ------
  
      
    // FUNCTION DECLARATIONS ------
    $.fn.declasse = function (re) {
        return this.each(function () {
            var c = this.classList
            for (var i = c.length - 1; i >= 0; i--) {
                var classe = "" + c[i]
                if (classe.match(re)) c.remove(classe)
            }
        })
    }
    // Start the quiz
    newQuiz = function () {
        prekidac = 1;
        bodovi = 0;
        // Set the question counter to 0
        questionCounter = 0;
        // Set the total correct answers counter to 0
        correctAnswersCounter = 0;
        // Hide other pages of the app
        questionsPage.hide();
        resultsPage.hide();
    };

    // Load the next question and set of answers
    generateQuestionAndAnswers = function () {
        l=[]
        question.html("<span style='font-size: 1.3rem;'>" + (questionCounter + 1) + "/" + frazemi.length + ".</span> <br>");
        $("#odgovor").val('')
        $(".popuni").show();
        if(frazemi[questionCounter]["frazem2"]!=undefined){
            $('<span>&nbsp;</span><span id="osnova"></span>').insertBefore( "#odgovor" );
            $('<span>&nbsp;</span><span id="frazem2">'+frazemi[questionCounter]["frazem2"]+'</span>').insertAfter( "#odgovor" );
        }
        else if (frazemi[questionCounter]["frazem"][0].toUpperCase() == frazemi[questionCounter]["frazem"][0]) {
            $('<span id="osnova"></span> <span>&nbsp;</span>').insertBefore( "#odgovor" );
            $('<span id="tocka">.</span>').insertAfter( "#odgovor" );
        }
        else {
            $('<span>&nbsp;</span><span id="osnova"></span><span id="tocka">.</span>').insertAfter( "#odgovor" );
        }
        //polje za ispunjavanje
        var el = document.getElementById('odgovor');
        // el.focus();
        el.onblur = function () {
            setTimeout(function () {
                el.focus();
            });
        };
        $(".questions-page__answer-list").hide()
        //$("#opis").html("<em>" + prezent[questionCounter].vrijeme + "</em>")
        $(".vrijeme").html('<progress value="' + tajming + '" max="' + tajming + '" id="pageBeginCountdown"></progress><p><span id="pageBeginCountdownText">' + tajming + '</span></p>')
        if (prekidac == 1 && iskljuci_v == 0) {
            ProgressCountdown(tajming, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => vrijeme());
        }
        $("#osnova").text(frazemi[questionCounter]["frazem"])
    };

    // Store the correct answer of a given question
    getCorrectAnswer = function () {
        // correctAnswer = prezent[questionCounter].correctAnswer;
    };

    // Store the user's selected (clicked) answer
    getUserAnswer = function (target) {
        userSelectedAnswer = $(target).find(answerSpan).text();
    };

    // Add the pointer to the clicked answer
    selectAnswer = function (target) {
        $(target).find(selectionDiv).addClass('ion-chevron-right');
        $(target).addClass("odabir")
    };

    // Remove the pointer from any answer that has it
    deselectAnswer = function () {
        if (selectionDiv.hasClass('ion-chevron-right')) {
            selectionDiv.removeClass('ion-chevron-right');
            selectionDiv.parent().removeClass("odabir")
        }
    };

    // Get the selected answer's div for highlighting purposes
    getSelectedAnswerDivs = function (target) {
        toBeHighlighted = $(target);
        toBeMarked = $(target).find(feedbackDiv);
    };

    // Make the correct answer green and add checkmark
    highlightCorrectAnswerGreen = function (target) {
        if (correctAnswer === answerA.text()) {
            answerDivA.addClass('questions-page--correct');
            answerDivA.find(feedbackDiv).addClass('ion-checkmark-round');
        }
        if (correctAnswer === answerB.text()) {
            answerDivB.addClass('questions-page--correct');
            answerDivB.find(feedbackDiv).addClass('ion-checkmark-round');
        }
        if (correctAnswer === answerC.text()) {
            answerDivC.addClass('questions-page--correct');
            answerDivC.find(feedbackDiv).addClass('ion-checkmark-round');
        }
        if (correctAnswer === answerD.text()) {
            answerDivD.addClass('questions-page--correct');
            answerDivD.find(feedbackDiv).addClass('ion-checkmark-round');
        }
    };

    // Make the incorrect answer red and add X
    highlightIncorrectAnswerRed = function () {
        toBeHighlighted.addClass('questions-page--incorrect');
        toBeMarked.addClass('ion-close-round');
    };

    // Clear all highlighting and feedback
    clearHighlightsAndFeedback = function () {
        answerDiv.removeClass('questions-page--correct');
        answerDiv.removeClass('questions-page--incorrect');
        feedbackDiv.removeClass('ion-checkmark-round');
        feedbackDiv.removeClass('ion-close-round');
    };

    // APP FUNCTIONALITY ------

    /* --- PAGE 1/3 --- */

    // Start the quiz:
    newQuiz();
    // Clicking on start button:
    startBtn.on('click', function () {
        tajming = 60;
        // Advance to questions page
        initPage.hide();
        questionsPage.show(300);
        // Load question and answers
        generateQuestionAndAnswers();
        // Store the correct answer in a variable
        getCorrectAnswer();
        // Hide the submit and continue buttons
        submitBtn.hide();
        continueBtn.hide();
    });

    /* --- PAGE 2/3 --- */

    // Clicking on an answer:
    answerDiv.on('click', function () {
        // Make the submit button visible
        submitBtn.show(300);
        // Remove pointer from any answer that already has it
        deselectAnswer();
        // Put pointer on clicked answer
        selectAnswer(this);
        // Store current selection as user answer
        getUserAnswer(this);
        // Store current answer div for highlighting purposes
        getSelectedAnswerDivs(this);
    });

    $('#odgovor').on("keyup", function () {
        if ($("#odgovor").val().length == 0) {
            submitBtn.hide(300)
        } else {
            submitBtn.show(300)
        }
    })
    l = []


    function isInArray(value, array) {
        return array.indexOf(value) > -1;
      }
    function odgovor() {
        $('#odgovor').val (function () {
            return this.value.toLowerCase();
        })
        odg = $("#odgovor").val()
        lista = frazemi[questionCounter]["odgovori"]
        $("#" + frazemi[questionCounter]["id"]).val(odg)
        if (jQuery.inArray(odg, frazemi[questionCounter]["lista"]) == -1) {
            $("#span").text("ta riječ ne postoji")
            $("#span").css('display', 'inline').fadeOut(4000);
            $("#" + frazemi[questionCounter]["id"]).val(odg + " -novo")
            $("#odgovor").val("")
            $("#bootstrapForm2").submit()
            $("#odgovor").val("")
            $(".mrzim").hide(300)
        }
        else if (isInArray(odg,l)) {
            $("#span").text("ova riječ je već iskorištena")
            $("#span").css('display', 'inline').fadeOut(4000);
            $("#odgovor").val("")
            $(".mrzim").hide(300)
        }
        else {
            l.push(odg)
            $("#bootstrapForm2").submit()
            brb++
            if (lista.indexOf(odg) > -1) {
                if (brb > 1) {
                    $(".odgovori").append(", " + odg + " " + frazemi[questionCounter]["bodovi"][lista.indexOf(odg)])
                }
                else { $(".odgovori").append(odg + " " + frazemi[questionCounter]["bodovi"][lista.indexOf(odg)]) }
            }
            else {
                if (brb > 1) {
                    $(".odgovori").append(", " + odg + " 1")
                }
                else { $(".odgovori").append(odg + " 1") }
            }
            $("#odgovor").val("")
            $(".mrzim").hide(300)
        }
    }


    function vrijeme() {
        brojevi = $(".odgovori").text().replace(/[^\d]/g, ',').split(",");
        brojevi = brojevi.filter(function (el) {
            return el != null && el != "";
        });

        var sveukupno = 0;
        for (var i = 0; i < brojevi.length; i++) {
            sveukupno += brojevi[i] << 0;
        }
        $("#osnova").remove()
        $("#tocka").remove()
        $("#frazem2").remove()
        swal({
            title: "Isteklo je vrijeme.",
            html: "Vaši odgovori su: " + $(".odgovori").text() + "<br>Sveukupno ste ostvarili idući broj bodova: " + sveukupno+"<br>  Frazem glasi: "+ frazemi[questionCounter]["odgovor"],
            showCloseButton: true,
            confirmButtonText: ' dalje',
            backdrop: false,
            allowOutsideClick: false
        });
        $(".swal2-confirm").unbind("click").click(function(){
            nastavi()
        })
        bodovi+=sveukupno
        $(".swal2-close").click(function () {
            /*clearInterval(countdownTimer)
            if (ide == 1) {
                ProgressCountdown(10, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => odgovor());
            }*/
        })
    }
    /*

    //nastavi()
    */


    submitBtn.on('click', function () {
        odgovor();
    });


    function nastavi() {
        brb = 0
        $(".odgovori").text("")
        clearInterval(countdownTimer)
        // Increment question number until there are no more questions, then advance to the next page
        $(".mrzim").hide()
        if (questionCounter != frazemi.length-1) {
            questionCounter++;
            generateQuestionAndAnswers();
            continueBtn.hide(300);
            
        } else {
            clearInterval(countdownTimer)
            questionsPage.hide();
            resultsPage.show(300);
            // Display user score as a percentage
            userScore.text(Math.floor((correctAnswersCounter / frazemi.length) * 100) + "%");
            $("#bodovi").text(bodovi)
            //$("#60656686").attr("value", bodovi)
        }
        // Load the next question and set of answers
       
        // Enable ability to select an answer
    }

 
    /* --- PAGE 3/3 --- */


function touchHandler(event) {
    var touches = event.changedTouches,
        first = touches[0],
        type = "";
    switch (event.type) {
        case "touchstart":
            type = "mousedown";
            break;
        case "touchmove":
            type = "mousemove";
            break;
        case "touchend":
            type = "mouseup";
            break;
        default:
            return;
    }
    // initMouseEvent(type, canBubble, cancelable, view, clickCount, 
    //                screenX, screenY, clientX, clientY, ctrlKey, 
    //                altKey, shiftKey, metaKey, button, relatedTarget);

    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent(type, true, true, window, 1,
        first.screenX, first.screenY,
        first.clientX, first.clientY, false,
        false, false, false, 0 /*left*/, null);

    first.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}
})
frazemi = [{
    "frazem": "Pijan kao",
    "odgovori": [],
    "bodovi": [],
    "id": 520301002,
    "tocan": "majka",
    "lista": [],
    "odgovor":"Pijan kao majka."
},
{
    "frazem": "je bačena",
    "odgovori": [],
    "bodovi": [],
    "id": 401743637,
    "tocan": "bačena",
    "lista": [],
    "odgovor":"Kocka je bačena."
},
{
    "frazem": "Bacati ",
    "frazem2": "na koga.",
    "odgovori": [],
    "bodovi": [],
    "id": 1245146218,
    "tocan": "kamen",
    "lista": [],
    "odgovor":"Bacati kamen na koga."
}]


imenice = ["abeceda", "adapter", "administracija", "admiral", "admiralica", "adresa", "advent", "aerodrom", "aga", "agencija", "agresivnost", "akcelerator", "akcija", "akna", "akrostih", "aktiv", "aktiv", "aktiva", "aktivist", "aktivistica", "aktivistkinja", "aktivizam", "aktivnost", "akuzativ", "akvarij", "alarm", "alat", "alatnica", "album", "alegorija", "alfabet", "algoritam", "aliteracija", "alkohol", "alkoholičar", "alkoholometar", "alpinist", "alpinistica", "alpinistkinja", "alpinizam", "aluminij", "amper", "ampermetar", "analiza", "ananas", "anđeo", "anglist", "anglistica", "anglistika", "anglistkinja", "anglizam", "antena", "antialkoholičar", "antialkoholičarka", "antologija", "aorist", "aparat", "apartman", "apologija", "apostrof", "apoteka", "apozicija", "ar", "argon", "arhitekt", "arhitektica", "arhitektura", "arhiv", "arhiva", "arhivist", "arhivistica", "arkanđeo", "arsen", "asonancija", "astat", "atribut", "aureola", "autić", "auto", "autobiografija", "autobus", "automat", "automehaničar", "automehaničarka", "automobil", "autopraona", "autopraonica", "autor", "autorica", "avion", "aviončić", "b", "b", "baba", "babica", "babura", "bacač", "bacačica", "bačva", "bačvar", "bačvarica", "bačvica", "badem", "badnjak", "bahatost", "bajka", "bajkovitost", "Bajram", "bajt", "baka", "bakalar", "bakar", "bakterija", "balada", "balerina", "balerinka", "balet", "baletan", "balista", "balkon", "balkončić", "balon", "balončić", "baloner", "bambus", "ban", "banana", "bananica", "banica", "banka", "bankomat", "bar", "barij", "barometar", "barun", "barunica", "basna", "bat", "baterija", "baza", "bazen", "bazenčić", "bazga", "beba", "bedro", "beg", "begonija", "begovica", "bekerel", "bel", "beletristika", "berilij", "beskonačnost", "besmislenost", "besmrtnost", "beton", "bevanda", "bezazlenost", "bezbrižnost", "bezizlaznost", "bezobzirnost", "bezosjećajnost", "bezvrijednost", "bicikl", "biciklist", "biciklistica", "biciklistkinja", "biciklizam", "biće", "bijeda", "bijes", "bik", "bilježnica", "bilježnik", "biljka", "biografija", "biser", "biskup", "biskupija", "bit", "bjelina", "bjelogorica", "blagajna", "blagdan", "blago", "blagost", "blagovaona", "blagovaonica", "blanja", "blato", "blazinica", "bliskost", "blitva", "blizanac", "blizanka", "blizina", "bluza", "bljedilo", "bod", "bodljika", "bofor", "Bog", "bog", "bogataš", "bogatašica", "bogatstvo", "boginje", "boja", "bojažljivost", "bojica", "bojler", "bojnica", "bojnik", "bok", "boks", "boksač", "boksačica", "bokserice", "bol", "bol", "bolest", "bolnica", "bombon", "bor", "bor", "boravak", "borba", "borovnica", "bosiljak", "Božić", "braća", "brada", "brajica", "brak", "branitelj", "braniteljica", "brašno", "brat", "bratić", "brava", "brbljaonica", "brbljavac", "brbljavica", "brbljavost", "brčić", "brčina", "brdo", "breskva", "breza", "brežuljak", "briga", "brigadir", "brigadirka", "brijeg", "brisač", "britva", "brižnost", "brk", "brklja", "brod", "brodić", "broj", "brojač", "brojalica", "brojčanik", "brojilo", "brojka", "brokula", "brom", "bronhitis", "bronhoskop", "broš", "bršljan", "brzina", "brzinomjer", "brzojav", "brzopletost", "bubanj", "bubnjar", "bubnjarica", "bubnjić", "bubreg", "budala", "budaletina", "budalica", "budućnost", "buđenje", "buka", "bukovina", "bukva", "bumerang", "bunda", "bundeva", "bundica", "bura", "burek", "buzdovan", "c", "c", "car", "carica", "carina", "carstvo", "celer", "centa", "centar", "centilitar", "centimetar", "centrifuga", "cepelin", "cesta", "cezij", "cigareta", "cigla", "cijelo", "cijena", "cijep", "cijepljenje", "cijev", "cijevka", "cijukanje", "cikla", "ciklama", "ciklograf", "cilj", "cimer", "cimerica", "cink", "cipela", "cipelica", "cirkus", "civil", "civilizacija", "cjedilo", "cjelina", "cjelovitost", "cjepivo", "cjevčica", "crijevo", "Crkva", "crkva", "crnogorica", "crpka", "crta", "crtalo", "crtančica", "crtanka", "crtež", "crtica", "crv", "cura", "curetak", "curica", "cvijeće", "cvijet", "cvijetak", "cvjećarna", "cvjećarnica", "cvjetača", "cvjetić", "cvrčak", "cvrkut", "č", "č", "čaj", "čamac", "čarapa", "čarobnica", "čarobnjak", "čas", "časnica", "časnik", "časopis", "čast", "čaša", "čavao", "čegrtaljka", "čekanje", "čekaona", "čekaonica", "čekić", "čekrk", "čelik", "čelo", "čempres", "čestica", "čestitka", "čestitost", "čestoća", "češalj", "češljanje", "češljaona", "češljaonica", "češnjak", "četka", "četkica", "četverokut", "četverosjed", "četvorka", "četvrtak", "četvrtaš", "četvrtašica", "četvrtina", "čičak", "čigra", "čimbenik", "čin", "činele", "činjenica", "čip", "čistač", "čistačica", "čistačičin", "čistiona", "čistionica", "čitač", "čitanka", "čitaona", "čitaonica", "čizma", "član", "članak", "članica", "čokolada", "čovjek", "čudo", "čvor", "čvor", "ć", "ć", "ćelavac", "ćelavost", "ćevapčić", "ćilim", "ćirilica", "ćud", "ćuk", "ćup", "d", "d", "dah", "dahija", "dalaj-lama", "dalekopisač", "dalekozor", "dalija", "dalton", "daltonist", "daltonistica", "dama", "dan", "dar", "darežljivost", "darovitost", "daska", "daščica", "dativ", "datoteka", "datulja", "datum", "deblo", "debljina", "decibel", "decilitar", "decimetar", "dečko", "deka", "dekagram", "denzimetar", "derviš", "deseterokut", "desetica", "desetina", "desetka", "desetljeće", "desetnica", "desetnik", "despot", "dešnjak", "dešnjakinja", "detalj", "deterdžent", "deva", "deveterokut", "devetina", "devetka", "difterija", "digitalizacija", "dijabetes", "dijalog", "dijaspora", "dijeljenje", "dijeta", "dijete", "dijetetika", "diktat", "dim", "dimovod", "dinamometar", "dinja", "dionica", "dioptrija", "diploma", "diplomat", "diplomatkinja", "direktor", "direktorica", "direktorij", "disciplina", "disk", "diskoteka", "div", "divkinja", "divljak", "divljakinja", "divljakuša", "dizajn", "dizajner", "dizajnerica", "dizalica", "dizalo", "djeca", "dječačić", "dječak", "djed", "djedovina", "djelatnost", "djelitelj", "djelo", "djelotvornost", "djeljenik", "djeljivost", "djetelina", "djetešce", "djetinjstvo", "djevac", "djevica", "djevojčica", "djevojka", "dlačica", "dlaka", "dlan", "dlijeto", "dnevnik", "dno", "doba", "doba", "dobro", "dobroćudnost", "dočasnica", "dočasnik", "dodatak", "dodir", "događaj", "događanje", "dogovor", "dojam", "dojka", "dokaz", "dokoljenica", "dokoljenka", "doktor", "doktorica", "dokument", "dolar", "dolazak", "dolina", "dom", "domaćica", "domaćin", "domišljatost", "domovina", "domovnica", "dopodne", "doručak", "dosada", "dosjetljivost", "dosljednost", "došašće", "doživljaj", "dragoljub", "drama", "dražica", "dres", "drskost", "drugaš", "drugašica", "društvenost", "društvo", "druženje", "drvce", "drveće", "drvo", "država", "dubina", "dubinomjer", "dućan", "duda", "dudaš", "dudašica", "dude", "dug", "duga", "dugme", "duh", "duhan", "duljina", "dunja", "duša", "dušik", "dušnik", "dužd", "dužina", "dužnost", "dva", "dvije", "dvojka", "dvokratnik", "dvoličnost", "dvorac", "dvorana", "dvorište", "dvosjed", "dvotočka", "dž", "dž", "džamija", "džem", "džemper", "džep", "džeparac", "džepić", "džez", "džip", "džul", "džungla", "đ", "đ", "đak", "đavao", "đavolak", "đavolčić", "đavolica", "đon", "đumbir", "đurđica", "e", "e", "e-adresa", "ehinokok", "ekipa", "ekologija", "ekonomija", "ekran", "ekskurzija", "ekstranet", "elektronvolt", "elektroskop", "element", "emisija", "enciklopedija", "energija", "enklitika", "ep", "epigram", "epika", "epitet", "epizoda", "esej", "e-sport", "etaža", "euro", "f", "f", "fabula", "fagot", "fagotist", "fagotistica", "faks", "faktor", "fakultet", "fanfara", "farad", "faraon", "fasada", "fauna", "faza", "fazan", "feferon", "fen", "festival", "figura", "fijukanje", "film", "financije", "fizičar", "fizičarka", "flauta", "flautist", "flautistica", "flora", "fluor", "fond", "fonologija", "forma", "forum", "fosfor", "fotelja", "fotoaparat", "fotografija", "fotokopija", "fotokopiranje", "fotokopiraona", "fotokopiraonica", "fragment", "frekvencija", "frizer", "frizerka", "frizura", "frula", "frulaš", "frulašica", "frulica", "funkcija", "futur", "g", "g", "gaće", "gajdaš", "gajdašica", "gajde", "galama", "galeb", "galerija", "galicizam", "garaža", "garsonijera", "gastroskop", "gemišt", "generacija", "general", "generalica", "genijalnost", "genitiv", "geometrija", "georgina", "germanizam", "giljotina", "gimnastičar", "gimnastičarka", "gimnastika", "gimnazija", "gipkost", "gitara", "gitarist", "gitaristica", "gizdavost", "glačalo", "glad", "glagol", "glagoljica", "glas", "glasnica", "glasovir", "glava", "glavobolja", "glazba", "glazbalo", "gledalište", "gledatelj", "gledateljica", "gliser", "glista", "glog", "glumac", "glumica", "glupost", "gljiva", "gmaz", "gnijezdo", "godina", "godišnjica", "gojzerica", "gol", "golub", "gomolj", "gora", "gorivo", "Gospa", "gospodar", "gospodarica", "gospodarstvo", "gospodin", "gospođa", "gospođica", "gost", "gostiona", "gostionica", "gošća", "gotovina", "govedina", "govedo", "govor", "govorenje", "govornik", "gozba", "grabilica", "grablje", "grad", "grad", "gradacija", "gradilište", "gradivo", "gradnja", "gradonačelnik", "građanin", "građevina", "grafoskop", "grah", "gram", "gramatika", "gramofon", "gramzivost", "grana", "granica", "grašak", "grb", "greda", "grejp", "grijač", "grijalica", "grijeh", "gripa", "grlo", "grm", "grmljavina", "grob", "groblje", "grof", "grom", "gromobran", "groteska", "grozd", "grubost", "grudnjak", "grupa", "guba", "gujavica", "gulaš", "guma", "gumica", "gusar", "gusjenica", "guska", "gustoća", "gušavost", "guščetina", "gušter", "gužva", "h", "h", "habit", "haiku", "halja", "haljina", "hamburger", "Hanuka", "harfa", "harfist", "harfistica", "harmonika", "harmonikaš", "harmonikašica", "heksametar", "hektar", "hektolitar", "hektopaskal", "helij", "helikopter", "hemofilija", "henri", "herc", "herceg", "herpes", "hidrofor", "hidrogliser", "himna", "hiperbola", "hitrost", "hlače", "hlad", "hladnoća", "hladnjak", "hladovina", "hobi", "hobotnica", "hodalica", "hodnik", "hodža", "hotel", "hrabrost", "hrana", "hrast", "hrastovina", "hrčak", "hren", "hrid", "hripavac", "hrpa", "hulahopke", "humor", "humoreska", "hungarizam", "hunjavica", "hvala", "hvalisavost", "hvalospjev", "hvat", "hvataljka", "i", "i", "idealist", "idealistica", "idealizam", "ideja", "igla", "igra", "igrač", "igračica", "igračka", "igralište", "igraonica", "igrifikacija", "ikona", "iluzija", "iluzionist", "iluzionistica", "imam", "ime", "imendan", "imenica", "imenik", "imperativ", "imperfekt", "induktor", "infinitiv", "informacija", "informatičar", "informatičarka", "informatika", "inicijal", "inkubator", "inozemstvo", "institucija", "instrument", "instrumental", "interes", "internet", "interpretacija", "intervju", "invalid", "invaliditet", "invalidnost", "invalitkinja", "invokacija", "inženjer", "injekcija", "iris", "iseljenica", "iseljenik", "iskaz", "iskra", "iskustvo", "ispit", "isprava", "ispravak", "ispravnost", "ispričnica", "isprika", "istina", "istinitost", "istok", "istraživanje", "ivančica", "izbor", "izbornik", "izdajnica", "izdajnik", "izdanje", "izdržljivost", "izgled", "izgovor", "izgradnja", "izjava", "izlaz", "izlet", "izložba", "iznenađenje", "iznimka", "iznos", "izolator", "izostanak", "izostavnik", "izrada", "izreka", "izvor", "j", "j", "jablan", "jabuka", "jačanje", "jačina", "jaglac", "jagoda", "jahta", "jaje", "jajnik", "jajovod", "jakna", "jakost", "janje", "janjetina", "japanka", "jarac", "jard", "jare", "jaretina", "jarić", "jaslice", "jasmin", "jastučnica", "jastuk", "jat", "jato", "javnost", "jedanaesterac", "jedinica", "jednačenje", "jednakost", "jednina", "jednosjed", "jednoslov", "jednostavnost", "jednostih", "jedrenje", "jedrilica", "jela", "jelen", "jelo", "jelovnik", "jesen", "jetra", "jezero", "jezgra", "jezičac", "jezičak", "jezičavost", "jezik", "jezikoslovac", "jezikoslovka", "jezikoslovlje", "jež", "ježinac", "jod", "jogurt", "jojoba", "jojobin", "jorgan", "jorgovan", "jotacija", "jug", "jugo", "jugoistok", "jugozapad", "juha", "junak", "junakinja", "june", "junetina", "Jupiter", "jutro", "jutro", "k", "k", "kabanica", "kabao", "kabina", "kaciga", "kada", "kada", "kadet", "kadetkinja", "kadifica", "kafić", "kakao", "kalcij", "kalendar", "kaligram", "kalij", "kalkulator", "kalorija", "kamen", "kamera", "kamilica", "kamion", "kan", "kanalizacija", "kandela", "kandidat", "kandidatkinja", "kanta", "kantina", "kap", "kapa", "kapaljka", "kapavac", "kapetan", "kapetanica", "kapuljača", "kaput", "karakteristika", "karakterizacija", "karanfil", "karat", "karcinom", "kardinal", "karijera", "karika", "karneval", "karta", "kartica", "kasa", "kastanjete", "kaša", "kašalj", "kat", "katapult", "katarza", "katastrofa", "katedrala", "kategorija", "katnica", "katolik", "katolkinja", "kauč", "kava", "kavana", "kavez", "kazalište", "kazaljka", "kazetofon", "kazna", "kazniona", "kaznionica", "kažiprst", "kćer", "kćerka", "kći", "keks", "kelvin", "kelj", "kemija", "kesten", "%ikick%i-boks", "kikiriki", "kila", "kilogram", "kilometar", "kilovat", "kilovatsat", "kim", "kinematografija", "kinoprojekcija", "kiosk", "kip", "kip", "kipar", "kiparica", "kisik", "kist", "kiša", "kišobran", "kitica", "kivi", "kladiona", "kladionica", "klapa", "klarinet", "klarinetist", "klarinetistica", "klatno", "klaun", "klavir", "klavirist", "klaviristica", "kleka", "kliješta", "klima", "klin", "klinika", "klip", "klizalište", "klizaljka", "klokot", "klompa", "klopka", "klopot", "klor", "klub", "klupa", "klupko", "ključ", "kljun", "knez", "knjiga", "knjižara", "književnica", "književnik", "književnost", "knjižnica", "knjižničar", "knjižničarka", "knjižničarstvo", "kobalt", "kobasica", "kobila", "kocka", "kockica", "kočijaš", "kočnica", "kod", "kofer", "kokodakanje", "kokos", "kokoš", "kola", "kolač", "kolaž", "kolega", "kolegica", "kolektor", "kolera", "kolica", "količina", "količnik", "kolijevka", "kolnik", "kolo", "kolodvor", "kolovoz", "koljeno", "komad", "komandant", "komandantica", "komandantov", "kombi", "kombinacija", "komedija", "komentar", "komoda", "komodor", "kompanija", "komparativ", "kompas", "kompilator", "kompjutor", "kompresor", "komunikacija", "komunikativnost", "konac", "konac", "konačnost", "koncert", "kondenzator", "kondicional", "konferencija", "konobar", "konobarica", "kontinent", "kontraadmiral", "kontrabas", "kontrabasist", "kontrabasistica", "kontrola", "konj", "konjak", "kopnica", "kopno", "kopriva", "kora", "koraba", "korak", "korica", "korice", "korijen", "korisnica", "korisnik", "korist", "korištenje", "korito", "korizma", "kormilo", "kornjača", "korov", "kosa", "kosa", "kosina", "kositar", "kost", "kostim", "kostur", "koš", "košara", "košarica", "košarka", "košarkaš", "košarkašica", "košulja", "košuta", "kotač", "koturaljka", "kovač", "kovčeg", "kovina", "kovrčavost", "koza", "kozica", "kozle", "kozletina", "kozlić", "kozmetičar", "kozmetičarka", "koža", "krađa", "kraj", "krajnik", "kralj", "kraljevstvo", "kralježnica", "kraljica", "krastavac", "kratica", "krava", "kravata", "kreda", "kredit", "krevet", "krik", "krilo", "kriza", "križ", "križaljka", "križić", "krletka", "krmača", "krojač", "krojačica", "krokodil", "krom", "krošnja", "krotkost", "krov", "krpa", "krpelj", "krstionica", "kršćanin", "kršćanka", "krtica", "krug", "kruh", "krumpir", "kruna", "kruška", "krutost", "kružić", "kružnica", "krv", "krzno", "ksilofon", "ksilofonist", "kuća", "kućanica", "kućica", "kuga", "kugla", "kuhalo", "kuhar", "kuharica", "kuhinja", "kukac", "kukurijek", "kukurijekanje", "kukurikanje", "kukuruz", "kula", "kulminacija", "kulon", "kultura", "kum", "kuma", "kumče", "kumica", "kumin", "kuna", "kunić", "kunićevina", "kupac", "kupaona", "kupaonica", "kupina", "kupovina", "kupus", "kut", "kutija", "kutomjer", "kvadar", "kvadrat", "kvadratić", "kvaka", "kvaliteta", "kvar", "kviz", "kvocijent", "l", "ladica", "ladičar", "lađa", "lakat", "lakomost", "lakovjernost", "lakrdija", "laktometar", "lampa", "lanac", "lančić", "lane", "laringoskop", "larva", "lastavica", "latica", "latinica", "lav", "lavanda", "lavež", "lavica", "lavić", "lavina", "laž", "leća", "leća", "led", "ledenjak", "ledomat", "leđa", "legenda", "lekcija", "lektira", "lepeza", "leptir", "let", "letjelica", "leukemija", "ležaljka", "libreto", "lice", "ličinka", "lift", "liga", "lignja", "liječnica", "liječnik", "lijek", "lijenost", "lijerica", "lijeričar", "lijeričarka", "lijes", "lijeska", "lik", "limenka", "limun", "limunada", "limunika", "lingvist", "lingvistica", "lingvistika", "linija", "lipa", "lipanj", "lisica", "lisice", "list", "lista", "listak", "litra", "loj", "lokativ", "lokomotiva", "lonac", "lončić", "lopata", "lopoč", "lopov", "lopta", "lov", "lovac", "lovkinja", "lozinka", "lubenica", "ludost", "luk", "luk", "luka", "lukavost", "lukovica", "lunapark", "luster", "lutak", "lutka", "luzitanizam", "lj", "lj", "ljekarna", "ljekarnica", "ljekarnik", "ljepilo", "ljepota", "ljestve", "lješnjak", "ljeto", "ljevak", "ljevakinja", "ljevaonica", "ljiljan", "ljubav", "ljubaznost", "ljubica", "ljubimac", "ljubimica", "ljudi", "ljuljačka", "ljupkost", "ljuska", "ljuskar", "ljuskaš", "ljutina", "ljutitost", "ljutnja", "m", "m", "mač", "mačak", "mače", "mačić", "mačka", "mačkica", "maćeha", "maćuhica", "madrac", "mađioničar", "mađioničarka", "maestral", "maestro", "magarac", "magla", "magnet", "magnetofon", "magnezij", "magnolija", "maharadža", "mahovina", "mahuna", "majica", "majka", "majmun", "major", "majorica", "mak", "malina", "malj", "mama", "mamac", "mana", "mandarina", "mandolina", "mandolinist", "mandolinistica", "mangan", "mapa", "marama", "maramica", "marelica", "marioneta", "marka", "markiz", "marljivost", "maršal", "maska", "maskenbal", "maslac", "maslačak", "maslina", "masnoća", "mast", "mašta", "matematičar", "matematičarka", "matematika", "materijal", "materijalist", "materijalistica", "materijalizam", "maternica", "matura", "maturant", "maturantica", "med", "medalja", "medenjak", "medij", "medvjed", "medvjedić", "međuspremnik", "megafon", "megavat", "megavolt", "mehaničar", "mehaničarka", "melodija", "melodika", "melodrama", "memorija", "mesar", "mesarica", "meso", "metal", "metar", "metla", "metoda", "metrika", "metronom", "metropolit", "mijaukanje", "mijeh", "miješalica", "mikrofon", "mikrometar", "mikroskop", "milijarda", "milijun", "milimetar", "milivat", "milivolt", "milosrdnost", "milja", "mimoza", "mina", "mina", "mineral", "miniračunalo", "ministar", "ministarstvo", "ministrica", "minobacač", "minuta", "mir", "miris", "miroljubivost", "misao", "miš", "mišić", "mišljenje", "mišolovka", "mit", "mjehur", "mjenjač", "mjera", "mjesec", "mjesečina", "mjesto", "mladić", "mladost", "mladunče", "mlat", "mlijeko", "mlinac", "mljekara", "mljekarnica", "mnogokut", "množenik", "množenje", "množitelj", "mobitel", "moda", "model", "moderna", "mogućnost", "molba", "molitva", "momak", "monitor", "monodrama", "monolog", "mononukleoza", "monostrofa", "more", "morfologija", "mornar", "mornarica", "most", "motika", "motiv", "motivacija", "motocikl", "motor", "mozak", "mrak", "mrav", "mreža", "mrkva", "mrtvac", "mržnja", "mudrost", "muha", "muka", "mukanje", "munja", "musliman", "muslimanka", "muškarac", "mušmula", "muzej", "muž", "mužjak", "n", "n", "način", "nada", "nadbiskup", "nadimak", "nadmadrac", "nadnarednica", "nadnarednik", "nadnevak", "nadstolnjak", "nadvojvoda", "nagrada", "naivnost", "najlonka", "nakit", "nametljivost", "namirnica", "namjesnik", "namještaj", "nanometar", "naočale", "napa", "napad", "napetost", "naprava", "naranča", "naredba", "narednica", "narednik", "narod", "narudžba", "narukvica", "naselje", "nasilnost", "nasilje", "naslonjač", "naslov", "nasrtljivost", "nastava", "nastavak", "nastavnica", "nastavnik", "nastup", "natikača", "natjecanje", "natjecatelj", "natjecateljica", "natječaj", "natkoljenka", "natporučnica", "natporučnik", "natrij", "naušnica", "navijač", "navijačica", "navika", "navodnik", "naziv", "nazivlje", "nazivoslovlje", "nebo", "neboder", "nećak", "nećakinja", "nedjelja", "nedjeljivost", "nedostižnost", "negativ", "nektarina", "nemilosrdnost", "neodgojenost", "neodgovornost", "neodlučnost", "neon", "neovisnost", "neozbiljnost", "nepismenost", "neplivač", "neplivačica", "neplodnost", "nepobjedivost", "nepromjenjivost", "neprozirnost", "nepušač", "nepušačica", "nesigurnost", "nespremnost", "nespretnost", "nesreća", "nestašnost", "nesvijest", "nesvjesnost", "neugodnost", "neuhvatljivost", "neumjerenost", "neuništivost", "neurednost", "neustrašivost", "nevinost", "nevodič", "nevolja", "nevozač", "nevozačica", "nevrijeme", "nezahvalnost", "nezaposlenost", "nezgoda", "nikal", "ništica", "nit", "niz", "noć", "noga", "nogomet", "noj", "nokat", "nominativ", "nos", "nosač", "nosila", "nošnja", "nota", "novac", "novčanica", "novčanik", "novela", "novina", "novinar", "novinarka", "novine", "novorođenče", "novost", "nož", "nula", "nuncij", "nj", "nj", "njakanje", "nježnost", "njihaljka", "njiva", "njuh", "njuška", "njutn", "njutnmetar", "o", "o", "obala", "obaveza", "obavijest", "obilazak", "obilje", "obitelj", "objed", "objekt", "oblak", "oblik", "obljetnica", "oboa", "oboist", "oboistica", "oborina", "obrana", "obraz", "obrazovanje", "obrok", "obrt", "obrtnica", "obrtnik", "obrva", "obuća", "obujam", "obveza", "obzir", "obzirnost", "ocat", "ocean", "ocjena", "oči", "očuh", "oda", "odanost", "odašiljač", "odbojka", "odbojkaš", "odbojkašica", "odbojnost", "odbor", "odgoj", "odgojitelj", "odgojiteljica", "odgovor", "odgovornost", "odijelo", "odjeća", "odjel", "odlazak", "odlomak", "odlučnost", "odluka", "odmaralište", "odmor", "odnos", "odojak", "odora", "odrastao", "odredište", "odrezak", "održavanje", "oduševljenost", "oduzimanje", "oficir", "oficirka", "oglas", "ogled", "ogledalo", "ograda", "ogrjev", "ogrlica", "ogrtač", "oholost", "oker", "oklop", "oko", "okoliš", "okomitost", "okrutnost", "oksimoron", "okućnica", "okulist", "okulistica", "okus", "okvir", "olovka", "olovo", "oluja", "om", "omiljenost", "omjer", "ommetar", "omot", "onomatopeja", "opasnost", "općina", "opeka", "operacija", "opip", "opis", "opkoračenje", "opna", "opomena", "oporuka", "oprema", "opreznost", "optimist", "optimistica", "orah", "oranica", "orao", "organ", "organizacija", "organizam", "orguljaš", "orguljašica", "orgulje", "orhideja", "orkestar", "ormar", "oruđe", "oružje", "os", "osa", "osigurač", "osiguranje", "osip", "osjećaj", "osjećajnost", "osjet", "osjetilo", "osjetljivost", "osmaš", "osmašica", "osmerac", "osmerokut", "osmica", "osmijeh", "osmina", "osnova", "osoba", "osobnost", "ospice", "ostatak", "ostava", "osvetoljubivost", "otac", "otklon", "otok", "otpad", "otpornik", "otpornost", "otrov", "otvarač", "otvorenost", "ovan", "ovca", "ovisnost", "ozbiljnost", "ozljeda", "oznaka", "ožujak", "p", "p", "pacijent", "pacijentica", "pače", "pačetina", "pad", "padavica", "padobran", "padobranac", "padobranka", "pahulja", "pak", "pakao", "palac", "palačinka", "palatalizacija", "palčić", "palma", "pamćenje", "pamet", "pamuk", "pandža", "papa", "papar", "papiga", "papir", "paprika", "papuča", "par", "para", "paralelogram", "parangal", "paranoja", "park", "parkiralište", "partner", "partnerica", "pas", "pas", "pasiv", "pasiva", "pasivnost", "pasjemenik", "paskal", "pastela", "pastir", "pastirica", "pastrva", "pašnjak", "pašteta", "patak", "patka", "patlidžan", "patnja", "patuljak", "paučica", "paučina", "pauk", "pčela", "pecivo", "pečenje", "peć", "pećnica", "pedagog", "pedagogica", "pedagoginja", "pedalj", "pegla", "pekar", "pekara", "pekarica", "pekarna", "pekarnica", "pekmez", "pelena", "pelin", "pelinkovac", "penjalica", "pepelnica", "pepeo", "peraja", "perfekt", "perilica", "periskop", "pernica", "pero", "peron", "peronospora", "peršin", "perunika", "pesimist", "pesimistica", "peta", "petak", "petaš", "petašica", "peterac", "peterokut", "peterosjed", "petica", "petina", "piće", "pidžama", "pijanist", "pijanistica", "pijesak", "pijetao", "pijuk", "piksel", "pila", "pile", "piletina", "pilot", "pingvin", "piramida", "pirat", "pisac", "pisač", "pisanica", "pisanka", "pismenost", "pismo", "pismonoša", "pista", "pištolj", "pita", "pitalica", "pitanje", "pivo", "%ipizza%i", "%ipizzeria%i", "pjena", "pjesma", "pjesmarica", "pjesnik", "pjesnikinja", "pješak", "pješakinja", "pješčanik", "pjevač", "pjevačica", "plač", "plačljivost", "plaća", "plahta", "plakat", "plamen", "plamenik", "plamenjača", "plan", "planet", "planina", "planinar", "planinarka", "plastelin", "plastika", "plašt", "platina", "platno", "plaža", "plemenitost", "pleonazam", "ples", "plesač", "plesačica", "pletenica", "plin", "pliš", "plivač", "plivačica", "ploča", "pločnik", "plod", "plodnost", "ploha", "plovilo", "pluća", "plug", "plus", "pluskvamperfekt", "pljačka", "pljusak", "pljuvačka", "pobjeda", "pobožnost", "početak", "početnica", "pod", "podatak", "podloga", "podlost", "podmornica", "podmuklost", "podne", "podnožje", "podrijetlo", "podrška", "područje", "podrum", "podstanar", "podstanarka", "poduzeće", "poglavlje", "pogled", "pogodak", "pogon", "pogreb", "pogreška", "pohlepa", "pohlepnost", "pohvala", "pohvalnica", "pojačalo", "pojam", "pojas", "pojas", "pojedinac", "pokazivač", "poklade", "poklon", "pokrajina", "pokrata", "pokret", "pokrivač", "pokušaj", "pol", "pola", "polazak", "polazište", "polica", "policajac", "policajka", "policija", "političar", "političarka", "politika", "polnoćka", "polovica", "polovina", "položaj", "polubrat", "polugodište", "polunavodnik", "poluotok", "polusestra", "polusloženica", "polje", "poljoprivreda", "poljoprivrednica", "poljoprivrednik", "poljubac", "pomagalo", "pomoć", "ponašanje", "ponedjeljak", "ponoć", "ponos", "ponton", "ponuda", "poosobljenje", "pop", "popis", "poplava", "poplun", "popodne", "popularnost", "poraz", "poredba", "porez", "poriluk", "porođaj", "portal", "portret", "poručnica", "poručnik", "poruka", "posada", "posao", "posebnost", "posjet", "poslanica", "poslijepodne", "poslovanje", "poslovica", "poslovnost", "poslušnost", "poslužilac", "posljedica", "pospanost", "post", "postaja", "postelja", "posteljica", "posteljina", "poster", "postupak", "posuda", "posuđenica", "pošiljka", "pošta", "poštar", "poštarica", "poštenje", "poštovanje", "potenciometar", "potištenost", "potkošulja", "potkrovlje", "potok", "potpetica", "potpetica", "potpis", "potpora", "potporučnica", "potporučnik", "potprogram", "potpukovnica", "potpukovnik", "potpunost", "potraga", "potreba", "pouzdanost", "povećalo", "poveznica", "povijest", "povjerenje", "povjerljivost", "povjestica", "povjetarac", "povoj", "povratak", "povrće", "površina", "pozdrav", "pozicija", "pozitiv", "poziv", "pozivnica", "poznanica", "poznanik", "pozornica", "pozornica", "pozornik", "požar", "prabaka", "praćka", "pradjed", "prah", "praksa", "praona", "praonica", "praporac", "prasak", "prase", "prašak", "prašina", "prašuma", "prati (se)", "pravac", "pravda", "pravednost", "pravilnost", "pravilo", "pravo", "pravokutnik", "pravopis", "pravoslavac", "pravoslavka", "praznik", "preciznost", "preča", "predavanje", "predavaona", "predavaonica", "predikat", "predmet", "prednaglasnica", "prednost", "predsjednica", "predsjednik", "predsoblje", "predstava", "predstavnica", "predstavnik", "predvorje", "pregled", "preglednik", "prehlada", "prehrana", "prekidač", "preklopnik", "premijer", "premijerka", "premosnica", "prepjev", "prepreka", "preslica", "preuzimanje", "prevodilac", "prezent", "prezime", "pribor", "pribrojnik", "priča", "pričanje", "pričest", "pridjev", "prigoda", "prigušivač", "prihvatljivost", "prijamnik", "prijatelj", "prijateljica", "prijava", "prijedlog", "prijelaz", "prijenos", "prijevoz", "prikladnost", "prilagodnik", "prilika", "prilog", "primalja", "primarijus", "primjer", "primjerenost", "primorje", "princ", "pripjev", "pripovijest", "pripovijetka", "pripovjedač", "pripovjedačica", "priprema", "priredba", "priroda", "prirodnost", "prispodoba", "pristojnost", "pristup", "pristupačnost", "privatnost", "privitak", "privlačnost", "prizemlje", "prizemnica", "priznanje", "prkos", "prljavština", "problem", "proces", "procesor", "pročelje", "pročišćivač", "prodaja", "prodavač", "prodavačica", "prodavaona", "prodavaonica", "prodornost", "produkt", "profesor", "profesorica", "prognoza", "program", "programator", "prohodnost", "proizvod", "proizvodnja", "proizvođač", "proizvođačica", "projekcija", "projekt", "projektil", "projektor", "proklitika", "prolaz", "prolaznica", "prolaznik", "prolaznost", "proljeće", "promet", "prometnica", "prometnica", "prometnik", "promjena", "promjenjivost", "propuh", "proračun", "prosinac", "prosječnost", "proslava", "prostirač", "prostor", "prostorija", "prostranost", "prošlost", "protivnica", "protivnik", "protokol", "proza", "prozirnost", "prozor", "proždrljivost", "prsa", "prskalica", "prsluk", "prst", "prsten", "prstenjak", "prstić", "pršut", "prtljaga", "pruga", "prut", "prvak", "prvakinja", "prvaš", "prvašica", "prvenstvo", "prvi", "pržilica", "psiholog", "psihologinja", "psihoza", "pšenica", "ptica", "ptičica", "publika", "puh", "pukovnica", "pukovnik", "pulover", "punjač", "pupoljak", "pura", "puran", "puretina", "purica", "pustinja", "puška", "put", "putnica", "putnik", "putovnica", "puž", "r", "r", "rabin", "račić", "račun", "računalo", "računalstvo", "rad", "radar", "radio", "radiona", "radionica", "radni", "radnica", "radnik", "radost", "rahitis", "raj", "rajčica", "rak", "raketa", "raketoplan", "rakija", "ral", "ralo", "ramazan", "rame", "rana", "ranjenica", "ranjenik", "raskrižje", "rasplinjač", "raspored", "rasprava", "raspršivač", "rast", "rastanak", "rastresenost", "rastrošnost", "raščlamba", "rašeljka", "rat", "ratnica", "ratnik", "ravnalo", "ravnatelj", "ravnateljica", "ravnopravnost", "razdoblje", "razdraganost", "razglas", "razglednica", "razgovijetnost", "razgovor", "razigranost", "razina", "različitost", "razlika", "razlog", "razred", "razum", "razumljivost", "razvodnica", "razvodnik", "razvoj", "ražnjić", "reaktor", "recept", "rečenica", "red", "redak", "redoslijed", "redovitost", "redovnica", "reduktor", "regija", "religioznost", "remen", "rendgen", "rep", "repa", "repetitor", "reprezentacija", "republika", "respirator", "restoran", "rešetka", "reuma", "reumatizam", "revolver", "rezervacija", "rezultat", "riba", "ribar", "ribarica", "ribarnica", "ribarstvo", "ribnjak", "ribolov", "riječ", "rijeka", "rijetkost", "rima", "ris", "ritam", "riža", "rječnik", "rješenje", "robot", "ročnica", "ročnik", "rod", "roda", "rodbina", "roditelj", "roditeljica", "rođak", "rođakinja", "rođendan", "rođenje", "rog", "rok", "rola", "rolanje", "roman", "romanizam", "romb", "romobil", "ronilac", "roniteljica", "rosa", "roštilj", "rotkva", "rotkvica", "rub", "rubac", "rubeola", "rublje", "ručak", "ruče", "ručnik", "rudnik", "rujan", "ruka", "rukav", "rukavica", "rukomet", "rukometaš", "rukometašica", "rukopis", "rum", "rupa", "rupčić", "ruža", "ružmarin", "s", "s", "sabirnica", "sablja", "sadržaj", "sag", "sako", "saksofon", "saksofonist", "saksofonistica", "salama", "salata", "salonka", "samoglasnik", "samostalnost", "samouvjerenost", "san", "sandala", "sanduk", "sanjke", "saonice", "sapun", "sastanak", "sastav", "sastavak", "sat", "satnica", "satnik", "satrap", "savijača", "savitljivost", "savjet", "scena", "sebičnost", "sedmaš", "sedmašica", "sedmerac", "sedmerokut", "sedmica", "sedmina", "seizmograf", "sekunda", "selo", "seljak", "seljakinja", "semafor", "sendvič", "senzor", "serija", "sestra", "sestrična", "sezona", "sibilarizacija", "sida", "sidro", "sifilis", "sigurnost", "siječanj", "sila", "silicij", "silovitost", "simulacija", "sin", "sinagoga", "sintaksa", "sintesajzer", "sir", "sirena", "siromah", "siromahinja", "siromaštvo", "sisaljka", "sisavac", "sito", "sitost", "situacija", "sjecište", "sjedalica", "sjedalo", "sjednica", "sjekira", "sjeme", "sjemenik", "sjemenka", "sjena", "sjever", "sjeveroistok", "sjeverozapad", "skakavac", "skela", "skener", "skija", "skijaš", "skijašica", "skladatelj", "skladateljica", "skladba", "skladište", "sklop", "sklopovlje", "skok", "skromnost", "skup", "skupina", "skupnica", "skupnik", "skupština", "slabost", "sladoled", "slagalica", "slama", "slamka", "slanost", "slap", "slastičarna", "slastičarnica", "slatkorječivost", "slava", "slavina", "slavlje", "slavuj", "slezena", "sličnost", "slika", "slikar", "slikarica", "slikarstvo", "slikovitost", "slikovnica", "slina", "sloboda", "slog", "slon", "slovo", "složenost", "slučaj", "slučajnost", "sluga", "sluh", "slušalica", "sluškinja", "služba", "smeće", "smijeh", "smiješak", "smionost", "smirenost", "smisao", "smislenost", "smjer", "smjernost", "smočnica", "smog", "smokva", "smreka", "smrt", "smrtnost", "snaga", "snijeg", "snjegović", "soba", "sobar", "sobarica", "sobica", "sočnost", "soja", "sok", "sokna", "sokol", "sol", "som", "sonda", "sopilaš", "sopilašica", "sopile", "sova", "spavaćica", "spavaona", "spavaonica", "spisateljica", "splav", "spojnica", "spol", "spolnost", "spolovilo", "spomenik", "sport", "sportaš", "sportašica", "spremač", "spremačica", "spremište", "spremnik", "spremnost", "spretnost", "sramežljivost", "srce", "srdačnost", "srdela", "srditost", "srebro", "sreća", "sredina", "središte", "srednjak", "sredstvo", "srijeda", "srna", "srndać", "srnetina", "srp", "srpanj", "stablo", "stabljika", "stadion", "stajaćica", "stajalište", "staklo", "stalak", "stalnost", "stan", "stanar", "stanarka", "stančić", "standard", "stanica", "stanka", "stanovnica", "stanovnik", "stanje", "starac", "starica", "starost", "start", "stav", "staza", "stepenica", "stezaljka", "stih", "stijeg", "stijena", "stil", "stočar", "stočarica", "stočarstvo", "stol", "stolac", "stolica", "stolnjak", "stoljeće", "stomatolog", "stomatologica", "stomatologinja", "stopa", "stopalo", "stožac", "strah", "strana", "stranac", "stranica", "stranka", "strankinja", "stražnjica", "stric", "strijelac", "strina", "strip", "strmina", "strogost", "stroj", "strojovođa", "stroncij", "strop", "strpljivost", "stručnost", "stručnjak", "stručnjakinja", "struja", "struk", "strunjača", "stuba", "stubište", "studeni", "student", "studentica", "stup", "stupanj", "stupica", "stvar", "stvaranje", "stvarnost", "subjekt", "subota", "sučelje", "sud", "sudac", "sudionica", "sudionik", "sudoper", "suglasnik", "suhoća", "suknja", "sulica", "sultan", "sumpor", "sunce", "suncobran", "sunovrat", "superlativ", "suprotnost", "suprug", "supruga", "suradnja", "surečenica", "susjed", "susjeda", "susjedstvo", "susnježica", "susret", "sustav", "sušilica", "sušilo", "suteren", "sutkinja", "suvenir", "suvozač", "suvozačica", "suza", "svađa", "svečanost", "svećenik", "svemir", "svetac", "svetica", "sveučilište", "svibanj", "svijeća", "svijećnjak", "svijest", "svijet", "svinja", "svinjetina", "svirač", "sviračica", "svjećica", "svjesnost", "svjetiljka", "svjetionik", "svjetlo", "svjetlost", "svježina", "svojstvo", "svrab", "svrbež", "svrha", "svršetak", "š", "šafran", "šah", "šah", "šahist", "šahistica", "šaka", "šal", "šala", "šalica", "šampon", "šansa", "šapa", "šaran", "šarlah", "šator", "šećer", "šeik", "šerif", "šestar", "šestaš", "šestašica", "šesterac", "šesterokut", "šestica", "šestina", "šešir", "šetalište", "šetnja", "šifra", "šilterica", "šiljilo", "šipak", "širina", "šišmiš", "škare", "škljocanje", "škola", "školjka", "školjkaš", "škorpion", "škrtost", "šljapkanje", "šljiva", "šljivovica", "šljunak", "špilja", "špinat", "štap", "štapić", "štednjak", "štene", "šteta", "štikla", "štit", "štitnik", "štivo", "štoperica", "štrample", "štramplice", "štrcaljka", "šubara", "šuma", "šumar", "šumarica", "šumarica", "šumarstvo", "šunka", "šupljina", "t", "t", "tablet", "tableta", "tablica", "tajice", "tajna", "tajnica", "tajnik", "taksi", "taksimetar", "taksist", "taksistica", "takt", "tama", "tambura", "tamburaš", "tamburašica", "tamburica", "tanjur", "tata", "tava", "tavan", "tečaj", "tehnologija", "tek", "tekst", "tekućica", "tekućina", "tele", "telefaks", "telefon", "telegraf", "telegram", "telenovela", "teleprinter", "teleskop", "teletina", "televizija", "televizor", "tema", "temelj", "tempera", "temperatura", "tempo", "tenis", "tenisač", "tenisačica", "tenisica", "tepih", "terasa", "teren", "teritorij", "terminolog", "terminologija", "terminologinja", "termoelement", "termofor", "termometar", "termostat", "tesla", "teta", "tetak", "tetanus", "tifus", "tigar", "tijelo", "Tijelovo", "tijesak", "tijesto", "tikva", "tikvica", "timpan", "timpanist", "timpanistica", "tip", "tipka", "tipkalo", "tipkovnica", "tisuća", "tišina", "tjedan", "tjeme", "tjestenina", "tkivo", "tlakomjer", "tlo", "tobogan", "točka", "točkica", "točnost", "ton", "tona", "top", "toplina", "toplomjer", "torba", "torta", "tračnica", "trag", "trajekt", "trajnost", "trak", "traktor", "tramvaj", "transformator", "traperice", "trapez", "tratinčica", "trava", "travanj", "travarica", "travnjak", "tražilica", "trbuh", "trećaš", "trećašica", "trećina", "tren", "trener", "trenerica", "trening", "trenirka", "trenutak", "trepavica", "trešnja", "trg", "trgovac", "trgovina", "trgovkinja", "triangl", "tricikl", "trn", "trojka", "trokut", "trokutić", "trombon", "trombonist", "trombonistica", "trosjed", "trošak", "trošilo", "trotočka", "truba", "trubač", "trubačica", "trudnoća", "trup", "trut", "tržište", "tržnica", "tuba", "tuberkuloza", "tubist", "tubistica", "tučak", "tuga", "tulipan", "tunel", "tunjevina", "turcizam", "turist", "turistica", "turizam", "turnir", "tuš", "tuš", "tuš", "tuš", "tvar", "tvornica", "tvrdoglavost", "tvrđava", "tvrtka", "u", "u", "ubrus", "učenica", "učenik", "učenje", "učestalost", "učiona", "učionica", "učitelj", "učiteljica", "ud", "udaljenost", "udarac", "udaraljka", "udica", "udobnost", "udruga", "udžbenik", "ugao", "ugljen", "ugljik", "ugovor", "uho", "ujak", "ujna", "ukosnica", "ukras", "ulaz", "ulazak", "ulaznica", "ulica", "uloga", "ulje", "umanjenica", "umanjenik", "umanjitelj", "umirovljenica", "umirovljenik", "umivaonik", "umjerenost", "umjetnica", "umjetnik", "umjetnost", "umnožak", "uniforma", "unija", "unuk", "unuka", "unutrašnjost", "upala", "upaljač", "upitnik", "uprava", "upravljač", "upućenost", "uputnica", "uranij", "ured", "urednost", "uređaj", "uređivač", "usisavač", "uskličnik", "usklik", "Uskrs", "usluga", "usna", "uspavanka", "uspinjača", "uspjeh", "usta", "uš", "ušće", "uši", "utakmica", "utjecaj", "utorak", "utrka", "uvid", "uvjet", "uvod", "uzbuna", "uzor", "uzrok", "uzvik", "užas", "uže", "užina", "užitak", "v", "v", "vadičep", "vaga", "vagon", "val", "Valentinovo", "valjak", "valjčić", "vanilija", "varičela", "varivo", "vat", "vaterpolist", "vaterpolistica", "vaterpolo", "vatra", "vatrogasac", "vatrogaskinja", "vaza", "važnost", "večer", "večera", "većina", "vedrina", "vegetarijanac", "vegetarijanka", "veličina", "veljača", "ventil", "ventilator", "verzija", "veselje", "veslač", "veslačica", "veslo", "vesta", "veterinar", "veterinarka", "veterinarov", "vez", "veza", "vezica", "veznik", "vic", "viceadmiral", "viceadmiralica", "vid", "video", "videoigra", "videorekorder", "vijeće", "vijenac", "vijest", "vikend", "vikendica", "vila", "vile", "vilica", "vino", "vinograd", "viola", "violina", "violinist", "violinistica", "violist", "violistica", "violončelist", "violončelistica", "violončelo", "viroza", "virus", "visibaba", "visina", "viski", "višekatnica", "višekratnik", "višnja", "vitamin", "vitao", "vitkost", "vitrina", "vječnost", "vjenčanje", "vjera", "vjernica", "vjernik", "vjerojatnost", "vjeronauk", "vjeroučitelj", "vjeroučiteljica", "vjesnik", "vješala", "vješalica", "vještica", "vještina", "vjetar", "vjeverica", "vježba", "vježbenica", "vježbenik", "vlada", "vladar", "vladarica", "vlaga", "vlak", "vlas", "vlasnica", "vlasnik", "vlast", "voćarna", "voćarnica", "voće", "voćka", "voćnjak", "voda", "vodenjak", "vodič", "vodičica", "vodik", "vodnica", "vodnik", "vodoinstalater", "vodoinstalaterka", "vodomjer", "vodovod", "vodozemac", "vojnik", "vojnikinja", "vojska", "vojvoda", "vokalizacija", "vokativ", "vol", "volan", "volt", "voltmetar", "volja", "vozač", "vozačica", "vozilo", "vožnja", "vrabac", "vrag", "vrat", "vrata", "vratilo", "vrba", "vreća", "vremeplov", "vrh", "vrhnje", "vrijednost", "vrijeme", "vrpca", "vrsta", "vrt", "vrtić", "vrtuljak", "vrućica", "vrućina", "vučić", "vuk", "vulkan", "vuna", "z", "z", "zabava", "zabrinutost", "zadaća", "zadatak", "zaglavlje", "zagonetka", "zagrada", "zagradica", "zagrljaj", "zahod", "zahtjev", "zahvalnost", "zajednica", "zajutrak", "zaključak", "zakon", "zalazak", "zaljev", "zaljubljenost", "zamjena", "zamjenica", "zamjenik", "zamka", "zamrzivač", "zanaglasnica", "zanimanje", "zanimljivost", "zapad", "zaporka", "zaposlenost", "zapovijed", "zarada", "zarez", "zaslon", "zastava", "zastor", "zastupništvo", "zaštita", "zatvor", "zavičaj", "zavisnost", "zavist", "zavjesa", "zavoj", "završetak", "zbirka", "zbor", "zbornica", "zbrajanje", "zbroj", "zbunjenost", "zdjela", "zdravlje", "zebra", "zec", "zečetina", "zečić", "zemlja", "zemljište", "zemljovid", "zglob", "zgodno", "zgrada", "zid", "zidar", "zidarica", "zima", "zimnica", "zjenica", "zlatarna", "zlatarnica", "zlato", "zlo", "zločin", "zlostavljanje", "zmaj", "zmija", "značka", "znak", "znamenka", "znanac", "znanost", "znanstvenica", "znanstvenik", "znanje", "znatiželja", "znoj", "zob", "zora", "zrak", "zrakoplov", "zrcalo", "zrno", "zub", "zubar", "zubarica", "zubobolja", "zvijer", "zvijezda", "zviždaljka", "zvjezdača", "zvjezdica", "zvonce", "zvono", "zvuk", "ž", "žaba", "žabac", "žalac", "žalo", "žalost", "žarulja", "ždrijebe", "ždrijelo", "žeđ", "želudac", "želja", "željeznica", "željezo", "žena", "ženka", "ženstvenost", "žetva", "žica", "žičara", "židov", "židovka", "žila", "žitarica", "žito", "živ", "živa", "živac", "živost", "život", "životinja", "životopis", "životopisni", "žlica", "žličica", "žmigavac", "žrtva", "žulj", "županica", "županija", "žutica"]
frazemi[0]["lista"] = imenice
frazemi[2]["lista"] = imenice
frazemi[1]["lista"] = imenice
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }

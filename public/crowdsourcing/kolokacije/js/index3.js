
// VARIABLE DECLARATIONS ------
// pages
var initPage,
    questionsPage,
    resultsPage,
    // buttons
    startBtn,
    submitBtn,
    continueBtn,
    spanishBtn,
    // question and answers
    question,
    answerList,
    answerSpan,
    answerA,
    answerB,
    answerC,
    answerD,
    // event listeners
    answerDiv,
    answerDivA,
    answerDivB,
    answerDivC,
    answerDivD,
    feedbackDiv,
    selectionDiv,
    toBeHighlighted,
    toBeMarked, iskljuci_v = 0,
    userScore,
    // quiz
    prezent,
    questionCounter,
    correctAnswer,
    correctAnswersCounter,
    userSelectedAnswer,
    // function names
    newQuiz,
    generateQuestionAndAnswers,
    getCorrectAnswer,
    getUserAnswer, tajming,
    selectAnswer,
    deselectAnswer,
    selectCorrectAnswer,
    deselectCorrectAnswer,
    getSelectedAnswerDivs,
    highlightCorrectAnswerGreen,
    highlightIncorrectAnswerRed,
    slikica, brb = 0,
    clearHighlightsAndFeedback, r1,
    prekidac, countdownTimer, bodovi = 0,
    vrijeme = 0;

function ProgressCountdown(timeleft, bar, text) {
    return new Promise((resolve, reject) => {
        countdownTimer = setInterval(() => {
            timeleft--;
            document.getElementById(bar).value = timeleft;
            document.getElementById(text).textContent = timeleft;
            if (timeleft <= 0) {
                clearInterval(countdownTimer);
                resolve(true);
            }
        }, 1000);
    });
}

var public_spreadsheet_url =
    'https://docs.google.com/spreadsheets/d/1riO3jTRbKMaLcKYVy43SRuQJbP9iO2QlAi5xGsh8eL4/edit#gid=439370884';

function init() {
    Tabletop.init({
        key: public_spreadsheet_url,
        callback: showInfo,
        simpleSheet: true
    });
}

lista_1 = []
lista_2 = []
lista_3 = []
function showInfo(data) {
    for (var i = 0; i < data.length; i++) {
        p = data[i].pratiKosu
        p2 = data[i].VrijednostPrati
        k = data[i].govoritiKao
        k2 = data[i].VrijednostGovoriti
        c = data[i].kajgod
        c2 = data[i].vrijednostBolovati
        if (p == undefined) {
            init()
            return
        }
        else {
            frazemi[0]["odgovori"].push(p)
            frazemi[0]["bodovi"].push(p2)
            frazemi[1]["odgovori"].push(k)
            frazemi[1]["bodovi"].push(k2)
            frazemi[2]["odgovori"].push(c)
            frazemi[2]["bodovi"].push(c2)
        }
    }
    lista_1 = frazemi[0]["odgovori"].filter(function (el) {
        return el != null && el != "";
    });
    lista_2 = frazemi[1]["odgovori"].filter(function (el) {
        return el != null && el != "";
    });
  
    lista_3 = frazemi[2]["odgovori"].filter(function (el) {
        return el != null && el != "";
    });
  
    $('.preloader').fadeOut('slow');
    shuffle(frazemi);
    $("#40").click()
}

init()

$(document).ready(function () {
    $('body').on('keydown', function (event) {
        var x = event.which;
        if (x === 13) {
            event.preventDefault();
        }
    });
    // DOM SELECTION ------
    // App pages
    // Page 1 - Initial
    initPage = $('.init-page');
    // Page 2 - Questions/answers
    questionsPage = $('.questions-page');
    // Page 3 - Results
    resultsPage = $('.results-page');
    slikica = $('.slikica');
    // Buttons
    startBtn = $('.init-page__btn, .results-page__retake-btn');
    submitBtn = $('.mrzim');
    continueBtn = $('.questions-page__continue-btn');
    spanishBtn = $('.results-page__spanish-btn');
    // Answer block divs
    answerDiv = $('.questions-page__answer-div');
    answerDivA = $('.questions-page__answer-div-a');
    answerDivB = $('.questions-page__answer-div-b');
    answerDivC = $('.questions-page__answer-div-c');
    answerDivD = $('.questions-page__answer-div-d');
    // Selection div (for the pointer, on the left)
    selectionDiv = $('.questions-page__selection-div');
    // Feedback div (for the checkmark or X, on the right)
    feedbackDiv = $('.questions-page__feedback-div');
    // Questions and answers
    question = $('.questions-page__question');
    answerList = $('.questions-page__answer-list');
    answerSpan = $('.questions-page__answer-span');
    answerA = $('.questions-page__answer-A');
    answerB = $('.questions-page__answer-B');
    answerC = $('.questions-page__answer-C');
    answerD = $('.questions-page__answer-D');
    // User final score
    userScore = $('.results-page__score');
    prikazBodova = $('.results-page__bodovi');
    // QUIZ CONTENT ------


    // FUNCTION DECLARATIONS ------
    $.fn.declasse = function (re) {
        return this.each(function () {
            var c = this.classList
            for (var i = c.length - 1; i >= 0; i--) {
                var classe = "" + c[i]
                if (classe.match(re)) c.remove(classe)
            }
        })
    }
    // Start the quiz
    newQuiz = function () {
        prekidac = 1;
        bodovi = 0;
        // Set the question counter to 0
        questionCounter = 0;
        // Set the total correct answers counter to 0
        correctAnswersCounter = 0;
        // Hide other pages of the app
        questionsPage.hide();
        resultsPage.hide();
    };

    // Load the next question and set of answers
    generateQuestionAndAnswers = function () {
        l = []
        question.html("<span style='font-size: 1.3rem;'>" + (questionCounter + 1) + "/" + frazemi.length + ".</span> <br>");
        $("#odgovor").val('')
        $(".popuni").show();
        if (frazemi[questionCounter]["frazem2"] != undefined) {
            $('<span>&nbsp;</span><span id="osnova"></span>').insertBefore("#odgovor");
            $('<span>&nbsp;</span><span id="frazem2">' + frazemi[questionCounter]["frazem2"] + '</span>').insertAfter("#odgovor");
        }
        else if (frazemi[questionCounter]["frazem"][0].toUpperCase() == frazemi[questionCounter]["frazem"][0]) {
            $('<span id="osnova"></span> <span>&nbsp;</span>').insertBefore("#odgovor");
            $('<span id="tocka">.</span>').insertAfter("#odgovor");
        }
        else {
            $('<span>&nbsp;</span><span id="osnova"></span><span id="tocka">.</span>').insertAfter("#odgovor");
        }
        //polje za ispunjavanje
        var el = document.getElementById('odgovor');
        // el.focus();
        el.onblur = function () {
            setTimeout(function () {
                el.focus();
            });
        };
        $(".questions-page__answer-list").hide()
        //$("#opis").html("<em>" + prezent[questionCounter].vrijeme + "</em>")
        $(".vrijeme").html('<progress value="' + tajming + '" max="' + tajming + '" id="pageBeginCountdown"></progress><p><span id="pageBeginCountdownText">' + tajming + '</span></p>')
        if (prekidac == 1 && iskljuci_v == 0) {
            ProgressCountdown(tajming, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => vrijeme());
        }
        $("#osnova").text(frazemi[questionCounter]["frazem"])
    };

    // Store the correct answer of a given question
    getCorrectAnswer = function () {
        // correctAnswer = prezent[questionCounter].correctAnswer;
    };

    // Store the user's selected (clicked) answer
    getUserAnswer = function (target) {
        userSelectedAnswer = $(target).find(answerSpan).text();
    };

    // Add the pointer to the clicked answer
    selectAnswer = function (target) {
        $(target).find(selectionDiv).addClass('ion-chevron-right');
        $(target).addClass("odabir")
    };

    // Remove the pointer from any answer that has it
    deselectAnswer = function () {
        if (selectionDiv.hasClass('ion-chevron-right')) {
            selectionDiv.removeClass('ion-chevron-right');
            selectionDiv.parent().removeClass("odabir")
        }
    };

    // Get the selected answer's div for highlighting purposes
    getSelectedAnswerDivs = function (target) {
        toBeHighlighted = $(target);
        toBeMarked = $(target).find(feedbackDiv);
    };

    // Make the correct answer green and add checkmark
    highlightCorrectAnswerGreen = function (target) {
        if (correctAnswer === answerA.text()) {
            answerDivA.addClass('questions-page--correct');
            answerDivA.find(feedbackDiv).addClass('ion-checkmark-round');
        }
        if (correctAnswer === answerB.text()) {
            answerDivB.addClass('questions-page--correct');
            answerDivB.find(feedbackDiv).addClass('ion-checkmark-round');
        }
        if (correctAnswer === answerC.text()) {
            answerDivC.addClass('questions-page--correct');
            answerDivC.find(feedbackDiv).addClass('ion-checkmark-round');
        }
        if (correctAnswer === answerD.text()) {
            answerDivD.addClass('questions-page--correct');
            answerDivD.find(feedbackDiv).addClass('ion-checkmark-round');
        }
    };

    // Make the incorrect answer red and add X
    highlightIncorrectAnswerRed = function () {
        toBeHighlighted.addClass('questions-page--incorrect');
        toBeMarked.addClass('ion-close-round');
    };

    // Clear all highlighting and feedback
    clearHighlightsAndFeedback = function () {
        answerDiv.removeClass('questions-page--correct');
        answerDiv.removeClass('questions-page--incorrect');
        feedbackDiv.removeClass('ion-checkmark-round');
        feedbackDiv.removeClass('ion-close-round');
    };

    // APP FUNCTIONALITY ------

    /* --- PAGE 1/3 --- */

    // Start the quiz:
    newQuiz();
    // Clicking on start button:
    startBtn.on('click', function () {
        tajming = 60;
        // Advance to questions page
        initPage.hide();
        questionsPage.show(300);
        // Load question and answers
        generateQuestionAndAnswers();
        // Store the correct answer in a variable
        getCorrectAnswer();
        // Hide the submit and continue buttons
        submitBtn.hide();
        continueBtn.hide();
    });

    /* --- PAGE 2/3 --- */

    // Clicking on an answer:
    answerDiv.on('click', function () {
        // Make the submit button visible
        submitBtn.show(300);
        // Remove pointer from any answer that already has it
        deselectAnswer();
        // Put pointer on clicked answer
        selectAnswer(this);
        // Store current selection as user answer
        getUserAnswer(this);
        // Store current answer div for highlighting purposes
        getSelectedAnswerDivs(this);
    });

    $('#odgovor').on("keyup", function () {
        if ($("#odgovor").val().length == 0) {
            submitBtn.hide(300)
        } else {
            submitBtn.show(300)
        }
    })
    l = []


    function isInArray(value, array) {
        return array.indexOf(value) > -1;
    }
    function odgovor() {
        $('#odgovor').val(function () {
            return this.value.toLowerCase();
        })
        odg = $("#odgovor").val()
        lista = frazemi[questionCounter]["odgovori"]
        $("#" + frazemi[questionCounter]["id"]).val(odg)
        if (jQuery.inArray(odg, frazemi[questionCounter]["lista"]) == -1) {
            $("#span").text("ta riječ ne postoji")
            $("#span").css('display', 'inline').fadeOut(4000);
            $("#" + frazemi[questionCounter]["id"]).val(odg + " -novo")
            $("#odgovor").val("")
            $("#bootstrapForm2").submit()
            $("#odgovor").val("")
            $(".mrzim").hide(300)
        }
        else if (isInArray(odg, l)) {
            $("#span").text("ova riječ je već iskorištena")
            $("#span").css('display', 'inline').fadeOut(4000);
            $("#odgovor").val("")
            $(".mrzim").hide(300)
        }
        else {
            l.push(odg)
            $("#bootstrapForm2").submit()
            brb++
            if (lista.indexOf(odg) > -1) {
                if (brb > 1) {
                    $(".odgovori").append(", " + odg + " " + frazemi[questionCounter]["bodovi"][lista.indexOf(odg)])
                }
                else { $(".odgovori").append(odg + " " + frazemi[questionCounter]["bodovi"][lista.indexOf(odg)]) }
            }
            else {
                if (brb > 1) {
                    $(".odgovori").append(", " + odg + " 1")
                }
                else { $(".odgovori").append(odg + " 1") }
            }
            $("#odgovor").val("")
            $(".mrzim").hide(300)
        }
    }


    function vrijeme() {
        brojevi = $(".odgovori").text().replace(/[^\d]/g, ',').split(",");
        brojevi = brojevi.filter(function (el) {
            return el != null && el != "";
        });

        var sveukupno = 0;
        for (var i = 0; i < brojevi.length; i++) {
            sveukupno += brojevi[i] << 0;
        }
        $("#osnova").remove()
        $("#tocka").remove()
        $("#frazem2").remove()
        swal({
            title: "Isteklo je vrijeme.",
            html: "Vaši odgovori su: " + $(".odgovori").text() + "<br>Sveukupno ste ostvarili idući broj bodova: " + sveukupno + "<br>  Frazem glasi: " + frazemi[questionCounter]["odgovor"],
            showCloseButton: true,
            confirmButtonText: ' dalje',
            backdrop: false,
            allowOutsideClick: false
        });
        $(".swal2-confirm").unbind("click").click(function () {
            nastavi()
        })
        bodovi += sveukupno
        $(".swal2-close").click(function () {
            /*clearInterval(countdownTimer)
            if (ide == 1) {
                ProgressCountdown(10, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => odgovor());
            }*/
        })
    }
    /*

    //nastavi()
    */


    submitBtn.on('click', function () {
        odgovor();
    });


    function nastavi() {
        brb = 0
        $(".odgovori").text("")
        clearInterval(countdownTimer)
        // Increment question number until there are no more questions, then advance to the next page
        $(".mrzim").hide()
        if (questionCounter != frazemi.length - 1) {
            questionCounter++;
            generateQuestionAndAnswers();
            continueBtn.hide(300);

        } else {
            clearInterval(countdownTimer)
            questionsPage.hide();
            resultsPage.show(300);
            // Display user score as a percentage
            userScore.text(Math.floor((correctAnswersCounter / frazemi.length) * 100) + "%");
            $("#bodovi").text(bodovi)
            //$("#60656686").attr("value", bodovi)
        }
        // Load the next question and set of answers

        // Enable ability to select an answer
    }



    function touchHandler(event) {
        var touches = event.changedTouches,
            first = touches[0],
            type = "";
        switch (event.type) {
            case "touchstart":
                type = "mousedown";
                break;
            case "touchmove":
                type = "mousemove";
                break;
            case "touchend":
                type = "mouseup";
                break;
            default:
                return;
        }
        // initMouseEvent(type, canBubble, cancelable, view, clickCount, 
        //                screenX, screenY, clientX, clientY, ctrlKey, 
        //                altKey, shiftKey, metaKey, button, relatedTarget);

        var simulatedEvent = document.createEvent("MouseEvent");
        simulatedEvent.initMouseEvent(type, true, true, window, 1,
            first.screenX, first.screenY,
            first.clientX, first.clientY, false,
            false, false, false, 0 /*left*/, null);

        first.target.dispatchEvent(simulatedEvent);
        event.preventDefault();
    }

})
frazemi = [{
    "frazem": "kosu",
    "odgovori": [],
    "bodovi": [],
    "id": 520301002,
    "tocan": "prati",
    "lista": [],
    "odgovor": "Prati kosu."
},
{
    "frazem": "kao baba",
    "odgovori": [],
    "bodovi": [],
    "id": 1245146218,
    "tocan": "govoriti",
    "lista": [],
    "odgovor": "Govoriti kao baba."
},
{
    "frazem": "od ljubavi",
    "odgovori": [],
    "bodovi": [],
    "id": 362612299,
    "tocan": "bolovati",
    "lista": [],
    "odgovor": "Bolovati od ljubavi."
}]


glagoli = ["administrirati", "aktivirati", "analizirati", "bacati", "baciti", "baratati", "baviti se", "bdjeti", "biciklirati", "bijeliti", "bijeljeti", "birati", "biti", "bježati", "blejati", "blijedjeti", "blokirati", "bojati se ", "bojiti", "boksati", "bolovati", "boraviti", "boriti se", "braniti", "brati", "brbljati", "brbotati", "brecati", "brektati", "brencati", "brenčati", "brijati (se)", "brinuti se", "brisati", "brojati", "brojiti", "bubnjati", "bučati", "bučiti", "bućkati", "bućnuti", "buditi (se)", "cičati", "cijediti", "cijeniti", "cijepiti", "cijukati", "cijuknuti", "ciknuti", "cilikati", "cimati", "cimnuti", "cinkati", "crknuti", "crniti", "crnjeti", "crtati", "crveniti", "crvenjeti", "cupkati", "cviliti", "cviljeti", "cvokotati", "cvrčati", "cvrkutati", "čekati", "čestitati", "češljati", "četkati", "čeznuti", "činiti", "činiti se", "čistiti", "čitati", "čučati", "čučnuti", "čuditi se", "čuti", "čuvati", "ćurlikati", "dahtati", "darivati", "darovati", "dati", "dati", "davati", "dičiti se", "digitalizirati", "dignuti", "dijeliti", "dirigirati", "disati", "diviti se", "dizati", "djelovati", "dobiti", "doći", "dodati", "dodavati", "dodijeliti", "dodjeljivati", "događati se", "dogoditi se", "dogovarati", "dogovoriti", "doktorirati", "dolaziti", "donijeti", "donositi", "doručkovati", "dosaditi", "dosađivati", "dovesti", "dovršavati", "dovršiti", "doživjeti ", "doživljavati", "drmati", "drmnuti", "drndati", "družiti se", "držati", "eksplodirati", "fijukati", "fijuknuti", "fotokopirati", "frfljati", "fufljati", "funkcionirati", "gakati", "gasiti", "gaziti", "glačati", "glasati se", "gledati", "goriti", "gorjeti", "gospodariti", "govoriti", "graditi", "graknuti", "graktati", "grepsti", "grgljati", "grgoljiti", "grijati", "grliti", "groktati", "gubiti", "gugutati", "gukati", "gunđati", "gurati", "gutati", "hladiti", "hodati", "hraniti", "hripati", "hroptati", "hrskati", "hrzati", "htjeti", "hučati", "hučiti", "hujati", "hujiti", "hukati", "huknuti", "huktati", "ići", "ignorirati", "igrati", "imati", "informirati", "instalirati", "interesirati", "isključiti", "iskoristiti", "ispeći", "ispraviti", "ispravljati", "ispričati", "ispričavati", "istaknuti", "isticati", "izabirati", "izabrati", "izaći", "izgledati", "izgubiti", "izići", "izjaviti", "izlaziti", "izmisliti", "izmišljati", "iznajmiti", "iznajmljivati", "iznenaditi", "iznenađivati", "iznositi", "iznositi", "izroniti", "izumiti", "izuti", "izuvati", "izvijestiti", "izvješćivati", "izvještavati", "izvući", "jačati", "jahati", "janjiti", "javiti", "javljati", "jecati", "ječati", "jeknuti", "jesti", "kajati se", "kasniti", "kašljati", "kazati", "kazniti", "kažnjavati", "kišiti", "kladiti se", "klečati", "klepetati", "klikati", "kliknuti", "kliktati", "klizati", "klokotati", "kloparati", "klopotati", "kljucati", "kljucnuti", "kokodakati", "kokodaknuti", "kolutati", "komentirati", "kopati", "koristiti", "koštati", "kotiti", "kovitlati", "krasti", "krckati", "krcnuti", "krčati", "kreketati", "krenuti", "krepati", "kreštati", "kretati", "kriještati", "kriti", "krkljati", "krojiti", "krstiti", "kruliti", "kruljiti", "kružiti", "kucati", "kuhati", "kukurijekati", "kukurijeknuti", "kukurikati", "kukuriknuti", "kupati (se)", "kupiti", "kupiti", "kupovati", "kvariti", "kvocati", "lagati", "lamatati", "lamati", "leći", "lepetati", "letjeti", "ležati", "liječiti", "lijegati", "lijepiti", "lijevati", "lipsati", "listati", "loviti", "lupati", "lutati", "ljetovati", "ljubiti", "ljuljati", "ljutiti", "maciti se", "mahati", "mahnuti", "maknuti", "maštati", "meketati", "micati", "mijaukati", "mijauknuti", "mijenjati", "miješati", "mirisati", "misliti", "mjeriti", "mljackati", "mljacnuti", "množiti", "moći", "moliti", "morati", "mrdati", "mrdnuti", "mrmljati", "mrmoriti", "mrziti", "mucati", "mučiti", "mukati", "muknuti", "mumljati", "nacrtati", "naći", "nadati se", "naglasiti", "najaviti", "najesti se", "nalaziti", "nalikovati", "namjeravati", "napadati", "napasti", "napisati", "napraviti", "naručiti", "naručivati", "nastati", "nastaviti", "nastavljati", "našminkati (se)", "natjecati se", "naučiti", "navoditi", "nazivati", "nazvati", "nedostajati", "nemati", "nijekati", "nositi", "nuditi", "njakati", "njaknuti", "njegovati", "njihati", "njištati", "obići", "obilovati", "obilježavati", "obilježiti", "objasniti", "objašnjavati", "objaviti", "objesiti", "oblačiti", "obojiti", "obožavati", "obrazlagati", "obrazložiti", "obući", "obuti", "obuvati", "očekivati", "odabirati", "odabrati", "odbolovati", "odgovarati", "odgovoriti", "odijevati", "odjekivati", "odjeknuti", "odjenuti", "odlagati", "odlaziti", "odložiti", "odlučiti", "odlučivati", "odmarati", "odmoriti se", "odnositi", "odrediti", "određivati", "održati", "održavati", "oduzeti", "oduzimati", "odvajati", "odvesti", "odvoditi", "odvojiti", "odzvanjati", "odzvoniti", "ohladiti", "ojanjiti", "ojariti", "okoristiti se", "okotiti", "okrenuti (se)", "okretati (se)", "omaciti", "omogućiti", "omogućivati", "omotati (se)", "omotavati (se)", "opisati", "opisivati", "oporabiti", "oporabljivati", "oprasiti", "opraštati", "oprostiti", "opustiti", "opuštati", "organizirati", "osigurati", "osjećati", "osjetiti", "osloboditi", "osnivati", "osnovati", "ostajati", "ostati", "ostaviti", "ostavljati", "ostvariti", "osvajati", "osvojiti", "ošteniti", "otapati", "oteliti", "otići", "otključati", "otključavati", "otkriti", "otkrivati", "otopiti", "otpustiti", "otpuštati", "otvarati", "otvoriti", "ovisiti", "ovladati", "ovladavati", "ozdraviti", "ozdravljati", "označavati", "označiti", "oždrijebiti", "oženiti", "padati", "paliti", "pamtiti", "parkirati (se)", "pasti", "pasti", "paziti", "pecati", "peći", "penjati se", "pijukati", "pisati", "piskutati", "pisnuti", "pištati", "pitati", "piti", "pjevati", "plaćati", "plakati", "planinariti", "planirati", "plašiti", "platiti", "plesati", "plesti", "plivati", "ploviti", "pljuskati", "pljusnuti", "pobijediti", "počešljati", "početi", "počinjati", "poći", "poderati", "podnijeti", "pogađati", "pogledati", "pogoditi", "pogoršati", "pogoršavati", "pojaviti se", "pojesti", "pokazati", "pokazivati", "poklanjati", "pokloniti", "pokrenuti", "pokretati", "pokušati", "pokušavati", "pokvariti", "polaziti", "polijevati (se)", "politi (se)", "pomagati", "pomoći", "ponašati se", "ponavljati", "ponositi se", "ponoviti", "ponuditi", "popiti", "posaditi", "posjećivati", "posjetiti", "poslati", "poslovati", "poslužiti", "posluživati", "postajati", "postati", "postaviti", "postići", "postojati", "postupati", "postupiti", "posuditi", "posuđivati", "posvađati", "poštivati", "poštovati", "potpisivati", "potvrditi", "povezati", "povezivati", "pozabaviti se", "pozdraviti", "pozdravljati", "pozivati", "pozvati", "praskati", "prasnuti", "prati", "pratiti", "precrtati", "precrtavati", "predsjedati", "predsjedavati", "predsjednikovati", "predstaviti", "predstavljati", "prelaziti", "premjestiti", "premještati", "prepisati", "prepisivati", "preseliti", "preskakati", "preskočiti", "prestajati", "prestati", "preurediti", "preuređivati", "preuzeti", "preuzimati", "pričati", "pričati", "prihvaćati", "prihvatiti", "prijeći", "prijetiti", "priključiti", "priključivati", "primati", "primijetiti", "primiti", "primjećivati", "pristajati", "pristati", "pritisnuti", "probati", "pročitati", "proći", "prodati", "prodavati", "prolaziti", "promijeniti", "pronaći", "propustiti", "propuštati", "proučavati", "provesti", "provjeravati", "provjeriti", "provoditi", "prozor", "prozujati", "prozviždati", "psikati", "pucati", "puckati", "pucketati", "puhati", "puhnuti", "puknuti", "puniti", "pustiti", "pušiti", "puštati", "putovati", "računati", "raditi", "radovati", "radovati se", "rađati", "raspolagati", "raspravljati", "rasti", "razgledati", "razgledavati", "razgovarati", "razlikovati", "razmisliti", "razmišljati", "razumijevati", "razumjeti", "reciklirati", "reći", "revati", "rezati", "rezervirati", "ridati", "riješiti", "rikati", "riknuti", "rješavati", "roktati", "romoniti", "roniti", "roptati", "roštiljati", "ručati", "rugati se", "rukovati", "rukovati se", "rukovoditi", "rzati", "sanjati", "sastajati se", "sastati se", "seliti se", "shvatiti", "sići", "sijati", "siknuti", "siktati", "silaziti", "sjećati se", "sjeći", "sjedati", "sjediti", "sjesti", "sjetiti se", "skakati", "skidati", "skijati", "skinuti", "skočiti", "skrenuti", "skretati", "skriti", "skrivati", "skupiti", "skupljati", "slagati", "slagati", "slati", "slaviti", "slijediti", "slikati", "slušati", "služiti", "smanjiti", "smatrati", "smetati", "smijati se", "smjeti", "smršavjeti", "sniježiti", "snimati", "snimiti", "soliti", "spasiti", "spašavati", "spavati", "spomenuti", "spominjati", "spremati", "spremiti", "spustiti", "spuštati", "stajati", "stanovati", "stati", "staviti", "stavljati", "stići", "stizati", "strašiti", "stvarati", "stvoriti", "suditi", "sudjelovati", "sunčati se", "surađivati", "susresti", "susretati", "sušiti", "svađati se", "svidjeti se", "sviđati se", "svijetliti", "svirati", "svući", "šaliti se", "šaptati", "šarati", "šetati", "šištati", "šiti", "škljocati", "škljocnuti", "školovati", "škrgutati", "škripati", "škripiti", "škripnuti", "šljapkati", "šljapnuti", "šminkati (se)", "štediti", "štedjeti", "štektati", "šteniti", "štititi", "štropotati", "šumiti", "šumjeti", "šuškati", "šušketati", "šušnuti", "šuštati", "šutiti", "šutjeti", "taknuti", "teći", "telefonirati", "teliti", "ticati", "ticati se", "tjerati", "točiti", "tonuti", "toptati", "trajati", "tražiti", "trčati", "trebati", "trenirati", "trepnuti", "treptati", "treskati", "tresnuti", "tresti", "treštati", "trgati", "trgovati", "trošiti", "trubiti", "truditi se", "tući", "tugovati", "tuliti", "tutnjiti", "tvrditi", "ubijati", "ubiti", "učiniti", "učiti", "ući", "udarati", "udariti", "udati", "udavati", "uginuti", "ukidati", "ukinuti", "uključiti", "uključivati", "ulaziti", "umiti (se)", "umivati (se)", "umrijeti", "unajmiti", "unajmljivati", "unijeti", "unositi", "upisati", "upisivati", "upotrebljavati", "upotrijebiti", "upoznati", "upoznavati", "urediti", "uređivati", "uspijevati", "uspjeti", "usporediti", "uspoređivati", "ustajati", "ustati", "usvinjiti", "utjecati", "uzeti", "uzimati", "uživati", "vaditi", "varati", "večerati", "veseliti se", "vezati", "vidjeti", "vijoriti", "vikati", "viknuti", "visiti", "visjeti", "vitlati", "vjenčati", "vjerovati", "vježbati", "vladati", "voditi", "voliti", "voljeti", "voziti", "vraćati", "vratiti", "vrcati", "vrckati", "vrcnuti", "vrijediti", "vrtiti", "vrtjeti", "vrtložiti", "vući", "zabaviti", "zabavljati", "zaboraviti", "zaboravljati", "zadati", "zadavati", "zahvaliti", "zahvaljivati", "zaječati", "zakasniti", "zaključiti", "zalijepiti", "zaljubiti se", "zaljubljivati se", "zanimati", "zaokružiti", "zaokruživati", "zapamtiti", "zapaziti", "zapažati", "zapjevati", "započeti", "zaposliti", "zapošljavati", "zaspati", "zaškripati", "zaškripiti", "zaštektati", "zatreptati", "zatvarati", "zatvoriti", "zaustaviti", "zaustavljati", "zavidjeti", "zavladati", "završavati", "završiti", "zbrajati", "zbrojiti", "zibati", "zijevati", "zimovati", "značiti", "znati", "zreti", "zvati", "zveckati", "zveketati", "zviždati", "zviždukati", "zvoniti", "zvrčati", "zvrndati", "žaliti", "žalostiti", "žamoriti", "žariti", "žbukati", "žderati", "ždrijebiti se", "žeđati", "željeti", "žeti", "žigosati", "živiti", "živjeti", "živkati", "žmiriti", "žuboriti", "žudjeti", "žumoriti", "žuriti se", "žvakati"]
frazemi[0]["lista"] = glagoli
frazemi[1]["lista"] = glagoli
frazemi[2]["lista"] = glagoli
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

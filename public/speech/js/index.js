function startRecording() {

    if (window.hasOwnProperty('webkitSpeechRecognition')) {
        document.getElementById("ikona").src="slike/govor2.png";
        var recognition = new webkitSpeechRecognition();
        recognition.continuous = false;
        recognition.interimResults = false;
        recognition.lang = 'hr_HR';
        recognition.start();
        recognition.onresult = function (e) {
            document.getElementById('transcript').value = e.results[0][0].transcript;
            recognition.stop();
            document.getElementById("ikona").src="slike/govor.png";
        };
        recognition.onerror = function (e) {
            recognition.stop();
        }
    }
    else{alert("radi u chromu")}
}
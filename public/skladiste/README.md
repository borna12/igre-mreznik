# Crate Typist
A 2D typing game with tower defense elements, made for [Ludum Dare 42](https://ldjam.com/events/ludum-dare/42/crate-typist) in 72 hours, where it took place #390/3065 overall, and the 56<sup>th</sup> most fun game. You can play it [here](https://luca1152.itch.io/crate-typist).

## Getting started
### Running the game
Windows: `gradlew desktop:run`  
Linux: `./gradlew desktop:run`

## Built with
- [Java](https://www.java.com/en/download/) - The language used
- [libGDX](https://libgdx.badlogicgames.com/) - The game framework used
- [Feather](https://github.com/zsoltherpai/feather) - Dependency Injection for Java

## License
This project is licensed under the MIT License - see the [LICENSE](https://github.com/Luca1152/crate-typist/blob/master/LICENSE) file for details.

$('.item').draggable({
    helper: 'clone'
});

$('.day').droppable({
    accept: '.item',
    hoverClass: 'hovering',
    drop: function(ev, ui) {
        ui.draggable.detach();
        $(this).append(ui.draggable);
        $(this).find(".dovuci").addClass("top")
        $(this).find(".dovuci").addClass("smanji")
        $(this).find(".podloga").addClass("smanji")
        $(this).find(".dovuci").removeAttr("style")
        $(this).css({ "background": "none" })
    }
});
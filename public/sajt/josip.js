$(function() {
    $(".img-responsive").hide()
    $("body").append('<script src="https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js" charset="UTF-8"></script>')
    $("body").append('<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>')

    $("body").append('<script src="https://osvaldas.info/examples/drop-down-navigation-touch-friendly-and-responsive/doubletaptogo.js" charset="UTF-8"></script>')
    $("body").prepend('<button  id="vrhGumb" title="vrati se na vrh">↑</button>')
    $(".hero").after('<p style="text-align:center; font-size:18px">Na projektu <em>Hrvatski mrežni rječnik - Mrežnik</em> izrađene su skice riječi za hrvatski jezik.</p>')
    $(".hero").on("click", function() {
        window.open("http://ihjj.hr/mreznik/page/o-mrezniku/1/", "_self");
    })
    $(".img-responsive").fadeIn(2500)
    $(".img-responsive").on("click", function() {
        window.open("http://ihjj.hr/mreznik/page/o-mrezniku/1/", "_self");
    })
   /* $('.navbar-nav li:eq(12)').after('<li><a href="https://borna12.gitlab.io/odostraznji/odostrazni-mreznik/" target="_blank">Odostražni rječnik</a></li>');*/

    window.onscroll = function() { scrollFunction() };
    if (window.location == "http://ihjj.hr/mreznik/page/knjizica-sazetaka-book-of-abstracts/29/") {
        location.href = "http://ihjj.hr/mreznik/uploads/7b5602698002896a0fd4f2dcee210796.pdf";
    }

    function provjeri() {
        if (localStorage.getItem("jezik") == "hr") {
            $(".eng").hide()
            $(".cro").show()
            $(".zascro").css({ "-webkit-filter": "grayscale(0%)", "filter": "grayscale(0%)" })
            $(".zaseng").css({ "-webkit-filter": "grayscale(100%)", "filter": "grayscale(100%)" })

            if (window.location == "http://ihjj.hr/mreznik/page/o-mrezniku/1/") {
                $('.content h2:first').text("O Mrežniku")
            } else if (window.location == "http://ihjj.hr/mreznik/page/e-rjecnici-i-e-leksikografija/8/") {
                $('.content h2:first').text("E-rječnici i e-leksikografija")
            }
        } else {
            $(".cro").hide()
            $(".eng").show()
            $(".zaseng").css({ "-webkit-filter": "grayscale(0%)", "filter": "grayscale(0%)" })
            $(".zascro").css({ "-webkit-filter": "grayscale(100%)", "filter": "grayscale(100%)" })
            if (window.location == "http://ihjj.hr/mreznik/page/o-mrezniku/1/") {
                $('.content h2:first').text("About Mrežnik")
            } else if (window.location == "http://ihjj.hr/mreznik/page/e-rjecnici-i-e-leksikografija/8/") {
                $('.content h2:first').text("E-dictionaries and E-lexicography")
            }
        }
    }
    provjeri()
        // When the user clicks on the button, scroll to the top of the document
    $("#vrhGumb").on('click', function(event) {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    $(".zascro").on("click", function(event) {

        $(".zascro").css({ "-webkit-filter": "grayscale(0%)", "filter": "grayscale(0%)" })
        $(".zaseng").css({ "-webkit-filter": "grayscale(100%)", "filter": "grayscale(100%)" })
        localStorage.setItem("jezik", "hr");
        provjeri()
    })

    $(".zaseng").on("click", function() {

        $(".zaseng").css({ "-webkit-filter": "grayscale(0%)", "filter": "grayscale(0%)" })
        $(".zascro").css({ "-webkit-filter": "grayscale(100%)", "filter": "grayscale(100%)" })
        localStorage.setItem("jezik", "eng");
        provjeri()
    })
//pojmovnik prošireno
    $("#eleksik").on("click", function() {
        Swal.fire({
            html:'<p> <strong>ELEXIS</strong>, Europska leksikografska infrastruktura – ELEXIS (European Lexicographic Infrastructure) platforma je i projekt čiji je nastanak potaknut činjenicom da u Europi postoji velik broj u leksikografskome radu nedovoljno povezanih i koordiniranih ustanova u kojima se stvaraju rječnici ili prikupljaju leksikografski podatci. Potreba za izgradnjom europske leksikografskeinfrastrukture jasno je izražena osobito tijekom COST-ove akcije<em>Europska leksikografska mreža</em> ( <em>European Network of e-Lexicography COST Action</em>), koja je završila 2017. godine. Cilj je projekta izgradnjom održive infrastrukture uskladiti napore u području leksikografije (i s obzirom na rječnike koji tek nastaju u digitalnome dobu i s obzirom na one koji su nastali prije njega), omogućiti jednostavan pristup leksičkim podatcima, premostiti razlike između u tehnološkome smislu razvijenijih i manje razvijenih znanstvenih zajednica, odnosno povećati kvalitetu leksikografskoga rada, u prvome redu razvojem javno dostupnih alata koji bi smanjili vrijeme i troškove potrebne za posuvremenjivanje postojećih izvora odnosno za razvoj novih te oblikovati jedinstveni standard za izradu rječnika, razviti strategije, alate i standarde za strukturiranje i povezivanje leksikografskih izvora kako bi mogli svojim punim potencijalom, u kontekstu digitalne humanistike, doprinijeti objavljivanju uzajamno povezanih strukturiranih podataka (Linked Open Data, LOD) i uspostavi semantičkih mreža (Semantic Web), skupova značenja i pojmova koji su u nekoj mjeri povezani sa središnjim značenjem. Koordinator je projekta Inštitut Jožef Stefan u Ljubljani.</p><p style="text-align:center"> <img src="https://jena.jezik.hr/wp-content/uploads/2021/12/elexis.jpg" style="max-width:100%;height:auto; margin:auto"/></p><p align="center"> 1. slika: Ustroj ELEXIS-a (Preuzeto iz Krek, Simon i dr. 2018. European Lexicographic Infrastructure (ELEXIS). U: <em> Lexicography in Global Contexts. Proceedings of the XVIII EURALEX International Congress </em> . Ur. Jaka Čibej, Vojko Gorjanc, Iztok Kosem, Simon Krek.)</p><p> Na stranicama ELEXIS-a opisuju se trenutačno sljedeći leksikografski alati i usluge (uz smjernice za korištenje njima; shemu koja pomaže korisniku vidi na 2. slici):</p><p> SketchEngine – alat za pretragu i izgradnju korpusa tvrtke Lexical Computing, koji sadržava mnoštvo drugih alata koji omogućuju analizu velikih korpusa (između ostaloga i s pomoću skica riječi), stvaranje novih korpusa te potpuno automatiziranu izradu rječnika.</p><p> Lexonomy – sustav za sastavljanje i mrežno objavljivanje rječnika utemeljen na oblačnome računalstvu; može se prilagoditi velikim (općim rječnicima) i manjim leksikografskim projektima (specijaliziranim ili terminološkim rječnicima i glosarima) povezan sa SketchEngineom tako da SketchEngine može poslati leksikografske podatke u Lexonomy kako bi se dobili automatski generirani rječnički nacrti te tako da Lexonomy može izvlačiti podatke iz SketchEngineovih korpusa tijekom procesa sastavljanja rječničkih članaka.</p><p> OneClickDictionary (OCD) – modul za sastavljanje skice riječi koji povezuje sustav za upravljanje korpusom (SketchEngine ili noSketch Engine) s Lexonomyjem i omogućuje automatsko stvaranje rječničkoga nacrta koji sadržava osnovne dijelove rječničkoga članka (natuknice, oblike riječi, primjere, kolokacije itd.), a koji u Lexonomyju dalje uređuje leksikograf koji se u potpunosti može usmjeriti na središnju fazu leksikografskoga rada.</p><p> Elexifier – sustav za rječničku konverziju utemeljen na oblačnome računalstvu koji s pomoću naprednoga XML parsiranja i strojnoga učenja pomaže u pretvaranju rječnika u formatu .pdf ili .xml u standardizirani računalno čitljiv oblik.</p><p> <img src="https://jena.jezik.hr/wp-content/uploads/2021/12/elsexis.guide_.jpg" style="max-width:100%;height:auto; margin:auto"/></p><p align="center"> 2. slika: Vodič za korištenje ELEXIS-ovim uslugama i alatima</p><p>Na ELEXIS-ovim stranicama dostupne su također usluge i alati za obradu prirodnih jezika (NLP), strojno učenje, računalno jezikoslovlje: Clusty – algoritam za provođenje leksičko-semantičke analiza za NLP: sense clustering, VerbAtlas – semantički izvor za sveobuhvatno,  i skalabilno označivanje uloga (Role Labeling), SyntagNet – baza leksičko-semantičkih kombinacija koja povezuje parove pojmova s parovima supojavljujućih riječi, NAISC 1.0  – alat za povezivanje skupova podataka, <a href="http://er.elex.is/">Elexifinder</a> – alat za pretraživanje koji pomaže leksikografima i drugim istraživačima da pronađu znanstvene rezultate u leksikografiji i srodnim poljima, <a href="https://elex.is/tools-and-services/lexicographic-news/">Lexicographic news feed</a> – ELEXIS-ov servis koji izlučuje najnovije novinske članke na (trenutačno) više od 35 jezika povezane s leksikografijom iz velikoga broja (30 000) novinskih izvora. (Lana Hudeček)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    $("#lexical-c").on("click", function() {
        Swal.fire({
            html:'<p><strong>Lexical Computing</strong> tvrtka je koju je osnovao Adam Kilgarriff 2003. Djeluje na području korpusne i računalne lingvistike te promiče pristup u kojemu u jezičnim istraživanjima korpusi imaju središnju ulogu. Njihov je glavni proizvod Sketch Engine, vodeći alat za upravljanje korpusom i korpusnim upitima, koji ima i iznimno važnu mogućnost sastavljanja skica riječi (Word Sketch), a kojim se koriste jezikoslovci, leksikografi, prevoditelji i izdavači širom svijeta. Lexical Computing dobavljač je baza podataka riječi, leksikona, n-gramskih baza podataka i sličnih jezičnih podataka za uporabu u drugom softveru ili za leksikografske projekte. Podatci koje pruža Sketch Engine i usluge Lexical Computinga temelje se na paketu od više od 500 tekstnih korpusa veličine do 50 milijardi riječi koji obuhvaćaju više od 90 jezika. (Lana Hudeček)</p><p style="t6ext-align:center"> <img src="https://jena.jezik.hr/wp-content/uploads/2021/12/Lexical-Computing.jpg" style="max-width:100%; height:auto; margin:auto"/></p><p align="center">mrežne stranice tvrtke <a href="https://www.lexicalcomputing.com/">Lexical Computing</a></p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    $("#korap").on("click", function() {
        Swal.fire({
            html:'<p><strong>KorAP</strong> je skalabilan, fleksibilan i održiv sustav otvorenoga koda Leibnizova Instituta za njemački jezik (Leibniz-Institut für Deutsche Sprache) za rads korpusima. Podržava višeslojnu anotaciju (engl.<em>Multiple Annotations Layers</em>), više upitnih jezika (engl. <em>Multiple Query Languages</em>) za pretraživanje poput CQL-a i regularnih izraza te podržava označavanje više tipova autorskih prava za određene dokumente korpusa (neki tekstovi u korpusu, npr. tekstovi iz medija, zatvoreni su za određene korisnike). KorAP se može testirati na službenoj stranici na virtualnome korpusu. Ima tražilicu na kojoj se mogu pretraživati riječi te se koristiti upitima za<strong><em>​</em></strong> pretraživanje. Uključuje mogućnost prikaza odnosa među riječima s pomoću granaste strukture. U planu je da se program razvije i za pretraživanje drugih jezika. U tome razvoju planira se i rad s usporednim korpusima. (Josip Mihaljević)</p><p style="text-align:center"> <img src="https://jena.jezik.hr/wp-content/uploads/2021/12/korap.jpg" style="max-width:100%;height:auto; margin:auto"/></p><p align="center">rad u sučelju <em>KorAp</em> (prikaz rezultata pretraživanja za riječ <em>Baum</em> (drvo)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    
    $("#elexico").on("click", function() {
        Swal.fire({
            html:'<p><strong>elexiko</strong> mrežni je rječnik suvremenoga njemačkog jezika Leibnizova Instituta za njemački jezik (Leibniz-Institut f&uuml;r Deutsche Sprache) koji uključuje oko 300 000 kratkih rječničkih članka s ortografskim podatcima, podatkom o rastavljanju na slogove, gramatičkim podatcima (preuzetim s www.canoo.net) te brojčanim podatcima o potvrđenosti u korpusu. Pisan je za mrežu. <em>elexiko</em> uključuje 1867 (podatak od 18. siječnja 2020.) potpuno zavr&scaron;enih rječničkih članaka riječi čija je frekencija dobro potvrđena u korpusu. Korpus za <em>elexiko</em> sastoji se u prvome redu od publicističkih tekstova i sastavljan je za potrebe toga rječnika. Potpuno obrađeni rječnički članci uz spomenute podatke, koje imaju sve natuknice, sadržavaju i podatke o tvorbi riječi (uključujući automatski iz korpusa generiran popis tvorenica), o značenju riječi (definicije značenja, dodatna obja&scaron;njenja značenja &ndash; <em>Bedeutungserl&auml;uterung</em>), primjere uporabe preuzete iz korpusa, podatke o kolokacijama i sintaktičkim odnosima u koje riječ stupa te riječima s kojima se riječ u natuknici nalazi u kakvome semantičkom odnosu te pragmatičku napomenu. Neke natuknice uključuju i ilustraciju. <em>elexiko</em> je vi&scaron;e od rječnika u tradicionalnome smislu, on je hipertekstni rječnik i leksička baza podataka za njemački jezik. Od svih mrežnih rječnika koji su konzultirani prije početka rada na <em>Hrvatskome mrežnom rječniku &ndash; Mrežniku</em> <em>elexiko </em>je imao najveći utjecaj na oblikovanje njegove strukture. Na slici je prikazana prva razina obrade natuknice <em>Abend</em> u <em>elexiku</em>. (Lana Hudeček)</p><p style="text-align:center"><img src="" style="max-width:100%;height:auto; margin:auto" src="https://jena.jezik.hr/wp-content/uploads/2021/12/abend.jpg"/></p><p style="text-align:center">rječnički članak (1. razina prikaza) natuknice <em>Abend</em> u elexiku</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    $("#owid").on("click", function() {
        Swal.fire({
            html:'<p><strong>OWID</strong> Portal OWID Leibnizova Instituta za njemački jezik (Leibniz-Institut f&uuml;r Deutsche Sprache) prikuplja vi&scaron;e rječnika i internetskih bibliografija (uglavnom za istraživačke svrhe) na jednome mjestu (digitalne i e-rječnike), koje je moguće istodobno pretraživati. Za neke zapise moguće je dobiti i izgovor s pomoću audiozapisa. Na portalu se nalaze ovi rječnici i baze: <em>elexiko</em><strong></strong> rječnik općega njemačkog jezika, portal paronima, priručnik glagola koji se upotrebljavaju pri komuniciranju na njemačkome, mali rječnik progresivnih oblika, njemački strani rječnik, rječnik frazema te odvojeni rječnici različitih diskursa, od kojih jedan obuhvaća diskurse od 1945. do 1955., a drugi od 1967. do 1968. Novije proizvode koji se razvijaju na stranici može se vidjeti na poveznici OWIDplus,<strong></strong> na kojoj se nalaze stranice za kvantitativne leksičke analize koje su grafički prikazane te interaktivne leksičke aplikacije. (Josip Mihaljević)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
    $("#ids").on("click", function() {
        Swal.fire({
            html:'<p><strong>Das Lehnwortportal Deutsch des IDS </strong>portal je posuđenica Leibnizova Instituta za njemački jezik (Leibniz-Institut f&uuml;r Deutsche Sprache), na kojemu se prikazuje koje su riječi iz njemačkoga u&scaron;le u poljski, hebrejski, tok pisin i slovenski te razlika u njihovu značenju u odnosu na njemački. Za posuđenice se može naći i izgovor te se za neke od njih mogu na interaktivnome grafu vidjeti odnosi među njima te kad se određena riječ pojavila u kojemu jeziku. Svaka točka koja sadržava riječ u grafu služi kao poveznica do stranice te riječi. Točke grafa mogu se i pomicati te se prijelazom mi&scaron;em mogu vidjeti osnovni oblici riječi. Grafovi su izrađeni s pomoću D3 javascript knjižnice (<a href="https://d3js.org/">https://d3js</a><a href="https://d3js.org/"><strong></strong></a><a href="https://d3js.org/">.org/</a>)<strong></strong>. Podatci o rječnicima spremljeni su u bazi Oracle te se za unos i obradu natuknica upotrebljava jezik XML. Stranica rječnika napravljena je u sučeljima Play 2.7. i Spark. Trenutačno se razvija novo sučelje za ovaj rječnik, koje će omogućiti vi&scaron;e mogućnosti pri usporednome pretraživanju vi&scaron;e jezika. Korisnicima će se omogućiti registracija, nakon čega će moći komentirati natuknice u rječniku. U planu je da se dodaju i posuđenice iz rumunjskoga i engleskoga. (Josip Mihaljević)</p><p style="text-align: center;"><img src="https://jena.jezik.hr/wp-content/uploads/2021/12/ids.jpg" style="max-width:100%;height:auto;margin:auto"></p><p style="text-align: center;">grafički prikaz za posuđenice na portalu <em>Das Lehnwortportal Deutsch des IDS</em></p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    $("#grammis").on("click", function() {
        Swal.fire({
            html:'<p><strong>Grammis</strong> je baza gramatičkih podataka Leibnizova Instituta za njemački jezik (Leibniz-Institut f&uuml;r Deutsche Sprache). Na početnoj stranici osim tražilice nalaze se poveznice na mrežne izvore koji uključuju bazu jezičnih savjeta, bibliografije znanstvenih radova o gramatičkim pitanjima i različite rječnike (valencijski rječnik, rječnik konektora, rječnik prijedloga, rječnik afikasa itd.). Svaki rječnik i baza ima svoju tražilicu s različitim poljima za pretraživanje i filtrima. Na početnoj stranici nalaze se poveznice na gramatička istraživanja. Moguća je registracija na stranici, koja korisniku omogućuje spremanje poveznica na natuknice na svoj profil i spremanje stranice s natuknicom u dokument u PDF-u. Neregistrirani korisnici to ne mogu, ali im je omogućeno da stvore citat za stranicu s koje preuzimaju informacije te da podijele stranicu na Facebooku ili Twitteru. Stranica Grammisa izrađuje se s pomoću programskoga alata CakePHP. (Josip Mihaljević)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
    $("#fran").on("click", function() {
        Swal.fire({
            html:' <p><strong>Fran</strong> je portal In&scaron;tituta za slovenski jezik Frana Ramov&scaron;a ZRC SAZU koji objedinjuje rječnike, izvore slovenskoga jezika i portale koji su stvoreni ili se stvaraju u Institutu za slovenski jezik Frana Ramov&scaron;a ZRC SAZU i rječnike koji su digitalizirani u Institutu&nbsp; (1. slika). Na njemu je omogućen sveobuhvatan uvid u slovenski jezik u suvremenome, povijesnome i dijalektnome obliku, nazivlje različitih struka, slovensku gramatiku i pravopis. U siječnju 2021. sadržava 38 rječnika, atlas, dva savjetnika, ukupno 689 748 rječničkih članaka te slovenske gramatike i pravopise od 16. stoljeća do danas te poveznice na različite vanjske jezične zbirke. Razvoj portala donio je i vi&scaron;e mogućnosti za aktivno sudjelovanje korisnika, posebno slanjem komentara. Rječnici i ostali izvori mogu se pretraživati skupno (2. slika) ili pojedinačno. Portal omogućuje i pretraživanje u drugim odabranim korpusima slovenskoga jezika. Svrha je portala pružiti pristup rječničkim informacijama naj&scaron;iremu mogućem krugu korisnika, tako da omogućuje potpuno jednostavne i vrlo složene upite. Prethodnik je ovoga portala portal <em>Zbirka rječnika i tekstova</em>, koju je 2000. godine dizajnirao Primož Jakopin i kojemu se također može pristupiti s portala <em>Fran</em>. Na portalu <em>Fran</em> trajno se radi na uključivanju novoga sadržaja (rastući rječnici, odnosno rječnici čiji se sadržaj periodično obnavlja), pobolj&scaron;avanju funkcionalnosti (grafičkih prikaza odnosa među natuknicama, slika i audiozapisa, uporabe prikaza na mapama koji su važni za dijalektne rječnike), pobolj&scaron;avanju korisničkoga iskustva te također na razvoju aplikacija za operativne sustave Android i iOS. (Lana Hudeček)</p><p style="text-align:center"><img style="max-width:100%;height:auto;margin:auto" src="https://jena.jezik.hr/wp-content/uploads/2021/12/fran.jpg"/></p><p>1. slika: Tražilica na portalu <em>Fran</em></p><p style="text-align:center"><img style="max-width:100%;height:auto;margin:auto" src="https://jena.jezik.hr/wp-content/uploads/2021/12/fran2.jpg"/></p><p>2. slika: Rezultati skupnoga pretraživanja svih izvora na portalu <em>Fran </em>po riječi <em>deklica</em>; s desne strane oznaka je izvora</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
    $("#esskj").on("click", function() {
        Swal.fire({
            html:'<p><strong>Slovar slovenskega knjižnega jezika</strong> (eSSKJ) jednojezični je mrežni rječnik slovenskoga jezika dostupan na portalu <em>Fran </em>In&scaron;tituta za slovenski jezik Frana Ramov&scaron;a ZRC SAZU. Od 2016. periodično se posuvremenjuje i dopunjuje. Nalazi se na sučelju koje hijerarhizira informacije kako se korisnika zainteresiranoga samo za definiciju značenja ne bi opterećivalo vi&scaron;kom podataka. Tako na početnoj rječničkoj stranici korisnik ima uvid u osnovne elemente rječničkoga članka (nagla&scaron;ena natuknica s nekoliko temeljnih oblika, naglasna inačica, definicija značenja, sinonim(i) te etimologija). Korisnik pritiskom na polje <em>Celotno geslo &nbsp;</em>dobiva op&scaron;irniji podatak o riječi u natuknici, npr. pregled njezina kolokacijskoga potencijala dan kroz sintaktičke opise, podatak o izgovoru (zvučni zapis) te dvije nagla&scaron;ene paradigme (u skladu s dvjema mogućnostima nagla&scaron;ivanja u slovenskome) te potpuni pregled oblika riječi. (Lana Hudeček)</p><p style="text-align: center;"><img src="https://jena.jezik.hr/wp-content/uploads/2021/12/armirobetonski.jpg" style="max-width:100%;height:auto;margin:auto"/></p><p style="text-align: center;">slika: 1. razina prikaza rječničkoga članka pridjeva <em>armiranobetonski </em>u <em>eSSKJ-u</em></p><p style="text-align: center;"><img src="https://jena.jezik.hr/wp-content/uploads/2021/12/armirobetonski-2.jpg" style="max-width:100%;height:auto;margin:auto"/></p><p style="text-align: center;">slika: Druga razina prikaza pridjeva <em>armiranobetonski</em> u <em>Slovaru slovenskega knjižnega jezika</em></p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
    $("#anw").on("click", function() {
        Swal.fire({
            html:'<p style="text-align: justify;"><strong>Algemeen Nederlands Woordenboek</strong> (Rječnik suvremenoga nizozemskog jezika) rječnik je koji se izrađuje u Institutu za nizozemski jezik. Njegova je izrada započela 1970., a 2009. objavljena je prva mrežna inačica. <em>Rječnik </em>je korpusno utemeljen i u njemu se opisuje suvremeni nizozemski jezik, koji se govori u Nizozemskoj, Flandriji, Surinamu i na Karibima, a s pomoću mrežnih poveznica, osim definicija, tj. opisa značenja, za svaku se natuknicu donose relevantni podatci o leksičkim odnosima s drugim riječima (hiperonimi, sinonimi i sl.), izgovoru, zapisu, primjerima uporabe, kolokacijama, tvorbenim gnijezdima, a također su dostupne i slike te videozapisi i zvučni zapisi. Rječnik trenutačno sadržava vi&scaron;e od 260 000 riječi i izraza koji su raspoređeni unutar&nbsp; 83 000 natuknica. &nbsp;(podatci prikupljeni s mrežne stranice Instituta za nizozemski jezik <a href="https://ivdnt.org/">https://ivdnt.org/</a>, pristupljeno 21. i 22. travnja 2020.) (Goranka Blagus Bartolec i Ivana Matas Ivanković)</p><p style="text-align: center;"><img src="https://jena.jezik.hr/wp-content/uploads/2021/12/anw.jpg" style="max-width:100%;height:auto;margin:auto"/></p><p style="text-align: center;">primjer obrade riječi <em>huis</em> &lsquo;kuća&rsquo; u <em>Rječniku suvremenoga nizozemskog jezika</em></p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
   
    $("#nieuwe").on("click", function() {
        Swal.fire({
            html:'<p><strong>Nieuwe woorden</strong> (Novotvorenice) mrežni je popis novotvorenica u nizozemskome jeziku. Novotvorenice se izrađuju u sklopu <em>Rječnika suvremenoga nizozemskog jezika </em>(<em>Algemeen Nederlands Woordenboek</em>), sustavno se unose u rječničku bazu i smatraju se dijelom leksika nizozemskoga jezika. Novotvorenicama se smatraju izvedenice od postojećih riječi u nizozemskome, tuđice, najče&scaron;će iz engleskoga, ali i iz drugih jezika te postojeće nizozemske riječi koje u skladu s nekom izvanjezičnom promjenom dobivaju novo značenje. Novotvorenice se popisuju svakoga tjedna i na stranicama projekta objavljuje se novotvorenica tjedna, a u tražilici je moguće pretraživati sve tvorenice od 2011. godine. Pri procjeni je li ne&scaron;to u nizozemskome jeziku novotvorenica i ima li trajniju uporabu u dru&scaron;tvenoj komunikaciji primjenjuje se tzv. test FUDGE, koji je izradio američki lingvist Allan Metcalf. FUDGE je pokrata nastala od prvih slova engleskih riječi i izraza <em>frequency</em> (F) &lsquo;čestoća&rsquo;, <em>unobtrusiveness</em> (U) &lsquo;diskretnost, nenametljivost&rsquo;, <em>diversity of users and situations</em> (D) &lsquo;raznolikost govornika i situacija&rsquo;, <em>generation of forms and meanings</em> (G) &lsquo;mogućnost izvođenja novih oblika i značenja&rsquo; i <em>endurance of the concept</em> (E) &lsquo;trajnost pojma, tj. značenja novotvorenice&rsquo;. (Goranka Blagus Bartolec i Ivana Matas Ivanković)</p><p style="text-align: center;"><img src="https://jena.jezik.hr/wp-content/uploads/2021/12/woorden.jpg" style="max-width:100%;height:auto;margin:auto"></p><p style="text-align: center;"><em>Nieuwe woorden</em> (mrežni popis novotvorenica u nizozemskome jeziku)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
    $("#taalportaal").on("click", function() {
        Swal.fire({
            html:'<p><strong>Taalportaal</strong> jezični je portal nizozemskoga, frizijskoga i afrikaansa, opsežan i zahtjevan projekt u kojemu sudjeluju istraživači Instituta za nizozemski jezik, Sveučili&scaron;ta u Leidenu, Frizijske akademije u Leeuwardenu te Meertensova instituta u Amsterdamu. Projekt je započeo 2011., a cilj mu je opisati gramatičku strukturu nizozemskoga, frizijskoga i afrikaansa na fonolo&scaron;koj, morfolo&scaron;koj, tvorbenoj, sintagmatskoj i sintaktičkoj razini. Posljednje dvije razine uključuju opis glagolskih, imeničkih, pridjevnih i priložnih sveza te položaja nesamostalnih riječi (veznika, prijedloga i čestica). Svi gramatički podatci mrežno su dostupni i pretraživi. (Goranka Blagus Bartolec i Ivana Matas Ivanković)</p><p style="text-align: center;"><img src="https://jena.jezik.hr/wp-content/uploads/2021/12/Woordenboeken.jpg" style="max-width:100%;height:auto;margin:auto"></p><p style="text-align: center;"><em>Taalportal </em>(jezični portal nizozemskoga, frizijskoga i afrikaansa)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    $("#historische").on("click", function() {
        Swal.fire({
            html:'<p><strong>Historische Woordenboeken</strong> portal je koji korisnicima omogućava pristup i pretraživanje povijesnih rječnika nizozemskoga i frizijskoga jezika u kojima se donosi popis, značenja i primjeri uporabe nizozemskih i frizijskih riječi od 500. do 1976. godine. Građa obuhvaća pet tiskanih povijesnih rječnika nastalih u Institutu za nizozemski jezik i Frizijskoj akademiji: <em>Rječnik staronizozemskoga</em>, koji obuhvaća razdoblje od 500. do 1200., <em>Rječnik ranosrednjovjekovnoga nizozemskoga</em>, koji obuhvaća razdoblje od 1200. do 1300., <em>Rječnik srednjovjekovnoga nizozemskoga</em>, koji obuhvaća razdoblje od 1250. do 1550., <em>Rječnik nizozemskoga jezika</em>, koji obuhvaća razdoblje od 1500. do 1976. te <em>Rječnik frizijskoga jezika</em>, koji obuhvaća razdoblje od 1800. do 1975. Mrežni povijesni rječnici pretraživi su po suvremenome obliku riječi i po svim rječnicima, a pretraga se može ograničiti i na pojedinačne rječnike, pri čemu je potrebno poznavati izvorni zapis riječi iz vremena kad je zabilježena. (Goranka Blagus Bartolec i Ivana Matas Ivanković)</p><p style="text-align: center;"><img src="https://jena.jezik.hr/wp-content/uploads/2021/12/tallportal.jpg" style="max-width:100%;height:auto;margin:auto"></p><p style="text-align: center;"><em>Historische Woordenboeken </em>(portal za pristup povijesnim rječnicima nizozemskoga i frizijskoga jezika)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    $("#terminologie").on("click", function() {
        Swal.fire({
            html:'<p><strong>Terminologie</strong> (Nazivlje) terminolo&scaron;ki je portal Ekspertnoga centra za nizozemsku terminologiju (Expertisecentrum Nederlandstalige Terminologie), koji je dio Instituta za nizozemski jezik od 2016., kad je Institut preuzeo aktivnosti nekada&scaron;njega Centra za nizozemsku terminologiju. Među glavnim zadaćama Centra ističe se podr&scaron;ka terminolo&scaron;komu djelovanju te se u njemu prikupljaju terminolo&scaron;ki podatci i alati. (Goranka Blagus Bartolec i Ivana Matas Ivanković)</p><p style="text-align: center;"><img src="https://jena.jezik.hr/wp-content/uploads/2021/12/terminologie.jpg" style="max-width:100%;height:auto;margin:auto"></p><p style="text-align: center;"><em>Terminologie </em>(terminolo&scaron;ki portal Ekspertnoga centra za nizozemsku terminologiju)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
    $("#woordenschat").on("click", function() {
        Swal.fire({
            html:'<p><strong>Beschrijving van de woordenschat</strong> (Opis vokabulara) projekt je u okviru kojega se osmi&scaron;ljava jedinstveni dijakronijski leksički opis nizozemskoga jezika u cilju izgradnje jedinstvene baze značenja, zapisa i uporabe riječi utemeljene na starijim i novijim izvorima i korpusima kojima Institut za nizozemski jezik raspolaže. S obzirom na to da se na projektu jo&scaron; radi, njegovi rezultati u ovome trenutku nisu mrežno dostupni. (Goranka Blagus Bartolec i Ivana Matas Ivanković)</p><p style="text-align: center;"><img style="max-width: 100%; height: auto; margin: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/Beschr.jpg" alt=""/></p><p style="text-align: center;">Naslovna mrežna stranica projekta <em>Beschrijving van de woordenschat</em></p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    $("#woordcombinaties").on("click", function() {
        Swal.fire({
            html:'<p><strong>Woordcombinaties </strong>(Sveze riječi) projekt je u okviru kojega se izrađuje kolokacijska baza nizozemskoga jezika sa semantički motiviranim rječnikom obrazaca (<em>semantically motivated pattern dictionary</em>). Namijenjen je učenju nizozemskoga kao drugoga jezika, a omogućit će brz pristup kolokacijama te će povezivati obrasce sa značenjem. I na ovome se projektu jo&scaron; radi te rezultati zasad nisu mrežno dostupni. (Goranka Blagus Bartolec i Ivana Matas Ivanković)</p><p style="text-align: center;"><img style="max-width: 100%; height: auto; margin: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/Woordcombinaties-.jpg" alt="" /></p><p style="text-align: center;"><em>Woordcombinaties </em>(prikaz obrade riječi <em>aanbieden</em> "nuditi" u okviru projekta <em>Sveze riječi</em>)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
    $("#ordbok").on("click", function() {
        Swal.fire({
            html:'<p style="text-align: justify;"><strong><em>Svensk ordbok</em></strong> (<a href="https://svenska.se/">https://svenska.se/</a>) jednojezični je rječnik &Scaron;vedske akademije (punim imenom <em>Svensk ordbok utgiven av Svenska Akademien</em>) dostupan na portalu <em>svenska.se</em>, koji okuplja jezične izvore &Scaron;vedske akademije te je ondje osim toga rječnika moguće usporedno pretraživanje jo&scaron; dvaju rječnika, popisa riječi s osnovnim podatcima i normativnim napomenama <em>Svenska Akademiens ordlista</em> te povijesnoga rječnika &scaron;vedskoga jezika <em>Svenska Akademiens ordbok</em>. <em>Svensk ordbok</em> rječnik je koji je u tiskanome obliku objavljen 2009. te obuhvaća 65&nbsp;000 natuknica. Sadržaj toga rječnika dostupan je od 2015. u elektroničkome obliku na portalu <em>svenska.se</em> te kao aplikacija za pametne telefone, a trenutačno se radi na novome izdanju rječnika koje će biti isključivo mrežno. Iako sadržaj trenutačno dostupnoga mrežnog rječnika nije mijenjan u odnosu na tiskano izdanje, iskori&scaron;tene su neke mogućnosti mrežnoga medija koje bitno olak&scaron;avaju služenje rječnikom.</p><p style="text-align: center;"><img style="max-width: 100%; height: auto; margin: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/sprak.jpg" alt=""/></p><p style="text-align: center;">1. slika: Rezultati pretraživanja riječi <em>spr&aring;k</em> &lsquo;jezik&rsquo; u trima rječnicima na portalu <em>svenska.se</em></p><p style="text-align: center;"><img style="max-width: 100%; height: auto; margin: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/prata.jpg" alt=""/></p><p style="text-align: center;"><em>2.&nbsp;</em>slika: Natuknica <em>prata</em> &lsquo;govoriti/razgovarati&rsquo; u rječniku <em>Svensk ordbok</em></p><p style="text-align: justify;">Na početku rječničkoga članka nalazi se natuknica te osnovni podatci o njoj koji uključuju izgovor (naznačen mjestom i vrstom naglaska na natuknici), nastavke osnovnih oblika i tvorbenu ra&scaron;člambu (<em>ordled</em>). Podatci o izgovoru obuhvaćaju i zvučni zapis. Slijedi značenje riječi, kojemu su pridruženi slobodno oblikovani primjeri uporabe (<em>exempel</em>), sintaktičko-semantičke konstrukcije u kojima se riječ u tome značenju pojavljuje (<em>konstruktion</em>) te frazemi s poveznicama na natuknice u okviru kojih se obrađuju. Na kraju članka navedeni su povijesni podatci (<em>hist.</em>), koji mogu uključivati podatak o vremenu najstarije poznate potvrde riječi, etimologiji i slično te neke tvorenice (imenice <em>pratande</em> &lsquo;govorenje&rsquo; i <em>prat</em> &lsquo;govor&rsquo;). Tvorenice i značenjski povezane natuknice poput sinonima hipertekstno su povezani. Veća je preglednost mrežnoga izdanja postignuta grafičkim isticanjem pojedinih dijelova članka te mogućno&scaron;ću prikaza cjelokupnoga članka ili samo njegova dijela (opcije <em>D&ouml;lj</em> &lsquo;skrij&rsquo; i <em>Visa mer</em> &lsquo;prikaži vi&scaron;e&rsquo;). Rječnički su članci unatoč tomu &scaron;to je riječ o općemu rječniku koji postoji u tiskanome izdanju često obogaćeni raznovrsnim komentarima koji se odnose na uporabu ili normativni status riječi, pa se tako na kraju natuknice nalazi napomena da se glagol <em>prata</em> sve če&scaron;će upotrebljava umjesto bliskoznačnoga glagola <em>tala</em>. (Daria Lazić)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
    $("#danske").on("click", function() {
        Swal.fire({
            html:'<p><strong><em>Den Danske Ordbog</em></strong> (<a href="https://ordnet.dk/ddo">https://ordnet.dk/ddo</a>) jednojezični je rječnik suvremenoga danskog jezika dostupan na portalu Danskoga dru&scaron;tva za jezik i književnost (<em>Det Danske Sprog- og Litteraturselskab</em>), na kojemu se osim spomenutoga rječnika nalazi i povijesni danski rječnik <em>Ordbog over det danske Sprog</em> te korpus danskih tekstova <em>KorpusDK</em>. Temelj rječnika istoimeni je tiskani rječnik, koji je u razdoblju od 2003. do 2005. izdan u &scaron;est svezaka te obuhvaća 63&nbsp;000 natuknica. Od 2009. rječnik je u izmijenjenu i prilagođenu obliku dostupan na mreži te se otad stalno dorađuje i dopunjuje isključivo kao mrežni rječnik. U studenome 2021. sadržava 104 153 natuknice. Riječ je o korpusno utemeljenome rječniku u kojemu su u velikoj mjeri iskori&scaron;tene mogućnosti mrežnoga medija. Na početnoj se stranici rječnika osim prozora za pretraživanje nalazi mno&scaron;tvo sadržaja kojima se potiče interes korisnika za sadržaj rječnika, primjerice riječ ili izraz dana (<em>Dagens ord </em>/ <em>Dagens udtryk</em>), jezični savjet (<em>Sprogligt</em>) s poveznicom na zbirku jezičnih savjeta na portalu, funkcija <em>Spordhund</em>, s pomoću koje korisnici mogu poslati prijedlog riječi i značenja koje bi trebalo dodati u rječnik i slično.</p><p style="text-align: center;"><img style="max-width: 100%; height: auto; margin: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/ordbog.jpg" alt=""/></p><p style="text-align: center;">1. slika: Početna stranica rječnika <em>Den Danske Ordbog</em></p><p style="text-align: center;"><img style="max-width: 100%; height: auto; margin: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/fredag.jpg" alt=""/></p><p style="text-align: center;">2. slika: Natuknica <em>fredag</em> &lsquo;petak&rsquo; u rječniku <em>Den Danske Ordbog</em></p><p>U prvome dijelu natuknice nalaze se osnovni podatci o riječi, u ovome slučaju vrsta riječi i rod. Izgovor je naveden fonetski i u obliku zvukovnoga zapisa koji se reproducira pritiskom na ikonu zvučnika. Obrada značenja sastoji se od definicije (koja nije puna rečenica), kratice (<em>forkortelse</em>), &bdquo;bliskih riječi&rdquo; (<em>ord i n&aelig;rheden</em>), primjera (<em>eksempler</em>), koji u ovome rječniku sadržavaju nekoliko čestih kolokacija, i citata preuzetih iz korpusa. &bdquo;Bliske riječi&rdquo; funkcija su kojom je danski tezaurus (<em>Den Danske Begrebsordbog</em>) ugrađen u opći rječnik. Pri osnovnome prikazu natuknice prikazuje se nekoliko semantički povezanih riječi i fraza (<em>datum</em>, <em>ponedjeljak</em>, <em>utorak</em>&hellip;), a prikaz se može i pro&scaron;iriti i tada se otvaraju dijelovi članaka u tezaurusu u kojima se tražena riječ pojavljuje. U toj sekciji do izražaja dolaze dobro iskori&scaron;tene mogućnosti unutarrječničkoga i izvanrječničkoga povezivanja: &bdquo;bliskim riječima&rdquo; koje postoje kao natuknice u rječniku pridružene su poveznice na odgovarajuću natuknicu, a uz svaku kolokaciju nalazi se poveznica s korpusom u obliku ikone K, pritiskom na koju se otvaraju rezultati pretraživanja tražene kolokacije u korpusu te se tako može dobiti uvid u &scaron;iri kontekst njihove uporabe. Nakon značenja slijede ustaljeni izrazi (<em>faste udtryk</em>) poput prijedložnih konstrukcija, frazema ili vi&scaron;erječnih naziva. Na kraju članka navode se tvorenice (<em>orddannelser</em>), koje su također povezane s natuknicama u rječniku ako one postoje, te podatak o tome kad je natuknica unesena u rječnik i koje je godine posljednji put ažurirana. Iako zasad nije moguće pristupiti prija&scaron;njim inačicama članaka, na taj se način ipak daje podatak o starosti natuknice, &scaron;to često nedostaje u mrežnim rječnicima.</p><p>Mogućnosti digitalnoga medija iskori&scaron;tene su i kad je riječ o prikazu sadržaja. Osim &scaron;to sve natuknice imaju mogućnost prikaza cjelokupnoga članka ili njegove kraće inačice, u kojoj su neki sadržaji izostavljeni, za duže natuknice nudi se mogućnost prikaza pregleda, u kojemu su navedeni dijelovi članka te se pritiskom na koji od elemenata otvara odgovarajući dio članka. Osim toga, kao oblik izvanrječničkoga povezivanja u lijevome se izborniku nalazi poveznica na odgovarajuću natuknicu u povijesnome rječniku danskoga jezika dostupnome na istome portalu te na dva korpusna pretraživanja: konkordancije s traženom riječi te popis najče&scaron;ćih kolokacija. Nadalje, prelaskom mi&scaron;a preko određenih polja u strukturi natuknice pojavljuju se obja&scaron;njenja koja korisniku olak&scaron;avaju služenje rječnikom. Naposljetku, na kraju članka nalazi se mogućnost prijave problema, s pomoću koje se otvara e-poruka u kojoj se može opisati problem. U naslovu poruke vidljivo je preko koje je natuknice poruka generirana kako bi uredni&scaron;tvu rječnika bilo jasno na koju se natuknicu problem odnosi. (Daria Lazić)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    $("#norske").on("click", function() {
        Swal.fire({
            html:'<p><strong><em><a href="https://naob.no/">Det Norske Akademis ordbok</a></em></strong> rječnik je Norve&scaron;ke akademije i najopsežniji je rječnik norve&scaron;koga jezika, odnosno njegove inačice <em>bokm&aring;l</em>, te se od 2017. nalazi na mreži. Rezultat je nastavka rada na Akademijinu rječniku <em>Norsk Riksm&aring;lsordbok</em>, čiji je posljednji tiskani svezak objavljen 1995. godine te se nadalje razvija isključivo u elektroničkome obliku. Rječnik sadržava oko 225&nbsp;000 natuknica i oko 400&nbsp;000 primjera koji dokumentiraju uporabu riječi tijekom posljednjih 200 godina u književnosti, novinskim tekstovima i drugim publikacijama. Pri dopunjavanju i posuvremenjivanju rječnika nastoji se uzeti u obzir jezik različitih dru&scaron;tvenih skupina te jezične domene koje su dosad bile manje zastupljene, poput razgovornoga jezika, te je, primjerice, popularna norve&scaron;ka tinejdžerska serija <em>Skam</em> poslužila kao izvor novih riječi i primjera koji su dodani rječniku. Struktura rječničkoga članka može se promotriti na primjeru natuknice <em>fredag</em> &lsquo;petak&rsquo; (vidjeti sliku). Na početku natuknice nalazi se podatak o vrsti riječi (<em>substantiv</em>) i rodu imenice (<em>b&oslash;yning</em>), no morfolo&scaron;ki su podatci oskudni te nisu navedeni oblici riječi. Potom slijedi fonetski zapis izgovora (<em>uttale</em>), bez zvučnoga zapisa, i etimologija (<em>etymologi</em>). Zatim se obrađuju značenja, &scaron;to uključuje definiciju, primjere (<em>eksempel</em>), koji obuhvaćaju kolokacije, konstrukcije i kraće primjere, dok se u odjeljku &bdquo;citati&rdquo; (<em>sitater</em>) donose potvrde riječi iz izvora. Citati su ovdje brojni jer je cilj dokumentirati njihovu uporabu tijekom duljega razdoblja. Naposljetku se nalaze ustaljeni izrazi (<em>uttryk</em>), čija obrada uključuje definicije i primjere uporabe. Mogućnosti e-rječnika iskori&scaron;tene u ovome rječniku odnose se u prvome redu na napredne mogućnosti pretraživanja (rječnik omogućuje i slobodno pretraživanje rječničkoga sadržaja), unutarrječničko povezivanje tvorbeno i značenjski povezanih natuknica te prikaz strukture složenijih natuknica s pomoću kojega se može izravno pristupiti željenim sadržajima. (Daria Lazić)</p><p style="text-align:center"><img style="max-width: 100%; height: auto; margin: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/fredag-1.jpg" alt=""/></p><p style="text-align: center;">natuknica <em>fredag</em> &lsquo;petak&rsquo; u rječniku <em>Det Norske Akademis ordbok</em></p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    $("#lexin").on("click", function() {
        Swal.fire({
            html:'<p><strong><em>Lexin</em> j</strong>e &scaron;vedski mrežni rječnik namijenjen neizvornim govornicima, nastao je kao dio istoimenoga projekta, &scaron;to stoji za <em>lexikon f&ouml;r invandrare</em> &lsquo;rječnici za doseljenike&rsquo;. Projekt je pokrenut 70-ih godina 20. stoljeća u cilju izrade dvojezičnih rječnika za govornike manjih jezika zastupljenih među doseljenicima. Prvo je sastavljen jednojezični rječnik, koji se potom prevodio na odabrane jezike. Objavljeno je nekoliko izdanja jednojezičnoga rječnika, a danas je dostupan na mreži zajedno s prijevodima na petnaestak jezika, među kojima je i hrvatski. Planirana je izrada sličnih rječnika za norve&scaron;ki i danski na temelju &scaron;vedskoga materijala, no dok je norve&scaron;ki projekt zaživio (norve&scaron;ka inačica rječnika nalazi se na <a href="https://lexin.oslomet.no">https://lexin.oslomet.no</a>), na danski su prevedeni samo interaktivni slikovni materijali, koji se mogu pregledavati u okviru &scaron;vedskoga i norve&scaron;koga rječnika.</p><p>Pretraživanje rječnika jednostavno je te se s lijeve strane nalazi okvir za pretraživanje i osnovne postavke, a u sredi&scaron;njemu dijelu prikazuju se rezultati pretraživanja. Moguće je odabrati jezik dvojezičnoga rječnika ili jednojezičnu inačicu, a s obzirom na odabir ispod prozora za pretraživanje prikazuju se posebni znakovi &scaron;vedske tipkovnice i tipkovnice ciljnoga jezika kako bi se olak&scaron;alo pretraživanje. Zanimljivo je da se pretraživati mogu i &scaron;vedske natuknice i prijevod na ciljni jezik, a &scaron;vedske riječi mogu se pretraživati prema bilo kojemu obliku, pa čak i prema oblicima koji nisu istaknuti u rječničkome članku. Inovativna je funkcija ovoga rječnika ta &scaron;to su sve riječi u rječničkome članku hiperveze te se pritiskom na bilo koju od njih otvara odgovarajuća natuknica u rječniku. Na taj način korisnici rječnika, za koje se pretpostavlja da nisu izvorni govornici &scaron;vedskoga, lako mogu provjeriti značenje nepoznatih riječi na koje naiđu pri pretraživanju rječnika. Rječnik sadržava oko 30&nbsp;000 natuknica, koje velikim dijelom čini osnovni leksik općega jezika, a s obzirom na ciljnu skupinu korisnika uključene su i određene riječi važne za snalaženje u &scaron;vedskome dru&scaron;tvu. Na slici je prikazana natuknica <em>Migrationsverket</em>, koja je na hrvatski prevedena kao <em>Zavod za migraciju</em>. Nakon prijevoda nalazi se obja&scaron;njenje o kakvoj je ustanovi riječ.</p><p style="text-align: center;"><img src="https://jena.jezik.hr/wp-content/uploads/2021/12/Migrationsverket.jpg" style="margin:auto; max-width:100%; height:auto"></p><p style="text-align: center;">natuknica <em>Migrationsverket</em> &lsquo;Zavod za migraciju&rsquo; u rječniku <em>Lexin</em> te natuknica <em>verk</em> &lsquo;uprava, zavod&rsquo;, u kojoj se tražena natuknica pojavljuje kao složenica</p><p style="text-align: justify;">Na početku rječničkoga ćlanka nalazi se fonetski zapis i snimka izgovora (<em>lyssna</em>) te podatak o vrsti riječi, a za promjenjive riječi navedeni su i njihovi oblici kao cjelovite riječi. Potom slijedi jednostavna definicija na &scaron;vedskome te prijevod u dvojezičnim inačicama. Kod glagola i nekih pridjeva nakon definicije naznačena je i rekcija. Kao zasebna rubrika izdvojene su složenice (<em>sammans&auml;ttningar</em>), koje su česte u &scaron;vedskome i stoga koristan podatak za neizvorne govornike, te &ndash; &scaron;to nije vidljivo u prikazanoj natuknici &ndash; ustaljeni izrazi (<em>uttryck</em>). Osim toga, natuknice često obuhvaćaju i kolokacije te primjere uporabe (<em>exempel</em>), &scaron;to također neizvornim govornicima olak&scaron;ava razumijevanje riječi i njihovu uporabu. Osim same natuknice u dvojezičnim inačicama u načelu su prevedeni i primjeri uporabe te složenice i ustaljeni izrazi.</p><p>Posebnost su rječnika brojni multimedijski sadržaji koji su dostupni na rječničkome portalu kao samostalni izvori, ali su i uklopljeni u rječnik s pomoću poveznica. Riječ je o trima tipovima sadržaja: interaktivnim slikovnim prikazima određenih tema (primjerice obitelj i rodbina, ljudsko tijelo, grad i promet, sat i kalendar i slično), videoprikazima te primjerima razgovora u uobičajenim dru&scaron;tvenim situacijama s videozapisom i transkripcijom. (Daria Lazić)</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    $("#islex").on("click", function() {
        Swal.fire({
            html:'<p><strong><a href="https://islex.arnastofnun.is/">Islex</a></strong> je mrežni vi&scaron;ejezični islandsko-nordijski rječnik namijenjen neizvornim govornicima, koji se sastoji od islandske baze te prijevoda na &scaron;est jezika nordijskih zemalja: danski, &scaron;vedski, dva norve&scaron;ka standarda (<em>bokm&aring;l</em> i <em>nynorsk</em>), ferski i finski. Islandski je dio rječnika izrađen u Odjelu za leksikologiju i leksikografiju Instituta &Aacute;rnija Magn&uacute;ssona za islandske studije u Reykjav&iacute;ku, dok su za prijevode na pojedine jezike bili zaduženi leksikografi iz znanstvenih ustanova u zemljama na čije se jezike rječnik prevodio. Projekt je započeo 2005., a rječnik je na mreži objavljen 2011., otkad se nastavlja uređivati i dopunjavati. Trenutačno obuhvaća oko 50&nbsp;000 natuknica te je izvorno nastao kao mrežni rječnik. Na početku je izrađena islandska baza, koja je potom prevedena na ciljne jezike, a postoji i mogućnost njezina prevođenja na dodatne jezike. Islandska baza naknadno je objavljena kao jednojezični rječnik (<a href="https://islenskordabok.arnastofnun.is">https://islenskordabok.arnastofnun.is</a>). Rječnik <em>Islex</em> važan je korak u promicanju međusobnoga razumijevanja na nordijskome prostoru s obzirom na to da islandski govornicima ostalih nordijskih jezika (osim ferskoga) nije razumljiv.</p><p style="text-align: center;"><img style="margin: auto; max-width: 100%; height: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/fostudagur.jpg" alt=""/></p><p style="text-align: center;">1. slika: Rječnički članak natuknice <em>f&ouml;studagur</em> &lsquo;petak&rsquo; u rječniku <em>Islex</em> i povezanost s morfolo&scaron;kom bazom</p><p style="text-align: justify;">Prikaz podataka u rječničkim natuknicama jednostavan je i pregledan, a različiti su dijelovi grafički istaknuti. Omogućen je odabir jezika sučelja, &scaron;to neizvornim govornicima olak&scaron;ava služenje rječnikom, i jezika prijevoda (pojedinoga jezika ili svih prijevodnih jezika istodobno) te uključivanje ili isključivanje slobodnoga pretraživanja teksta (<em>Leita &iacute; texta</em> ispod prozora za pretraživanje). Izgovor je u rječniku zabilježen isključivo u obliku zvučnoga zapisa (poveznica <em>fram</em><em>bur&eth;ur</em>), a posebnost je rječnika to &scaron;to je podatak o oblicima prisutan u obliku poveznice (<em>beyging</em>) s islandskom morfolo&scaron;kom bazom (<a href="https://bin.arnastofnun.is"><em>Bey</em><em>gingarl&yacute;sing &iacute;slensks n&uacute;t&iacute;mam&aacute;ls</em></a>), &scaron;to je vrlo korisno s obzirom na morfolo&scaron;ku složenost islandskoga.</p><p style="text-align: justify;">Značenja riječi navedena su u obliku istovrijednica na odabranome ciljnom jeziku, no ne postoji definicija na islandskome. Nakon kolokacija slijede kratki primjeri uporabe, konstrukcije, kolokacije i ustaljeni izrazi s prijevodima na ciljni jezik. Budući da je islandski frazeolo&scaron;ki veoma bogat, na dnu članka često su izdvojeni frazemi (na sličan način kao vi&scaron;erječna jedinica <em>f&ouml;studagurinn langi</em> &lsquo;Veliki petak&rsquo; u ovome slučaju), &scaron;to je neizvornim govornicima veoma koristan podatak. Budući da je riječ o rječniku izvorno sastavljanomu kao mrežni rječnik, iskori&scaron;tene su neke mogućnosti toga medija. Već je spomenuta izvanrječnička povezanost s morfolo&scaron;kom bazom, a kao oblik unutarrječničkoga povezivanja članci katkad sadržavaju poveznice na značenjski srodne riječi. Naposljetku, rječnik je opremljen slikovnim materijalom. Riječ je o oko 3200 fotografija i ilustracija, a među njima se nalazi i oko 500 ilustracija biljaka i životinja islandskoga umjetnika J&oacute;na Baldura Hl&iacute;&eth;berga. (Daria Lazić)</p><p style="text-align: center;"><img style="margin: auto; max-width: 100%; height: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/Islexu.jpg" alt=""/></p><p style="text-align: center;">2. slika: Primjer slika u <em>Islexu</em> za natuknice <em>hver</em> &lsquo;gejzir&rsquo;, <em>kind</em> &lsquo;ovca&rsquo; i&nbsp;<em>baldursbr&aacute;</em> &lsquo;bezmirisna kamilica&rsquo;</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
    $("#frazemi").on("click", function() {
        Swal.fire({
            html:'<p><strong><a href="http://frazemi.ihjj.hr/">Baza frazema hrvatskoga jezika</a></strong> korpus je frazema unutar <em><a href="http://ihjj.hr/kolokacije">Kolokacijske baze hrvatskoga jezika</a></em>. U njoj se nalazi oko 22 000 frazemskih pojavnica, a frazemi su pretraživi po svim frazemskim sastavnicama bez obzira na to jesu li one nosive ili nenosive, punoznačne ili nepunoznačne. Uz frazeme je, kad to njihova struktura zahtijeva, upisana rekcijsko-valencijska dopuna koja proizlazi iz njihova sintaktičkoga i uporabnoga potencijala. Baza je utemeljena na frazemima ekscerpiranim iz različitih izvora: dnevnoga, tjednoga i mjesečnoga tiska, znanstvene i stručne jezikoslovne literature, lijepe književnosti dostupne na institutskome računalnom korpusu <em>Hrvatska mrežna riznica</em> te suvremenih hrvatskih leksikografskih mrežnih i tiskanih izvora (<em><a href="http://rjecnik.hr/">&Scaron;kolskoga rječnika hrvatskoga jezika</a></em>, <em><a href="https://hjp.znanje.hr/">Hrvatskoga jezičnoga portala</a>, Velikoga rječnika hrvatskoga standardnog jezika</em>. (Barbara Kovačević)</p><p style="text-align: center;"><img style="margin: auto; max-width: 100%; height: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/baza-frazema.jpg" alt=""/></p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })

    $("#frazeologija").on("click", function() {
        Swal.fire({
            html:'<p style="text-align: justify;"><strong><a href="http://www.knjigra.hr/elektronicka-izdanja/">Bibliografija hrvatske frazeologije &ndash; Frazeobibliografski rječnik</a></strong> bibliografija je objavljena 2010. godine i uz nju se nalazi CD s popisom frazema analiziranih u znanstvenim i stručnim radovima. Sedam godina poslije njezina objavljivanja kod istoga je izdavača objavljena e-knjiga <em>Bibliografija hrvatske frazeologije &ndash; Frazeobibliografski rječnik</em> (Ž. Fink-Arsovski &ndash; B. Kovačević &ndash; A. Hrnjak. Knjigra. 2017.) dostupna na mrežnim stranicama izdavača Knjigre i mrežnim stranicama <em><a href="http://haw.nsk.hr/">Hrvatskoga arhiva weba</a></em>. Frazeolozi su prepoznali izazove modernih tehnologija i predstavili svoj rad na suvremenim medijima, a rječnici su uključili dostignuća suvremene frazeolo&scaron;ke teorije te su pored minimalnih frazema (<em>od prve</em>, <em>pod mus</em>,<em> i amen</em>), frazemskih skupova riječi (<em>držati jezik za zubima</em>, <em>bura u ča&scaron;i vode</em>, <em>brz na jeziku</em>, <em>jedva jedvice</em>) i frazemskih rečenica (<em>crno (lo&scaron;e, slabo) se pi&scaron;e komu, čemu</em>, <em>&lt;pa&gt; kud puklo da puklo</em>) u rječnik uključene frazemske polusloženice (<em>rak-rana</em>,<em> amo-tamo</em>, <em>hoće&scaron;-neće&scaron;</em>) i frazemske sraslice (<em>akobogda</em>, <em>bogtepitaj</em>, <em>dovraga</em>), a posebna je pozornost posvećena frazemima u kojima je nosiva sastavnica, a samim time i frazemska nadnatuknica, izražena vi&scaron;erječnim nazivom ili imenskom formulom (npr. BOŽJA OVČICA, SMRDLJIVI MARTIN, SLIJEPO CRIJEVO, ZIMSKI SAN; BOŽO PETROV, DON ŽUAN, ZLATA PJERINA, SVETI PETAR), &scaron;to je novost u hrvatskoj leksikografiji općenito. &nbsp;(Barbara Kovačević)</p><p style="text-align: center;"><img style="margin: auto; max-width: 100%; height: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/bibliograf_fraz.jpg" alt=""/></p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
    
    $("#hrvatskihr").on("click", function() {
        Swal.fire({
            html:'<p><strong>Frazemi &ndash; baza frazemskih etimologija </strong>baza je koja nastaje usporedno s radom na <em>Hrvatskom mrežnom rječniku</em> <em>&ndash; Mrežniku</em>. Nalazi se na portalu Instituta za hrvatski jezik i jezikoslovlje <em>Hrvatski u &scaron;koli</em>. U njoj su uz kanonski oblik frazema doneseni podatci o motivaciji i podrijetlu frazema kod kojih se izgubila veza s motivacijskom bazom ili je do&scaron;lo do remotivacije frazema, a ti se frazemi mogu tematski razvrstati u nekoliko skupina, npr.: animalistički frazemi (npr. <em>crna ovca</em>, <em>kupiti/kupovati mačka (mačku) u vreći</em>, <em>krokodilske suze</em>), somatski frazemi (npr. <em>slomiti nogu</em>, <em>puna &scaron;aka brade</em>, <em>oprati/prati ruke &lt;od koga, od čega&gt;</em>), antički frazemi (npr. <em>Ahilova peta</em>, <em>Sizifov posao</em>,<em> Pandorina kutija</em>), biblijski frazemi (npr. <em>baciti/bacati bisere (biserje) pred svinje</em>, <em>provući se kroz u&scaron;icu igle</em>, <em>nevjerni Toma</em>) te frazemi motivirani književnim djelima (npr. <em>trojanski konj</em>, <em>između Scile i Haribde</em>, <em>vaditi kestene (kestenje) iz vatre</em>, <em>učiniti/činiti (napraviti/praviti) medvjeđu uslugu komu</em>). Osim frazemskih etimologija koje su priložene uz obradu frazema u <em>Mrežniku</em>, tu se nalaze i etimologije onih frazema koji su u posljednjih osam godina (često i na poticaj građana koji su postavljali upite na institutski telefon za davanje jezičnih savjeta) obrađeni u rubrici <em>Od A do Ž</em> institutskoga znanstveno-popularnog časopisa za kulturu hrvatskoga jezika &ndash; <em>Hrvatski jezik</em> koji je javno dostupan na <em><a href="https://hrcak.srce.hr/hrjezik">Portalu hrvatskih znanstvenih i stručnih časopisa &ndash; Hrčak</a></em>. &nbsp;Obrada frazema u bazi frazemskih etimologija preklapa se s etimolo&scaron;kom informacijom u frazeografskim obradama u <em>Hrvatskome mrežnom rječniku</em>, &scaron;to rezultira činjenicom da je to prvi hrvatski općejezični rječnik u kojemu se donose podatci o frazemskim etimologijama. Pritom i sama baza frazemskih etimologija biva prvim hrvatskim frazeolo&scaron;ko-etimolo&scaron;kim mrežnim izvorom, iz kojega bi u dogledno vrijeme mogao nastati prvi hrvatski frazeolo&scaron;ko-etimolo&scaron;ki rječnik. Također, baza frazemskih etimologija u budućnosti bi se, kad se za to stvore uvjeti, mogla povezati i s <em>Bazom hrvatskih somatskih frazema</em>, koji će se ponuditi kao jedan od mrežnih rezultata rada na unutarinstitutskome projektu <em>Rječnih hrvatskih somatskih frazema</em>, koji se također izrađuje na Odjelu za hrvatski standardni jezik Instituta za hrvatski jezik i jezikoslovlje. (Barbara Kovačević)</p><p style="text-align: center;"><img style="margin: auto; max-width: 100%; height: auto;" src="https://jena.jezik.hr/wp-content/uploads/2021/12/hrv_skoli_frazemi.jpg" alt=""/></p><p style="text-align: center;"><em>Frazemi </em>&ndash; baza frazemskih etimologija</p>',
            showCloseButton: true,
            focusConfirm: false,
            confirmButtonText:
              'zatvori',
              width:'800px',
              confirmButtonColor:"#28B7DB"
          })
    })
    // The function actually applying the offset
    $('a[href*=\\#]').on('click', function(event) {
        event.preventDefault();
        $('html,body').animate({ scrollTop: $(this.hash).offset().top - 50 }, 500);
    });

    //skrolanje
    window.onscroll = function() { scrollFunction() };

    function scrollFunction() {
        if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
            document.getElementById("vrhGumb").style.display = "block";
        } else {
            document.getElementById("vrhGumb").style.display = "none";
        }
    }
})
# -*- coding: utf-8 -*-
from cStringIO import StringIO
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
import os
import io
import sys, getopt
import re
import unicodedata
from itertools import islice

#rapson dokumenta
br1=0
br2=None

#converts pdf, returns its text content as a string
def convert(fname, pages=None):
    if not pages:
        pagenums = set()
    else:
        pagenums = set(pages)
        print(pagenums)

    output = StringIO()
    manager = PDFResourceManager()
    converter = TextConverter(manager, output, laparams=LAParams())
    interpreter = PDFPageInterpreter(manager, converter)
    
    print(fname)
    infile = file(fname, 'rb')
    for page in islice(PDFPage.get_pages(infile, pagenums), br1, br2):
        interpreter.process_page(page)      
    infile.close()
    converter.close()
    text = output.getvalue()
    output.close
    print('gotovo')
    return text 
   
def convertMultiple(pdfDir, txtDir):
    if pdfDir == r"": pdfDir = os.getcwd() + "\\" #if no pdfDir passed in 
    i=0
    i2=0
    i3=0
    i4=0
    for pdf in os.listdir(unicode(pdfDir.decode('utf-8'))): #iterate through pdfs in pdf directory
        fileExtension = pdf.split(".")[-1]
        if fileExtension == "pdf": 
            pdfFilename = pdfDir + pdf
            text = convert(pdfFilename) #get string of text content of pdf
            #text =re.sub(r'(?s)(\n)(.*?)(?)',"",text)

            """
            text=text.replace("\n\n","\n")
            text=text.replace('-\n', '')            
            text = re.sub("\s\s+", " ",text)
            text=text.split("\n")
            for x in text:
                try:
                    if x[0]==" ":
                        text[i3]=x[1:]
                    if x[0].isdigit() and x[1] not in [":",".",")"]:
                        text.pop(i3)
                    elif x=="\n":
                        text.pop(i3)
                except:
                    print("An exception occurred") 
                i3+=1
           
            text="\n".join(text)

            text=text.split(".")
            for t in text:
                t=t.lstrip('0123456789.-: ')
                text[i]=t
                #t= "".join(filter(lambda x: not x.isdigit(), t))
                #print (t)
                i+=1
            
            
            text=". ".join(text)

            text=text.split(" ")
            for t in text:
                t=t.lstrip('.')
                t=t.rstrip('0123456789 ')
                text[i2]=t
                #t= "".join(filter(lambda x: not x.isdigit(), t))
                #print (t)
                i2+=1
            text=" ".join(text)
            text=text.split("\n")
            for x in text:
                try:
                    if x[0]==" ":
                        text[i4]=x[1:]
                    if x[0].isdigit() or x[1].isdigit() or x=="\n" or x[0]=="(":
                        text.pop(i4)
                except:
                    print("An exception occurred") 
                i4+=1
            text="\n".join(text)
            text=text.replace("  "," ")
            text=text.replace("..",".")
            text=text.replace(" )",")").replace(" .",".").replace(" ,",",").replace(" !","!").replace(" ?","?").replace("( ","(")
            text=text.replace("  "," ")
            text=text.replace("\n\n","\n")
            text=text.replace("(. )","(...)")
            """
            textFilename = txtDir + pdf[:-4] + ".txt"
            textFile = open(textFilename, "w") #make text file
            text="<doc>"+text+"</doc>"
            textFile.write("%s" % text) #write text to text file
            
			#textFile.close

pdfDir = r"pdf/"
txtDir = r"txt/"
convertMultiple(pdfDir, txtDir)

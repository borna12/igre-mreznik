# -*- coding: utf8 -*-
import tika
from tika import parser
tika.TikaClientOnly = True
import os
import codecs
import requests
import re


def extract_text_from_pdfs_recursively(dir):
    broj=0
    for root, dirs, files in os.walk(dir):
        for file in files:
            broj+=1
            path_to_pdf = os.path.join(root, file)
            [stem, ext] = os.path.splitext(path_to_pdf)
            if ext == '.docx':
                print("Processing " + path_to_pdf)
                os.rename(path_to_pdf,str(broj)+".docx")
                pdf_contents = parser.from_file(str(broj)+".docx")
                path_to_txt = stem + '.txt'
                tekst=pdf_contents['content']
                putanja=re.compile(r"\d*([^\d\W]+)\d*")
                tekst=putanja.sub(r"\1", tekst)
                zapisi(tekst,path_to_txt)
                
def zapisi(tekst,path_to_txt):
    br=0
    br2=0
    br3=0
    #tekst=tekst.replace(" \n"," ")


    tekst=tekst.split("\n")
    
    for x in tekst:

        if x =="Literatura " or x =="Literatura: " or x =="Literatura:" or x=="Literatura" or x=="Vrela: " or x=="Vrela:" or x=="Citirana literatura:" or x=="Citirana literatura: "  or x=="Izvori: " or x=="Izvori:" or x=="Izvori" or x=="Izvori " or x=="Literatura i vrela:" or x=="Literatura i vrela: " or x=="Literatura i izvori: " or x=="Literatura i izvori:"  or x=="Bibliografija:" or x=="Bibliografija: ":
            tekst=tekst[0:br]
            break
        else:
            try:
                if x[-3:]==".hr" or x[-3:]==".de" or x[-3:]==".pe" or x[-4:]==".com":
                    br3=br
                elif x[0:3]=="UDK":
                    br2=br
            except:
                a=45
        br+=1
    del tekst[br2:br3+1]
    tekst="\n".join(tekst)

    tekst=tekst.replace("\n\n\n","")
    tekst=tekst.replace("-\n","")
    tekst=tekst.replace("- -","-")
    tekst=tekst.replace("- \n","")
    tekst=tekst.replace("  "," ").replace("�","ȕ").replace("","ȑ").replace("·","•")
    crtice=re.findall('([a-zA-Z]+-)\s+(\w+)', tekst)
    tekst = re.sub(r'(?m)^\*.*\n?', '', tekst)
    for x in crtice:
        tekst=tekst.replace(x[0]+" "+x[1],x[0][:-1]+x[1])
    tekst=re.sub(r'(?<=[.,])(?=[a-zA-Z0-9])', r' ', tekst)
    
    with codecs.open(path_to_txt, 'w',"utf-8") as txt_file:
        print("Writing contents to " + path_to_txt)
        txt_file.write(tekst)
    

if __name__ == "__main__":
    extract_text_from_pdfs_recursively(os.getcwd())
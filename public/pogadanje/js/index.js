// window.onerror=function(err,url,line){
// 	alert("Error: "+err);
// 	alert("Line: "+line);
// };

/*
 *Initializes all global variables from the DOM
 */


const scrabText = document.querySelectorAll(".scrab-text p");
const answerInput = document.querySelector(".answer input[type='text']");
const checkbtn = document.querySelector(".answer button");
const scoreBoard = document.querySelector(".score-board #score");
const resetBtn = document.querySelector(".score-board button");
const category = document.querySelectorAll("li[data-value]");
const catTag = document.querySelector(".category-tag h5");

var catValue = 1;
var words = new Array();
var hints = new Array();

checkbtn.addEventListener("click", checkWord);
resetBtn.addEventListener("click", resetScore);
answerInput.addEventListener("keypress", (e) => {
	e.keyCode == 13 ? checkWord() : "";
});

category.forEach(function (element) {
	element.addEventListener("click", changeCategory);
});

function changeCategory() {
	catValue = this.dataset.value;
	activeCard = 0;
	next = 0;
	setCategory();
	init();
};

function mjesaj(array) {
	var currentIndex = array.length, temporaryValue, randomIndex;
  
	// While there remain elements to shuffle...
	while (0 !== currentIndex) {
  
	  // Pick a remaining element...
	  randomIndex = Math.floor(Math.random() * currentIndex);
	  currentIndex -= 1;
  
	  // And swap it with the current element.
	  temporaryValue = array[currentIndex];
	  array[currentIndex] = array[randomIndex];
	  array[randomIndex] = temporaryValue;
	}
  
	return array;
  }

function setCategory() {
	let tech = {
		words: ["class", "java", "python", "object", "json", "variable", "method", "developer", "forloop", "software", "algorithm", "data", "encryption", "server", "internet", "google", "android", "binary", "linux", "integer"],
		hint: ["c . a . s", "ja. .", ". .thon", "obj. .t", "j. o .", "v. . iabl .", "method", "developer", "forloop", "software", "algorithm", "data", "encryption", "server", "internet", "google", "android", "binary", "linux", "integer"],
		catName: "Tech"
	}
	let sports = {
		words: ["couch", "referee", "hockey", "tenisball", "commentrator", "pitch", "olympics", "football", "goalkeeper", "playoff", "basketball", "dribble"],
		hint: ["", "", "", "", "", "", "", "", "", "", "", ""],
		catName: "Sports"
	}
	let health = {
		words: ["cancer", "pedrician", "drugs", "excercise", "", "", "", "", "", "", "", ""],
		hint: ["", "", "", "", "", "", "", "", "", "", "", ""],
		catName: "Health"
	}
	let nutrition = {
		words: ["", "", "", "", "", "", "", "", "", "", "", ""],
		hint: ["", "", "", "", "", "", "", "", "", "", "", ""],
		catName: "Nutrition"
	}
	let countries = {
		words: ["nigeria", "brazil", "mexico", "", "russia", "togo", "australia", "india", "kenya", "usa", "england", "germany"],
		hint: ["", "", "", "", "", "", "", "", "", "", "", ""],
		catName: "Countries"
	}
	let categories = ["", tech, sports, health, nutrition, countries];
	let choice = catValue;
	words = categories[choice].words;
	
	hints = categories[choice].hint;
	catTag.innerHTML = `${categories[choice].catName}  <i class="fa fa-caret-down"></i>`;
	words=mjesaj(words)

}

var score = 0;
var next = 0;
var activeCard = 0;
var slide = 0;


init();
/*
 * Initializes the random Texts on the cards
 */
function init() {
	setCategory();
	if (next < words.length) {
		if (activeCard == 1) {
			scrabText[activeCard].innerHTML = "<span class='rijec'>"+shuffle(words[next])+"</span>";
			activeCard = 0;
		} else {
			scrabText[activeCard].innerHTML = "<span class='rijec'>"+shuffle(words[next])+"</span>";
			activeCard = 1;
		}

	}
}


/*
 * Checks if the player's answer Matches the Correct Word.
 *and increases the score if it does.
 */

function checkWord(e) {
	e.preventDefault();
	let answer = answerInput.value.toLowerCase();
	let correct = (answer == words[next]);
	if (correct === true) {
		answerInput.value = "";
		score++;
		setScore();
		//turns the background of the active card to green if answer is correct
		$(".rijec").css({"background":"green"})
		scrabText[Math.abs(activeCard - 1)]

		setTimeout(function () {
			if (next < words.length) {
				switchCards();
			} else {
				scrabText[activeCard].innerHTML = "You Won!!!";
				switchCards();
			}
		}, 800);

		next++;
		init();
	} else {
		
		//turns the background of the active card to red if answer is incorrect
		$(".rijec").css({"background":"red"})
		scrabText[Math.abs(activeCard - 1)]

	}
}


function switchCards() {
	slide++;
	for (let i = 0; i < scrabText.length; i++) {

		if (scrabText[i].classList.contains("isActive")) {
			scrabText[i].classList.add("notActive");
			scrabText[i].classList.remove("isActive");
			slide = 0;
		} else {
			if (scrabText[i].classList.contains("notActive")) {
				scrabText[i].classList.remove("notActive");
			}
			scrabText[i].classList.add("isActive");
		}
	}
}


function setScore() {
	scoreBoard.innerText = score;
}

function resetScore() {
	answerInput.value = "";
	score = 0;
	next = 0;
	activeCard = 0;
	words =[];
	catValue = 1;

	setScore();
	init();
}

/*
 *This function randomizes the letters in the Word.
 *passed to it.
 */
/*Using Fisher-Yates Shuffle Algorithm*/

function shuffle(word) {

	let wordChar = word.split("");
	for (i = 0; i < wordChar.length; i++) {
		let rand = Math.ceil(Math.random() * word.length - 1);
		temp = wordChar[i];
		wordChar[i] = wordChar[rand];
		wordChar[rand] = temp;
	}
	//Turns shuffled Array into a String and returns it.
	return wordChar.join("");
}

